﻿using System;
using System.Collections.Generic;
using BSC.WordLines.Data;
using BSC.WordLines.ToolKit.ViewModels;
using Caliburn.Micro;

namespace BSC.WordLines.ToolKit {
    public class AppBootstrapper : Bootstrapper<ShellViewModel> {
        private readonly SimpleContainer _container = new SimpleContainer();

        protected override void Configure() {
            _container.RegisterPerRequest(typeof (IWindowManager), null, typeof (WindowManager));
            _container.RegisterPerRequest(typeof (IEventAggregator), null, typeof (EventAggregator));
            _container.RegisterSingleton(typeof(IDataConfig), null, typeof(WpfDataConfig));
            _container.RegisterSingleton(typeof(ILocalDataProvider), null, typeof(SqliteLocalDataProvider));
            _container.PerRequest<ShellViewModel>();
            _container.PerRequest<AbstractResourcesPageViewModel>();
            _container.PerRequest<LocalizedStringsPageViewModel>();
            _container.PerRequest<DictionaryPageViewModel>();
            _container.PerRequest<EnglishDictionaryPageViewModel>();
            _container.PerRequest<GermanDictionaryPageViewModel>();
        }

        protected override object GetInstance(Type service, string key) {
            var instance = _container.GetInstance(service, key);
            if (instance != null) {
                return instance;
            }
            throw new Exception("Could not locate any instances.");
        }

        protected override IEnumerable<object> GetAllInstances(Type service) {
            return _container.GetAllInstances(service);
        }

        protected override void BuildUp(object instance) {
            _container.BuildUp(instance);
        }
    }
}