﻿using System;
using System.Diagnostics;
using System.Linq;
using BSC.WordLines.Data;
using Caliburn.Micro;

namespace BSC.WordLines.ToolKit.ViewModels {
    public class EditLocalizedStringViewModel : Screen {
        private readonly ILocalizedStringData _targetLocalizedString;
        private readonly ILocalDataProvider _provider;
        private readonly IObservableCollection<LanguageListItemViewModel> _languages;
        private readonly IWindowManager _windowManager;
        private readonly IStringImplementationData _implementation;
        private LanguageListItemViewModel _selectedLanguage;
        private string _concreteString;

        public EditLocalizedStringViewModel(
            ILocalizedStringData targetLocalizedString,
            IStringImplementationData implementation,
            ILocalDataProvider provider) {
            if (provider == null) {
                throw new ArgumentNullException("provider");
            }
            if (targetLocalizedString == null) {
                throw new ArgumentNullException("targetLocalizedString");
            }
            _windowManager = IoC.Get<IWindowManager>();
            if(_windowManager == null) {
                throw new InvalidOperationException("Cannot resolve IWindowsManager.");
            }
            _targetLocalizedString = targetLocalizedString;
            _implementation = implementation;
            _provider = provider;
            
            _languages = new BindableCollection<LanguageListItemViewModel>(
                _provider.GetAllLanguages()
                .OrderBy(language => language.Name)
                .Select(data=>new LanguageListItemViewModel(data)));
            _languages.Insert(0, new LanguageListItemViewModel(null));
            
            if (implementation != null) {
                Debug.Assert(implementation.LocalizedStringId == targetLocalizedString.Id,
                             "implementation.LocalizedStringId == targetLocalizedString.Id");
                _selectedLanguage = _languages.First(language => {
                    var languageData = language.Data;
                    if (languageData != null) {
                        return languageData.Id == implementation.LanguageId;
                    }
                    return !implementation.LanguageId.HasValue;
                });
                ConcreteString = implementation.ConcreteString;
                DisplayName = string.Format("Изменить значение для {0}", targetLocalizedString.Key);
            }
            else {
                _selectedLanguage = _languages.FirstOrDefault();
                ConcreteString = string.Empty;
                DisplayName = string.Format("Добавить значение для {0}", targetLocalizedString.Key);
            }
        }

        public ILocalizedStringData TargetLocalizedString {
            get { return _targetLocalizedString; }
        }

        public IObservableCollection<LanguageListItemViewModel> Languages {
            get { return _languages; }
        }

        public LanguageListItemViewModel SelectedLanguage {
            get { return _selectedLanguage; }
            set {
                if (!ReferenceEquals(_selectedLanguage, value)) {
                    _selectedLanguage = value;
                    NotifyOfPropertyChange(() => SelectedLanguage);
                }
            }
        }

        public string ConcreteString {
            get { return _concreteString; }
            set {
                if (!string.Equals(_concreteString, value, StringComparison.Ordinal)) {
                    _concreteString = value;
                    NotifyOfPropertyChange(() => ConcreteString);
                }
            }
        }

        public void Save() {
            int? newLanguageId = _selectedLanguage.Data != null ? _selectedLanguage.Data.Id : (int?)null;
            if (_implementation != null) {
                _implementation.LanguageId = newLanguageId;
                _implementation.ConcreteString = ConcreteString;
                _provider.SaveStringImplementation(_implementation);
            }
            else {
                _provider.InsertStringImplementation(_targetLocalizedString.Id, newLanguageId, ConcreteString);
            }
            TryClose(true);
        }

        public void Cancel() {
            TryClose(false);
        }
    }
}