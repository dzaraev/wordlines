﻿using System;
using BSC.WordLines.Data;
using BSC.WordLines.Utils;

namespace BSC.WordLines.ToolKit.ViewModels {
    public class AbstractResourceImplViewModel {
        private readonly IResourceImplementationData _resourceImplementation;
        private readonly ILocalDataProvider _provider;
        private readonly string _environment;
        private readonly string _language;
        private readonly string _theme;
        private readonly string _concreteValue;

        public AbstractResourceImplViewModel(IResourceImplementationData resourceImplementation, ILocalDataProvider provider) {
            if (resourceImplementation == null) {
                throw new ArgumentNullException("resourceImplementation");
            }
            if (provider == null) {
                throw new ArgumentNullException("provider");
            }
            _resourceImplementation = resourceImplementation;
            _provider = provider;

            int? environmentId = _resourceImplementation.EnvironmentId;
            _environment = environmentId.HasValue
                              ? _provider.GetEnvironment(environmentId.Value).Key
                              : "NULL";
            int? languageId = _resourceImplementation.LanguageId;
            _language = languageId.HasValue
                           ? _provider.GetLanguage(languageId.Value).Name
                           : "NULL";
            int? themeId = _resourceImplementation.ThemeId;
            _theme = themeId.HasValue
                         ? _provider.GetTheme(themeId.Value).Name
                         : "NULL";

            int concreteRersourceId = _resourceImplementation.ConcreteResourceId;
            var data = _provider.GetConcreteResource(concreteRersourceId).Data;
            //TODO сделать поддержки изображений
            _concreteValue = data != null ? data.BytesToString() : "NULL !!!";
        }

        public IResourceImplementationData Data {
            get { return _resourceImplementation; }
        }

        public int Id {
            get { return _resourceImplementation.Id; }
        }

        public string Environment {
            get { return _environment; }
        }

        public string Language {
            get { return _language; }
        }

        public string Theme {
            get { return _theme; }
        }

        public string ConcreteValue {
            get { return _concreteValue; }
        }
    }
}