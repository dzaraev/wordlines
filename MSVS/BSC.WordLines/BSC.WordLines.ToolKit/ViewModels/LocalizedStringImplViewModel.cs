﻿using System;
using BSC.WordLines.Data;

namespace BSC.WordLines.ToolKit.ViewModels {
    public class LocalizedStringImplViewModel {
        private readonly IStringImplementationData _stringImplementation;
        private readonly ILocalDataProvider _provider;
        private readonly string _language;
        private readonly string _concreteString;

        public LocalizedStringImplViewModel(IStringImplementationData stringImplementation, ILocalDataProvider provider) {
            if (stringImplementation == null) {
                throw new ArgumentNullException("stringImplementation");
            }
            if (provider == null) {
                throw new ArgumentNullException("provider");
            }
            _stringImplementation = stringImplementation;
            _provider = provider;

            int? languageId = _stringImplementation.LanguageId;
            _language = languageId.HasValue
                           ? _provider.GetLanguage(languageId.Value).Name
                           : "NULL";

            _concreteString = _stringImplementation.ConcreteString;
        }

        public IStringImplementationData Data {
            get { return _stringImplementation; }
        }

        public int Id {
            get { return _stringImplementation.Id; }
        }

        public string Language {
            get { return _language; }
        }

        public string ConcreteString {
            get { return _concreteString; }
        } 
    }
}