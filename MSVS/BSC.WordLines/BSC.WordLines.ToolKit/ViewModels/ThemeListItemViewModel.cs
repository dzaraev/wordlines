﻿using BSC.WordLines.Data;
using Caliburn.Micro;

namespace BSC.WordLines.ToolKit.ViewModels {
    public class ThemeListItemViewModel : PropertyChangedBase, IHaveDisplayName {
        private readonly IThemeData _data;
        private string _displayName;

        public ThemeListItemViewModel(IThemeData data) {
             _data = data;
            DisplayName = _data != null ? _data.Name : "NULL";
        }

        public IThemeData Data {
            get { return _data; }
        }

        public string DisplayName {
            get { return _displayName; }
            set {
                if (value != _displayName) {
                    _displayName = value;
                    NotifyOfPropertyChange(() => DisplayName);
                }
            }
        }
    }
}