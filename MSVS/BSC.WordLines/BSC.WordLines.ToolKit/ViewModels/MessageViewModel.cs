﻿using Caliburn.Micro;

namespace BSC.WordLines.ToolKit.ViewModels {
    public class MessageViewModel : Screen {
        private string _message;
        private bool _isQuestion;

        public MessageViewModel(string caption, string message) {
            DisplayName = caption;
            _message = message;
        }
        
        public string Message {
            get { return _message; }
            set {
                if (!string.Equals(_message, value)) {
                    _message = value;
                    NotifyOfPropertyChange(() => Message);
                }
            }
        }

        public bool IsQuestion {
            get { return _isQuestion; }
            set {
                if (_isQuestion != value) {
                    _isQuestion = value;
                    NotifyOfPropertyChange(() => IsQuestion);
                }
            }
        }

        public void Yes() {
            TryClose(true);
        }

        public void No() {
            TryClose(false);
        }
    }
}