﻿using System;
using System.Linq;
using System.Collections.ObjectModel;
using BSC.WordLines.Data;
using Caliburn.Micro;

namespace BSC.WordLines.ToolKit.ViewModels {
    public class AbstractResourcesPageViewModel : Screen {
        private readonly ILocalDataProvider _provider;
        private readonly IWindowManager _windowManager;
        private readonly ObservableCollection<IAbstractResourceData> _abstractResources;
        private IAbstractResourceData _selectedAbstractResource;
        private string _newAbstractResourceKey;
        private string _newAbstractResourceDescription;
        private AbstractResourceImplListViewModel _implementationsList;

        public AbstractResourcesPageViewModel(ILocalDataProvider provider, IWindowManager windowManager) {
            if (provider == null) {
                throw new ArgumentNullException("provider");
            }
            if (windowManager == null) {
                throw new ArgumentNullException("windowManager");
            }

            _provider = provider;
            _windowManager = windowManager;
            _abstractResources = new ObservableCollection<IAbstractResourceData>();
            DisplayName = "Абстрактные ресурсы";
        }

        protected override void OnInitialize() {
            UpdateData();
        }

        #region Abstract Resources

        public ObservableCollection<IAbstractResourceData> AbstractResources {
            get { return _abstractResources; }
        }

        public IAbstractResourceData SelectedAbstractResource {
            get { return _selectedAbstractResource; }
            set {
                if (!ReferenceEquals(_selectedAbstractResource, value)) {
                    _selectedAbstractResource = value;
                    NotifyOfPropertyChange(() => SelectedAbstractResource);
                    ImplementationsList = value != null
                                              ? new AbstractResourceImplListViewModel(value, _provider)
                                              : null;
                }
            }
        }

        public string NewAbstractResourceKey {
            get { return _newAbstractResourceKey; }
            set {
                if (!string.Equals(_newAbstractResourceKey, value)) {
                    _newAbstractResourceKey = value;
                    NotifyOfPropertyChange(() => NewAbstractResourceKey);
                }
            }
        }

        public string NewAbstractResourceDescription {
            get { return _newAbstractResourceDescription; }
            set {
                if (!string.Equals(_newAbstractResourceDescription, value)) {
                    _newAbstractResourceDescription = value;
                    NotifyOfPropertyChange(() => NewAbstractResourceDescription);
                }
            }
        }

        public void InsertNewAbstractResource() {
            var key = NewAbstractResourceKey;
            var description = NewAbstractResourceDescription;
            if (!string.IsNullOrWhiteSpace(key)) {
                _provider.InsertAbstractResource(key, description ?? string.Empty);
                UpdateData();
                NewAbstractResourceKey = string.Empty;
                NewAbstractResourceDescription = string.Empty;
            }
            else {
                _windowManager.ShowDialog(new MessageViewModel("Ошибка", "Не заданы параметры."));
            }
        }

        #endregion

        public AbstractResourceImplListViewModel ImplementationsList {
            get { return _implementationsList; }
            private set {
                if (!ReferenceEquals(value, _implementationsList)) {
                    _implementationsList = value;
                    NotifyOfPropertyChange(() => ImplementationsList);
                }
            }
        }

        private void UpdateData() {
            _abstractResources.Clear();
            var resources = _provider.GetAbstractResources().OrderBy(res => res.Key);
            foreach (var resource in resources) {
                _abstractResources.Add(resource);
            }
            SelectedAbstractResource = _abstractResources.FirstOrDefault();
        }
    }
}