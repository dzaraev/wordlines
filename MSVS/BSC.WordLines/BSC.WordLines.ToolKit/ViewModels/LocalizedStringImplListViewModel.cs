﻿using System;
using System.Linq;
using BSC.WordLines.Data;
using Caliburn.Micro;

namespace BSC.WordLines.ToolKit.ViewModels {
    public class LocalizedStringImplListViewModel : Screen {
        private readonly ILocalizedStringData _localizedStringData;
        private readonly ILocalDataProvider _provider;
        private readonly IWindowManager _windowManager;
        private readonly IObservableCollection<LocalizedStringImplViewModel> _implementations;
        private LocalizedStringImplViewModel _selectedImplementation; 

        public LocalizedStringImplListViewModel(
            ILocalizedStringData localizedStringData,
            ILocalDataProvider provider
            ) {
            if (localizedStringData == null) {
                    throw new ArgumentNullException("localizedStringData");
            }
                if (provider == null) {
                    throw new ArgumentNullException("provider");
            }
            _windowManager = IoC.Get<IWindowManager>();
            if (_windowManager == null) {
                throw new InvalidOperationException("Cannot resolve IWindowsManager.");
            }
            _localizedStringData = localizedStringData;
            _provider = provider;
            _implementations = new BindableCollection<LocalizedStringImplViewModel>();

            RefreshImplementations();
        }

        public IObservableCollection<LocalizedStringImplViewModel> Implementations {
            get { return _implementations; }
        }

        public LocalizedStringImplViewModel SelectedImplementation {
            get { return _selectedImplementation; }
            set {
                if (!ReferenceEquals(_selectedImplementation, value)) {
                    _selectedImplementation = value;
                    NotifyOfPropertyChange(() => SelectedImplementation);
                }
            }
        }

        private void RefreshImplementations() {
            var implementations = _provider
                .GetStringImplementations(_localizedStringData.Key)
                .OrderBy(data => data.Id)
                .Select(data => new LocalizedStringImplViewModel(data, _provider));
            _implementations.Clear();
            _implementations.AddRange(implementations);
        }

        public void Edit() {
            var editedItem = SelectedImplementation;
            if (editedItem == null) {
                _windowManager.ShowDialog(new MessageViewModel("Нельзя", "Выберите элемент."));
                return;
            }
            var editedItemData = editedItem.Data;
            var editDialog = new EditLocalizedStringViewModel(_localizedStringData, editedItemData, _provider);
            bool? answer = _windowManager.ShowDialog(editDialog);
            if (answer.HasValue && answer.Value) {
                RefreshImplementations();
            }
        }

        public void Insert() {
            var insertDialog = new EditLocalizedStringViewModel(_localizedStringData, null, _provider);
            bool? answer = _windowManager.ShowDialog(insertDialog);
            if (answer.HasValue && answer.Value) {
                RefreshImplementations();
            }
        }

        public void Delete() {
            var deletedItem = SelectedImplementation;
            if (deletedItem == null) {
                _windowManager.ShowDialog(new MessageViewModel("Нельзя", "Выберите элемент."));
                return;
            }
            var question = new MessageViewModel("Вопрос", "Уверен?") { IsQuestion = true };
            bool? answer = _windowManager.ShowDialog(question);
            if (answer.HasValue && answer.Value) {
                _provider.DeleteStringImplementation(deletedItem.Data);
                RefreshImplementations();
            }
        }
    }
}