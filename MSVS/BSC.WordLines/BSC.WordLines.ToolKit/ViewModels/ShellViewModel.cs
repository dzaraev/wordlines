﻿using BSC.WordLines.Data;
using Caliburn.Micro;

namespace BSC.WordLines.ToolKit.ViewModels {
    public class ShellViewModel : Conductor<object>.Collection.OneActive {
         public ShellViewModel(
             AbstractResourcesPageViewModel abstractResourcesPage,
             LocalizedStringsPageViewModel localizedStringsPage,
             DictionaryPageViewModel dictionaryPage,
             EnglishDictionaryPageViewModel englishDictionaryPage,
             GermanDictionaryPageViewModel germanDictionaryPage,
             ILocalDataProvider provider) {

             Items.Add(abstractResourcesPage);
             Items.Add(localizedStringsPage);
             Items.Add(englishDictionaryPage);
             Items.Add(germanDictionaryPage);
             Items.Add(dictionaryPage);
             
             ActiveItem = abstractResourcesPage;
             var versions = provider.GetVersions();
             DisplayName = string.Format("Schema Version = {0}   Release Version = {1}", versions.SchemaVersion, versions.ReleaseVersion);
         }
    }
}