﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using BSC.WordLines.Data;
using BSC.WordLines.Data.SqliteNet.ORM;
using BSC.WordLines.Utils;
using Caliburn.Micro;
using SQLite;

namespace BSC.WordLines.ToolKit.ViewModels {
    public class DictionaryPageViewModel : Screen {
        private readonly ILocalDataProvider _provider;
        private readonly IWindowManager _windowManager;
        private readonly IDataConfig _dataConfig;
        private readonly IObservableCollection<string> _words;
        private readonly List<string> _badWords;
        private readonly IObservableCollection<LanguageListItemViewModel> _languages;
        private IEnumerable<LetterStatsViewModel> _letters;
        private string _categoryLinksFileName;
        private string _articlesFileName;
        private LanguageListItemViewModel _selectedLanguage;
        private string _includeCategory;
        private string _excludeCategory;
        private int _parseProgress;
        private bool _isParsing;

        public DictionaryPageViewModel(ILocalDataProvider provider, IWindowManager windowManager, IDataConfig dataConfig) {
            provider.EnsureNotNull("provider");
            windowManager.EnsureNotNull("windowManager");
            dataConfig.EnsureNotNull("dataConfig");

            DisplayName = "Словарь";
            _provider = provider;
            _windowManager = windowManager;
            _dataConfig = dataConfig;
            _words = new BindableCollection<string>();
            _badWords = new List<string>();
            _languages = new BindableCollection<LanguageListItemViewModel>(
                _provider.GetAllLanguages()
                .OrderBy(language => language.Name)
                .Select(data => new LanguageListItemViewModel(data)));
            _selectedLanguage = _languages.FirstOrDefault();

            CategoryLinksFileName = @"D:\WORK\AllLines\Dic\categorylinks_script.sql";
            ArticlesFileName = @"D:\WORK\AllLines\Dic\articles.xml";
            IncludeCategory = "Русские_существительные;Русские_глаголы;Русские_прилагательные";
            ExcludeCategory = "Имена_собственные;Женские_имена/ru;Мужские_имена/ru;Женские_имена;Мужские_имена;Топонимы;Топонимы/ru;Названия_городов;Названия_городов/ru;Названия_городов_России;Названия_городов_России/ru";
        }

        public IObservableCollection<string> Words {
            get { return _words; }
        }

        public IEnumerable<LetterStatsViewModel> Letters {
            get { return _letters; }
            private set {
                if (!ReferenceEquals(_letters, value)) {
                    _letters = value;
                    NotifyOfPropertyChange(() => Letters);
                }
            }
        }

        public string CategoryLinksFileName {
            get { return _categoryLinksFileName; }
            set {
                _categoryLinksFileName = value;
                NotifyOfPropertyChange(() => CategoryLinksFileName);
            }
        }

        public string ArticlesFileName {
            get { return _articlesFileName; }
            set {
                _articlesFileName = value;
                NotifyOfPropertyChange(() => ArticlesFileName);
            }
        }

        public string IncludeCategory {
            get { return _includeCategory; }
            set {
                _includeCategory = value;
                NotifyOfPropertyChange(() => IncludeCategory);
            }
        }

        public string ExcludeCategory {
            get { return _excludeCategory; }
            set {
                _excludeCategory = value;
                NotifyOfPropertyChange(() => ExcludeCategory);
            }
        }

        public IObservableCollection<LanguageListItemViewModel> Languages {
            get { return _languages; }
        }

        public LanguageListItemViewModel SelectedLanguage {
            get { return _selectedLanguage; }
            set {
                if (!ReferenceEquals(_selectedLanguage, value)) {
                    _selectedLanguage = value;
                    NotifyOfPropertyChange(() => SelectedLanguage);
                }
            }
        }

        public int ParseProgress {
            get { return _parseProgress; }
            set {
                if (_parseProgress != value) {
                    _parseProgress = value;
                    NotifyOfPropertyChange(() => ParseProgress);
                }
            }
        }

        public bool IsParsing {
            get { return _isParsing; }
            set {
                if (_isParsing != value) {
                    _isParsing = value;
                    NotifyOfPropertyChange(() => IsParsing);
                }
            }
        }

        public void ParseWords() {
            if (!File.Exists(CategoryLinksFileName)) {
                string message = string.Format("Файл не найден: {0}", CategoryLinksFileName);
                _windowManager.ShowDialog(new MessageViewModel("Ошибка", message));
            }
            
            var uiContext = SynchronizationContext.Current;
            var includeCategories = IncludeCategory.Split(';');
            var excludeCategories = ExcludeCategory.Split(';');
            string categoryLinksFileName = CategoryLinksFileName;
            string articlesFilesName = ArticlesFileName;
            IsParsing = true;
            Task.Factory.StartNew(() => {
                var includeIndexes = new List<int>();
                var excludeIndexes = new List<int>();
                using (var categoryLinksStream = new FileStream(categoryLinksFileName, FileMode.Open))
                using (var categoryLinksReader = new StreamReader(categoryLinksStream, Encoding.UTF8)) {
                    int prevProgress = 0;
                    long bytesPerPercent = categoryLinksStream.Length / 100;
                    while (categoryLinksStream.Position < categoryLinksStream.Length) {
                        if (!SeekUntilString(categoryLinksReader, "INSERT INTO `categorylinks` VALUES (", "','page'),(")) {
                            break;
                        }
                        int articleIndex;
                        string articleIndexString = ReadUntilSymbol(categoryLinksReader, ',');

                        if (!int.TryParse(articleIndexString, out articleIndex)) {
                            Debug.WriteLine("Error parse article index string: {0}", articleIndexString);
                            continue;
                        }
                        string categoryString = ReadUntilSymbol(categoryLinksReader, ',').Trim('\'');
                        if (includeCategories.Contains(categoryString)) {
                            includeIndexes.Add(articleIndex);
                        }
                        else if (excludeCategories.Contains(categoryString)) {
                            excludeIndexes.Add(articleIndex);
                        }

                        long streamPosition = categoryLinksStream.Position;
                        if (streamPosition % 10 == 0) {
                            int progress = (int) (streamPosition / bytesPerPercent);
                            if (progress > prevProgress) {
                                uiContext.Post(state => ParseProgress = progress, null);
                            }
                        }
                    }
                }
                
                uiContext.Post(state => ParseProgress = 0, null);

                var words = new List<string>();
                var actualIndexes = includeIndexes.Except(excludeIndexes).Distinct().ToList();
                using (var articlesStream = new FileStream(articlesFilesName, FileMode.Open))
                using (var articlesReader = new StreamReader(articlesStream, Encoding.UTF8)) {
                    int prevProgress = 0;
                    long bytesPerPercent = articlesStream.Length / 100;
                    while (articlesStream.Position < articlesStream.Length) {
                        if (!SeekUntilString(articlesReader, "<title>")) {
                            break;
                        }
                        string title = ReadUntilSymbol(articlesReader, '<');
                        if (string.IsNullOrWhiteSpace(title)) {
                            Debug.WriteLine("Error article title: \"{0}\" at position: {1}", title, articlesStream.Position);
                            continue;
                        }
                        if (!SeekUntilString(articlesReader, "<id>")) {
                            break;
                        }
                        int articleId;
                        string articleIdString = ReadUntilSymbol(articlesReader, '<');
                        if (!int.TryParse(articleIdString, out articleId)) {
                            Debug.WriteLine("Error parse article Id: {0}", articleIdString);
                            continue;
                        }
                        if (title.Length > 1 && actualIndexes.Contains(articleId)) {
                            words.Add(title.Trim().ToUpperInvariant());
                        }

                        long streamPosition = articlesStream.Position;
                        if (streamPosition % 10 == 0) {
                            int progress = (int) (streamPosition / bytesPerPercent);
                            if (progress > prevProgress) {
                                uiContext.Post(state => ParseProgress = progress, null);
                            }
                        }
                    }
                }
                uiContext.Post(state => ParseProgress = 100, null);
                uiContext.Post(state => HandleParseFinished(words), null);
            });
        }

        private void HandleParseFinished(IEnumerable<string> words) {
            Words.Clear();
            Words.AddRange(words.Distinct());
            IsParsing = false;
        }

        private string ReadUntilSymbol(TextReader reader, char symbol) {
            var builder = new StringBuilder();
            int nextInt = reader.Read();
            while (nextInt != -1) {
                char nextChar = (char) nextInt;
                if (nextChar != symbol) {
                    builder.Append(nextChar);
                    nextInt = reader.Read();
                }
                else {
                    break;
                }
            }
            return builder.ToString();
        }

        private bool SeekUntilString(TextReader reader, params string [] stopStrings) {
            int maxStopStringLength = stopStrings.Max(str => str.Length);
            var builder = new StringBuilder(maxStopStringLength);
            int nextInt = reader.Read();
            while (nextInt != -1) {
                char nextChar = (char) nextInt;
                builder.Append(nextChar);
                string builderString = builder.ToString();
                foreach (var stopString in stopStrings) {
                    if (builderString.Length >= stopString.Length) {
                        string tail = builderString.Substring(builderString.Length - stopString.Length);
                        if (tail == stopString) {
                            return true;
                        }
                    }
                }
                if (builder.Length > maxStopStringLength) {
                    builder.Remove(0, 1);
                }
                nextInt = reader.Read();
            }
            return false;
        }

        public void InsertParsedWords() {
            /*if (Words.Any()) {
                _provider.InsertWords(Words.Select(w => w.ToUpperInvariant()), SelectedLanguage.Data.Id);
            }*/
            //Obsolete
        }

        public void FetchBadWords() {
            _badWords.Clear();
            var badWordsTarget = new List<string> {
                "АЗ", "ВЫ", "ВИ", "ВОНИ", "МЫ", "ОН", "ОНА", "ОНЕ", "ОНИ", "ОНО", "ОНЪ", "ТЫ", 
                "ХУЙ", "ПИЗДА", "БЛЯДЬ", "ПИДАРАС", "ПИДАР",
                "ЛОЛ", "БД", "МИЛАН", "СОЧИ", "ЛОХ", "КИСА",
                "НИИ", "МТС", "ГА", "БА", "ПЕРУ", "НАЙРОБИ", "ЛИТВА", "АА", "ПО", "ПРАГА", "КОЛИН",
                "ЛИВИЯ", "РИГА", "ИИ", "ИКАО", "ЛИВАН", "МИРОН", "МОГОЛ", "ЛУВР", "НУРИК", "КОЩЕЙ",
                "ОБЬ", "ОМЬ", "ВИЙ", "НИДЕРЛАНДЫ", "МОНАКО", "ПАНА", "МИШЕГОСС", "ЕНИСЕЙ", "ОКА", "ТЕРЕК", "ТВИТ",
                "ААК", "АБ", "АВ", "АВА", "АБЭВЭГЭДЭЙКА", "АГА", "АИ", "АМН", "АСС", "АТ", "АТС", "АТЭС", "АУДИ", "АУТО",
                "АХАТЬ", "АХАТЬСЯ", "АХУЙ", "ОХАТЬ", "ОХАТЬСЯ", "ПОХУЙ", "АЭС", "АЯН", "АЯ", "БАГ", "БАМ", "БЕ", "БЕЖ", "БЕН",
                "БИТЛ", "БОТЪ", "БУФФ", "БЭ", "БЭСМ", "ВАК", "ВЕРП", "ВИГ", "ВИЧ", "ВРИО", "ВТУЗ", "ВУЙ", "ВЦ", "ВЧК", "ГАЭЛ", "ГАЯ",
                "ГЕ", "ГЕЗ", "ГЕА", "ГЕЙТ", "ГЕРР", "ГЕЯ", "ГЕТТЕР", "ГИГ", "ГИППОПОТОМОНСТРОСЕСКВИПЕДАЛИОФОБИЯ", "ГМК", "ГОМНО",
                "ГОМИ", "ГПУ", "АИЛ", "АЙН", "АНЯ", "БУФ", "БЭР", "БАШ", "ГЕЗ", "ГЭГ", "ДАГ", "ДАК", "ДИЙ", "ДОС", "ДЫЙ", "ЕВА", "ЕР",
                "ЕРА", "ЕРЬ", "ЕТЬ", "ЗАВ", "ЗГА", "ЗЕТ", "ЗИЛ", "ИБО", "ИДЫ", "ИЖЕ", "ИЗО", "ИР", "ИТО", "КВА", "КВН", "КЕД", "КЗ", "КО", "КОХ",
                "КПД", "КРИ", "ЛВС", "ЛЕЯ", "ЛИ", "ЛЯД", "МА", "МАЕ", "МГА", "МГУ", "МВБ", "МЕГ", "МММ", "МНС", "МНЯ", "МФК", "МЫТ",
                "МЫШ", "МЯО", "НАШ", "НЕЯ", "НИР", "НО", "НОВ", "НОД", "НС", "НУН", "НЭК", "НЭП", "ОХ", "ПВД", "ПК", "ПОД", "ПРЯ", "ПЭ",
                "РА", "РАФ", "РОЭ", "РТС", "РЦЫ", "РЮХ", "СДР", "СИУ", "СОТ", "ТАТ", "ТАЮ", "ТЕ", "ТЕР", "ТОЛ", "ТТЛ", "ТУГ", "ТУТ", "ТЭК",
                "ТЯ", "ТЯГ", "УВЧ", "УДК", "УЕМ", "УЙ", "УК", "УПК", "УШИ", "УСЫ", "ФАК", "ФЕС", "ФИШ", "ФОБ", "ФРУ", "ФСС", "ФУК", "ФЭР",
                "ХАЙ", "ХАЧ", "ХЕК", "ЦЕК", "ЦОС", "ЦПУ", "ЦУК", "ЦЫ", "ЧВИ", "ЧЕ", "ЧЕЛ", "ЧЕТ", "ЧОХ", "ЧУР", "ШАФ", "ШОП", "ШОТ", "ЩЕЦ",
                "ЫЙМ", "ЫР", "ЭВЕ", "ЭЙС", "ЭРГ", "ЭФ", "ЭЦП", "ЮГО", "ЮЙ", "ЮЛЯ", "ЮР", "ЮРА", "ЮТА", "ЮЭ", "ЯГА", "ЯДЬ", "ЯЕИ", "ЯЕЙ", "ЯИК",
                "ЯМ", "ЯНА", "ЯО", "ЯРЬ", "ЯС", "ЙЕМЕН", "КНДР", "ЙИФФ", "ЙИФФАНУТЫЙ", "АЙОВА", "АЙОВАИН"
            };
            foreach (var word in Words) {
                if (word.Contains(":") || 
                    word.Contains("-") ||
                    word.Contains(" ") ||
                    word.Contains("?") ||
                    word.Contains("D") ||
                    badWordsTarget.Contains(word)) {

                    if (word.Contains(":")) {
                        Console.WriteLine("Bad word with \":\" = {0}", word);
                    }
                    _badWords.Add(word);
                }
            }
        }

        public void DeleteBadWords() {
            Words.RemoveRange(_badWords);
        }

        public void CalculateStats() {
            var letterStatsDictionary = new Dictionary<char, LetterStatsViewModel>();
            foreach (string word in Words) {
                foreach (char letter in word) {
                    if (letterStatsDictionary.ContainsKey(letter)) {
                        letterStatsDictionary[letter].UsingCount++;
                    }
                    else {
                        letterStatsDictionary[letter] = new LetterStatsViewModel {
                            Letter = letter.ToString(CultureInfo.InvariantCulture),
                            UsingCount = 1,
                        };
                    }
                }
            }

            double maxUsingCount = letterStatsDictionary.Max(stats => stats.Value.UsingCount);
            letterStatsDictionary.Apply(stats => {
                int usingCount = stats.Value.UsingCount;
                stats.Value.Chance = Math.Round(usingCount / maxUsingCount, 3);
            });

            Letters = letterStatsDictionary
                .Select(pair => pair.Value)
                .OrderBy(vm => vm.Letter);
        }

        public void ReplaceConflictLetter() {
            for (int i = 0; i < Words.Count; i++) {
                Words[i] = Words[i].Replace('Ё', 'Е');
            }
            Words.Refresh();
        }

        public void DumpToFile() {
            var wordsFile = File.CreateText(_dataConfig.GetLocalWordsFileFullPath("Russian"));
            var sortedWords = new List<string>(Words);
            sortedWords.Sort();
            foreach (string word in sortedWords) {
                wordsFile.WriteLine(word);
            }
            wordsFile.Close();

            //debug
            var wordsdebugFile = File.CreateText(_dataConfig.GetLocalWordsFileFullPath("debugwords"));
            var shortWords = sortedWords.Where(w => w.Length <= 3);
            foreach (string word in shortWords) {
                wordsdebugFile.WriteLine(word);
            }
            wordsdebugFile.Close();
        }
    }
}