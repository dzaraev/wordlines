﻿using System;
using System.Diagnostics;
using System.Linq;
using BSC.WordLines.Data;
using BSC.WordLines.Utils;
using Caliburn.Micro;

namespace BSC.WordLines.ToolKit.ViewModels {
    public class EditResourceImplViewModel : Screen {
        private readonly IAbstractResourceData _targetAbstractResources;
        private readonly ILocalDataProvider _provider;
        private readonly IObservableCollection<ThemeListItemViewModel> _themes;
        private readonly IObservableCollection<EnvironmentListItemViewModel> _environments;
        private readonly IObservableCollection<LanguageListItemViewModel> _languages;
        private readonly IWindowManager _windowManager;
        private readonly IResourceImplementationData _implementation;
        private ThemeListItemViewModel _selectedTheme;
        private EnvironmentListItemViewModel _selectedEnvironment;
        private LanguageListItemViewModel _selectedLanguage;
        private string _concreteResourceStringValue;
        private int _existsConcreteResourceId;
        private bool _useExistsConcreteResource;
        private bool _reuseConcreteResourceEnabled;

        public EditResourceImplViewModel(
            IAbstractResourceData targetAbstractResources,
            IResourceImplementationData implementation,
            ILocalDataProvider provider) {
            if (provider == null) {
                throw new ArgumentNullException("provider");
            }
            if (targetAbstractResources == null) {
                throw new ArgumentNullException("targetAbstractResources");
            }
            _windowManager = IoC.Get<IWindowManager>();
            if(_windowManager == null) {
                throw new InvalidOperationException("Cannot resolve IWindowsManager.");
            }
            _targetAbstractResources = targetAbstractResources;
            _implementation = implementation;
            _provider = provider;
            _themes = new BindableCollection<ThemeListItemViewModel>(
                _provider.GetAllThemes()
                    .OrderBy(theme => theme.Name)
                    .Select(data => new ThemeListItemViewModel(data)));
            _themes.Insert(0, new ThemeListItemViewModel(null));

            _environments = new BindableCollection<EnvironmentListItemViewModel>(
                _provider.GetAllEnvironments()
                    .OrderBy(environment => environment.Key)
                    .Select(data => new EnvironmentListItemViewModel(data)));
            _environments.Insert(0, new EnvironmentListItemViewModel(null));
            
            _languages = new BindableCollection<LanguageListItemViewModel>(
                _provider.GetAllLanguages()
                .OrderBy(language => language.Name)
                .Select(data=>new LanguageListItemViewModel(data)));
            _languages.Insert(0, new LanguageListItemViewModel(null));
            
            if (implementation != null) {
                Debug.Assert(implementation.AbstractResourceId == targetAbstractResources.Id,
                             "implementation.AbstractResourceId == targetAbstractResources.Id");
                _selectedTheme = _themes.First(theme => {
                    var themeData = theme.Data;
                    if (themeData != null) {
                        return themeData.Id == implementation.ThemeId;
                    }
                    return !implementation.ThemeId.HasValue;
                });
                _selectedLanguage = _languages.First(language => {
                    var languageData = language.Data;
                    if (languageData != null) {
                        return languageData.Id == implementation.LanguageId;
                    }
                    return !implementation.LanguageId.HasValue;
                });
                _selectedEnvironment = _environments.First(env => {
                    var envData = env.Data;
                    if (envData != null) {
                        return envData.Id == implementation.EnvironmentId;
                    }
                    return !implementation.EnvironmentId.HasValue;
                });
                var concreteResource = _provider.GetConcreteResource(implementation.ConcreteResourceId);
                ConcreteResourceStringValue = concreteResource.Data.BytesToString();
                ReuseConcreteResourceEnabled = false;
                DisplayName = string.Format("Изменить значение для {0}", targetAbstractResources.Key);
            }
            else {
                _selectedTheme = _themes.FirstOrDefault();
                _selectedLanguage = _languages.FirstOrDefault();
                _selectedEnvironment = _environments.FirstOrDefault();
                ConcreteResourceStringValue = string.Empty;
                ReuseConcreteResourceEnabled = true;
                DisplayName = string.Format("Добавить значение для {0}", targetAbstractResources.Key);
            }
        }

        public IAbstractResourceData TargetAbstractResource {
            get { return _targetAbstractResources; }
        }

        public IObservableCollection<ThemeListItemViewModel> Themes {
            get { return _themes; }
        }

        public IObservableCollection<EnvironmentListItemViewModel> Environments {
            get { return _environments; }
        }

        public IObservableCollection<LanguageListItemViewModel> Languages {
            get { return _languages; }
        }

        public ThemeListItemViewModel SelectedTheme {
            get { return _selectedTheme; }
            set {
                if (!ReferenceEquals(_selectedTheme, value)) {
                    _selectedTheme = value;
                    NotifyOfPropertyChange(() => SelectedTheme);
                }
            }
        }

        public EnvironmentListItemViewModel SelectedEnvironment {
            get { return _selectedEnvironment; }
            set {
                if (!ReferenceEquals(_selectedEnvironment, value)) {
                    _selectedEnvironment = value;
                    NotifyOfPropertyChange(() => SelectedEnvironment);
                }
            }
        }

        public LanguageListItemViewModel SelectedLanguage {
            get { return _selectedLanguage; }
            set {
                if (!ReferenceEquals(_selectedLanguage, value)) {
                    _selectedLanguage = value;
                    NotifyOfPropertyChange(() => SelectedLanguage);
                }
            }
        }

        //TODO поддержку нестроковых конкретных ресурсов(изображений, звуков) (храниться будет все равно строка - URI)
        public string ConcreteResourceStringValue {
            get { return _concreteResourceStringValue; }
            set {
                if (!string.Equals(_concreteResourceStringValue, value, StringComparison.Ordinal)) {
                    _concreteResourceStringValue = value;
                    NotifyOfPropertyChange(() => ConcreteResourceStringValue);
                }
            }
        }

        public int ExistsConcreteResourceId {
            get { return _existsConcreteResourceId; }
            set {
                if (_existsConcreteResourceId != value) {
                    if (value <= 0) {
                        return;
                    }
                    _existsConcreteResourceId = value;
                    NotifyOfPropertyChange(() => ExistsConcreteResourceId);
                }
            }
        }

        public bool UseExistsConcreteResource {
            get { return _useExistsConcreteResource; }
            set {
                if (_useExistsConcreteResource != value) {
                    _useExistsConcreteResource = value;
                    NotifyOfPropertyChange(() => UseExistsConcreteResource);
                }
            }
        }

        public bool ReuseConcreteResourceEnabled {
            get { return _reuseConcreteResourceEnabled; }
            private set {
                if (_reuseConcreteResourceEnabled != value) {
                    _reuseConcreteResourceEnabled = value;
                    NotifyOfPropertyChange(() => ReuseConcreteResourceEnabled);
                }
            }
        }

        public void Save() {
            IResourceImplementationData savedImplementation;
            if (_implementation != null) {
                var allImplementations = _provider.GetAllResourceImplementations();
                bool isSideLinksExists = allImplementations.Any(
                    impl =>
                    impl.ConcreteResourceId == _implementation.ConcreteResourceId &&
                    impl.Id != _implementation.Id);
                if (isSideLinksExists) {
                    var question = new MessageViewModel("Вопрос", 
                            "На связанный конкретный ресурс есть сторонние ссылки, изменить его всё равно?") { IsQuestion = true };
                    bool? answer = _windowManager.ShowDialog(question);
                    if (!answer.HasValue || !answer.Value) {
                        return;
                    }
                }
                savedImplementation = _implementation;
                var savedConcreteResource = _provider.GetConcreteResource(_implementation.ConcreteResourceId);
                savedConcreteResource.Data = ConcreteResourceStringValue.StringToBytes();
                _provider.SaveConcreteResource(savedConcreteResource);
            }
            else {
                int concreteResourceId;
                if(UseExistsConcreteResource) {
                    concreteResourceId = ExistsConcreteResourceId;
                }
                else {
                    var newConcreteResource = _provider.InsertConcreteResource(ConcreteResourceStringValue);
                    concreteResourceId = newConcreteResource.Id;
                }
                int abstractResourceId = _targetAbstractResources.Id;
                savedImplementation = _provider.InsertEmptyResourceImplementation(abstractResourceId, concreteResourceId);
            }
            savedImplementation.EnvironmentId = _selectedEnvironment.Data != null ? _selectedEnvironment.Data.Id : (int?)null;
            savedImplementation.ThemeId = _selectedTheme.Data != null ? _selectedTheme.Data.Id : (int?)null;
            savedImplementation.LanguageId = _selectedLanguage.Data != null ? _selectedLanguage.Data.Id : (int?)null;
            _provider.SaveResourceImplementation(savedImplementation);
            TryClose(true);
        }

        public void Cancel() {
            TryClose(false);
        }
    }
}