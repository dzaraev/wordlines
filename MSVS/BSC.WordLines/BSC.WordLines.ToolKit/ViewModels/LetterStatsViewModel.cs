﻿using Caliburn.Micro;

namespace BSC.WordLines.ToolKit.ViewModels {
    public class LetterStatsViewModel : PropertyChangedBase {
        public string Letter { get; set; }
        public int UsingCount { get; set; }
        public double Chance { get; set; }
    }
}