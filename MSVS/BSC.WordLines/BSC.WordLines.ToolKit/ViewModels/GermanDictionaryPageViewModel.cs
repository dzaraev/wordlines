﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using BSC.WordLines.Data;
using Caliburn.Micro;

namespace BSC.WordLines.ToolKit.ViewModels {
    public class GermanDictionaryPageViewModel : Screen {
         private readonly IWindowManager _windowManager;
        private readonly IDataConfig _dataConfig;
        private IEnumerable<LetterStatsViewModel> _letters;
        private int _parseProgress;

        public GermanDictionaryPageViewModel(IWindowManager windowManager, IDataConfig dataConfig) {
            _windowManager = windowManager;
            _dataConfig = dataConfig;
            DisplayName = "German Dictionary";
        }

        public IEnumerable<LetterStatsViewModel> Letters {
            get { return _letters; }
            private set {
                if (!ReferenceEquals(_letters, value)) {
                    _letters = value;
                    NotifyOfPropertyChange(() => Letters);
                }
            }
        }

        public int ParseProgress {
            get { return _parseProgress; }
            set {
                if (_parseProgress != value) {
                    _parseProgress = value;
                    NotifyOfPropertyChange(() => ParseProgress);
                }
            }
        }

        public void ParseDictionary() {
            string dicFileName1 = @"D:\WORK\AllLines\Dic\german_words.txt";
            List<string> errorWords = new List<string> {
                "ABT", "ADA", "ADE", "AFA", "ALB", "ALF", "PC",
                "MC", "PFG", "SRP", "SS", "SSD", "ST", "STK", "TNT", "THE", "", "DB", "VGL", "VDI", "VII", "VIA", "VIF", "VI",
                "VTG", "WC", "WM", "WSJ", "ZB", "ZDF", "ZK", "SPD", "SRG", "SRI", "SFR", "SD", "QM", "PTT", "PR", "PPS", "PFG",
                "NDP", "MRD", "MR", "MHZ", "MM", "MBB", "MNA", "LSD", "LH", "LDP", "KPJ", "KPD", "KM", "KLM", "KGB", "KLZ", "KG", "IV", "IX", "", 
                "ITT", "IOS", "II", "III", "IMI", "IRR", "GST", "FT", "FRL", "FR", "FP", "FLN", "FG", "FFR", "ETH", "EWS", "ETC", "DM", "DNS", "DJ", 
                "DGB", "DDR", "DB", "CM", "CDC", "CD", "BZW", "BP", "BMW", "BMC", "BH", "BBC", "BCM",
                "VT", "VU", "WK", "XI", "XII", "XIV", "XIX", "XVI", "XX", "XXI", "XXV", "XXX", "YR", "ZA", "XIII", "XVII", 
                "XVIII", "XXII", "XXIII", "XXIV", "XYLA"
            };

            if (!File.Exists(dicFileName1)) {
                string message = string.Format("Файл словаря не найден");
                _windowManager.ShowDialog(new MessageViewModel("Ошибка", message));
            }

            var uiContext = SynchronizationContext.Current;
            Task.Factory.StartNew(() => {
                uiContext.Post(state => ParseProgress = 1, null);
                var parsedWords = new List<string>();
                var dicWords = File.ReadAllLines(dicFileName1).ToList();
                long linesPerPercent = dicWords.Count / 100;
                for (int i = 0; i < dicWords.Count; i++) {
                    string word = dicWords[i];
                    if (!word.Contains(" ") && !word.Contains("-") && !word.Contains("'") && word.Length > 1 &&
                        !word.Contains("�") && !word.Contains("(") && !word.Contains(",") && !word.Contains(".") &&
                        !word.Contains("/") && !word.Contains("2") && !word.Contains("4")) {
                        parsedWords.Add(word.ToUpperInvariant());
                    }

                    if (i % 10 == 0) {
                        int progress = (int)(i / linesPerPercent);
                        uiContext.Post(state => ParseProgress = progress, null);
                    }
                }
                parsedWords = parsedWords.Distinct().ToList();
                parsedWords.Sort();
                parsedWords.RemoveAll(errorWords.Contains);
                DumpToFile(parsedWords);
                uiContext.Post(state => CalculateStats(parsedWords), null);
                uiContext.Post(state => ParseProgress = 100, null);
            });
        }

        public void CalculateStats(List<string> parsedWords) {
            var letterStatsDictionary = new Dictionary<char, LetterStatsViewModel>();
            foreach (string word in parsedWords) {
                foreach (char letter in word) {
                    if (letterStatsDictionary.ContainsKey(letter)) {
                        letterStatsDictionary[letter].UsingCount++;
                    }
                    else {
                        letterStatsDictionary[letter] = new LetterStatsViewModel {
                            Letter = letter.ToString(CultureInfo.InvariantCulture),
                            UsingCount = 1,
                        };
                    }
                }
            }

            double maxUsingCount = letterStatsDictionary.Max(stats => stats.Value.UsingCount);
            letterStatsDictionary.Apply(stats => {
                int usingCount = stats.Value.UsingCount;
                stats.Value.Chance = Math.Round(usingCount / maxUsingCount, 3);
            });

            Letters = letterStatsDictionary
                .Select(pair => pair.Value)
                .OrderBy(vm => vm.Letter);
        }

        public void DumpToFile(List<string> words) {
            var wordsFile = File.CreateText(_dataConfig.GetLocalWordsFileFullPath("German"));
            var sortedWords = new List<string>(words);
            sortedWords.Sort();
            foreach (string word in sortedWords) {
                wordsFile.WriteLine(word);
            }
            wordsFile.Close();

            //debug
            var wordsdebugFile = File.CreateText(_dataConfig.GetLocalWordsFileFullPath("debugwords.German"));
            var shortWords = sortedWords.Where(w => w.Length <= 3);
            foreach (string word in shortWords) {
                wordsdebugFile.WriteLine(word);
            }
            wordsdebugFile.Close();
        }
    }
}