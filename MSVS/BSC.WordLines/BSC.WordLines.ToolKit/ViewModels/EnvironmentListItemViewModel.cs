﻿using BSC.WordLines.Data;
using Caliburn.Micro;

namespace BSC.WordLines.ToolKit.ViewModels {
    public class EnvironmentListItemViewModel: PropertyChangedBase, IHaveDisplayName {
        private readonly IEnvironmentData _data;
        private string _displayName;

        public EnvironmentListItemViewModel(IEnvironmentData data) {
            _data = data;
            DisplayName = data != null ? data.Key : "NULL";
        }

        public IEnvironmentData Data {
            get { return _data; }
        }

        public string DisplayName {
            get { return _displayName; }
            set {
                if (value != _displayName) {
                    _displayName = value;
                    NotifyOfPropertyChange(() => DisplayName);
                }
            }
        }
    }
}