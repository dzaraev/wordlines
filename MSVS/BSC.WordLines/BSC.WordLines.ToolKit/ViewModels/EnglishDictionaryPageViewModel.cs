﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using BSC.WordLines.Data;
using BSC.WordLines.Data.SqliteNet.ORM;
using Caliburn.Micro;

namespace BSC.WordLines.ToolKit.ViewModels {
    public class EnglishDictionaryPageViewModel : Screen {
        private readonly IWindowManager _windowManager;
        private readonly IDataConfig _dataConfig;
        private IEnumerable<LetterStatsViewModel> _letters;
        private int _parseProgress;

        public EnglishDictionaryPageViewModel(IWindowManager windowManager, IDataConfig dataConfig) {
            _windowManager = windowManager;
            _dataConfig = dataConfig;
            DisplayName = "English Dictionary";
        }

        public IEnumerable<LetterStatsViewModel> Letters {
            get { return _letters; }
            private set {
                if (!ReferenceEquals(_letters, value)) {
                    _letters = value;
                    NotifyOfPropertyChange(() => Letters);
                }
            }
        }

        public int ParseProgress {
            get { return _parseProgress; }
            set {
                if (_parseProgress != value) {
                    _parseProgress = value;
                    NotifyOfPropertyChange(() => ParseProgress);
                }
            }
        }

        public void ParseDictionary() {
            string dicFileName1 = @"D:\WORK\AllLines\Dic\eng_words.txt";
            string dicFileName2 = @"D:\WORK\AllLines\Dic\wordsEn.txt";
            List<string> errorWords = new List<string> {
                "AA", "AAL", "AAM", "AAH", "ABC", "AFB", "AHS", "BKS", "BLO", "BPS", "BR", "CC", "CD", "CHM", "CGS", "CPL",
                "CPS", "CRC", "CS", "CSP", "CST", "CT", "CTG", "CTS", "CWM", "CWT", "DB", "DBL", "DC", "DP", "DX", "ETC", "FPS",
                "FWD", "GDS", "GRR", "HP", "HR", "HRS", "HSI", "HTS", "II", "III", "IYO", "JCT", "JI", "JU", "KB", "KL", "KUI",
                "KUA", "LF", "LH", "LL", "LPM", "LR", "MB", "MC", "MD", "MF", "MFD", "MFG", "MG", "MKT", "MN", "MPG", "MPH", "MR", "MRS", "MRU", 
                "MS", "MSG", "MSS", "MW", "MWA", "NAA", "NJ", "NM", "NTH", "OII", "OOH", "PBX", "PC", "PCT", "PF", "PKG", "PL", "PM", "PP", "PPD", 
                "PSF", "PST", "PU", "RD", "RF", "RH", "RN", "RPM", "SAA", "SC", "SCI", "SD", "SH", "SN", "SP", "SR", "SS", "ST",
                "PTS", "TBS", "TCH", "TCK", "TD", "THA", "TH", "TM", "TMH", "TNT", "TPK", "TSP", "TST", "TTY", "TX", "UH", "UHS", "VVA", "VS", 
                "VT", "VU", "WK", "XI", "XII", "XIV", "XIX", "XVI", "XX", "XXI", "XXV", "XXX", "YR", "ZA", "XIII", "XVII", 
                "XVIII", "XXII", "XXIII", "XXIV", "XYLA"
            };

            if (!File.Exists(dicFileName1) || !File.Exists(dicFileName2)) {
                string message = string.Format("Файл словаря не найден");
                _windowManager.ShowDialog(new MessageViewModel("Ошибка", message));
            }

            var uiContext = SynchronizationContext.Current;
            Task.Factory.StartNew(() => {
                uiContext.Post(state => ParseProgress = 1, null);
                var parsedWords = new List<string>();
                var dicLines1 = File.ReadAllLines(dicFileName1);
                var dicLines2 = File.ReadAllLines(dicFileName2);
                var dicWords = dicLines1.Concat(dicLines2).Distinct().ToList();
                long linesPerPercent = dicWords.Count / 100;
                for (int i = 0; i < dicWords.Count; i++) {
                    string word = dicWords[i];
                    if (!word.Contains(" ") && !word.Contains("-") && !word.Contains("'") && word.Length > 1) {
                        parsedWords.Add(word.ToUpperInvariant());
                    }

                    if (i % 10 == 0) {
                        int progress = (int)(i / linesPerPercent);
                        uiContext.Post(state => ParseProgress = progress, null);
                    }
                }
                parsedWords = parsedWords.Distinct().ToList();
                parsedWords.Sort();
                parsedWords.RemoveAll(errorWords.Contains);
                DumpToFile(parsedWords);
                uiContext.Post(state => CalculateStats(parsedWords), null);
                uiContext.Post(state => ParseProgress = 100, null);
            });
        }

        public void CalculateStats(List<string> parsedWords) {
            var letterStatsDictionary = new Dictionary<char, LetterStatsViewModel>();
            foreach (string word in parsedWords) {
                foreach (char letter in word) {
                    if (letterStatsDictionary.ContainsKey(letter)) {
                        letterStatsDictionary[letter].UsingCount++;
                    }
                    else {
                        letterStatsDictionary[letter] = new LetterStatsViewModel {
                            Letter = letter.ToString(CultureInfo.InvariantCulture),
                            UsingCount = 1,
                        };
                    }
                }
            }

            double maxUsingCount = letterStatsDictionary.Max(stats => stats.Value.UsingCount);
            letterStatsDictionary.Apply(stats => {
                int usingCount = stats.Value.UsingCount;
                stats.Value.Chance = Math.Round(usingCount / maxUsingCount, 3);
            });

            Letters = letterStatsDictionary
                .Select(pair => pair.Value)
                .OrderBy(vm => vm.Letter);
        }

        public void DumpToFile(List<string> words) {
            var wordsFile = File.CreateText(_dataConfig.GetLocalWordsFileFullPath("English"));
            var sortedWords = new List<string>(words);
            sortedWords.Sort();
            foreach (string word in sortedWords) {
                wordsFile.WriteLine(word);
            }
            wordsFile.Close();

            //debug
            var wordsdebugFile = File.CreateText(_dataConfig.GetLocalWordsFileFullPath("debugwords.English"));
            var shortWords = sortedWords.Where(w => w.Length <= 3);
            foreach (string word in shortWords) {
                wordsdebugFile.WriteLine(word);
            }
            wordsdebugFile.Close();
        }
    }
}