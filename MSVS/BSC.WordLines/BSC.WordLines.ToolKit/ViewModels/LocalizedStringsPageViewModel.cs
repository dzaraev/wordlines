﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using BSC.WordLines.Data;
using Caliburn.Micro;

namespace BSC.WordLines.ToolKit.ViewModels {
    public class LocalizedStringsPageViewModel : Screen {
        private readonly ILocalDataProvider _provider;
        private readonly ObservableCollection<ILocalizedStringData> _localizedStrings;
        private readonly IWindowManager _windowManager;
        private ILocalizedStringData _selectedLocalizedString;
        private string _newLocalizedStringKey;
        private string _newLocalizedStringDescription;
        private LocalizedStringImplListViewModel _implementationsList;

        public LocalizedStringsPageViewModel(ILocalDataProvider provider, IWindowManager windowManager) {
            if (provider == null) {
                throw new ArgumentNullException("provider");
            }
            if (windowManager == null) {
                throw new ArgumentNullException("windowManager");
            }

            _provider = provider;
            _windowManager = windowManager;
            _localizedStrings = new ObservableCollection<ILocalizedStringData>();
            DisplayName = "Строки";
        }

        protected override void OnInitialize() {
            UpdateData();
        }

        public ObservableCollection<ILocalizedStringData> LocalizedStrings {
            get { return _localizedStrings; }
        }

        public ILocalizedStringData SelectedLocalizedString {
            get { return _selectedLocalizedString; }
            set {
                if (!ReferenceEquals(_selectedLocalizedString, value)) {
                    _selectedLocalizedString = value;
                    NotifyOfPropertyChange(() => SelectedLocalizedString);
                    ImplementationsList = value != null
                                              ? new LocalizedStringImplListViewModel(value, _provider)
                                              : null;
                }
            }
        }

        private void UpdateData() {
            _localizedStrings.Clear();
            var stringDatas = _provider.GetAllLocalizedStrings().OrderBy(str => str.Key);
            foreach (var stringData in stringDatas) {
                _localizedStrings.Add(stringData);
            }
            SelectedLocalizedString = _localizedStrings.FirstOrDefault();
        }

        public string NewLocalizedStringKey {
            get { return _newLocalizedStringKey; }
            set {
                if (!string.Equals(_newLocalizedStringKey, value)) {
                    _newLocalizedStringKey = value;
                    NotifyOfPropertyChange(() => NewLocalizedStringKey);
                }
            }
        }

        public string NewLocalizedStringDescription {
            get { return _newLocalizedStringDescription; }
            set {
                if (!string.Equals(_newLocalizedStringDescription, value)) {
                    _newLocalizedStringDescription = value;
                    NotifyOfPropertyChange(() => NewLocalizedStringDescription);
                }
            }
        }

        public void InsertNewLocalizedString() {
            var key = NewLocalizedStringKey;
            var description = NewLocalizedStringDescription;
            if (!string.IsNullOrWhiteSpace(key)) {
                _provider.InsertLocalizedString(key, description ?? string.Empty);
                UpdateData();
                NewLocalizedStringKey = string.Empty;
                NewLocalizedStringDescription = string.Empty;
            }
            else {
                _windowManager.ShowDialog(new MessageViewModel("Ошибка", "Не заданы параметры."));
            }
        }

        public LocalizedStringImplListViewModel ImplementationsList {
            get { return _implementationsList; }
            private set {
                if (!ReferenceEquals(value, _implementationsList)) {
                    _implementationsList = value;
                    NotifyOfPropertyChange(() => ImplementationsList);
                }
            }
        }
    }
}