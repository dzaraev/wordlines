﻿using BSC.WordLines.Data;
using Caliburn.Micro;

namespace BSC.WordLines.ToolKit.ViewModels {
    public class LanguageListItemViewModel : PropertyChangedBase, IHaveDisplayName {
        private readonly ILanguageData _data;
        private string _displayName;

        public LanguageListItemViewModel(ILanguageData data) {
            _data = data;
            DisplayName = data != null ? data.Name : "NULL";
        }

        public ILanguageData Data {
            get { return _data; }
        }

        public string DisplayName {
            get { return _displayName; }
            set {
                if (value != _displayName) {
                    _displayName = value;
                    NotifyOfPropertyChange(() => DisplayName);
                }
            }
        }
    }
}