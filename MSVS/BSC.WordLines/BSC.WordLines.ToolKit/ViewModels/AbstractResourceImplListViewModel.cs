﻿using System;
using System.Linq;
using BSC.WordLines.Data;
using Caliburn.Micro;

namespace BSC.WordLines.ToolKit.ViewModels {
    public class AbstractResourceImplListViewModel : Screen {
        private readonly IAbstractResourceData _abstractResource;
        private readonly ILocalDataProvider _provider;
        private readonly IWindowManager _windowManager;
        private readonly IObservableCollection<AbstractResourceImplViewModel> _implementations;
        private AbstractResourceImplViewModel _selectedImplementation;

        public AbstractResourceImplListViewModel(
            IAbstractResourceData abstractResource, 
            ILocalDataProvider provider
            ) {
            if (abstractResource == null) {
                throw new ArgumentNullException("abstractResource");
            }
            if (provider == null) {
                throw new ArgumentNullException("provider");
            }
            _windowManager = IoC.Get<IWindowManager>();
            if (_windowManager == null) {
                throw new InvalidOperationException("Cannot resolve IWindowsManager.");
            }

            _abstractResource = abstractResource;
            _provider = provider;
            _implementations = new BindableCollection<AbstractResourceImplViewModel>();

            RefreshImplementations();
        }

        public IObservableCollection<AbstractResourceImplViewModel> Implementations {
            get { return _implementations; }
        }

        public AbstractResourceImplViewModel SelectedImplementation {
            get { return _selectedImplementation; }
            set {
                if (!ReferenceEquals(_selectedImplementation, value)) {
                    _selectedImplementation = value;
                    NotifyOfPropertyChange(() => SelectedImplementation);
                }
            }
        }

        private void RefreshImplementations() {
            var implementations = _provider
                .GetResourceImplementations(_abstractResource)
                .OrderBy(data => data.Id)
                .Select(data => new AbstractResourceImplViewModel(data, _provider));
            _implementations.Clear();
            _implementations.AddRange(implementations);
        }

        public void Edit() {
            var editedItem = SelectedImplementation;
            if (editedItem == null) {
                _windowManager.ShowDialog(new MessageViewModel("Нельзя", "Выберите элемент."));
                return;
            }
            var editedItemData = editedItem.Data;
            var editDialog = new EditResourceImplViewModel(_abstractResource, editedItemData, _provider);
            bool? answer = _windowManager.ShowDialog(editDialog);
            if (answer.HasValue && answer.Value) {
                RefreshImplementations();
            }
        }

        public void Insert() {
            var insertDialog = new EditResourceImplViewModel(_abstractResource, null, _provider);
            bool? answer = _windowManager.ShowDialog(insertDialog);
            if (answer.HasValue && answer.Value) {
                RefreshImplementations();
            }
        }

        public void Delete() {
            var deletedItem = SelectedImplementation;
            if (deletedItem == null) {
                _windowManager.ShowDialog(new MessageViewModel("Нельзя", "Выберите элемент."));
                return;
            }
            var deletedImplData = deletedItem.Data;
            var allImplementations = _provider.GetAllResourceImplementations();
            var relatedImplementations = allImplementations
                .Where(impl => impl.ConcreteResourceId == deletedImplData.ConcreteResourceId)
                .ToList();
            if (relatedImplementations.All(impl => impl.Id != deletedImplData.Id)) {
                throw new InvalidOperationException("Чё за херня?!");
            }
            if (relatedImplementations.Count == 1) {
                var question =
                    new MessageViewModel("Вопрос", "На связанный конкретный ресурс больше нет ссылок, удалить также и его?")
                    {IsQuestion = true};
                bool? answer = _windowManager.ShowDialog(question);
                if (answer.HasValue && answer.Value) {
                    _provider.DeleteConcreteResource(deletedImplData.ConcreteResourceId);
                }
            }
            _provider.DeleteResourceImplementation(deletedImplData.Id);
            RefreshImplementations();
        }
    }
}