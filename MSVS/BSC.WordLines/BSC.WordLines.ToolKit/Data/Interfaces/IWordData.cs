namespace BSC.WordLines.Data {
    public interface IWordData {
        int Id { get; }
        string Word { get; set; }
        int LanguageId { get; set; }
        bool HasId { get; }
        bool HasLanguageId { get; }
    }
}