﻿using SQLite;

namespace BSC.WordLines.Data.SqliteNet.ORM {
    public class Words : IWordData {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        public int LanguageId { get; set; }

        public string Word { get; set; }

        //Not intended for mapping.
        public bool HasId {
            get { return Id != 0; }
        }

        //Not intended for mapping.
        public bool HasLanguageId {
            get { return LanguageId != 0; }
        }
    }
}