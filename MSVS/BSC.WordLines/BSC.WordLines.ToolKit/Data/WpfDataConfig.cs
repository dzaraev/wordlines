﻿using System;
using BSC.WordLines.Utils;

namespace BSC.WordLines.Data {
    public class WpfDataConfig : IDataConfig {
        private readonly string _fullPathFormat;

        /// <summary>
        /// Gets supported by app local DB RELEASE version.
        /// </summary>
        public int SupportedReleaseVersion { get; private set; }

        /// <summary>
        /// Gets supported by app local DB SCHEMA version.
        /// </summary>
        public int SupportedSchemaVersion { get; private set; }

        /// <summary>
        /// Gets just name of file of DB.
        /// </summary>
        public string LocalDbFileName { get; private set; }

        /// <summary>
        /// Gets full path to DB in isolated storage.
        /// </summary>
        public string LocalDbFullPath { get; private set; }

        /// <summary>
        /// <paramref name="languageName"/> must match Name field of Languages table in DB (BY DESIGN).
        /// </summary>
        public string GetLocalWordsFileName(string languageName) {
            return "{0}.dic".FormatWith(languageName);
        }

        /// <summary>
        /// <paramref name="languageName"/> must match Name field of Languages table in DB (BY DESIGN).
        /// </summary>
        public string GetLocalWordsFileFullPath(string languageName) {
            return _fullPathFormat.FormatWith(GetLocalWordsFileName(languageName));
        }

        /// <summary>
        ///  Gets full path to specified file in isolated storage.
        /// </summary>
        public string GetLocalFileFullPath(string fileName) {
            return _fullPathFormat.FormatWith(fileName);
        }

        public WpfDataConfig() {
            _fullPathFormat = AppDomain.CurrentDomain.BaseDirectory + "..\\..\\..\\BSC.WordLines.App\\{0}";

            LocalDbFileName = "data.db";
            LocalDbFullPath = _fullPathFormat.FormatWith(LocalDbFileName);
            SupportedReleaseVersion = 6;
            SupportedSchemaVersion = 3;
        }
    }
}