﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using BSC.WordLines.Data;
using BSC.WordLines.Model;
using BSC.WordLines.Presentation;
using BSC.WordLines.Presentation.ViewModels;
using BSC.WordLines.Presentation.Views;
using BSC.WordLines.Utils;
using Caliburn.Micro;

namespace BSC.WordLines.App {
    public class AppBootstrapper : PhoneBootstrapper {
        private const int BOARD_COLUMNS_COUNT = 5;
        private const int BOARD_ROW_COUNT = 5;
        private PhoneContainer _container;
        private string _errorMessageCaption;
        private string _unhandledExceptionMessage;

        protected override void Configure() {
            _container = new PhoneContainer();

            if (!Execute.InDesignMode) {
                _container.RegisterPhoneServices(RootFrame);
            }

            _container.Singleton<IDiagnost, WinPhoneDiagnost>();
            _container.Singleton<IGame, Game>();
            _container.Singleton<IBoard, Board>();
            _container.Singleton<IPlayer, Player>();
            _container.Handler<ModelFactory.GetBoardSize>(
                ioc => new ModelFactory.GetBoardSize(
                           (out int columnsCount, out int rowsCount) => {
                               columnsCount = BOARD_COLUMNS_COUNT;
                               rowsCount = BOARD_ROW_COUNT;
                           }));
            _container.PerRequest<MainMenuPageViewModel>();
            _container.PerRequest<LeaderboardsPageViewModel>();
            _container.PerRequest<GamePageViewModel>();
            _container.PerRequest<GameOverPageViewModel>();
            _container.PerRequest<SettingsPageViewModel>();
            _container.PerRequest<MiscPageViewModel>();
            _container.PerRequest<TutorialPageViewModel>();
            _container.PerRequest<RulesPageViewModel>();
            _container.PerRequest<BoostersPageViewModel>();
            _container.PerRequest<WaitingPageViewModel>();
            _container.PerRequest<StorePageViewModel>();
            _container.PerRequest<FreePointsPageViewModel>();
            _container.PerRequest<FreePointsPromotionPageViewModel>();
            _container.PerRequest<GameViewModel>();
            _container.PerRequest<BoardViewModel>();
            _container.PerRequest<BoostersViewModel>();
            _container.PerRequest<LeaderboardListViewModel>();
            _container.PerRequest<LeaderboardViewModel>();
            _container.PerRequest<RoundResultsViewModel>();
            _container.PerRequest<ButtonViewModel>();
            _container.PerRequest<UserMessageViewModel>();
            _container.PerRequest<ErrorPageViewModel>();
            _container.Handler<IGameViewModel>(ioc => ioc.GetInstance(typeof(GameViewModel), null));
            _container.Handler<IBoardViewModel>(ioc => ioc.GetInstance(typeof(BoardViewModel), null));

            _container.Handler<ViewFactory.CreateBoardView>(
                ioc => new ViewFactory.CreateBoardView(
                           (boardViewModel, canvas) =>
                           new BoardView(boardViewModel, canvas, ioc.Get<ViewFactory.CreateBallView>(),
                                         ioc.ResolveEventAggregator())));
            _container.Handler<ViewFactory.CreateBallView>(
                ioc => new ViewFactory.CreateBallView(
                           (ballViewModel, canvas) =>
                           new BallView(ballViewModel, canvas, ioc.ResolveEventAggregator())));
            _container.Handler<ViewModelFactory.CreateBall>(
                ioc => new ViewModelFactory.CreateBall(ball => new BallViewModel(
                                                                   ball,
                                                                   ioc.ResolveEventAggregator(),
                                                                   ioc.Get<IGame>(),
                                                                   ioc.Get<AbstractResourceConverter>(),
                                                                   ioc.Get<ISoundsManager>(),
                                                                   ioc.Get<INavigationService>())));
            _container.Handler<ViewModelFactory.CreateCell>(
                ioc => new ViewModelFactory.CreateCell(cell => new CellViewModel(cell)));
            _container.Handler<ModelFactory.CreateBall>(
                ioc => new ModelFactory.CreateBall((ballType, letter) => new Ball(ballType, letter)));
            _container.Handler<ModelFactory.CreateCell>(
                ioc => new ModelFactory.CreateCell((column, row) => new Cell(column, row)));
            _container.Handler<ViewModelFactory.CreateButton>(
                ioc => new ViewModelFactory.CreateButton(
                           autoResetSelection => new ButtonViewModel(
                                                     autoResetSelection,
                                                     ioc.Get<ILocalizedStringCache>(),
                                                     ioc.Get<AbstractResourceConverter>(),
                                                     ioc.Get<ISoundsManager>())));
            _container.Handler<ViewModelFactory.CreateUserMessage>(
                ioc => new ViewModelFactory.CreateUserMessage(
                           () => new UserMessageViewModel(
                                     ioc.Get<ViewModelFactory.CreateButton>(),
                                     ioc.Get<ISoundsManager>())));
            _container.Handler<ViewModelFactory.CreateProduct>(
                ioc => new ViewModelFactory.CreateProduct(
                           (productListing, storePage) => new StoreProductViewModel(
                                                              productListing,
                                                              storePage,
                                                              ioc.Get<ViewModelFactory.CreateButton>(),
                                                              ioc.Get<IStoreManager>(),
                                                              ioc.Get<ILocalizedStringCache>())));
            _container.Handler<LeaderboardViewModel.Factory>(
                ioc => new LeaderboardViewModel.Factory(
                           leaderboard => new LeaderboardViewModel(
                                              leaderboard,
                                              ioc.Get<ILeaderboardManager>(),
                                              ioc.Get<ViewModelFactory.CreateButton>(),
                                              ioc.Get<ILocalizedStringCache>(),
                                              ioc.Get<IPlayer>(),
                                              ioc.Get<SettingsManager>()
                                              )));

            _container.PerRequest<AbstractResourceConverter>();
            _container.Singleton<SettingsManager>();
            _container.Singleton<IStoreManager, StoreManager>();
            _container.Singleton<ILocalizedStringCache, LocalizedStringCache>();
            _container.Singleton<IAlphabet, Alphabet>();
            _container.Singleton<IWordDictionary, WordDictionary>();
            _container.Singleton<IDataConfig, WP8DataConfig>();
            _container.Singleton<ILocalDataProvider, SqliteLocalDataProvider>();
            _container.Singleton<IWordsProvider, FileWordsProvider>();
            _container.Singleton<IServerDataProvider, ParseServerDataProvider>();
            _container.Singleton<ILeaderboardManager, LeaderboardManager>();
            _container.Singleton<IPlatformDataHelper, PhoneSQLitePlatformDataHelper>();
            _container.Singleton<ISoundsManager, SoundsManager>();
            _container.Singleton<IGameStateSaver, FileGameStateSaver>();
            _container.Singleton<GameModeDataMapper>();
            _container.Singleton<HighScorePeriodDataMapper>();
            _container.Singleton<LanguageDataMapper>();

            AddCustomConventions();
        }

        protected override object GetInstance(Type service, string key) {
            var instance = _container.GetInstance(service, key);
            if (instance != null) {
                return instance;
            }
            throw new Exception("Could not locate any instances.");
        }

        protected override IEnumerable<object> GetAllInstances(Type service) {
            return _container.GetAllInstances(service);
        }

        protected override void BuildUp(object instance) {
            _container.BuildUp(instance);
        }

        protected override IEnumerable<Assembly> SelectAssemblies() {
            var assemblies = base.SelectAssemblies().ToList();
            assemblies.Add(typeof(GameViewModel).Assembly); //presentation assembly
            return assemblies;
        }

        protected override PhoneApplicationFrame CreatePhoneApplicationFrame() {
            CurrentRootFrame = new TransitionFrame();
            return CurrentRootFrame;
        }

        public static PhoneApplicationFrame CurrentRootFrame { get; private set; }

        protected async override void OnLaunch(object sender, LaunchingEventArgs e) {
            //Assign unlocalized fail messages before data ensured.
            _unhandledExceptionMessage = "oops... some problems with data occured, try reinstall app please :( " +
                                         "if problem persists - email us: " + GameConst.GameSupportMail + " we try to help.";
            _errorMessageCaption = "error";

            //Deploy database if need.
            DeploymentHelper.DeployLocalDatabaseIfNeed();

            //Pre-loads.
            IoC.Get<SettingsManager>().UpdateFromDb();
            AbstractResourceConverter.UpdateCache();
            IoC.Get<ILocalizedStringCache>().Update();
            IoC.Get<ISoundsManager>().Update();
            await IoC.Get<IWordDictionary>().UpdateAsync();

            //Assign localized fail messages.
            var stringCache = IoC.Get<ILocalizedStringCache>();
            _unhandledExceptionMessage = stringCache.GetLocalizedString("UserMessage_UnhandledException");
            Diagnost.Assert(!string.IsNullOrWhiteSpace(_unhandledExceptionMessage),
                            ErrorMessage.UnhandledExceptionUserMessageEmpty);
            _errorMessageCaption = stringCache.GetLocalizedString("UserMessage_ErrorMessageCaption");
            Diagnost.Assert(!string.IsNullOrWhiteSpace(_errorMessageCaption),
                            ErrorMessage.UnhandledExceptionUserMessageEmpty);

            //Adduplex install tracking
            try {
                AdDuplex.AdDuplexTrackingSDK.StartTracking("134543");
            }
            catch (Exception) {
                /*DO NOTHING*/
            }

            //Set transitions background.
            System.Action updateRootFrameBackground =
                () => {
                    RootFrame.Background = IoC.Get<AbstractResourceConverter>().Convert<Brush>("brush_PageBackground");
                };
            updateRootFrameBackground();
            AbstractResourceConverter.CacheUpdated += (s, args) => updateRootFrameBackground();

            //Fulfillment in-apps if need.
            MainMenuPageViewModel.PurchasedMoneyNotifying = await FulfillmentIfNeed();

            //TODO facebook future
            //RootFrame.UriMapper = new FacebookUriMapper();

            //Goto Start page.
            try {
                var navigator = IoC.Get<INavigationService>();
                navigator.UriFor<MainMenuPageViewModel>().Navigate();
            }
            catch (Exception ex) {
                MainMenuPageViewModel.NavigationTroubleOccurred = true;//Issue #73
                Diagnost.LogHandledException(ex);
            }
        }

        protected override void OnUnhandledException(object sender, ApplicationUnhandledExceptionEventArgs e) {
            e.Handled = true;
            Diagnost.LogUnhandledException(e.ExceptionObject);

            MessageBox.Show(_unhandledExceptionMessage, _errorMessageCaption, MessageBoxButton.OK);

            UIHelp.GoBackIfWainting();
        }

        private async Task<int> FulfillmentIfNeed() {
            int purchasedMoney = 0;
            try {
                purchasedMoney = await IoC.Get<IStoreManager>().EnsureFulfillmentAsync();
            }
            catch (Exception) {
                /*DO NOTHING*/
            }
            return purchasedMoney;
        }

        private static void AddCustomConventions() {
            ConventionManager.AddElementConvention<Pivot>(Pivot.ItemsSourceProperty, "SelectedItem", "SelectionChanged")
                .ApplyBinding =
                (viewModelType, path, property, element, convention) => {
                    if (ConventionManager
                        .GetElementConvention(typeof(ItemsControl))
                        .ApplyBinding(viewModelType, path, property, element, convention)) {
                        ConventionManager
                            .ConfigureSelectedItem(element, Pivot.SelectedItemProperty, viewModelType, path);
                        ConventionManager
                            .ApplyHeaderTemplate(element, Pivot.HeaderTemplateProperty, null, viewModelType);
                        return true;
                    }

                    return false;
                };

            ConventionManager.AddElementConvention<Panorama>(Panorama.ItemsSourceProperty, "SelectedItem",
                                                             "SelectionChanged").ApplyBinding =
                (viewModelType, path, property, element, convention) => {
                    if (ConventionManager
                        .GetElementConvention(typeof(ItemsControl))
                        .ApplyBinding(viewModelType, path, property, element, convention)) {
                        ConventionManager
                            .ConfigureSelectedItem(element, Panorama.SelectedItemProperty, viewModelType, path);
                        ConventionManager
                            .ApplyHeaderTemplate(element, Panorama.HeaderTemplateProperty, null, viewModelType);
                        return true;
                    }

                    return false;
                };
        }
    }
}