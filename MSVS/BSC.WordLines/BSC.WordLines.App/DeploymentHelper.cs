﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using BSC.WordLines.Data;
using BSC.WordLines.Data.SqliteNet.ORM;
using System.IO;
using System.IO.IsolatedStorage;
using System.Windows;
using BSC.WordLines.Utils;
using Caliburn.Micro;
using Microsoft.Phone.Info;
using Windows.Phone.System.Analytics;
using SQLite;

namespace BSC.WordLines.App {
    public static class DeploymentHelper {
        private const string TEMP_FILE_PREFIX = "temp_";

        //WARNING before this method ends - app must not instantiate ILocalDataProvider, IServerDataProvider or any entities that depends from it!
        public static void DeployLocalDatabaseIfNeed() {

            //WARNING! Current Release version = 3 and Schema version = 1
            var dataConfig = IoC.Get<IDataConfig>();
            string dbFileName = dataConfig.LocalDbFileName;
            string dbFullPath = dataConfig.LocalDbFullPath;

            using (var storage = IsolatedStorageFile.GetUserStoreForApplication()) {
                if (storage.FileExists(dbFileName)) {
                    var currentDbVersions = GetDbVersions(dbFullPath);
                    //Verify db versions (and upgrade DB if need)
                    if (currentDbVersions.ReleaseVersion == dataConfig.SupportedReleaseVersion) {
                        EnsureDbSchemaSupported(dataConfig.SupportedSchemaVersion, currentDbVersions.SchemaVersion);
                        EnsureUserNameAndPasswordExists(dbFullPath);
                        return;
                    }

                    MigrateWithSchema1Or2();
                }
                else {
                    //Deploy new db file from installation.
                    //Order is important, because username setup can be failed.
                    CopyDbToStorage(dbFileName, dbFileName, storage);
                    SetupDefaultLanguage(dbFullPath);
                    SetupDefaultEnvironment(dbFullPath);
                    SetupUsernameAndPassword(dbFullPath);    
                }
            }

            //Verify deployed DB versions.
            var newDbVersions = GetDbVersions(dbFullPath);
            EnsureDbReleaseSupported(dataConfig.SupportedReleaseVersion, newDbVersions.ReleaseVersion);
            EnsureDbSchemaSupported(dataConfig.SupportedSchemaVersion, newDbVersions.SchemaVersion);
        }

        private static void CopyDbToStorage(string installationFileName, string storageFileName, IsolatedStorageFile storage) {
            //Deploy db from install folder to local folder (to isolated storage).
            using (Stream input = Application.GetResourceStream(new Uri(installationFileName, UriKind.Relative)).Stream) {
                using (IsolatedStorageFileStream output = storage.CreateFile(storageFileName)) {
                    byte[] readBuffer = new byte[4096];
                    int bytesRead = -1;
                    while ((bytesRead = input.Read(readBuffer, 0, readBuffer.Length)) > 0) {
                        output.Write(readBuffer, 0, bytesRead);
                    }
                }
            }
        }

        private static void MigrateWithSchema1Or2() {
            var dataConfig = IoC.Get<IDataConfig>();
            string dbFileName = dataConfig.LocalDbFileName;
            string dbFullPath = dataConfig.LocalDbFullPath;//used for SQLite.NET
            string tempDbFileName = TEMP_FILE_PREFIX + dbFileName;
            string tempDbFullPath = dataConfig.GetLocalFileFullPath(tempDbFileName);//used for SQLite.NET

            using (var storage = IsolatedStorageFile.GetUserStoreForApplication()) {
                if (storage.FileExists(tempDbFileName)) {
                    storage.DeleteFile(tempDbFileName);
                }
                CopyDbToStorage(dbFileName, tempDbFileName, storage);

                Players oldPlayer;
                Settings oldSettings;
                List<LocalHighScores> oldLocalHighScores;
                List<GlobalHighScores> oldGlobalHighScores;
                List<FacebookHighScores> oldFacebookHighScores;
                List<Themes> oldThemes;
                List<DynamicSettings> oldDynamicSettings;
                using (var oldDb  = OpenDbConnection(dbFullPath)) {
                    var oldVersions = oldDb.Table<Versions>().First();
                    
                    oldPlayer = oldDb.Table<Players>().First();
                    oldSettings = oldDb.Table<Settings>().First();
                    oldLocalHighScores = oldDb.Table<LocalHighScores>().ToList();
                    oldGlobalHighScores = oldDb.Table<GlobalHighScores>().ToList();
                    oldFacebookHighScores = oldVersions.SchemaVersion >= 3 ? oldDb.Table<FacebookHighScores>().ToList() : null;
                    oldThemes = oldDb.Table<Themes>().ToList();
                    oldDynamicSettings = oldVersions.SchemaVersion >= 2
                                             ? oldDb.Table<DynamicSettings>().ToList()
                                             : new List<DynamicSettings> {
                                                 new DynamicSettings {Id = 1, Key = "IsFreePointsFeatureNotified", Value = "False"}
                                             };
                }
                using (var newDb = OpenDbConnection(tempDbFullPath)) {
                    //Migrate Player settings
                    var newPlayer = newDb.Table<Players>().First();
                    newPlayer.Name = oldPlayer.Name;
                    newPlayer.Username = oldPlayer.Username;
                    newPlayer.Password = oldPlayer.Password;
                    newPlayer.Money = oldPlayer.Money;
                    newPlayer.KillBallCount = oldPlayer.KillBallCount;
                    newPlayer.WildcardCount = oldPlayer.WildcardCount;
                    newPlayer.AdditionCount = oldPlayer.AdditionCount;
                    newPlayer.IsPurchaseNotFinished = oldPlayer.IsPurchaseNotFinished;
                    newPlayer.IsRenamingNotFinished = oldPlayer.IsRenamingNotFinished;
                    newPlayer.IsSigned = oldPlayer.IsSigned;
                    newDb.Update(newPlayer);

                    //Migrate Settings
                    var newSettings = newDb.Table<Settings>().First();
                    newSettings.CurrentEnvironmentId = oldSettings.CurrentEnvironmentId;
                    newSettings.CurrentLanguageId = oldSettings.CurrentLanguageId;
                    newSettings.CurrentPlayerId = oldSettings.CurrentPlayerId;
                    newSettings.CurrentThemeId = oldSettings.CurrentThemeId;
                    newSettings.IdleReviewOffersCount = oldSettings.IdleReviewOffersCount;
                    newSettings.IsAppReviewed = oldSettings.IsAppReviewed;
                    newSettings.IsFirstLaunch = oldSettings.IsFirstLaunch;
                    newSettings.IsMusicEnabled = oldSettings.IsMusicEnabled;
                    newSettings.IsSoundsEnabled = oldSettings.IsSoundsEnabled;
                    newSettings.IsTutorialConfirmed = oldSettings.IsTutorialConfirmed;

                    //Discover new theme and set as current for presentation to all 
                    //WARNING!!! add new themes only to end of Themes table!!!
                    List<Themes> newThemes = newDb.Table<Themes>().ToList();
                    int maxOldThemeId = oldThemes.Max(theme => theme.Id);
                    int maxNewThemeId = newThemes.Max(theme => theme.Id);
                    if (maxNewThemeId > maxOldThemeId) {
                        newSettings.CurrentThemeId = maxNewThemeId;
                    }
                    newDb.Update(newSettings);

                    //Migrate HighScores
                    newDb.InsertAll(oldLocalHighScores);
                    newDb.InsertAll(oldGlobalHighScores);
                    if (oldFacebookHighScores != null) {
                        newDb.InsertAll(oldFacebookHighScores);
                    }

                    //Migrate DynamicSettings
                    var newDynamicSettings = newDb.Table<DynamicSettings>().ToList();
                    var newDynamicSettingsToUpdate = new List<DynamicSettings>();
                    foreach (var oldDynamic in oldDynamicSettings) {
                        var newDynamic = newDynamicSettings.FirstOrDefault(s => s.Key == oldDynamic.Key);
                        if (newDynamic != null) {
                            newDynamic.Value = oldDynamic.Value;
                            newDynamicSettingsToUpdate.Add(newDynamic);
                        }
                    }
                    foreach (var newDynamic in newDynamicSettingsToUpdate) {
                        newDb.Update(newDynamic);
                    }
                }

                storage.DeleteFile(dbFileName);
                storage.MoveFile(tempDbFileName, dbFileName);
            }
        }

        /// <summary>
        /// Try fill empty username if it is.
        /// </summary>
        private static void EnsureUserNameAndPasswordExists(string dbFullPath) {
            using (var db = OpenDbConnection(dbFullPath)) {
                var player = db.Table<Players>().First();
                if (string.IsNullOrWhiteSpace(player.Username)) {
                    string fixedUsername;
                    string fixedPassword;
                    GenerateUserNamePassword(out fixedUsername, out fixedPassword);
                    player.Username = fixedUsername;
                    player.Password = fixedPassword;
                    db.Update(player);
                }
            }
        }

        private static void GenerateUserNamePassword(out string username, out string password) {
            //Generate Username
            string newUsername = null;
#if DEBUG
            newUsername = "debugUser123";
#else
            //Primary user indentification
            try {
                object anid2;
                if (UserExtendedProperties.TryGetValue("ANID2", out anid2)) {
                    newUsername = anid2.ToString();
                }
            }
            catch (Exception) {
                newUsername = null;
            }

            //Alternative user identification (for logout devices)
            try {
                if (string.IsNullOrWhiteSpace(newUsername)) {
                    byte[] deviceId = (byte[])DeviceExtendedProperties.GetValue("DeviceUniqueId");
                    newUsername = Convert.ToBase64String(deviceId);
                }
            }
            catch (Exception) {
                newUsername = null;
            }

#endif
            //!!!Extreme measure!!!
            if (string.IsNullOrWhiteSpace(newUsername)) {
                newUsername = Guid.NewGuid().ToString();
            }

            //Generate simple Password
            var usernameChars = newUsername.ToCharArray().ToList();
            usernameChars.Sort();
            var passwordBuilder = new StringBuilder();
            passwordBuilder.Append(usernameChars.ToArray());

            password = passwordBuilder.ToString();
            username = newUsername;
        }

        private static void SetupUsernameAndPassword(string dbFullPath) {
            string username;
            string password;
            GenerateUserNamePassword(out username, out password);
            
            //Setup Player account requisites.
            using (var db = OpenDbConnection(dbFullPath)) {
                var player = db.Table<Players>().First();
                player.Username = username;
                player.Password = password;
                db.Update(player);
            }
        }

        private static void SetupDefaultLanguage(string dbFullPath) {
            var currentCulture = CultureInfo.CurrentCulture;
            if (currentCulture.AssertNotNullFailed("currentCulture")) {
                return;
            }
            string currentLcid = currentCulture.Name;
            
            using (var db = OpenDbConnection(dbFullPath)) {
                var lcid = db.Query<SupportedLcids>(
                    "select * from SupportedLcids where Lcid = ?", currentLcid)
                    .FirstOrDefault();
                if (lcid.AssertNotNullFailed("lcid")) {
                    return;
                }
                var lcidLanguage = db.Find<Languages>(lcid.LanguageId);
                if (lcidLanguage.AssertNotNullFailed("language")) {
                    return;
                }

                var settings = db.Table<Settings>().First();
                settings.CurrentLanguageId = lcid.LanguageId;
                db.Update(settings);
            }
        }

        private static void SetupDefaultEnvironment(string dbFullPath) {
            string environmentKey = null;
            switch (ResolutionHelper.CurrentResolution) {
                case ResolutionHelper.Resolutions.WVGA:
                    environmentKey = "WP8_WVGA";
                    break;
                case ResolutionHelper.Resolutions.WXGA:
                    environmentKey = "WP8_WXGA";
                    break;
                case ResolutionHelper.Resolutions.HD:
                    environmentKey = "WP8_HD";
                    break;
                default:
                    return;
            }

            using (var db = OpenDbConnection(dbFullPath)) {
                var environment = db.Query<Environments>(
                    "select * from Environments where Key = ?", environmentKey)
                    .FirstOrDefault();

                if (environment.AssertNotNullFailed("environment")) {
                    return;
                }

                var settings = db.Table<Settings>().First();
                settings.CurrentEnvironmentId = environment.Id;
                db.Update(settings);
            }
        }

        private static void EnsureDbSchemaSupported(int supportedVersion, int deployedVersion) {
            if (deployedVersion != supportedVersion) {
                throw new InvalidOperationException(
                    ErrorMessage.DbSchemaVersionUnsupported(supportedVersion, deployedVersion));
            }
        }

        private static void EnsureDbReleaseSupported(int supportedVersion, int deployedVersion) {
            if (deployedVersion != supportedVersion) {
                throw new InvalidOperationException(
                    ErrorMessage.DbReleaseVersionUnsupported(supportedVersion, deployedVersion));
            }
        }

        private static IVersions GetDbVersions(string fullPath) {
            using (var db = OpenDbConnection(fullPath)) {
                return db.Table<Versions>().First();
            }
        }

        private static SQLiteConnection OpenDbConnection(string fullPath) {
            return new SQLiteConnection(fullPath, SQLiteOpenFlags.ReadWrite, DataConstants.SqliteDateTimeAsTicks);
        }
    }
}
