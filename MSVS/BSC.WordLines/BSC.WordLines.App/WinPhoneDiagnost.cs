using System;
using System.Diagnostics;
using System.IO.IsolatedStorage;
using System.Threading.Tasks;
using BSC.WordLines.Data;
using BSC.WordLines.Data.SqliteNet.ORM;
using BSC.WordLines.Utils;
using Caliburn.Micro;
using Parse;
using SQLite;

namespace BSC.WordLines.App {
    public class WinPhoneDiagnost : IDiagnost {
        
        //Parse schema for Log class:
        private const string LOG_CLASS = "Log";
        private const string USER_FIELD = "User";
        private const string TECH_INFO_FIELD = "TechnicalInfo";
        private const string MESSAGE_FIELD = "Message";
        private const string LOG_TYPE_FIELD = "Type";
        private const string TIMESTAMP_FIELD = "Timestamp";

        private readonly IPlatformDataHelper _platformDataHelper;
        private readonly IDataConfig _dataConfig;

        public WinPhoneDiagnost(IPlatformDataHelper platformDataHelper, IDataConfig dataConfig) {
            platformDataHelper.EnsureNotNull("platformDataHelper");
            dataConfig.EnsureNotNull("dataConfig");
            _platformDataHelper = platformDataHelper;
            _dataConfig = dataConfig;
        }

        public void Assert(bool condition, string message) {
            if (Execute.InDesignMode) {
                return;
            }
#if !DEBUG
            //Not used for now.
            //SaveLogAsync("Assert: " + message, ELogType.Error);
#else
            Debug.Assert(condition, message ?? string.Empty);
#endif
        }

        public void LogHandledException(Exception exception) {
            SaveLogAsync(ErrorMessage.HandledException(exception), ELogType.Error);
        }

        public void LogUnhandledException(Exception exception) {
            SaveLogAsync(ErrorMessage.UnhandledException(exception), ELogType.Error);
        }

        public async Task SaveLogAsync(string message, ELogType logType) {
            var logObject = new ParseObject(LOG_CLASS);
            var user = ParseUser.CurrentUser;
            if (user != null) {
                logObject[USER_FIELD] = user;
            }
            string username = GetUsername() ?? "<null>";
            logObject[TECH_INFO_FIELD] = string.Format(
                "{0} {1} {2}", username, _platformDataHelper.GetSystemVersion(), _platformDataHelper.GetAppVersion());
            
            logObject[MESSAGE_FIELD] = message;
            logObject[TIMESTAMP_FIELD] = DateTime.UtcNow;
            logObject[LOG_TYPE_FIELD] = logType.ToString();
            
            try {
                await logObject.SaveAsync();
            }
            catch (Exception ex) {
                Diagnost.Assert(false, ErrorMessage.ExceptionOccured(ex));
            }
        }

        private string GetUsername() {
            try {
                string dbFullPath = _dataConfig.LocalDbFullPath;
                using (var storage = IsolatedStorageFile.GetUserStoreForApplication()) {
                    if (!storage.FileExists(dbFullPath)) {
                        return ErrorMessage.UsernameGetNoDatabase;
                    }
                }
                using (var db = OpenDbConnection(dbFullPath)) {
                    return db.Table<Players>().First().Username;
                }
            }
            catch (Exception) {
                return ErrorMessage.UsernameGetException;
            }
        }

        private static SQLiteConnection OpenDbConnection(string fullPath) {
            return new SQLiteConnection(fullPath, SQLiteOpenFlags.ReadWrite, DataConstants.SqliteDateTimeAsTicks);
        }
    }
}