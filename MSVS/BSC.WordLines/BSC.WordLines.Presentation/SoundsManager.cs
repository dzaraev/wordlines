﻿using System;
using System.Windows;
using BSC.WordLines.Data;
using BSC.WordLines.Utils;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;

namespace BSC.WordLines.Presentation {
    public class SoundsManager : ISoundsManager {
        private readonly SettingsManager _settingsManager;
        private readonly AbstractResourceConverter _resourceConverter;
        private SoundEffect _gameOverEffect;
        private SoundEffect _clickEffect;
        private SoundEffect _shortChainKillEffect;
        private SoundEffect _mediumChainKillEffect;
        private SoundEffect _longChainKillEffect;
        private SoundEffect _killBallEffect;
        private SoundEffect _wildcardOnEffect;
        private SoundEffect _wildcardOffEffect;
        private SoundEffect _additionEffect;
        private SoundEffect _metronomeEffect;
        private SoundEffectInstance _lastPlayedEffect;

        public SoundsManager(AbstractResourceConverter resourceConverter, SettingsManager settingsManager) {
            resourceConverter.EnsureNotNull("resourceConverter");
            settingsManager.EnsureNotNull("settingsManager");
            _resourceConverter = resourceConverter;
            _settingsManager = settingsManager;
        }

        public void Update() {
            _gameOverEffect = CreateEffect("uri_GameOverSound");
            _clickEffect = CreateEffect("uri_ButtonClickSound");
            _shortChainKillEffect = CreateEffect("uri_ShortChainKillSound");
            _mediumChainKillEffect = CreateEffect("uri_MediumChainKillSound");
            _longChainKillEffect = CreateEffect("uri_LongChainKillSound");
            _killBallEffect = CreateEffect("uri_KillBallSound");
            _wildcardOnEffect = CreateEffect("uri_WildcardOnSound");
            _wildcardOffEffect = CreateEffect("uri_WildcardOffSound");
            _additionEffect = CreateEffect("uri_AdditionSound");
            _metronomeEffect = CreateEffect("uri_MetronomeSound");
        }

        public void PlayClick() {
            PlayEffect(_clickEffect);
        }

        public void PlayKillBall() {
            PlayEffect(_killBallEffect);
        }

        public void PlayWildcardOn() {
            PlayEffect(_wildcardOnEffect);
        }

        public void PlayWildcardOff() {
            PlayEffect(_wildcardOffEffect);
        }

        public void PlayAddition() {
            PlayEffect(_additionEffect);
        }

        public void PlayShortChainKill() {
            PlayEffect(_shortChainKillEffect);
        }

        public void PlayMediumChainKill() {
            PlayEffect(_mediumChainKillEffect);
        }

        public void PlayLongChainKill() {
            PlayEffect(_longChainKillEffect);
        }

        public void PlayMetronome() {
            PlayEffect(_metronomeEffect);
        }

        public void PlayGameOver() {
            PlayEffect(_gameOverEffect);
        }

        private void PlayEffect(SoundEffect soundEffect, float? specialVolume = null) {
            if (soundEffect.AssertNotNullFailed("soundEffect")) {
                return;
            }

            if (!_settingsManager.CurrentSettings.IsSoundsEnabled) {
                return;
            }

            _lastPlayedEffect = soundEffect.CreateInstance();
            
            try {
                if (specialVolume.HasValue) {
                    _lastPlayedEffect.Volume = specialVolume.Value;
                }
                _lastPlayedEffect.Play();
            }
            catch { /*DO NOTHING, JUST FEAR OF XNA UNEXPECTED FAIL*/ }
            FrameworkDispatcher.Update();
        }

        private SoundEffect CreateEffect(string resourceKey) {
            try {
                Uri resourceUri = _resourceConverter.Convert<Uri>(resourceKey);
                Diagnost.Assert(resourceUri != null, ErrorMessage.AbstractResourceValueIsNull);
                if (resourceKey == null) {
                    return null;
                }
                var resourceStreamInfo = Application.GetResourceStream(resourceUri);
                Diagnost.Assert(resourceStreamInfo != null, ErrorMessage.ArgumentOrVarCannotBeNull("resourceStreamInfo"));
                if (resourceStreamInfo == null) {
                    return null;
                }
                //TODO здесь иногда падает хер знает почему, возможно стоит предпринимать вторую попытку, чтобы юзер не остался без звуков
                return SoundEffect.FromStream(resourceStreamInfo.Stream);
            }
            catch (Exception) {
                return null;
            }
        }
    }
}