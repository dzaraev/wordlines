﻿using BSC.WordLines.Data;

namespace BSC.WordLines.Presentation {
    public static class UIConst {
        public static readonly int BallZIndex = 0;
        public static readonly int ChainZIndex = -1;
        public static readonly EGameMode DefaultLeaderboardPageGameMode = EGameMode.Moves;
    }
}