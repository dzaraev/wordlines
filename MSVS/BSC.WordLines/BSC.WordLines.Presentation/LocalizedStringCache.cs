﻿using System.Collections.Generic;
using BSC.WordLines.Data;
using BSC.WordLines.Utils;

namespace BSC.WordLines.Presentation {
    public class LocalizedStringCache : ILocalizedStringCache {
        private readonly ILocalDataProvider _provider;
        private readonly Dictionary<string, string> _cache;

        public LocalizedStringCache(ILocalDataProvider provider) {
            provider.EnsureNotNull("provider");
            _provider = provider;
            _cache = new Dictionary<string, string>();
        }

        public string GetLocalizedString(string localizedStringKey) {
            string localizedString;
            bool concreteStringFound = _cache.TryGetValue(localizedStringKey, out localizedString);
            if (Help.AssertFailed(concreteStringFound, ErrorMessage.CachedStringNotFound(localizedStringKey))) {
                return string.Empty;
            }
            return localizedString;
        }

        public void Update() {
            _cache.Clear();
            foreach (var keyedConcreteString in _provider.GetActualConcreteStringsKeyed()) {
                _cache.Add(keyedConcreteString.Key, keyedConcreteString.Value);
            }
        }
    }
}