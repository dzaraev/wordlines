﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using BSC.WordLines.Utils;
using Caliburn.Micro;

namespace BSC.WordLines.Presentation {
    /// <summary>
    /// Resource to UI connection service.
    /// </summary>
    public static class Resources {
        private const char ASSIGNMENTS_SEPARATOR = ',';
        private const char EQUALITY_SYMBOL = '=';
        private static readonly Dictionary<string, object> _cache;
        private static readonly AbstractResourceConverter _converter;

        static Resources() {
            _cache = new Dictionary<string, object>();

            if (!Execute.InDesignMode) {
                _converter = IoC.Get<AbstractResourceConverter>();
                if (_converter == null) {
                    throw new InvalidOperationException(ErrorMessage.CannotResolveIoC<AbstractResourceConverter>());
                }
            }
            else {
                _converter = null;
            }

            AbstractResourceConverter.CacheUpdated += HandleAbstractResourceConverterCacheUpdated;
        }

        /// <summary>
        /// Must be a string in format: "%PropertyName%=%AbstractResourceKey%[, ...]".
        /// Assigned properties must exist on target and have available setter.
        /// </summary>
        public static readonly DependencyProperty AssignProperty =
            DependencyProperty.RegisterAttached(
                "Assign",
                typeof (string),
                typeof (Resources),
                new PropertyMetadata(null, HandleAssignChanged));

        public static string GetAssign(DependencyObject element) {
            return (string)element.GetValue(AssignProperty);
        }
        public static void SetAssign(DependencyObject element, string value) {
            element.SetValue(AssignProperty, value);
        }

        public static void ReAssignDeep(DependencyObject target) {
            target.EnsureNotNull("target");
            string targetAssignment = GetAssign(target);
            if (targetAssignment != null) {
                AssignResources(target, targetAssignment);
            }
            int childrenCount = VisualTreeHelper.GetChildrenCount(target);
            if (childrenCount > 0) {
                for (int i = 0; i < childrenCount; i++) {
                    var child = VisualTreeHelper.GetChild(target, i);
                    if (child != null) {
                        ReAssignDeep(child);
                    }
                }
            }
            else {
                var contentControl = target as ContentControl;
                var content = contentControl != null ? contentControl.Content as DependencyObject : null;
                if (content != null) {
                    ReAssignDeep(content);
                }
            }
        }

        private static void AssignResources(DependencyObject target, string source) {
            var assignmentsSources = source.Split(ASSIGNMENTS_SEPARATOR);
            bool assignmentsExists = assignmentsSources.Length > 0;
            Diagnost.Assert(assignmentsExists, ErrorMessage.IncorrectResourceAssigment);
            if (!assignmentsExists) {
                return;
            }

            foreach (var assignmentString in assignmentsSources) {
                var assignmentParts = assignmentString.Split(EQUALITY_SYMBOL);
                bool assignmentHasTwoParts = assignmentParts.Length == 2;
                Diagnost.Assert(assignmentHasTwoParts, ErrorMessage.IncorrectResourceAssigment);
                if (!assignmentHasTwoParts) {
                    continue;
                }
                string targetPropertyName = assignmentParts[0].Trim();
                string abstractResourceKey = assignmentParts[1].Trim();

                var targetProperty = target.GetType().GetProperty(targetPropertyName);
                Diagnost.Assert(targetProperty != null, ErrorMessage.ResourceAssignmentPropertyNotFound(targetPropertyName));
                if (targetProperty == null) {
                    continue;
                }

                object resourceValue;
                bool needCacheResource;
                if (_cache.ContainsKey(abstractResourceKey)) {
                    resourceValue = _cache[abstractResourceKey];
                    needCacheResource = false;
                }
                else if (_converter != null) {
                    resourceValue = _converter.Convert(abstractResourceKey);
                    needCacheResource = true;

                    //SPECIAL CASE! (cause i don't want to create special resource type for it).
                    if (targetProperty.PropertyType == typeof(GridLength) && resourceValue is double) {
                        resourceValue = new GridLength((double)resourceValue);
                    }
                }
                else {
                    continue;
                }

                Diagnost.Assert(resourceValue != null, ErrorMessage.AbstractResourceValueIsNull);
                if (resourceValue == null) {
                    continue;
                }

                try {
                    targetProperty.SetValue(target, resourceValue);
                }
                catch (Exception) {
                    Diagnost.Assert(false, ErrorMessage.ResourceAssignmentPropertySetValueException);
                    continue;
                }

                if (needCacheResource) {
                    _cache.Add(abstractResourceKey, resourceValue);
                }
            }
        }

        private static void HandleAssignChanged(DependencyObject target, DependencyPropertyChangedEventArgs args) {
            var newValue = args.NewValue;
            string source = newValue != null ? newValue.ToString() : string.Empty;
            AssignResources(target, source);
        }

        private static void HandleAbstractResourceConverterCacheUpdated(object sender, EventArgs e) {
            _cache.Clear();
        }
    }
}