﻿using System.Windows;
using System.Windows.Controls;

namespace BSC.WordLines.Presentation {
    public class ViewFactory {
        public delegate IBoardView CreateBoardView(IBoardViewModel boardViewModel, Canvas canvas);

        public delegate FrameworkElement CreateBallView(IBallViewModel ballViewModel, Canvas canvas);
    }
}