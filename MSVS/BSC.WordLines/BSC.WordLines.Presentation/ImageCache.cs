﻿using System.Collections.Generic;
using System.IO;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using BSC.WordLines.Data;
using BSC.WordLines.Utils;

namespace BSC.WordLines.Presentation {
    public class ImageCache {
        private readonly ILocalDataProvider _provider;
        private readonly Dictionary<string, ImageSource> _cache;

        public ImageCache(ILocalDataProvider provider) {
            _provider = provider;
            _cache = new Dictionary<string, ImageSource>();
        }

        public ImageSource GetImageByKey(string abstractResourceKey) {
            if (_cache.ContainsKey(abstractResourceKey)) {
                return _cache[abstractResourceKey];
            }
            else {
                var newImageResource = _provider.GetConcreteResource(abstractResourceKey);
                ImageSource newImage = null;
                if (newImageResource != null) {
                    newImage = CreateImageSource(newImageResource.Data);
                }
                Diagnost.Assert(newImage != null, ErrorMessage.ImageResourceCannotBeNull);
                _cache.Add(abstractResourceKey, newImage);
                return newImage;
            }
        }

        private ImageSource CreateImageSource(byte[] bytes) {
            if (bytes == null || bytes.Length == 0) {
                return null;
            }
            var bitmapImage = new BitmapImage();
            var stream = new MemoryStream(bytes);
            bitmapImage.SetSource(stream);
            return bitmapImage;
        }
    }
}