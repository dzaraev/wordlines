﻿using System;
using System.Globalization;
using System.Windows.Data;
using BSC.WordLines.Utils;

namespace BSC.WordLines.Presentation {
    public class MultiplierConverter : IValueConverter {
        public double Multiplier { get; set; }

        public MultiplierConverter() {
            Multiplier = 1;
        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            if (value == null) {
                return 0;
            }
            if (value is Double) {
                return Multiplier * System.Convert.ToDouble(value);
            }
            if (value is Int32) {
                return Multiplier * System.Convert.ToInt32(value);
            }
            if (value is Int64) {
                return Multiplier * System.Convert.ToInt64(value);
            }
            double doubleValue;
            if (value is string && double.TryParse((string)value, out doubleValue)) {
                return Multiplier * doubleValue;
            }
            throw new InvalidOperationException(ErrorMessage.ArgumentTypeUnsupported(
                value.GetType(), typeof (Double), typeof (Int32), typeof (Int64), typeof (string)));
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
            throw new NotSupportedException();
        }
    }
}