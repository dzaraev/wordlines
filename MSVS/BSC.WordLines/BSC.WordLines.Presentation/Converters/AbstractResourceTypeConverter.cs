﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace BSC.WordLines.Presentation {
    public class AbstractResourceTypeConverter : IValueConverter {
        private const string UNDEFINDED_TYPE = "<undefined>";
        private const string TYPE_SEPARATOR = "_";

        /// <summary>
        /// Converts Abstract Resource Key into 
        /// Abstract Reource Type by rules provided by system design.
        /// </summary>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            string stringValue = value as string;
            if (string.IsNullOrWhiteSpace(stringValue)) {
                return UNDEFINDED_TYPE;
            }
            int separatorIndex = stringValue.IndexOf(TYPE_SEPARATOR, StringComparison.Ordinal);
            if (separatorIndex > 0) {
                return stringValue.Substring(0, separatorIndex);
            }
            else {
                return UNDEFINDED_TYPE;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
            throw new NotSupportedException();
        }
    }
}