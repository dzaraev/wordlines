﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace BSC.WordLines.Presentation {
    public class BoolToDouble : IValueConverter {
        public double TrueValue { get; set; }
        public double FalseValue { get; set; }

        public BoolToDouble() {
            TrueValue = 1;
            FalseValue = 0;
        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            if (!(value is bool)) {
                return null;
            }
            return (bool)value ? TrueValue : FalseValue;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
            throw new NotSupportedException();
        }
    }
}