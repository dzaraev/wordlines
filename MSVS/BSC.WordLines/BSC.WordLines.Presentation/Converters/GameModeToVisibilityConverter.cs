﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using BSC.WordLines.Data;
using BSC.WordLines.Model;
using BSC.WordLines.Utils;

namespace BSC.WordLines.Presentation {
    public class GameModeToVisibilityConverter : IValueConverter{

        public Visibility MovesModeVisibility { get; set; }
        public Visibility TimeModeVisibility { get; set; }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            EGameMode gameMode = (EGameMode) value;
            if (gameMode == EGameMode.Moves) {
                return MovesModeVisibility;
            }
            if (gameMode == EGameMode.Time) {
                return TimeModeVisibility;
            }
            throw new InvalidOperationException(ErrorMessage.UnknownGameModeFormat.FormatWith(gameMode));
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
            throw new NotSupportedException();
        }
    }
}
