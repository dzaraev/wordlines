﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;
using BSC.WordLines.Data;
using BSC.WordLines.Utils;
using Caliburn.Micro;

namespace BSC.WordLines.Presentation {
    /// <summary>
    /// Converts abstract resource key into value of concrete type.
    /// Must fully support ARTS specification.
    /// </summary>
    public class AbstractResourceConverter : IValueConverter {
        private const char RESOURCE_TYPE_FROM_NAME_SEPARATOR = '_';
        private readonly Dictionary<string, Func<byte[], object>> _typedConverters;

        #region Cache

        private class CacheEntry {
            public byte[] Data { get; private set; }
            public object RuntimeValue { get; set; }
            public bool RuntimeValueCached { get; set; }

            public CacheEntry(byte[] data) {
                Data = data ?? new byte[] {};
                RuntimeValue = null;
                RuntimeValueCached = false;
            }
        }

        private static readonly Dictionary<string, CacheEntry> _cache = new Dictionary<string, CacheEntry>();

        public static event EventHandler CacheUpdated;

        public static void UpdateCache() {
            _cache.Clear();
            var provider = IoC.Get<ILocalDataProvider>();
            if (provider == null) {
                throw new InvalidOperationException(ErrorMessage.CannotResolveIoC<ILocalDataProvider>());
            }
            var keyedConcreteResources = provider.GetActualConcreteResourcesKeyed();
            foreach (var keyedResource in keyedConcreteResources) {
                //Runtime value not add immediately because of optimisation.
                _cache.Add(keyedResource.Key, new CacheEntry(keyedResource.Value.Data));
            }
            OnCacheUpdated();
        }

        private static void OnCacheUpdated() {
            var handler = CacheUpdated;
            if (handler != null) {
                handler(null, EventArgs.Empty);
            }
        }

        #endregion

        public AbstractResourceConverter() {
            _typedConverters = new Dictionary<string, Func<byte[], object>> {
                {"double", ConvertToDouble},
                {"thickness", ConvertToThickness},
                {"brush", ConvertToBrush},
                {"fontFamily", ConvertToFontFamily},
                {"fontWeight", ConvertToFontWeight},
                {"duration", ConvertToDuration},
                {"cornerRadius", ConvertToCornerRadius},
                {"uri", ConvertToUri}
            };
        }

        #region IValueConverter

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            string abstractResourceKey = parameter != null ? parameter.ToString() : string.Empty;
            return Convert(abstractResourceKey);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
            throw new NotImplementedException();
        }

        #endregion

        /// <summary>
        /// Converts abstract resource key to value of appropriate CLR type.
        /// </summary>
        /// <param name="abstractResourceKey"></param>
        /// <returns>Value of CLR type corresponded to parameter if convertion was successful, otherwise returns <c>null</c>.</returns>
        public object Convert(string abstractResourceKey) {
            //Check key
            string checkedKey = abstractResourceKey.Trim();
            bool keyIsValid = CheckAbstractResourceKeyValid(checkedKey);
            Diagnost.Assert(keyIsValid, ErrorMessage.AbstractResourceKeyHasIncorectFormat(checkedKey));
            if (!keyIsValid) {
                return null;
            }

            //Take concrete converter
            int resourceTypeLength = checkedKey.IndexOf(RESOURCE_TYPE_FROM_NAME_SEPARATOR);
            string resourceType = checkedKey.Substring(0, resourceTypeLength);
            bool typeConverterExists = _typedConverters.ContainsKey(resourceType);
            Diagnost.Assert(typeConverterExists, ErrorMessage.AbstractResourceConcreteConverterNotFound);
            if (!typeConverterExists) {
                return null;
            }
            var convertToClrType = _typedConverters[resourceType];

            //Take resource data only from CACHE!
            CacheEntry cacheEntry;
            if (Help.AssertFailed(_cache.TryGetValue(checkedKey, out cacheEntry), ErrorMessage.CachedResourceNotFound)) {
                return null;
            }
            
            if (!cacheEntry.RuntimeValueCached) {
                cacheEntry.RuntimeValue = convertToClrType(cacheEntry.Data);
                cacheEntry.RuntimeValueCached = true;
            }
            return cacheEntry.RuntimeValue;
        }

        public T Convert<T>(string abstractResourceKey) {
            return (T) Convert(abstractResourceKey);
        }

        /// <summary>
        /// Checks abstract resource key syntax corresponding to ARTS spec.
        /// </summary>
        private bool CheckAbstractResourceKeyValid(string key) {
            if (key.Contains(" ")) {
                return false;
            }
            var keyParts = key.Split(RESOURCE_TYPE_FROM_NAME_SEPARATOR);
            if (keyParts.Length != 2) {
                return false;
            }
            string type = keyParts[0];
            string name = keyParts[1];
            if (string.IsNullOrWhiteSpace(type) || string.IsNullOrWhiteSpace(name)) {
                return false;
            }
            return true;
        }

        private bool IsNullOrEmpty(byte[] bytes) {
            return bytes == null || bytes.Length == 0;
        }

        /// <summary>
        /// Converts bytes to <see cref="Thickness"/>.
        /// Format: single double value (uniform thickness).
        /// </summary>
        private object ConvertToThickness(byte[] sourceBytes) {
            Thickness defaultValue = new Thickness(0);
            if (IsNullOrEmpty(sourceBytes)) {
                return defaultValue;
            }
            string sourceString = sourceBytes.BytesToString();
            string[] stringValues = sourceString.Split(',');
            if (stringValues.Length == 4) {
                string stringLeft = stringValues[0];
                string stringTop = stringValues[1];
                string stringRight = stringValues[2];
                string stringBottom = stringValues[3];
                double left, top, right, bottom;
                if (double.TryParse(stringLeft, out left) &&
                    double.TryParse(stringTop, out top) &&
                    double.TryParse(stringRight, out right) &&
                    double.TryParse(stringBottom, out bottom)) {
                    return new Thickness(left, top, right, bottom);
                }
            }
            else if (stringValues.Length == 2) {
                string stringLeftRight = stringValues[0];
                string stringTopBottom = stringValues[1];
                double leftRight, topBottom;
                if (double.TryParse(stringLeftRight, out leftRight) &&
                    double.TryParse(stringTopBottom, out topBottom)) {
                    return new Thickness {
                        Left = leftRight,
                        Right = leftRight,
                        Top = topBottom,
                        Bottom = topBottom
                    };
                }
            }
            else if (stringValues.Length == 1) {
                string stringUniform = stringValues[0];
                double uniform;
                if (double.TryParse(stringUniform, out uniform)) {
                    return new Thickness(uniform);
                }
            }
            Diagnost.Assert(false, ErrorMessage.CannotConvertConvcreteValueToType(sourceString, "thickness"));
            return defaultValue;
        }

        private object ConvertToCornerRadius(byte[] sourceBytes) {
            var thickness = (Thickness) ConvertToThickness(sourceBytes);
            return new CornerRadius(thickness.Left, thickness.Top, thickness.Right, thickness.Bottom);
        }

        /// <summary>
        /// Converts bytes to <see cref="Brush"/>.
        /// Format: single value of AARRGGBB format (color).
        /// </summary>
        private object ConvertToBrush(byte[] sourceBytes) {
            var defaultValue = new SolidColorBrush(Colors.White);
            if (IsNullOrEmpty(sourceBytes) || sourceBytes.Length != 8) {
                return defaultValue;
            }
            string valueString = sourceBytes.BytesToString();
            string alphaString = valueString.Substring(0, 2);
            string redString = valueString.Substring(2, 2);
            string greenString = valueString.Substring(4, 2);
            string blueString = valueString.Substring(6, 2);

            byte alpha, red, green, blue;
            if (!byte.TryParse(alphaString, NumberStyles.HexNumber, null, out alpha) ||
                !byte.TryParse(redString, NumberStyles.HexNumber, null, out red) ||
                !byte.TryParse(greenString, NumberStyles.HexNumber, null, out green) ||
                !byte.TryParse(blueString, NumberStyles.HexNumber, null, out blue)) {

                return defaultValue;
            }

            Color color = new Color {A = alpha, R = red, G = green, B = blue};
            return new SolidColorBrush(color);
        }

        /// <summary>
        /// Converts bytes to <see cref="double"/>.
        /// Format: single double value.
        /// </summary>
        private object ConvertToDouble(byte[] sourceBytes) {
            double defaultValue = 0.0D;
            if (IsNullOrEmpty(sourceBytes)) {
                return defaultValue;
            }
            string valueString = sourceBytes.BytesToString();
            double value;
            return double.TryParse(valueString, out value)
                       ? value
                       : defaultValue;
        }

        /// <summary>
        /// Converts bytes to <see cref="FontFamily"/>.
        /// Format: string that match specs described 
        /// in FontFamily class MSDN article.
        /// </summary>
        private object ConvertToFontFamily(byte[] sourceBytes) {
            FontFamily defaultValue = new FontFamily("Portable User Interface");
            if (IsNullOrEmpty(sourceBytes)) {
                return defaultValue;
            }
            string valueString = sourceBytes.BytesToString();
            return new FontFamily(valueString);
        }

        /// <summary>
        /// Converts bytes to <see cref="FontWeight"/>.
        /// Format: string represets of <see cref="FontWeights"/> enum value.
        /// </summary>
        private object ConvertToFontWeight(byte[] sourceBytes) {
            FontWeight defaultValue = FontWeights.Normal;
            if (IsNullOrEmpty(sourceBytes)) {
                return defaultValue;
            }
            string valueString = sourceBytes.BytesToString();
            try {
                var propertyInfo = typeof (FontWeights).GetProperty(valueString);
                return propertyInfo != null
                           ? (FontWeight) propertyInfo.GetValue(null)
                           : defaultValue;
            }
            catch (Exception) {
                return defaultValue;
            }
        }

        /// <summary>
        /// Converts bytes to <see cref="Duration"/>.
        /// Format: string represents of <see cref="Duration"/> in XAML.
        /// </summary>
        private object ConvertToDuration(byte[] sourceBytes) {
            Duration defaultValue = new Duration(TimeSpan.Zero);
            if (IsNullOrEmpty(sourceBytes)) {
                return defaultValue;
            }
            string valueString = sourceBytes.BytesToString();
            TimeSpan value;
            return TimeSpan.TryParse(valueString, out value) ? new Duration(value) : defaultValue;
        }

        /// <summary>
        /// Converts bytes to <see cref="Uri"/>.
        /// Format: string represents RELATIVE URI path.
        /// </summary>
        /// <param name="sourceBytes"></param>
        /// <returns></returns>
        private object ConvertToUri(byte[] sourceBytes) {
            if (IsNullOrEmpty(sourceBytes)) {
                return null;
            }
            string valueString = sourceBytes.BytesToString();
            return new Uri(valueString, UriKind.Relative);
        }
    }
}