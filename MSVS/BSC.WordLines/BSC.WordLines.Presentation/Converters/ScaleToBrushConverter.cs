﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;
using BSC.WordLines.Model;
using BSC.WordLines.Utils;

namespace BSC.WordLines.Presentation {
    public class ScaleToBrushConverter : IValueConverter {
        private readonly Brush _defaultFill;
        private readonly Brush _shortFill;
        private readonly Brush _mediumFill;
        private readonly Brush _longFill;

        public ScaleToBrushConverter(AbstractResourceConverter resourceConverter) {
            resourceConverter.EnsureNotNull("resourceConverter");
            _defaultFill = resourceConverter.Convert<Brush>("brush_GameBackground");
            _shortFill = resourceConverter.Convert<Brush>("brush_ScaleBackgroundShortWord");
            _mediumFill = resourceConverter.Convert<Brush>("brush_ScaleBackgroundMediumWord");
            _longFill = resourceConverter.Convert<Brush>("brush_ScaleBackgroundLongWord");
        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            int tipIndex = (int)value;
            int currentIndex;
            if (!int.TryParse(parameter.ToString(), out currentIndex)) {
                Diagnost.Assert(false, ErrorMessage.CannotParseValue(parameter, typeof(int)));
                return _defaultFill;
            }

            return currentIndex <= tipIndex ? GetSectorFill(currentIndex) : _defaultFill;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
            throw new NotImplementedException();
        }

        private Brush GetSectorFill(int index) {
            if (index < 0) {
                return _defaultFill;
            }
            if (index <= GameConst.ShortWordMaxLength) {
                return _shortFill;
            }
            if (index <= GameConst.MediumWordMaxLength) {
                return _mediumFill;
            }
            return _longFill;
        }
    }
}