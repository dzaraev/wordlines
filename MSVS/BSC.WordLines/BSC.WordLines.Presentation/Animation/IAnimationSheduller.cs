﻿namespace BSC.WordLines.Presentation {
    public interface IAnimationSheduller {
        void Shedule(IAnimationTask task);
    }
}