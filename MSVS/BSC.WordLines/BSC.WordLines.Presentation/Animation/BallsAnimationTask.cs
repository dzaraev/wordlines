using System.Collections.Generic;

namespace BSC.WordLines.Presentation {
    public class BallsAnimationTask : AnimationTaskBase {
        public IEnumerable<IBallViewModel> Balls { get; private set; }
        public EBallAnimation Animation { get; private set; }
        
        public BallsAnimationTask(IEnumerable<IBallViewModel> balls, EBallAnimation animation) {
            Balls = new List<IBallViewModel>(balls);
            Animation = animation;
        }
    }
}