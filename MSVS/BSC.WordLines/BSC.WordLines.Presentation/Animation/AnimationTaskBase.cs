using System;

namespace BSC.WordLines.Presentation {
    public class AnimationTaskBase : IAnimationTask {
        public event EventHandler Completed;
        private bool _isCompleted = false;

        public void Complete() {
            if (!_isCompleted) {
                _isCompleted = true;
                OnCompleted();
            }
        }

        private void OnCompleted() {
            var handler = Completed;
            if (handler != null) {
                handler(this, EventArgs.Empty);
            }
        }
    }
}