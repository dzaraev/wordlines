﻿using System;
using System.Collections.Generic;
using BSC.WordLines.Utils;
using Caliburn.Micro;

namespace BSC.WordLines.Presentation {
    public class QueueAnimationScheduller : IAnimationSheduller {
        public event EventHandler Finished;

        private readonly Queue<IAnimationTask> _tasks;
        private readonly IEventAggregator _eventAggregator;
        private IAnimationTask _currentTask;
        private bool _inProcess;

        public QueueAnimationScheduller(IEventAggregator eventAggregator) {
            _eventAggregator = eventAggregator;
            _tasks = new Queue<IAnimationTask>();
            _currentTask = null;
            _inProcess = false;
        }

        public void Shedule(IAnimationTask task) {
            _tasks.Enqueue(task);
        }

        public void StartPublishTasks() {
            Diagnost.Assert(!_inProcess, ErrorMessage.TaskPublishingAlreadyStarted);
            if (_inProcess) {
                return;
            }
            _inProcess = true;
            PublishNextTask();
        }

        public bool InProcess {
            get { return _inProcess; }
        }

        public void Reset() {
            _tasks.Clear();
            if (_currentTask != null) {
                _currentTask.Completed -= HandleTaskCompleted;
                _currentTask = null;
            }
            _inProcess = false;
        }

        private void PublishNextTask() {
            if (_currentTask != null) {
                _currentTask.Completed -= HandleTaskCompleted;
            }
            if (_tasks.Count > 0) {
                var newTask = _tasks.Dequeue();
                newTask.Completed += HandleTaskCompleted;
                _currentTask = newTask;
                _eventAggregator.Publish(newTask);
            }
            else {
                _currentTask = null;
                _inProcess = false;
                OnFinished();
            }
        }

        private void HandleTaskCompleted(object sender, EventArgs e) {
            if (!ReferenceEquals(sender, _currentTask)) {
                throw new InvalidOperationException(ErrorMessage.AnimationTaskCompletedUnexpectedly);
            }
            PublishNextTask();
        }

        private void OnFinished() {
            var handler = Finished;
            if (handler != null) {
                handler(this, EventArgs.Empty);
            }
        }
    }
}