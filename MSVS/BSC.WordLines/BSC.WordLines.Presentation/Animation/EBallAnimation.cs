namespace BSC.WordLines.Presentation {
    public enum EBallAnimation {
        Appear,
        Disappear,
        Fall,
    }
}