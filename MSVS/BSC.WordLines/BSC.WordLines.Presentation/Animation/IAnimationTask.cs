﻿using System;

namespace BSC.WordLines.Presentation {
    public interface IAnimationTask {
        event EventHandler Completed;
        void Complete();
    }
}