﻿namespace BSC.WordLines.Presentation {
    public class BallsSelectionModeEvent {
        public bool IsSelectionModeEnabled { get; private set; }

        public BallsSelectionModeEvent(bool isEnabled) {
            IsSelectionModeEnabled = isEnabled;
        }
    }
}