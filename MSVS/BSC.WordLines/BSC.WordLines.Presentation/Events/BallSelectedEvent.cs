﻿namespace BSC.WordLines.Presentation {
    public class BallSelectedEvent {
        public IBallViewModel Ball { get; private set; }

        public BallSelectedEvent(IBallViewModel ball) {
            Ball = ball;
        }
    }
}