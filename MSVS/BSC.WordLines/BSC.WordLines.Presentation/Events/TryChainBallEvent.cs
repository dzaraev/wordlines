﻿namespace BSC.WordLines.Presentation {
    public class TryChainBallEvent {
        public IBallViewModel Ball { get; private set; }

        public TryChainBallEvent(IBallViewModel ball) {
            Ball = ball;
        }
    }
}