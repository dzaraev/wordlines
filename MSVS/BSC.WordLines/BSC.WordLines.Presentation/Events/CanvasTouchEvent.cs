﻿using System.Windows;

namespace BSC.WordLines.Presentation {
    public class CanvasTouchEvent {
        public Point Position { get; private set; } 


        public CanvasTouchEvent(Point position) {
            Position = position;
        }
    }
}