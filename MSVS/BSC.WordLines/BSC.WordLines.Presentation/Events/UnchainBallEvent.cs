﻿namespace BSC.WordLines.Presentation {
    public class UnchainBallEvent {
        public IBallViewModel Ball { get; private set; }

        public UnchainBallEvent(IBallViewModel ball) {
            Ball = ball;
        } 
    }
}