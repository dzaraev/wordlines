﻿using BSC.WordLines.Utils;

namespace BSC.WordLines.Presentation {
    public class TryApplyKillBallBoosterEvent {
        /// <summary>
        /// Gets removed ball. Cannot be null.
        /// </summary>
        public IBallViewModel Ball { get; private set; }
        /// <summary>
        /// Gets whether KillBall booster request was handled.
        /// </summary>
        public bool Handled { get; set; }
        public TryApplyKillBallBoosterEvent(IBallViewModel ball) {
            ball.EnsureNotNull("ball");
            Ball = ball;
            Handled = false;
        }
    }
}