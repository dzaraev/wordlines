﻿using Caliburn.Micro;

namespace BSC.WordLines.Presentation {
    public class ShowTooltipEvent {
        public string Tooltip { get; private set; }
        public string TooltipExtra { get; private set; }
        public bool IsAutoHide { get; private set; }
        public ShowTooltipEvent() {
            Tooltip = null;
            TooltipExtra = null;
            IsAutoHide = false;
        }

        public ShowTooltipEvent(string stringKey, bool autoHide) {
            var stringCache = IoC.Get<ILocalizedStringCache>();
            Tooltip = stringCache.GetLocalizedString(stringKey ?? string.Empty);
            TooltipExtra = null;
            IsAutoHide = autoHide;
        }

        public ShowTooltipEvent(string stringKey, string stringKeyExtra, bool autoHide) {
            var stringCache = IoC.Get<ILocalizedStringCache>();
            Tooltip = stringCache.GetLocalizedString(stringKey ?? string.Empty);
            TooltipExtra = stringCache.GetLocalizedString(stringKeyExtra ?? string.Empty);
            IsAutoHide = autoHide;
        }
    }
}