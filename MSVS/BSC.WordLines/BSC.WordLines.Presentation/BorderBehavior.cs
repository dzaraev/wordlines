﻿using System.Windows;
using System.Windows.Controls;

namespace BSC.WordLines.Presentation {
    public static class BorderBehavior {
        public static readonly DependencyProperty IsRoundTopEnabledProperty =
            DependencyProperty.RegisterAttached(
                "IsRoundTopEnabled",
                typeof (bool),
                typeof (BorderBehavior),
                new PropertyMetadata(false, IsRoundTopEnabledPropertyChanged));

        public static bool GetIsRoundTopEnabled(DependencyObject target) {
            return (bool)target.GetValue(IsRoundTopEnabledProperty);
        }
        public static void SetIsRoundTopEnabled(DependencyObject target, bool value) {
            target.SetValue(IsRoundTopEnabledProperty, value);
        }

        private static void IsRoundTopEnabledPropertyChanged(DependencyObject target, DependencyPropertyChangedEventArgs e) {
            bool isEnabled = GetIsRoundTopEnabled(target);
            Border borderTarget = target as Border;

            if (isEnabled && borderTarget != null) {
                borderTarget.SizeChanged += HandleBorderSizeChanged;
            }
            else if (!isEnabled && borderTarget != null) {
                borderTarget.SizeChanged -= HandleBorderSizeChanged;
            }
        }

        private static void HandleBorderSizeChanged(object sender, RoutedEventArgs e) {
            Border border = sender as Border;
            if (border == null) {
                return;
            }

            double radius = border.ActualWidth / 2.0;
            border.CornerRadius = new CornerRadius(radius, radius, 0, 0);
        }
    }
}