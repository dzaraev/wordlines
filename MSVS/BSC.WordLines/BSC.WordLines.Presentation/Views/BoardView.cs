﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Windows;
using System.Windows.Controls;
using System.Linq;
using System.Windows.Media.Animation;
using BSC.WordLines.Utils;
using Caliburn.Micro;

namespace BSC.WordLines.Presentation.Views {
    public class BoardView : IBoardView, IHandle<BallsAnimationTask> {
        private readonly Canvas _canvas;
        private readonly ViewFactory.CreateBallView _createBall;
        private readonly IEventAggregator _eventAggregator;
        private readonly IBoardViewModel _boardViewModel;
        private readonly BallsAnimationStoryboardFactory _ballsAnimationFactory;
        private readonly List<Storyboard> _ballsAnimationStoryboards;
        private bool _isLoaded;
        
        public BoardView(
            IBoardViewModel boardViewModel,
            Canvas canvas,
            ViewFactory.CreateBallView createBall,
            IEventAggregator eventAggregator
            ) {
            boardViewModel.EnsureNotNull("boardViewModel");
            canvas.EnsureNotNull("canvas");
            createBall.EnsureNotNull("createBall");
            eventAggregator.EnsureNotNull("eventAggregator");

            _canvas = canvas;
            _createBall = createBall;
            _eventAggregator = eventAggregator;
            _boardViewModel = boardViewModel;
            _ballsAnimationFactory = new BallsAnimationStoryboardFactory(canvas, this);
            _ballsAnimationStoryboards = new List<Storyboard>();
            _isLoaded = false;
        }

        public IEnumerable<BallView> Balls {
            get {
                return _canvas
                    .Children
                    .Where(child => child is BallView)
                    .Cast<BallView>();
            }
        }

        public bool IsLoaded {
            get { return _isLoaded; }
            private set { _isLoaded = value; }
        }

        public bool IsReady {
            get { return IsLoaded && _boardViewModel != null && _boardViewModel.IsActive; }
        }

        public void Load() {
            _eventAggregator.Subscribe(this);
            var ballsViewModels = _boardViewModel.Balls;
            ballsViewModels.CollectionChanged += HandleBallsChanged;
            AddBalls(ballsViewModels, false);
            IsLoaded = true;
        }

        public void Unload() {
            _boardViewModel.Balls.CollectionChanged -= HandleBallsChanged;
            _eventAggregator.Unsubscribe(this);
            _ballsAnimationStoryboards.Clear();
            Balls.ToList().ForEach(ball => _canvas.Children.Remove(ball));
            IsLoaded = false;
        }

        public double CalculateBallTop(FrameworkElement ballView) {
            var ballViewModel = ballView.DataContext as IBallViewModel;
            if (ballViewModel == null) {
                throw new InvalidOperationException(ErrorMessage.BallViewInvalidDataContext);
            }

            double ballTop;
            if (ballViewModel.Row.HasValue) {
                int rowsCount = _boardViewModel.RowsCount;
                double boardHeight = _canvas.ActualHeight;
                double topShift = (boardHeight - ballView.Height * rowsCount) / 2.0;
                ballTop = ballViewModel.Row.Value * ballView.Height + topShift;
            }
            else {
                ballTop = - 1 * ballView.Height;
            }
            return ballTop;
        }

        public double CalculateBallLeft(FrameworkElement ballView) {
            var ballViewModel = ballView.DataContext as IBallViewModel;
            if (ballViewModel == null) {
                throw new InvalidOperationException(ErrorMessage.BallViewInvalidDataContext);
            }

            return ballViewModel.Column.HasValue
                       ? ballViewModel.Column.Value * ballView.Width
                       : -1 * ballView.Width;
        }

        private IEnumerable<BallView> GetBallsForViewModels(IEnumerable<IBallViewModel> ballsViewModels) {
            var ballsList = Balls.ToList();
            var ballsViewModelsList = ballsViewModels.ToList();

            return from ballViewModel in ballsViewModelsList
                   select ballsList.First(ball => ReferenceEquals(ball.DataContext, ballViewModel));
        }
        
        private void PlaceBallOnCanvas(FrameworkElement ball, bool isAnimationPending) {
            SetBallSize(ball);

            double ballTop = isAnimationPending ? -1 * ball.Height : CalculateBallTop(ball);
            Canvas.SetTop(ball, ballTop);
            Canvas.SetLeft(ball, CalculateBallLeft(ball));
            Canvas.SetZIndex(ball, UIConst.BallZIndex);

            _canvas.Children.Add(ball);
        }

        private void SetBallSize(FrameworkElement ball) {
            double boardWidth = _canvas.ActualWidth;
            int columnsCount = _boardViewModel.ColumnsCount;

            double ballSize = boardWidth / columnsCount;
            ball.Width = ballSize;
            ball.Height = ballSize;
        }

        private void AddBalls(IEnumerable<IBallViewModel> ballsViewModels, bool isAnimationPending) {
            var existingBallList = Balls.ToList();
            foreach (var newBallViewModel in ballsViewModels) {
                var duplicatedBall = existingBallList.FirstOrDefault(
                    ball => ReferenceEquals(ball.DataContext, newBallViewModel));
                if (duplicatedBall != null) {
                    throw new InvalidOperationException(ErrorMessage.DuplicatedBallOnCanvas);
                }

                var newBall = _createBall(newBallViewModel, _canvas);
                PlaceBallOnCanvas(newBall, isAnimationPending);
            }
        }

        private void ClearBalls() {
            var balls = Balls.ToList();
            balls.ForEach(ball => _canvas.Children.Remove(ball));
        }

        private void HandleBallsChanged(object sender, NotifyCollectionChangedEventArgs e) {
            if (e.NewItems != null && e.NewItems.Count > 0) {
                var newBallViewModelList = e.NewItems.Cast<IBallViewModel>().ToList();
                AddBalls(newBallViewModelList, _boardViewModel.IsBallsAnimationEnabled);
            }
            if (e.OldItems != null && e.OldItems.Count > 0) {
                var oldBallsViewModels = e.OldItems.Cast<IBallViewModel>();
                var oldBalls = GetBallsForViewModels(oldBallsViewModels).ToList();
                oldBalls.ForEach(ball => _canvas.Children.Remove(ball));
            }
            if (e.Action == NotifyCollectionChangedAction.Reset) {
                ClearBalls();
                AddBalls(_boardViewModel.Balls, _boardViewModel.IsBallsAnimationEnabled);
            }
        }

        public void Handle(BallsAnimationTask task) {

            /* Uncomment for trace animation
             * Debug.WriteLine("balls anim task: {0}", task.Animation);
            task.Balls.ToList().ForEach(ball => Debug.WriteLine("letter: {0}", ball.BallModel.Letter));*/

            var storyboard = _ballsAnimationFactory.CreateStoryboard(task);
            EventHandler handleCompleted = null;
            handleCompleted = (sender, e) => {
                storyboard.Completed -= handleCompleted;
                _ballsAnimationStoryboards.Remove(storyboard);
                task.Complete();
            };
            _ballsAnimationStoryboards.Add(storyboard);
            storyboard.Completed += handleCompleted;
            storyboard.Begin();
        }
    }
}
