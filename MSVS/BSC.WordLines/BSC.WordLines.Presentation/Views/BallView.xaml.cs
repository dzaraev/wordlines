﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Caliburn.Micro;
using BSC.WordLines.Utils;

namespace BSC.WordLines.Presentation.Views {
    public partial class BallView : Grid, IHandle<CanvasTouchEvent> {
        private const double UNCHAIN_ANGLE_RADIANS = 0.35;//30 degrees
        private const double RADIUS_KOEF_TO_TRY_UNCHAIN = 2;
        
        private readonly IBallViewModel _ballViewModel;
        private readonly Canvas _canvas;
        private readonly IEventAggregator _eventAggregator;

        public BallView(IBallViewModel ballViewModel, Canvas canvas, IEventAggregator eventAggregator) {
            ballViewModel.EnsureNotNull("ballViewModel");
            canvas.EnsureNotNull("canvas");
            eventAggregator.EnsureNotNull("eventAggregator");

            InitializeComponent();

            DataContext = ballViewModel;
            _ballViewModel = ballViewModel;
            _canvas = canvas;
            _eventAggregator = eventAggregator;
            Loaded += HandleThisLoaded;
            Unloaded += HandleThisUnloaded;
        }

        public double Radius {
            get { return Diameter / 2; }
        }

        public double Diameter {
            get { return Math.Min(DisplayElement.ActualHeight, DisplayElement.ActualWidth); }
        }

        public double CenterX {
            get { return Canvas.GetLeft(this) + ActualWidth / 2; }
        }

        public double CenterY {
            get { return Canvas.GetTop(this) + ActualHeight / 2; }
        }
        
        private void UnchainSelf() {
            _eventAggregator.Publish(new UnchainBallEvent(_ballViewModel));
        }

        private void HandleBallTouch() {
            if (_ballViewModel.BallModel.IsOnBoard &&
                !_ballViewModel.InChain &&
                !_ballViewModel.IsAnimate) {
                _eventAggregator.Publish(new TryChainBallEvent(_ballViewModel));
            }
        }

        private void HandleOutOfBallTouch(Point touchPosition) {
            if (!_ballViewModel.InChain || !_ballViewModel.IsLastInChain) {
                return;
            }

            var prevBallViewModel = _ballViewModel.PreviousBall;
            if (prevBallViewModel == null) {
                return;
            }

            var prevBall = _canvas.GetBallView(prevBallViewModel);
            double prevBallCenterX = prevBall.CenterX;
            double prevBallCenterY = prevBall.CenterY;
            double touchX = touchPosition.X;
            double touchY = touchPosition.Y;
            double radius = Radius;
            double touchToCenter = Geometry.SegmentLength(touchX, touchY, CenterX, CenterY);
            double touchToPrevCenter = Geometry.SegmentLength(touchX, touchY, prevBallCenterX, prevBallCenterY);
            double betweenCenters = Geometry.SegmentLength(CenterX, CenterY, prevBallCenterX, prevBallCenterY);

            double x1 = CenterX;
            double y1 = CenterY;
            double x2 = prevBallCenterX;
            double y2 = prevBallCenterY;
            double a = y2 - y1;
            double b = x1 - x2;
            double c = x2 * y1 - y2 * x1;
            double touchToCentersLineDistance = Math.Abs(a * touchX + b * touchY + c) /
                                                Math.Sqrt(Math.Pow(a, 2) + Math.Pow(b, 2));
            double anglePrevAndCurrentLines = Math.Asin(touchToCentersLineDistance / touchToCenter);
            if (touchToCenter > radius &&
                touchToCenter < radius * RADIUS_KOEF_TO_TRY_UNCHAIN &&
                touchToPrevCenter < betweenCenters &&
                anglePrevAndCurrentLines < UNCHAIN_ANGLE_RADIANS) {

                UnchainSelf();
            }
        }

        public void Handle(CanvasTouchEvent touch) {
            if (_ballViewModel.InSelectionMode) {
                return;
            }

            var touchPosition = touch.Position;
            double touchToCenterDistance = Geometry.SegmentLength(touchPosition.X, touchPosition.Y, CenterX, CenterY);

            if (touchToCenterDistance < Radius) {
                HandleBallTouch();
                return;
            }

            if (touchToCenterDistance < Radius * RADIUS_KOEF_TO_TRY_UNCHAIN) {
                HandleOutOfBallTouch(touch.Position);
            }
        }
        
        private void HandleThisLoaded(object sender, RoutedEventArgs e) {
            _eventAggregator.Subscribe(this);
        }

        private void HandleThisUnloaded(object sender, RoutedEventArgs e) {
            _eventAggregator.Unsubscribe(this);
        }
    }
}
