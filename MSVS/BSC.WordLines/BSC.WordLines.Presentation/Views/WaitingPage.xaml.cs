﻿using BSC.WordLines.Presentation.ViewModels;
using Microsoft.Phone.Controls;

namespace BSC.WordLines.Presentation.Views {
    public partial class WaitingPage : PhoneApplicationPage {
        public WaitingPage() {
            InitializeComponent();
        }

        public int GetWaitingKey() {
            var dataContext = DataContext as WaitingPageViewModel;
            return dataContext != null ? dataContext.WaitingKey : 0;
        }
    }
}