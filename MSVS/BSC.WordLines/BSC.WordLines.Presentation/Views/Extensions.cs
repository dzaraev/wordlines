﻿using System.Linq;
using System.Windows.Controls;

namespace BSC.WordLines.Presentation.Views {
    public static class Extensions {
        public static BallView GetBallView(this Canvas source, IBallViewModel ballViewModel) {
            return (BallView) source.Children.FirstOrDefault(item => {
                var ball = item as BallView;
                return ball != null && ReferenceEquals(ball.DataContext, ballViewModel);
            });
        }
    }
}