﻿using Caliburn.Micro;
using Microsoft.Phone.Controls;

namespace BSC.WordLines.Presentation.Views {
    public partial class FreePointsPage : PhoneApplicationPage {
        public FreePointsPage() {
            InitializeComponent();
        }

        private void HandleAdClick(object sender, AdDuplex.AdClickEventArgs e) {
            IoC.Get<ISoundsManager>().PlayClick();
            FreePointsManager.OnBannerClick();
        }
    }
}