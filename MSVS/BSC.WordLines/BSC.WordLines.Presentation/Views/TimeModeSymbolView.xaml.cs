﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace BSC.WordLines.Presentation.Views {
    public partial class TimeModeSymbolView : UserControl {
        private const double LINE1_Y_FACTOR = 0.45;
        private const double LINE2_FACTOR = 0.66;
        private const double LINE_THICKNESS_DIVISOR = 22;

        public TimeModeSymbolView() {
            InitializeComponent();
            _grid.Loaded += HandleCircleLoaded;
            _grid.Visibility = Visibility.Collapsed;
        }

        private void HandleCircleLoaded(object sender, RoutedEventArgs e) {
            _grid.Visibility = Visibility.Visible;
            _grid.UpdateLayout();
            
            double squareSide = Math.Min(_circle.ActualWidth, _circle.ActualHeight); 
            double center = squareSide / 2;
            double xShift = (_grid.ActualWidth - squareSide) / 2;//centering X
            double yShift = (_grid.ActualHeight - squareSide) / 2;//centering Y
            double lineThickness = squareSide / LINE_THICKNESS_DIVISOR;
            _circle.StrokeThickness = lineThickness;
            _circle.Height = squareSide;
            _circle.Width = squareSide;

            _line1.StrokeThickness = lineThickness;
            _line1.X1 = center + xShift;
            _line1.Y1 = center + yShift;
            _line1.X2 = center + xShift;
            _line1.Y2 = LINE1_Y_FACTOR * center + yShift;

            _line2.StrokeThickness = lineThickness;
            _line2.X1 = center + xShift;
            _line2.Y1 = center + yShift;
            _line2.X2 = LINE2_FACTOR * center * 2 + xShift;
            _line2.Y2 = LINE2_FACTOR * center * 2 + yShift;
        }
    }
}
