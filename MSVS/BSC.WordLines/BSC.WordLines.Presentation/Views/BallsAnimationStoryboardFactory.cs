﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Animation;
using BSC.WordLines.Utils;

namespace BSC.WordLines.Presentation.Views {
    public class BallsAnimationStoryboardFactory {
        private const int APPEAR_MILLISECONDS_PER_CELL = 150;
        private const int DISAPPEAR_MILLISECONDS = 150;
        private const int FALL_MILLISECONDS_PER_CELL = 70;
        
        private readonly Canvas _canvas;
        private readonly IBoardView _boardView;

        public BallsAnimationStoryboardFactory(Canvas canvas, IBoardView boardView) {
            _canvas = canvas;
            _boardView = boardView;
        }

        public Storyboard CreateStoryboard(BallsAnimationTask task) {
            if (!task.Balls.Any()) {
                return null;
            }

            var animatedBalls = GetAnimatedBallViews(task.Balls);

            Storyboard storyboard = new Storyboard();
            EBallAnimation animation = task.Animation;
            switch (animation) {
                case EBallAnimation.Appear:
                    GetAppearAnimations(animatedBalls)
                        .ToList()
                        .ForEach(storyboard.Children.Add);
                    break;
                case EBallAnimation.Disappear:
                    GetDisappearAnimations(animatedBalls)
                        .ToList()
                        .ForEach(storyboard.Children.Add);
                    break;
                case EBallAnimation.Fall:
                    GetFallAnimations(animatedBalls)
                        .ToList()
                        .ForEach(storyboard.Children.Add);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(
                        ErrorMessage.UnexpectedBallAnimationType(animation.ToString()));
            }
            return storyboard;
        }
        
        private IEnumerable<Timeline> GetAppearAnimations(IEnumerable<BallView> balls) {
            var ballsList = (from ball in balls
                             orderby Canvas.GetLeft(ball) ascending
                             orderby Canvas.GetTop(ball) descending
                             select ball).ToList();
            if (!ballsList.Any()) {
                throw new InvalidOperationException(ErrorMessage.NoBallsForAnimation);
            }

            var beginTime = TimeSpan.FromMilliseconds(0);
            var beginTimeIncrement = TimeSpan.FromMilliseconds((double)APPEAR_MILLISECONDS_PER_CELL / ballsList.Count);
            var topPropertyPath = new PropertyPath(Canvas.TopProperty);
            var duration = CalculateDuration(APPEAR_MILLISECONDS_PER_CELL);

            var animationList = new List<Timeline>();
            foreach (var ball in ballsList) {
                var ballViewModel = (IBallViewModel) ball.DataContext;
                if (!ballViewModel.Row.HasValue) {
                    throw new InvalidOperationException(ErrorMessage.BallRowHasNotValue);
                }
                var to = _boardView.CalculateBallTop(ball);
                var animation = new DoubleAnimation {
                    BeginTime = beginTime,
                    Duration = duration,
                    To = to,
                };
                Storyboard.SetTarget(animation, ball);
                Storyboard.SetTargetProperty(animation, topPropertyPath);
                animationList.Add(animation);
                beginTime = beginTime.Add(beginTimeIncrement);
            }
            return animationList;
        }

        private IEnumerable<Timeline> GetFallAnimations(IEnumerable<BallView> balls) {
            var ballsList = (from ball in balls
                             orderby Canvas.GetLeft(ball) descending 
                             orderby Canvas.GetTop(ball) descending
                             select ball).ToList();
            if (!ballsList.Any()) {
                throw new InvalidOperationException(ErrorMessage.NoBallsForAnimation);
            }
            var beginTime = TimeSpan.FromMilliseconds(0);
            var beginTimeIncrement = TimeSpan.FromMilliseconds((double)FALL_MILLISECONDS_PER_CELL / ballsList.Count);
            var topPropertyPath = new PropertyPath(Canvas.TopProperty);

            var animationList = new List<Timeline>();
            foreach (var ball in ballsList) {
                var ballViewModel = (IBallViewModel)ball.DataContext;
                if (!ballViewModel.Row.HasValue) {
                    throw new InvalidOperationException(ErrorMessage.BallRowHasNotValue);
                }
                if (!ballViewModel.PreviousRow.HasValue) {
                    throw new InvalidOperationException(ErrorMessage.BallPreviousRowHasNotValue);
                }
                double totalMilliseconds = FALL_MILLISECONDS_PER_CELL *
                    Math.Abs(ballViewModel.Row.Value - ballViewModel.PreviousRow.Value);
                var duration = CalculateDuration(totalMilliseconds);
                var to = _boardView.CalculateBallTop(ball);

                var animation = new DoubleAnimation {
                    BeginTime = beginTime,
                    Duration = duration,
                    To = to,
                };
                Storyboard.SetTarget(animation, ball);
                Storyboard.SetTargetProperty(animation, topPropertyPath);
                animationList.Add(animation);
                beginTime = beginTime.Add(beginTimeIncrement);
            }
            return animationList;
        }

        private IEnumerable<Timeline> GetDisappearAnimations(IEnumerable<BallView> balls) {
            var ballsList = balls.ToList();
            if (!ballsList.Any()) {
                throw new InvalidOperationException(ErrorMessage.NoBallsForAnimation);
            }
            var beginTime = TimeSpan.FromMilliseconds(0);
            var widthPropertyPath = new PropertyPath(FrameworkElement.WidthProperty);
            var heightPropertyPath = new PropertyPath(FrameworkElement.HeightProperty);
            var topPropertyPath = new PropertyPath(Canvas.TopProperty);
            var leftPropertyPath = new PropertyPath(Canvas.LeftProperty);
            var fontSizePropertyPath = new PropertyPath(TextBlock.FontSizeProperty);
            var to = 0.0;
            var duration = CalculateDuration(DISAPPEAR_MILLISECONDS);
            Func<Timeline> makeSizeAnimation = () => new DoubleAnimation {
                BeginTime = beginTime,
                Duration = duration,
                To = to,
            };
            Func<double, Timeline> makePositionAnimation = toCoordinate => new DoubleAnimation {
                BeginTime = beginTime,
                Duration = duration,
                To = toCoordinate
            };

            var animationList = new List<Timeline>();
            foreach (var ball in ballsList) {
                var widthAnimation = makeSizeAnimation();
                Storyboard.SetTarget(widthAnimation, ball);
                Storyboard.SetTargetProperty(widthAnimation, widthPropertyPath);

                var heightAnimation = makeSizeAnimation();
                Storyboard.SetTarget(heightAnimation, ball);
                Storyboard.SetTargetProperty(heightAnimation, heightPropertyPath);

                var fontAnimation = makeSizeAnimation();
                Storyboard.SetTarget(fontAnimation, ball.LetterElement);
                Storyboard.SetTargetProperty(fontAnimation, fontSizePropertyPath);

                double ballToX = Canvas.GetLeft(ball) + ball.ActualWidth / 2;
                var leftAnimation = makePositionAnimation(ballToX);
                Storyboard.SetTarget(leftAnimation, ball);
                Storyboard.SetTargetProperty(leftAnimation, leftPropertyPath);

                double ballToY = Canvas.GetTop(ball) + ball.ActualHeight / 2;
                var topAnimation = makePositionAnimation(ballToY);
                Storyboard.SetTarget(topAnimation, ball);
                Storyboard.SetTargetProperty(topAnimation, topPropertyPath);

                animationList.Add(widthAnimation);
                animationList.Add(heightAnimation);
                animationList.Add(fontAnimation);
                animationList.Add(leftAnimation);
                animationList.Add(topAnimation);
            }
            return animationList;
        }

        private IEnumerable<BallView> GetAnimatedBallViews(IEnumerable<IBallViewModel> animatedBallViewModels) {
            var animatedBallViewsList = new List<BallView>();
            var animatedBallViewModelsList = animatedBallViewModels.ToList();
            var canvasChildren = _canvas.Children;
            foreach (var child in canvasChildren) {
                var ballView = child as BallView;
                if (ballView == null) {
                    continue;
                }
                var ballViewModel = ballView.DataContext as IBallViewModel;
                if (ballViewModel == null) {
                    continue;
                }
                if (animatedBallViewModelsList.ReferenceContains(ballViewModel)) {
                    animatedBallViewsList.Add(ballView);
                }
            }
            return animatedBallViewsList;
        }

        private Duration CalculateDuration(double milliseconds) {
            return new Duration(TimeSpan.FromMilliseconds(milliseconds));
        }
    }
}