﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Animation;
using BSC.WordLines.Presentation.ViewModels;
using BSC.WordLines.Utils;

namespace BSC.WordLines.Presentation.Views.Button {
    public partial class Text : UserControl {
        private const double ELONGATE_ANIMATION_DURATION = 300;//Milliseconds;

        public Text() {
            InitializeComponent();
            BackLine.Loaded += HandleBackLineLoaded;
        }

        private double LineThickness {
            get { return BackLine.StrokeThickness; }
        }

        private void HandleBackLineLoaded(object sender, RoutedEventArgs e) {
            ButtonViewModel viewModel;
            if (!TryGetViewModel(out viewModel)) {
                return;
            }
            RootCanvas.Clip = new RectangleGeometry {
                Rect = new Rect(0, 0, RootCanvas.ActualWidth, RootCanvas.ActualHeight)
            };

            if (!viewModel.LongLeftOnSelection || viewModel.IsSelected) {
                Canvas.SetLeft(RootGrid, 0);
            }
            else {
                Canvas.SetLeft(RootGrid, LineThickness);
            }

            BackLine.X1 = LineThickness / 2;
            BackLine.X2 = RootCanvas.ActualWidth - LineThickness / 2;
            BackLine.Y1 = BackLine.Y2 = RootCanvas.ActualHeight / 2;
            
            BackLine.StrokeStartLineCap = viewModel.HasLeftCap ? PenLineCap.Round : PenLineCap.Square;
            BackLine.StrokeEndLineCap = viewModel.HasRightCap ? PenLineCap.Round : PenLineCap.Square;

            viewModel.PropertyChanged += HandleViewModelPropertyChanged;
        }

        private void AnimateLineIfNeed() {
            ButtonViewModel viewModel;
            if (!TryGetViewModel(out viewModel)) {
                return;
            }
            if (!viewModel.LongLeftOnSelection) {
                return;
            }

            
            var animation = new DoubleAnimation {
                BeginTime = TimeSpan.FromMilliseconds(0),
                Duration = new Duration(TimeSpan.FromMilliseconds(ELONGATE_ANIMATION_DURATION)),
                To = viewModel.IsSelected ? 0.0 : LineThickness,
            };
            Storyboard.SetTarget(animation, RootGrid);
            Storyboard.SetTargetProperty(animation, new PropertyPath(Canvas.LeftProperty));
            var storyboard = new Storyboard();
            storyboard.Children.Add(animation);
            storyboard.Begin();
        }

        private bool TryGetViewModel(out ButtonViewModel viewModel) {
            viewModel = DataContext as ButtonViewModel;
            Diagnost.Assert(viewModel != null, ErrorMessage.ArgumentOrVarCannotBeNull("viewModel"));
            return viewModel != null;
        }

        private void HandleViewModelPropertyChanged(object sender, PropertyChangedEventArgs e) {
            if (e.PropertyName == "IsSelected") {
                AnimateLineIfNeed();
            }
        }
    }
}
