﻿using System;
using System.ComponentModel;
using System.Windows.Media;
using BSC.WordLines.Presentation.ViewModels;
using BSC.WordLines.Utils;
using Caliburn.Micro;
using Microsoft.Phone.Controls;

namespace BSC.WordLines.Presentation.Views {
    public partial class MainMenuPage : PhoneApplicationPage {
        private readonly AbstractResourceConverter _resourceConverter;
        private MainMenuPageViewModel ViewModel { get; set; }

        public MainMenuPage() {
            InitializeComponent();
            
            var resourceConverter = IoC.Get<AbstractResourceConverter>();
            if (resourceConverter == null) {
                string message = ErrorMessage.CannotResolveIoC<AbstractResourceConverter>();
                throw new InvalidOperationException(message);
            }
            _resourceConverter = resourceConverter;

            SetMenuItemsText();
            SetApplicationBarColors();
            
            Loaded += (s, e) => {
                ViewModel = (MainMenuPageViewModel) DataContext;
                ViewModel.PropertyChanged -= HandleViewModelPropertyChanged;
                ViewModel.PropertyChanged += HandleViewModelPropertyChanged;
                ViewModel.ThemeInvalidated -= HandleThemeInvalidated;
                ViewModel.ThemeInvalidated += HandleThemeInvalidated;
                ViewModel.LanguageInvalidated -= HandleLanguageInvalidated;
                ViewModel.LanguageInvalidated += HandleLanguageInvalidated;
            };
        }

        private void SetApplicationBarColors() {
            var appBar = ApplicationBar;
            appBar.BackgroundColor = _resourceConverter.Convert<SolidColorBrush>("brush_AppBarBackground").Color;
            appBar.ForegroundColor = _resourceConverter.Convert<SolidColorBrush>("brush_AppBarForeground").Color;
        }

        private void SetMenuItemsText() {
            var stringCache = IoC.Get<ILocalizedStringCache>();
            if (stringCache == null) {
                string message = ErrorMessage.CannotResolveIoC<ILocalizedStringCache>();
                throw new InvalidOperationException(message);
            }

            var appBar = ApplicationBar;
            var highScoresMenuItem = (AppBarMenuItem)appBar.MenuItems[0];
            var boostersMenuItem = (AppBarMenuItem)appBar.MenuItems[1];
            var settingsMenuItem = (AppBarMenuItem)appBar.MenuItems[2];
            var miscMenuItem = (AppBarMenuItem)appBar.MenuItems[3];

            string highScoresText = stringCache.GetLocalizedString("MenuHighScores");
            string boostersText = stringCache.GetLocalizedString("MenuBoosters");
            string settingsText = stringCache.GetLocalizedString("MenuSettings");
            string miscText = stringCache.GetLocalizedString("MenuMisc");

            highScoresMenuItem.Text = !string.IsNullOrWhiteSpace(highScoresText) ? highScoresText : "...";
            boostersMenuItem.Text = !string.IsNullOrWhiteSpace(boostersText) ? boostersText : "...";
            settingsMenuItem.Text = !string.IsNullOrWhiteSpace(settingsText) ? settingsText : "...";
            miscMenuItem.Text = !string.IsNullOrWhiteSpace(miscText) ? miscText : "...";
        }

        private void HandleViewModelPropertyChanged(object sender, PropertyChangedEventArgs e) {
            if (e.PropertyName == "Tooltip") {
                string tooltip = ViewModel.Tooltip;
                if (tooltip == null) {
                    Tooltip.Opacity = 0;
                }
                else {
                    Tooltip.Opacity = 1;
                    HideTooltipStoryboard.Begin();
                }
            }
        }

        private void HandleThemeInvalidated(object sender, EventArgs e) {
            SetApplicationBarColors();
        }

        private void HandleLanguageInvalidated(object sender, EventArgs e) {
            SetMenuItemsText();
        }
    }
}