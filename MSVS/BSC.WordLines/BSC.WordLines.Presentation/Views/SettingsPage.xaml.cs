﻿using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using BSC.WordLines.Presentation.ViewModels;
using BSC.WordLines.Utils;
using Caliburn.Micro;
using Microsoft.Phone.Controls;

namespace BSC.WordLines.Presentation.Views {
    public partial class SettingsPage : PhoneApplicationPage {
        public SettingsPage() {
            InitializeComponent();
            var brushColor = IoC.Get<AbstractResourceConverter>().Convert<SolidColorBrush>("brush_TextBoxForeground").Color;
            PlayerName.CaretBrush = new SolidColorBrush(brushColor);

            Loaded += HandleLoaded;
        }

        private void HandleLoaded(object sender, RoutedEventArgs e) {
            var viewModel = DataContext as SettingsPageViewModel;
            if (viewModel != null) {
                IsSoundsEnabledSwitch.IsChecked = viewModel.IsSoundsEnabled;
            }
            else {
                Diagnost.LogAsync(ErrorMessage.NoViewModelFoundOnLoadedFormat(typeof (SettingsPage)), ELogType.Error);
            }
        }

        private void HandlePlayerNameKeyUp(object sender, KeyEventArgs e) {
            if (e.Key == Key.Enter) {
                this.Focus();
            }
        }
    }
}