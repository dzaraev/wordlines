﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace BSC.WordLines.Presentation.Views {
    public partial class MovesModeSymbolView : UserControl {
        private const double ARROW_LENGTH_FACTOR = 0.40;
        private const double ARROWHEAD_X_FACTOR = 0.1; 
        private const double ARROWHEAD_Y_FACTOR = 0.12;
        private const double LINE_THICKNESS_DIVISOR = 22;
        private const double LINE1_X1_FACTOR = 0.30;
        private const double LINE1_Y1_FACTOR = 0.32;
        private const double LINE2_X1_FACTOR = 0.30;
        private const double LINE2_Y1_FACTOR = 0.50;
        private const double LINE3_X1_FACTOR = 0.30;
        private const double LINE3_Y1_FACTOR = 0.68;
        private double _arrowLength;

        public MovesModeSymbolView() {
            InitializeComponent();
            _grid.Loaded += HandleSquareLoaded;
            _grid.Visibility = Visibility.Collapsed;
        }

        private void HandleSquareLoaded(object sender, RoutedEventArgs e) {
            _grid.Visibility = Visibility.Visible;
            _grid.UpdateLayout();
            
            double squareSide = Math.Min(_circle.ActualWidth, _circle.ActualHeight);
            double xShift = (_grid.ActualWidth - squareSide) / 2;//centering X
            double yShift = (_grid.ActualHeight - squareSide) / 2;//centering Y
            double lineThickness = squareSide / LINE_THICKNESS_DIVISOR;
            _arrowLength = ARROW_LENGTH_FACTOR * squareSide;

            _circle.StrokeThickness = lineThickness;
            _circle.Height = squareSide;
            _circle.Width = squareSide;
            
            _line1.StrokeThickness = lineThickness;
            _line1_1.StrokeThickness = lineThickness;
            _line1_2.StrokeThickness = lineThickness;

            _line1.X1 = LINE1_X1_FACTOR * squareSide + xShift;
            _line1.Y1 = LINE1_Y1_FACTOR * squareSide + yShift;
            _line1.X2 = _line1.X1 + _arrowLength;
            _line1.Y2 = _line1.Y1;
            _line1_1.X1 = _line1.X2;
            _line1_1.Y1 = _line1.Y2;
            _line1_1.X2 = GetArrowheadX(_line1.X2);
            _line1_1.Y2 = GetArrowheadTopY(_line1.Y2);
            _line1_2.X1 = _line1.X2;
            _line1_2.Y1 = _line1.Y2;
            _line1_2.X2 = GetArrowheadX(_line1.X2);
            _line1_2.Y2 = GetArrowheadBottomY(_line1.Y2);
            
            _line2.StrokeThickness = lineThickness;
            _line2_1.StrokeThickness = lineThickness;
            _line2_2.StrokeThickness = lineThickness;

            _line2.X1 = LINE2_X1_FACTOR * squareSide + xShift;
            _line2.Y1 = LINE2_Y1_FACTOR * squareSide + yShift;
            _line2.X2 = _line2.X1 + _arrowLength;
            _line2.Y2 = _line2.Y1;
            _line2_1.X1 = _line2.X2;
            _line2_1.Y1 = _line2.Y2;
            _line2_1.X2 = GetArrowheadX(_line2.X2);
            _line2_1.Y2 = GetArrowheadTopY(_line2.Y2);
            _line2_2.X1 = _line2.X2;
            _line2_2.Y1 = _line2.Y2;
            _line2_2.X2 = GetArrowheadX(_line2.X2);
            _line2_2.Y2 = GetArrowheadBottomY(_line2.Y2);

            _line3.StrokeThickness = lineThickness;
            _line3_1.StrokeThickness = lineThickness;
            _line3_2.StrokeThickness = lineThickness;

            _line3.X1 = LINE3_X1_FACTOR * squareSide + xShift;
            _line3.Y1 = LINE3_Y1_FACTOR * squareSide + yShift;
            _line3.X2 = _line3.X1 + _arrowLength;
            _line3.Y2 = _line3.Y1;
            _line3_1.X1 = _line3.X2;
            _line3_1.Y1 = _line3.Y2;
            _line3_1.X2 = GetArrowheadX(_line3.X2);
            _line3_1.Y2 = GetArrowheadTopY(_line3.Y2);
            _line3_2.X1 = _line3.X2;
            _line3_2.Y1 = _line3.Y2;
            _line3_2.X2 = GetArrowheadX(_line3.X2);
            _line3_2.Y2 = GetArrowheadBottomY(_line3.Y2);
        }

        private double GetArrowheadX(double arrowTipX) {
            return arrowTipX - ARROWHEAD_X_FACTOR * _arrowLength;
        }

        private double GetArrowheadTopY(double arrowTipY) {
            return arrowTipY - ARROWHEAD_Y_FACTOR * _arrowLength;
        }
        
        private double GetArrowheadBottomY(double arrowTipY) {
            return arrowTipY + ARROWHEAD_Y_FACTOR * _arrowLength;
        }
    }
}
