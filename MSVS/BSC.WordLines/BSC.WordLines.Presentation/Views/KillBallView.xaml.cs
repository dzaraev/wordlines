﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace BSC.WordLines.Presentation.Views {
    public partial class KillBallView : UserControl {
        public KillBallView() {
            InitializeComponent(); 
            _grid.Loaded += HandleSquareLoaded;
            _grid.Visibility = Visibility.Collapsed;
        }

        private void HandleSquareLoaded(object sender, RoutedEventArgs e) {
            _grid.Visibility = Visibility.Visible;
            _grid.UpdateLayout();

            double squareSide = Math.Min(_grid.ActualWidth, _grid.ActualHeight);
            double xShift = (_grid.ActualWidth - squareSide) / 2;//centering

            _line1.X1 = 0.25 * squareSide + xShift;
            _line1.Y1 = 0.20 * squareSide;

            _line1.X2 = 0.75 * squareSide + xShift;
            _line1.Y2 = 0.80 * squareSide;

            _line2.X1 = _line1.X2;
            _line2.Y1 = _line1.Y1;

            _line2.X2 = _line1.X1;
            _line2.Y2 = _line1.Y2;

        }
    }
}
