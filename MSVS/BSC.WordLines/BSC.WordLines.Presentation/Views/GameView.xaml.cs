﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Windows;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using BSC.WordLines.Utils;
using Caliburn.Micro;

namespace BSC.WordLines.Presentation.Views {
    public partial class GameView : UserControl {
        private readonly double CHAIN_OPACITY = 1;

        private readonly IEventAggregator _eventAggregator;
        private readonly List<UIElement> _chainLines;
        private readonly AbstractResourceConverter _resourceConverter;
        private Brush _chainBrush;
        private IGameViewModel _gameViewModel;
        private IBoardView _board;
        private UIElement _chainTail;
        private Point _lastCanvasTouchPoint;
        private bool _isInitialized;

        public GameView() {
            _isInitialized = false;
            _gameViewModel = null;
            _board = null;
            _eventAggregator = IoC.Get<IEventAggregator>();
            if (_eventAggregator == null) {
                throw new InvalidOperationException(ErrorMessage.EventAggregatorNotAccessable);
            }
            var resourceConverter = IoC.Get<AbstractResourceConverter>();
            if (resourceConverter == null) {
                string message = ErrorMessage.CannotResolveIoC<AbstractResourceConverter>();
                throw new InvalidOperationException(message);
            }
            _resourceConverter = resourceConverter;
            _chainBrush = new SolidColorBrush();
            _chainLines = new List<UIElement>();
            Loaded += HandleThisLoaded;
            Unloaded += HandleThisUnloaded;
        }

        private void DrawChainTail() {
            if (_chainTail != null) {
                RemoveChainLine(_chainTail);
                _chainTail = null;
            }
            var lastBallViewModel = _gameViewModel.BallChain.LastOrDefault();
            if (lastBallViewModel != null) {
                var lastBall = _board.Balls.First(ball => ReferenceEquals(ball.DataContext, lastBallViewModel));
                Point tailEndPoint = _lastCanvasTouchPoint;
                _chainTail = DrawChainLine(lastBall.CenterX, lastBall.CenterY, tailEndPoint.X, tailEndPoint.Y, lastBall.Diameter);
            }
        }

        private UIElement DrawChainLine(double startX, double startY, double endX, double endY, double thickness) {
            Line line = new Line {
                X1 = startX,
                Y1 = startY,
                X2 = endX,
                Y2 = endY,
                Stroke = _chainBrush,
                StrokeThickness = thickness,
                Opacity = CHAIN_OPACITY,
                IsHitTestVisible = false,
                StrokeStartLineCap = PenLineCap.Round,
                StrokeEndLineCap = PenLineCap.Round
            };
            Canvas.SetZIndex(line, UIConst.ChainZIndex);
            _chainLines.Add(line);
            GameCanvas.Children.Add(line);
            return line;
        }

        private void RemoveChainLine(UIElement line) {
            _chainLines.Remove(line);
            GameCanvas.Children.Remove(line);
        }

        private void HandleThisLoaded(object sender, RoutedEventArgs e) {
            if (!_isInitialized) {
                _isInitialized = true;
                _gameViewModel = (IGameViewModel)DataContext;
                _chainBrush = _resourceConverter.Convert<Brush>("brush_GameChainBackground{0}Mode".FormatWith(_gameViewModel.Mode));
                _board = IoC.Get<ViewFactory.CreateBoardView>()(_gameViewModel.Board, GameCanvas);
            }
            _board.Load();
            _gameViewModel.BallChain.CollectionChanged += HandleBallChainChanged;
            _gameViewModel.PropertyChanged += HandleViewModelPropertyChanged;
            Touch.FrameReported += HandleTouchFrameReported;
        }

        private void HandleThisUnloaded(object sender, RoutedEventArgs e) {
            Touch.FrameReported -= HandleTouchFrameReported;
            _gameViewModel.BallChain.CollectionChanged -= HandleBallChainChanged;
            _gameViewModel.PropertyChanged -= HandleViewModelPropertyChanged;
            _board.Unload();
        }

        private void HandleBallChainChanged(object sender, NotifyCollectionChangedEventArgs e) {
            if (_board == null || !_board.IsReady) {
                return;
            }
            _chainLines.ForEach(item => GameCanvas.Children.Remove(item));

            var chainBallsViewModelsList = _gameViewModel.BallChain;
            var chainBallsList = new List<BallView>();
            var boardBallsList = _board.Balls.ToList();
            foreach (var chainBallViewModel in chainBallsViewModelsList) {
                var chainBall = boardBallsList.First(ball => ReferenceEquals(ball.DataContext, chainBallViewModel));
                chainBallsList.Add(chainBall);
            }

            if (chainBallsList.Count > 0) {
                DrawChainTail();
            }

            while (chainBallsList.Count > 0) {
                BallView currentBall = chainBallsList.First();
                chainBallsList.RemoveAt(0);
                BallView nextBall = chainBallsList.FirstOrDefault();
                if (nextBall != null) {
                    DrawChainLine(
                        currentBall.CenterX,
                        currentBall.CenterY,
                        nextBall.CenterX,
                        nextBall.CenterY,
                        currentBall.Diameter);
                }
            }
        }

        private void HandleViewModelPropertyChanged(object sender, PropertyChangedEventArgs e) {
            if (e.PropertyName == "Tooltip") {
                if (_gameViewModel.Tooltip == null) {
                    TooltipGrid.Opacity = 0;
                    CountersGrid.Opacity = 1;
                }
                else if(_gameViewModel.IsTooltipsAutoHideEnabled) {
                    TooltipGrid.Opacity = 1;
                    CountersGrid.Opacity = 0;
                    HideTooltipStoryboard.Begin();
                }
                else {
                    HideTooltipStoryboard.Stop();
                    TooltipGrid.Opacity = 1;
                    CountersGrid.Opacity = 0;
                }
            }
        }

        private void HandleMouseLeave(object sender, MouseEventArgs e) {
            _eventAggregator.Publish(new FinishChainEvent());
        }
        
        private void HandleTouchFrameReported(object sender, TouchFrameEventArgs e) {
            if (_board == null || !_board.IsReady) {
                return;
            }
            _lastCanvasTouchPoint = e.GetPrimaryTouchPoint(null).Position;
            DrawChainTail();
            _eventAggregator.Publish(new CanvasTouchEvent(_lastCanvasTouchPoint));
        }
    }
}