﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BSC.WordLines.Data;
using BSC.WordLines.Model;
using BSC.WordLines.Utils;

namespace BSC.WordLines.Presentation {
    public class LeaderboardManager : ILeaderboardManager {
        private readonly ILocalDataProvider _localProvider;
        private readonly IServerDataProvider _serverProvider;
        private readonly GameModeDataMapper _gameModesMapper;
        private readonly LanguageDataMapper _languageMapper;
        private readonly HighScorePeriodDataMapper _highScoreMapper;
        private readonly SettingsManager _settingsManager;
        private readonly IPlayer _player;
        private readonly Dictionary<GameModeLanguageKey, DateTime> _globalHighScoresRefreshLastTime; 
        
        public LeaderboardManager(
            ILocalDataProvider localProvider, 
            IServerDataProvider serverProvider,
            GameModeDataMapper gameModesMapper, 
            LanguageDataMapper languageMapper,
            HighScorePeriodDataMapper highScoreMapper,
            SettingsManager settingsManager,
            IPlayer player
            ) {
            localProvider.EnsureNotNull("localProvider");
            serverProvider.EnsureNotNull("serverProvider");
            gameModesMapper.EnsureNotNull("gameModesMapper");
            languageMapper.EnsureNotNull("languageMapper");
            highScoreMapper.EnsureNotNull("highScoreMapper");
            settingsManager.EnsureNotNull("settingsManager");
            player.EnsureNotNull("player");

            _globalHighScoresRefreshLastTime = new Dictionary<GameModeLanguageKey, DateTime>();
            _localProvider = localProvider;
            _serverProvider = serverProvider;
            _gameModesMapper = gameModesMapper;
            _languageMapper = languageMapper;
            _highScoreMapper = highScoreMapper;
            _settingsManager = settingsManager;
            _player = player;
        }

        public async Task SynchronizeHighScoresWithServer(
            ILanguageData refreshFromServerLanguage, IGameModeData refreshFromServerGameMode = null) {
            refreshFromServerLanguage.EnsureNotNull("languageData");
            try {
                var saveOnServerRequests = _localProvider.GetSaveHighScoreOnServerRequests();
                bool isSaveOnServerNeeded = saveOnServerRequests.Count > 0;
                if (isSaveOnServerNeeded) {
                    await _serverProvider.SaveHighScoresOnServerAsync(_player.Data, saveOnServerRequests);
                }
                await RefreshScoresFromServer(isSaveOnServerNeeded, refreshFromServerLanguage, refreshFromServerGameMode);
            }
            catch (Exception ex) {
                Diagnost.Assert(false, ErrorMessage.ExceptionOccured(ex));
            }
        }

        public async Task RegisterLocalScore(
            EGameMode gameMode,
            ILanguageData languageData,
            IPlayerData playerData,
            int score
            ) {
            DateTime timestamp = DateTime.UtcNow;
            var newScore = _localProvider.CreateLocalHighScore();
            newScore.GameModeId = _gameModesMapper.GetGameModeDataId(gameMode);
            newScore.LanguageId = languageData.Id;
            newScore.PlayerId = playerData.Id;
            newScore.Timestamp = DateTimeHelper.GetUtcTimestampString(timestamp);
            newScore.Score = score;
            _localProvider.InsertLocalHighScore(newScore);
            var localHighScores = _localProvider.GetLocalHighScores().ToList();
            CleanUpLocalHighScores(localHighScores);

            var gameModeData = _gameModesMapper.GetGameModeData(gameMode);
            await SynchronizeHighScoresWithServer(languageData, gameModeData);
        }

        public IEnumerable<ILocalHighScoreData> GetLocalHighScoresTop(
            EHighScorePeriods period, EGameMode gameMode, ELanguage language) {

            var focusedHighScores = GetFocusedLocalHighScores(language, gameMode);
            switch (period) {
                case EHighScorePeriods.AllTime:
                    return GetAllTimeLocalHighScoresTop(focusedHighScores);
                case EHighScorePeriods.Week:
                    return GetWeekLocalHighScoresTop(focusedHighScores);
                default:
                    throw new InvalidOperationException(ErrorMessage.UnexpectedlyEnumValue(period));
            }
        }

        public IEnumerable<IGlobalHighScoreData> GetGlobalHighScoresTop(
            EHighScorePeriods period, EGameMode gameMode, ELanguage language) {

            var focusedHighScores = _localProvider.GetGlobalHighScores()
                .Where(score => score.GameModeId == _gameModesMapper.GetGameModeDataId(gameMode) &&
                                score.LanguageId == _languageMapper.GetLanguageDataId(language) &&
                                score.HighScorePeriodId == _highScoreMapper.GetHighScorePeriodDataId(period));

            if (period == EHighScorePeriods.Week) {
                focusedHighScores = focusedHighScores.Where(score => DateTimeHelper.IsWithinCurrentWeek(score.Timestamp));
            }

            return focusedHighScores
                .OrderByDescending(highScore => highScore.Score)
                .Take(DataConstants.HighScoresTopCount);
        }

        public IEnumerable<IFacebookHighScoreData> GetFacebookHighScoresTop(
            EHighScorePeriods period, EGameMode gameMode, ELanguage language) {

            var focusedHighScores = _localProvider.GetFacebookHighScores()
                .Where(score => score.GameModeId == _gameModesMapper.GetGameModeDataId(gameMode) &&
                                score.LanguageId == _languageMapper.GetLanguageDataId(language) &&
                                score.HighScorePeriodId == _highScoreMapper.GetHighScorePeriodDataId(period));

            if (period == EHighScorePeriods.Week) {
                focusedHighScores = focusedHighScores.Where(score => DateTimeHelper.IsWithinCurrentWeek(score.Timestamp));
            }

            return focusedHighScores
                .OrderByDescending(highScore => highScore.Score)
                .Take(DataConstants.HighScoresTopCount);
        }

        public bool IsRefreshTimeoutExpired(ILanguageData languageData, IGameModeData gameModeData = null) {
            languageData.EnsureNotNull("languageData");
            var lastTimeKey = new GameModeLanguageKey(languageData, gameModeData);
            var refreshPeriod = DataConstants.GlobalHighScoresRefreshPeriod;
            return !_globalHighScoresRefreshLastTime.ContainsKey(lastTimeKey) ||
                   DateTime.Now - _globalHighScoresRefreshLastTime[lastTimeKey] > refreshPeriod;
        }

        private async Task RefreshScoresFromServer(bool mandatory, ILanguageData languageData, IGameModeData gameModeData = null) {
            languageData.EnsureNotNull("languageData");

            if (!mandatory && !IsRefreshTimeoutExpired(languageData, gameModeData)) {
                return;
            }

            var lastTimeKey = new GameModeLanguageKey(languageData, gameModeData);
            _globalHighScoresRefreshLastTime[lastTimeKey] = DateTime.Now;
            
            try {
                //Fetch GLOBAL highscores
                var globalHighScores = await _serverProvider.FetchGlobalHighScores(_settingsManager.CurrentPlayer, languageData, gameModeData);
                var globalHighScoresList = globalHighScores.ToList();
                _localProvider.RemoveGlobalHighScores(languageData, gameModeData);
                _localProvider.InsertGlobalHighScores(globalHighScoresList);

                //Fetch FACEBOOK highscores
                if (FacebookHelper.IsFacebokSigned()) {
                    var facebookHighScores = await _serverProvider.FetchFacebookHighScores(_settingsManager.CurrentPlayer, languageData, gameModeData);
                    var facebookHighScoresList = facebookHighScores.ToList();
                    _localProvider.RemoveFacebookHighScores(languageData, gameModeData);
                    _localProvider.InsertFacebookHighScores(facebookHighScoresList);
                }

                //Try to finish Player renaming if need.
                if (_player.IsRenamingNotFinished) {
                    _player.FinishRenamingAsync(_player.DisplayName);
                }
            }
            catch (Exception ex) {
                Diagnost.Assert(false, ErrorMessage.ExceptionOccured(ex));
            }
        }
        
        private void CleanUpLocalHighScores(List<ILocalHighScoreData> highScores) {
            var players = _localProvider.GetPlayers().ToList();
            var gameModes = _localProvider.GetGameModes().ToList();
            var languages = _localProvider.GetAllLanguages().ToList();
            var scoresToRemove = new List<ILocalHighScoreData>(highScores);

            foreach (IPlayerData player in players) {
                foreach (ILanguageData language in languages) {
                    foreach (IGameModeData gameMode in gameModes) {
                        var focusedScores = GetFocusedLocalHighScores(player, language, gameMode, highScores);

                        var allTimePeriodBests = GetAllTimeLocalHighScoresTop(focusedScores);
                        scoresToRemove.RemoveAll(allTimePeriodBests.ReferenceContains);

                        var weekPeriodBests = GetWeekLocalHighScoresTop(focusedScores);
                        scoresToRemove.RemoveAll(weekPeriodBests.ReferenceContains);
                    }
                }
            }
            _localProvider.RemoveLocalHighScores(scoresToRemove);
        }

        private IEnumerable<ILocalHighScoreData> GetAllTimeLocalHighScoresTop(IEnumerable<ILocalHighScoreData> sourceHighScores) {
            return sourceHighScores
                .OrderByDescending(scoreData => scoreData.Score)
                .Distinct(new LocalHighScoreComparer())
                .Take(DataConstants.HighScoresTopCount);
        }

        private IEnumerable<ILocalHighScoreData> GetWeekLocalHighScoresTop(IEnumerable<ILocalHighScoreData> sourceHighScores) {
            var list=
             sourceHighScores
                .Where(scoreData => DateTimeHelper.IsWithinCurrentWeek(scoreData.Timestamp))
                .OrderByDescending(scoreData => scoreData.Score)
                .Distinct(new LocalHighScoreComparer())
                .Take(DataConstants.HighScoresTopCount).ToList();
            return list;
        }

        private IEnumerable<ILocalHighScoreData> GetFocusedLocalHighScores(ELanguage language, EGameMode gameMode) {
            var gameModeData = _gameModesMapper.GetGameModeData(gameMode);
            var languageData = _languageMapper.GetLanguageData(language);
            var playerData = _settingsManager.CurrentPlayer;
            var allHighScores = _localProvider.GetLocalHighScores();

            return GetFocusedLocalHighScores(playerData, languageData, gameModeData, allHighScores);
        }

        private IEnumerable<ILocalHighScoreData> GetFocusedLocalHighScores(
            IPlayerData playerData,
            ILanguageData languageData,
            IGameModeData gameModeData,
            IEnumerable<ILocalHighScoreData> sourceHighScores
            ) {
            return sourceHighScores.Where(
                score => score.PlayerId == playerData.Id &&
                         score.LanguageId == languageData.Id &&
                         score.GameModeId == gameModeData.Id);
        }
        
        private class LocalHighScoreComparer : IEqualityComparer<ILocalHighScoreData> {
            public bool Equals(ILocalHighScoreData x, ILocalHighScoreData y) {
                if (ReferenceEquals(x, y)) {
                    return true;
                }

                if (ReferenceEquals(x, null) || ReferenceEquals(y, null)) {
                    return false;
                }
                return x.Score == y.Score;
            }

            public int GetHashCode(ILocalHighScoreData obj) {
                if (ReferenceEquals(obj, null)) {
                    return 0;
                }
                int hash = obj.Score.GetHashCode();
                return hash;
            }
        }
    }
}