﻿using System.Windows;
using BSC.WordLines.Utils;
using Microsoft.Phone.Controls;

namespace BSC.WordLines.Presentation {
    public static class PageBehavior {
        public static readonly DependencyProperty TurnstileEffectProperty =
            DependencyProperty.RegisterAttached(
                "TurnstileEffect",
                typeof (bool),
                typeof (PageBehavior),
                new PropertyMetadata(false, HandleTurnstileEffectChanged));

        public static bool GetTurnstileEffect(DependencyObject target) {
            return (bool)target.GetValue(TurnstileEffectProperty);
        }
        public static void SetTurnstileEffect(DependencyObject target, bool value) {
            target.SetValue(TurnstileEffectProperty, value);
        }

        private static void HandleTurnstileEffectChanged(
            DependencyObject target,
            DependencyPropertyChangedEventArgs e
            ) {
            var page = target as PhoneApplicationPage;
            Diagnost.Assert(page != null, ErrorMessage.PhoneBehaviorTargetTypeUnsupported);
            if (page == null) {
                return;
            }
            bool useTurnstile = GetTurnstileEffect(target);
            if (useTurnstile) {
                var inTransition = new NavigationInTransition {
                    Backward = new TurnstileTransition {Mode = TurnstileTransitionMode.BackwardIn},
                    Forward = new TurnstileTransition {Mode = TurnstileTransitionMode.ForwardIn}
                };
                page.SetValue(TransitionService.NavigationInTransitionProperty, inTransition);
                var outTransition = new NavigationOutTransition {
                    Backward = new TurnstileTransition {Mode = TurnstileTransitionMode.BackwardOut},
                    Forward = new TurnstileTransition {Mode = TurnstileTransitionMode.ForwardOut}
                };
                page.SetValue(TransitionService.NavigationOutTransitionProperty, outTransition);
            }
            else {
                page.SetValue(TransitionService.NavigationInTransitionProperty, null);
                page.SetValue(TransitionService.NavigationOutTransitionProperty, null);
            }
        }
    }
}