﻿using System.Globalization;
using BSC.WordLines.Data;
using BSC.WordLines.Model;
using BSC.WordLines.Utils;
using Caliburn.Micro;

namespace BSC.WordLines.Presentation {
    public class FreePointsManager {
        private static SettingsManager _settingsManager;
        private static IPlayer _player;

        private static void EnsureSettingsManager() {
            if (_settingsManager == null) {
                _settingsManager = IoC.Get<SettingsManager>();
            }
        }

        private static void EnsurePlayer() {
            if (_player == null) {
                _player = IoC.Get<IPlayer>();
            }
        }

        public static void OnAppReviewed() {
            EnsureSettingsManager();
            if (!_settingsManager.CurrentSettings.IsAppReviewed) {
                _settingsManager.CurrentSettings.IsAppReviewed = true;
                _settingsManager.SaveInDb();
            }
        }

        public static void OnShared() {
            EnsureSettingsManager();
            var isAppSharedSetting = _settingsManager.DynamicSettings[EDynamicSetting.IsAppShared];
            bool isShared = bool.Parse(isAppSharedSetting.Value);
            if (!isShared) {
                isAppSharedSetting.Value = true.ToString();
                _settingsManager.SaveDynamicSettings();
            }
        }

        public static void OnTwitterFollowed() {
            EnsureSettingsManager();
            var isTwitterFollowedSetting = _settingsManager.DynamicSettings[EDynamicSetting.IsTwitterFollowed];
            bool isTwitterFollowed = bool.Parse(isTwitterFollowedSetting.Value);
            if (!isTwitterFollowed) {
                isTwitterFollowedSetting.Value = true.ToString();
                _settingsManager.SaveDynamicSettings();
            }
        }

        public static void OnSocialFollowed() {
            EnsureSettingsManager();
            var isVkFollowedSetting = _settingsManager.DynamicSettings[EDynamicSetting.IsVkFollowed];
            bool isVkFollowed = bool.Parse(isVkFollowedSetting.Value);
            if (!isVkFollowed) {
                isVkFollowedSetting.Value = true.ToString();
                _settingsManager.SaveDynamicSettings();
            }
        }

        public static void OnSponsorFollowed() {
            EnsureSettingsManager();
            var isSponsorFollowedSetting = _settingsManager.DynamicSettings[EDynamicSetting.IsSponsorFollowed];
            bool isSponsorFollowed = bool.Parse(isSponsorFollowedSetting.Value);
            if (!isSponsorFollowed) {
                isSponsorFollowedSetting.Value = true.ToString();
                _settingsManager.SaveDynamicSettings();
            }
        }

        public static void OnBannerClick() {
            EnsureSettingsManager();
            var isBannerClickedSetting = _settingsManager.DynamicSettings[EDynamicSetting.IsBannerClicked];
            bool isBannerClicked = bool.Parse(isBannerClickedSetting.Value);
            if (!isBannerClicked) {
                isBannerClickedSetting.Value = true.ToString();
                _settingsManager.SaveDynamicSettings();
            }
        }

        public static void Fulfill(bool supressMessage = false) {
            EnsureSettingsManager();
            int pointsSum = 0;
            if (bool.Parse(_settingsManager.DynamicSettings[EDynamicSetting.IsBannerClicked].Value)) {
                pointsSum += int.Parse(_settingsManager.DynamicSettings[EDynamicSetting.BannerClickAwardPoints].Value);
                _settingsManager.DynamicSettings[EDynamicSetting.IsBannerClicked].Value = false.ToString();
            }
            if (_settingsManager.CurrentSettings.IsAppReviewed) {
                var isAppReviewFulfilledSetting =
                    _settingsManager.DynamicSettings[EDynamicSetting.IsAppReviewAwardFulfilled];
                if (!bool.Parse(isAppReviewFulfilledSetting.Value)) {
                    pointsSum += int.Parse(_settingsManager.DynamicSettings[EDynamicSetting.AppReviewAwardPoints].Value);
                    isAppReviewFulfilledSetting.Value = true.ToString();
                }
            }
            if (bool.Parse(_settingsManager.DynamicSettings[EDynamicSetting.IsAppShared].Value)) {
                var isAppSharedFulfilledSetting =
                    _settingsManager.DynamicSettings[EDynamicSetting.IsAppSharedAwardFulfilled];
                if (!bool.Parse(isAppSharedFulfilledSetting.Value)) {
                    pointsSum += int.Parse(_settingsManager.DynamicSettings[EDynamicSetting.AppSharedAwardPoints].Value);
                    isAppSharedFulfilledSetting.Value = true.ToString();
                }
            }

            if (bool.Parse(_settingsManager.DynamicSettings[EDynamicSetting.IsTwitterFollowed].Value)) {
                var isTwitterFollowedFulfilledSetting =
                    _settingsManager.DynamicSettings[EDynamicSetting.IsTwitterFollowedAwardFulfilled];
                if (!bool.Parse(isTwitterFollowedFulfilledSetting.Value)) {
                    pointsSum += int.Parse(_settingsManager.DynamicSettings[EDynamicSetting.TwitterFollowedAwardPoints].Value);
                    isTwitterFollowedFulfilledSetting.Value = true.ToString();
                }
            }

            if (bool.Parse(_settingsManager.DynamicSettings[EDynamicSetting.IsVkFollowed].Value)) {
                var isVkFollowedFulfilledSetting =
                    _settingsManager.DynamicSettings[EDynamicSetting.IsVkFollowedAwardFulfilled];
                if (!bool.Parse(isVkFollowedFulfilledSetting.Value)) {
                    pointsSum += int.Parse(_settingsManager.DynamicSettings[EDynamicSetting.VkFollowedAwardPoints].Value);
                    isVkFollowedFulfilledSetting.Value = true.ToString();
                }
            }

            if (bool.Parse(_settingsManager.DynamicSettings[EDynamicSetting.IsSponsorFollowed].Value)) {
                var isSponsorFollowedFulfilledSetting =
                    _settingsManager.DynamicSettings[EDynamicSetting.IsSponsorFollowedAwardFulfilled];
                if (!bool.Parse(isSponsorFollowedFulfilledSetting.Value)) {
                    pointsSum += int.Parse(_settingsManager.DynamicSettings[EDynamicSetting.SponsorFollowedAwardPoints].Value);
                    isSponsorFollowedFulfilledSetting.Value = true.ToString();
                }
            }

            if (pointsSum > 0) {
                EnsurePlayer();
                _player.Money += pointsSum;
                _settingsManager.SaveDynamicSettings();
                if (!supressMessage) {
                    string message = IoC.Get<ILocalizedStringCache>()
                        .GetLocalizedString("UserMessage_SuccessfulPurchaseFormat")
                        .FormatWith(pointsSum.ToString(CultureInfo.InvariantCulture));
                    DialogHelper.ShowUserMessage(message);
                }
            }
        }

        public static bool NotifyAboutFreePointsFeature() {
            EnsureSettingsManager();
            var isFeatureNotifiedSetting = _settingsManager.DynamicSettings[EDynamicSetting.IsFreePointsFeatureNotified];
            if (!bool.Parse(isFeatureNotifiedSetting.Value)) {
                isFeatureNotifiedSetting.Value = true.ToString();
                _settingsManager.SaveDynamicSettings();
                string message = IoC.Get<ILocalizedStringCache>().GetLocalizedString("FreePointsFeatureNotification");
                DialogHelper.ShowUserMessage(message);
                return true;
            }
            return false;
        }
    }
}