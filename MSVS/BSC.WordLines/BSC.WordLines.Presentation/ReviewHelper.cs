﻿using BSC.WordLines.Data;
using BSC.WordLines.Utils;
using Caliburn.Micro;
using Microsoft.Phone.Tasks;

namespace BSC.WordLines.Presentation {
    public static class ReviewHelper {
        private const int IDLE_REVIEW_OFFERS_MAX = 2;
        
        public static void OfferAppReviewIfNeed() {
            var settingsManager = IoC.Get<SettingsManager>();
            if (settingsManager.AssertNotNullFailed("settingsManager")) {
                return;
            }
            if (settingsManager.CurrentSettings.IsAppReviewed) {
                return;
            }
            
            int idleCount = settingsManager.CurrentSettings.IdleReviewOffersCount;
            if (idleCount == IDLE_REVIEW_OFFERS_MAX) {
                ShowReviewOffer();
            }

            if (idleCount > IDLE_REVIEW_OFFERS_MAX) {
                return;
            }

            if (idleCount < int.MaxValue) {
                idleCount++;
                settingsManager.CurrentSettings.IdleReviewOffersCount = idleCount;
                settingsManager.SaveInDb();
            }
        }

        public static void ReviewAppAndShutUp() {
            FreePointsManager.OnAppReviewed();
            var review = new MarketplaceReviewTask();
            review.Show();
        }

        private static void ShowReviewOffer() {
            var stringCache = IoC.Get<ILocalizedStringCache>();
            if (stringCache.AssertNotNullFailed("stringCache")) {
                return;
            }

            string message = stringCache.GetLocalizedString("ReviewOffer");
            string cancelText = stringCache.GetLocalizedString("ButtonReviewOfferCancel");
            string confirmText = stringCache.GetLocalizedString("ButtonReviewOfferConfirm");
            DialogHelper.ShowUserMessage(message, cancelText, confirmText, ReviewAppAndShutUp);
        }
    }
}