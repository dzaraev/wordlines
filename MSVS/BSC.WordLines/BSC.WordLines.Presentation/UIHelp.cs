﻿using System.Windows;
using BSC.WordLines.Presentation.Views;
using BSC.WordLines.Utils;
using Caliburn.Micro;

namespace BSC.WordLines.Presentation {
    public static class UIHelp {
        public static bool IsWaitingForKey(this INavigationService navigationService, int waitingKey) {
            navigationService.EnsureNotNull("navigationService");
            var currentPage = navigationService.CurrentContent as WaitingPage;
            return currentPage != null && currentPage.GetWaitingKey() == waitingKey;
        }

        public static int CreateWaitingKey() {
            return (new object()).GetHashCode();
        }

        public static void GoBackIfWainting() {
            var navigationService = IoC.Get<INavigationService>();
            var currentContent = navigationService.CurrentContent;
            bool isWaitingPlace =
                Application.Current.RootVisual == null ||
                currentContent != null && currentContent.GetType().Name == "WaitingPage";

            if (isWaitingPlace && navigationService.CanGoBack) {
                navigationService.GoBack();
            }
        }
    }
}