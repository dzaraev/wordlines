﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using BSC.WordLines.Data;
using BSC.WordLines.Utils;
using Caliburn.Micro;
using Microsoft.Phone.Controls;

namespace BSC.WordLines.Presentation {
    public class Text {
        public static readonly DependencyProperty KeyProperty =
            DependencyProperty.RegisterAttached(
                "Key",
                typeof(string),
                typeof(Text),
                new PropertyMetadata(null, HandleKeyChanged));

        public static string GetKey(DependencyObject target) {
            return (string)target.GetValue(KeyProperty);
        }

        public static void SetKey(DependencyObject target, string value) {
            target.SetValue(KeyProperty, value);
        }

        public static void ReAssignDeep(DependencyObject target) {
            target.EnsureNotNull("target");
            string targetTextKey = GetKey(target);
            if (targetTextKey != null) {
                AssignLocalizedString(target, targetTextKey);
            }
            int childrenCount = VisualTreeHelper.GetChildrenCount(target);
            if (childrenCount > 0) {
                for (int i = 0; i < childrenCount; i++) {
                    var child = VisualTreeHelper.GetChild(target, i);
                    if (child != null) {
                        ReAssignDeep(child);
                    }
                }
            }
            else {
                var contentControl = target as ContentControl;
                var content = contentControl != null ? contentControl.Content as DependencyObject : null;
                if (content != null) {
                    ReAssignDeep(content);
                }
            }
        }

        private static void HandleKeyChanged(DependencyObject target, DependencyPropertyChangedEventArgs e) {
            AssignLocalizedString(target, (string) e.NewValue);
        }

        private static void AssignLocalizedString(DependencyObject target, string stringKey) {
            var stringCache = IoC.Get<ILocalizedStringCache>();
            if (stringCache == null) {
                if (!Execute.InDesignMode) {
                    string message = ErrorMessage.CannotResolveIoC<ILocalizedStringCache>();
                    throw new InvalidOperationException(message);
                }
                return;
            }
            TextBlock textBlock = target as TextBlock;
            if (textBlock != null) {
                textBlock.Text = stringCache.GetLocalizedString(stringKey);
                return;
            }

            Button button = target as Button;
            if (button != null) {
                button.Content = stringCache.GetLocalizedString(stringKey);
                return;
            }

            PivotItem pivotItem = target as PivotItem;
            if (pivotItem != null) {
                pivotItem.Header = stringCache.GetLocalizedString(stringKey);
                return;
            }

            string error = ErrorMessage.ArgumentTypeUnsupported(target.GetType(), typeof (TextBlock), typeof (Button));
            Diagnost.Assert(false, error);
        }
    }
}