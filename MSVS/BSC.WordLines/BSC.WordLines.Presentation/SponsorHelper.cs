﻿using System;
using BSC.WordLines.Data;
using Caliburn.Micro;

namespace BSC.WordLines.Presentation {
    public class SponsorHelper {
        private static SettingsManager _settingsManager;

        public static string SponsorDescription {
            get { return "наши друзья - крупнейшая группа о Windows Phone"; }
        }

        public static string SponsorLinkName {
            get { return "vk.com/nokialumia"; }
        }

        public static Uri SponsorLinkUri {
            get { return new Uri("https://vk.com/nokialumia"); }
        }

        public static string SponsorButtonText {
            get { return "лучший паблик про WP!"; }
        }

        private static void EnsureSettignsManager() {
            if (_settingsManager == null) {
                _settingsManager = IoC.Get<SettingsManager>();
            }
        }
    }
}