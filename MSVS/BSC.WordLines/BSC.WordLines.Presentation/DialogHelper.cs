﻿using System;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Caliburn.Micro;
using Microsoft.Phone.Controls;

namespace BSC.WordLines.Presentation {
    public static class DialogHelper {
        public static void ShowError(string caption, string message) {
            var messageBox = new CustomMessageBox {
                Caption = caption,
                Message = message,
                IsRightButtonEnabled = true,
                RightButtonContent = "OK",
            };
            messageBox.LostFocus += HandleErrorDialogClosed;
            messageBox.Dismissed += HandleErrorDialogDismissed;
            messageBox.Show();
        }

        public static void ShowUserMessage(
            string message,
            string leftButtonText = null,
            string rightButtonText = null,
            System.Action rightButtonAction = null
            ) {
            var messageBox = new CustomMessageBox {
                Message = message,
                IsRightButtonEnabled = true,
            };
            if (leftButtonText != null) {
                messageBox.LeftButtonContent = leftButtonText;
                messageBox.IsLeftButtonEnabled = true;
            }
            else {
                messageBox.IsLeftButtonEnabled = false;
            }

            messageBox.IsRightButtonEnabled = true;
            messageBox.RightButtonContent =
                rightButtonText ?? IoC.Get<ILocalizedStringCache>().GetLocalizedString("ButtonUserMessageOk");

            var resourceConverter = IoC.Get<AbstractResourceConverter>();
            messageBox.Background = resourceConverter.Convert<Brush>("brush_PageBackground");

            EventHandler tuneHandler = null;
            tuneHandler = (s, e) => {
                try {
                    messageBox.LayoutUpdated -= tuneHandler;
                    var soundManager = IoC.Get<ISoundsManager>();
                    var rightButtonField = messageBox.GetType().GetField(
                        "_rightButton", BindingFlags.Instance | BindingFlags.GetField | BindingFlags.NonPublic);
                    var rightButton = rightButtonField.GetValue(messageBox) as Button;
                    TuneDialogButton(rightButton, () => {
                        soundManager.PlayClick();
                        if (rightButtonAction != null) {
                            rightButtonAction();
                        }
                    });
                    var leftButtonField = messageBox.GetType().GetField(
                        "_leftButton", BindingFlags.Instance | BindingFlags.GetField | BindingFlags.NonPublic);
                    var leftButton = leftButtonField.GetValue(messageBox) as Button;
                    TuneDialogButton(leftButton, soundManager.PlayClick);
                    var messageTextBlockField = messageBox.GetType().GetField(
                        "_messageTextBlock", BindingFlags.Instance | BindingFlags.GetField | BindingFlags.NonPublic);
                    var messageTextBlock = messageTextBlockField.GetValue(messageBox) as TextBlock;
                    TuneDialogText(messageTextBlock);
                }
                catch (Exception) {
                    /*DO NOTHING*/
                }
            };
            messageBox.LayoutUpdated += tuneHandler;
            messageBox.Show();
        }

        private static void HandleErrorDialogDismissed(object sender, DismissedEventArgs e) {
            UIHelp.GoBackIfWainting();
        }

        private static void HandleErrorDialogClosed(object sender, RoutedEventArgs e) {
            UIHelp.GoBackIfWainting();
        }
        
        private static void TuneDialogButton(Button button, System.Action clickAction = null) {
            if (button == null) {
                return;
            }
            var resourceConverter = IoC.Get<AbstractResourceConverter>();
            button.Background = resourceConverter.Convert<Brush>("brush_PageBackground");
            button.Foreground = resourceConverter.Convert<Brush>("brush_PageForeground");
            button.FontFamily = resourceConverter.Convert<FontFamily>("fontFamily_Default");
            button.FontSize = resourceConverter.Convert<double>("double_TextFontSize");
            button.BorderBrush =
                IoC.Get<AbstractResourceConverter>().Convert<Brush>("brush_PageForeground");
            if (clickAction != null) {
                button.Click += (s, e) => clickAction();
            }
        }
        private static void TuneDialogText(TextBlock textBlock) {
            if (textBlock == null) {
                return;
            }
            var resourceConverter = IoC.Get<AbstractResourceConverter>();
            textBlock.Foreground = resourceConverter.Convert<Brush>("brush_PageForeground");
            textBlock.FontFamily = resourceConverter.Convert<FontFamily>("fontFamily_Default");
            textBlock.FontSize = resourceConverter.Convert<double>("double_TextFontSize");
            textBlock.TextAlignment = TextAlignment.Center;
            textBlock.TextWrapping = TextWrapping.Wrap;
        }
    }
}