﻿using System;
using System.Windows;
using System.Windows.Controls;
using BSC.WordLines.Utils;

namespace BSC.WordLines.Presentation {
    public class CanvasBehavior {
        private const string ROLE_FOOTER = "Footer";
        private const string ROLE_HEADER = "Header";

        #region Dependency Properties

        public static readonly DependencyProperty RoleProperty =
            DependencyProperty.RegisterAttached(
                "Role",
                typeof (string),
                typeof (CanvasBehavior),
                new PropertyMetadata(null, HandleRoleChanged));

        public static string GetRole(DependencyObject d) {
            return (string) d.GetValue(RoleProperty);
        }

        public static void SetRole(DependencyObject d, string value) {
            d.SetValue(RoleProperty, value);
        }

        #endregion
        
        private static void HandleRoleChanged(DependencyObject target, DependencyPropertyChangedEventArgs e) {
            var targetElement = target as FrameworkElement;
            if (targetElement == null) {
                throw new InvalidOperationException(ErrorMessage.TargetMustBeFrameworkElement);
            }
            targetElement.Loaded -= HandleTargetLoaded;
            
            if (e.NewValue == null) {
                return;
            }

            targetElement.Visibility = Visibility.Collapsed;
            Canvas.SetTop(targetElement, -1);
            Canvas.SetLeft(targetElement, -1);
            targetElement.Loaded += HandleTargetLoaded;
        }

        private static void HandleTargetLoaded(object sender, RoutedEventArgs e) {
            var targetElement = sender as FrameworkElement;
            if (targetElement == null) {
                throw new InvalidOperationException(ErrorMessage.TargetMustBeFrameworkElement);
            }
            targetElement.Loaded -= HandleTargetLoaded;
            AdjustTarget(targetElement, GetRole(targetElement));
        }

        private static void AdjustTarget(FrameworkElement target, string alignment) {
            var canvas = target.Parent as Canvas;
            if (canvas == null) {
                throw new InvalidOperationException(ErrorMessage.ParentMustBeCanvas);
            }

            double targetWidth = canvas.ActualWidth;
            double targetHeight = canvas.ActualHeight / 4.0;

            if (string.Equals(alignment, ROLE_HEADER)) {
                Canvas.SetLeft(target, 0.0);
                Canvas.SetTop(target, 0.0);
            }
            else if (string.Equals(alignment, ROLE_FOOTER)) {
                Canvas.SetLeft(target, 0.0);
                Canvas.SetTop(target, canvas.ActualHeight - targetHeight);
            }
            else {
                Diagnost.Assert(false, ErrorMessage.CanvasBehaviorAlignmentValueNotSupported);
                return;
            }

            target.Width = targetWidth;
            target.Height = targetHeight;
            target.Visibility = Visibility.Visible;
        }
    }
}