using System.Collections.ObjectModel;
using System.ComponentModel;
using BSC.WordLines.Data;
using BSC.WordLines.Model;
using BSC.WordLines.Presentation.ViewModels;

namespace BSC.WordLines.Presentation {
    public interface IGameViewModel : INotifyPropertyChanged {
        int CurrentScore { get; }
        IBoardViewModel Board { get; }
        bool IsNewGamePending { get; set; }
        EGameMode Mode { get;}
        ObservableCollection<IBallViewModel> BallChain { get; }
        int MovesCount { get; }
        int SecondsCount { get; }
        BoostersViewModel Boosters { get; }
        IGame Model { get; }
        ScaleViewModel Scale { get; }
        string Tooltip { get; set; }
        bool IsTooltipsAutoHideEnabled { get; set; }
        bool UseTooltipExtra { get; set; }
    }
}