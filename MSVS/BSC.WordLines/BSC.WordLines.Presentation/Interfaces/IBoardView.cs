using System.Collections.Generic;
using System.Windows;
using BSC.WordLines.Presentation.Views;

namespace BSC.WordLines.Presentation {
    public interface IBoardView {
        IEnumerable<BallView> Balls { get; }
        bool IsLoaded { get; }
        bool IsReady { get; }
        double CalculateBallTop(FrameworkElement ballView);
        double CalculateBallLeft(FrameworkElement ballView);
        void Load();
        void Unload();
    }
}