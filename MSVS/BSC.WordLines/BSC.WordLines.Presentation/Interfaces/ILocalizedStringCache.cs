namespace BSC.WordLines.Presentation {
    public interface ILocalizedStringCache {
        string GetLocalizedString(string localizedStringKey);
        void Update();
    }
}