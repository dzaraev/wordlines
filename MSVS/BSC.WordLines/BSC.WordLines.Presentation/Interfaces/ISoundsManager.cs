﻿namespace BSC.WordLines.Presentation {
    public interface ISoundsManager {
        void PlayClick();
        void PlayShortChainKill();
        void PlayMediumChainKill();
        void PlayLongChainKill();
        void PlayGameOver();
        void PlayKillBall();
        void PlayWildcardOn();
        void PlayWildcardOff();
        void PlayAddition();
        void Update();
        void PlayMetronome();
    }
}