using BSC.WordLines.Model;

namespace BSC.WordLines.Presentation {
    public interface IBallViewModel {
        bool InChain { get; set; }
        int? Row { get; }
        int? Column { get; }

        /// <summary>
        /// Gets underlying "business logic" ball 
        /// Cannot be <c>null</c>.
        /// </summary>
        IBall BallModel { get; }

        IBallViewModel PreviousBall { get; set; }
        bool IsLastInChain { get; set; }
        int? PreviousRow { get; }
        bool IsAnimate { get; set; }
        bool InSelectionMode { get; }
        string Symbol { get; }
        void Activate();
        void Deactivate();
    }
}