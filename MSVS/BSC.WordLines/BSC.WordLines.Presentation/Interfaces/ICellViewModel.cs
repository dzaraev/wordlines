namespace BSC.WordLines.Presentation {
    public interface ICellViewModel {
        int Row { get; }
        int Column { get; }
    }
}