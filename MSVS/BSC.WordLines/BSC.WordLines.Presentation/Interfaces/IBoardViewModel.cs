using System.Collections.ObjectModel;
using Caliburn.Micro;

namespace BSC.WordLines.Presentation {
    public interface IBoardViewModel : IScreen {
        ObservableCollection<IBallViewModel> Balls { get; }
        ObservableCollection<ICellViewModel> Cells { get; }
        IAnimationSheduller AnimationSheduller { get; set; }
        int ColumnsCount { get; }
        int RowsCount { get; }
        bool IsBallsAnimationEnabled { get; }
    }
}