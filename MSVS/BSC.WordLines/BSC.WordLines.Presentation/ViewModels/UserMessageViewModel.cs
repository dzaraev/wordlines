﻿using BSC.WordLines.Utils;
using Caliburn.Micro;
using Action = System.Action;

namespace BSC.WordLines.Presentation.ViewModels {
    public class UserMessageViewModel : Screen {
        private readonly ISoundsManager _soundsManager;
        private string _message;
        private Action _cancelAction;
        private Action _confirmAction;
        private string _cancelText;
        private string _confirmText;
        private bool _isClosed;
        private bool _isCancelButtonVisible;
        private bool _isConfirmButtonVisible;

        public UserMessageViewModel(ViewModelFactory.CreateButton buttonFactory, ISoundsManager soundsManager) {
            buttonFactory.EnsureNotNull("buttonFactory");
            soundsManager.EnsureNotNull("soundsManager");
            _soundsManager = soundsManager;
            CancelButton = buttonFactory(true);
            ConfirmButton = buttonFactory(true);
            IsCancelButtonVisible = true;
            IsConfirmButtonVisible = true;
            _isClosed = false;
        }

        public ButtonViewModel CancelButton { get; private set; }
        public ButtonViewModel ConfirmButton { get; private set; }

        public string Message {
            get { return _message; }
            set {
                if (value != _message) {
                    _message = value;
                    NotifyOfPropertyChange(() => Message);
                }
            }
        }

        public bool IsCancelButtonVisible {
            get { return _isCancelButtonVisible; }
            set {
                if (value != _isCancelButtonVisible) {
                    _isCancelButtonVisible = value;
                    NotifyOfPropertyChange(() => IsCancelButtonVisible);
                }
            }
        }

        public bool IsConfirmButtonVisible {
            get { return _isConfirmButtonVisible; }
            set {
                if (value != _isConfirmButtonVisible) {
                    _isConfirmButtonVisible = value;
                    NotifyOfPropertyChange(() => IsConfirmButtonVisible);
                }
            }
        }

        public string CancelText {
            get { return _cancelText; }
            set {
                if (value != _cancelText) {
                    _cancelText = value;
                    NotifyOfPropertyChange(() => CancelText);
                }
            }
        }

        public string ConfirmText {
            get { return _confirmText; }
            set {
                if (value != _confirmText) {
                    _confirmText = value;
                    NotifyOfPropertyChange(() => ConfirmText);
                }
            }
        }

        public Action CancelAction {
            get { return _cancelAction; }
            set {
                if (value != _cancelAction) {
                    _cancelAction = value;
                    NotifyOfPropertyChange(() => CancelAction);
                }
            }
        }

        public Action ConfirmAction {
            get { return _confirmAction; }
            set {
                if (value != _confirmAction) {
                    _confirmAction = value;
                    NotifyOfPropertyChange(() => ConfirmAction);
                }
            }
        }

        public void Cancel() {
            if (_isClosed) {
                return;
            }
            _isClosed = true;
            _soundsManager.PlayClick();
            if (CancelAction != null) {
                CancelAction();
            }
            TryClose();
        }

        public void Confirm() {
            if (_isClosed) {
                return;
            }
            _isClosed = true;
            _soundsManager.PlayClick();
            if (ConfirmAction != null) {
                ConfirmAction();
            }
            TryClose();
        }
    }
}