﻿using System.Windows.Navigation;
using BSC.WordLines.Model;
using BSC.WordLines.Utils;
using Caliburn.Micro;

namespace BSC.WordLines.Presentation.ViewModels {
    public class BoostersPageViewModel : Screen {
        private readonly int _killBallPacketCount = GameConst.KillBallPacketCount;
        private readonly int _wildcardPacketCount = GameConst.WildcardPacketCount;
        private readonly int _additionPacketCount = GameConst.AdditionPacketCount;
        private readonly int _killBallPacketPrice = GameConst.KillBallPacketCount * GameConst.KillBallPrice;
        private readonly int _wildcardPacketPrice = GameConst.WildcardPacketCount * GameConst.WildcardPrice;
        private readonly int _additionPacketPrice = GameConst.AdditionPacketCount * GameConst.AdditionPrice;
        private readonly IPlayer _player;
        private readonly ILocalizedStringCache _stringCache;
        private readonly IStoreManager _storeManager;
        private readonly INavigationService _navigationService;
        private readonly ButtonViewModel _buyKillBallsButton;
        private readonly ButtonViewModel _buyWildcardsButton;
        private readonly ButtonViewModel _buyAdditionsButton;
        private readonly ButtonViewModel _buyMoneyButton;
        private bool _isStoreEnterFailed;

        public BoostersPageViewModel(
            IPlayer player,
            ViewModelFactory.CreateButton createButton,
            ILocalizedStringCache stringCache,
            IStoreManager storeManager,
            INavigationService navigationService
            ) {
            player.EnsureNotNull("player");
            createButton.EnsureNotNull("createButton");
            stringCache.EnsureNotNull("stringCache");
            storeManager.EnsureNotNull("storeManager");
            navigationService.EnsureNotNull("navigationService");

            string buyButtonTextFormat = stringCache.GetLocalizedString("BuyButtonTextFormat");
            
            _player = player;
            _stringCache = stringCache;
            _storeManager = storeManager;
            _navigationService = navigationService;
            _buyKillBallsButton = createButton(true);
            _buyKillBallsButton.PressAction = BuyKillBalls;
            _buyKillBallsButton.Text = buyButtonTextFormat.FormatWith(_killBallPacketCount, _killBallPacketPrice);
            _buyKillBallsButton.DefaultFillKey = "brush_ButtonDefaultBackgroundBuyBooster";
            _buyKillBallsButton.ActiveFillKey = "brush_ButtonActiveBackgroundBuyBooster";

            _buyWildcardsButton = createButton(true);
            _buyWildcardsButton.PressAction = BuyWildcards;
            _buyWildcardsButton.Text = buyButtonTextFormat.FormatWith(_wildcardPacketCount, _wildcardPacketPrice);
            _buyWildcardsButton.DefaultFillKey = "brush_ButtonDefaultBackgroundBuyBooster";
            _buyWildcardsButton.ActiveFillKey = "brush_ButtonActiveBackgroundBuyBooster";

            _buyAdditionsButton = createButton(true);
            _buyAdditionsButton.PressAction = BuyAdditions;
            _buyAdditionsButton.Text = buyButtonTextFormat.FormatWith(_additionPacketCount, _additionPacketPrice);
            _buyAdditionsButton.DefaultFillKey = "brush_ButtonDefaultBackgroundBuyBooster";
            _buyAdditionsButton.ActiveFillKey = "brush_ButtonActiveBackgroundBuyBooster";

            _buyMoneyButton = createButton(true);
            _buyMoneyButton.PressAction = BuyMoney;
            _buyMoneyButton.Text = stringCache.GetLocalizedString("BankBuyLetters");
            _isStoreEnterFailed = false;
        }

        public int KillBallCount {
            get { return _player.KillBallCount; }
        }

        public int WildcardCount {
            get { return _player.WildcardCount; }
        }

        public int AdditionCount {
            get { return _player.AdditionCount; }
        }

        public ButtonViewModel BuyKillBallsButton {
            get { return _buyKillBallsButton; }
        }

        public ButtonViewModel BuyWildcardsButton {
            get { return _buyWildcardsButton; }
        }

        public ButtonViewModel BuyAdditionsButton {
            get { return _buyAdditionsButton; }
        }

        public ButtonViewModel BuyMoneyButton {
            get { return _buyMoneyButton; }
        }

        public int Money {
            get { return _player.Money; }
        }

        public void BuyKillBalls() {
            if (_player.KillBallCount >= int.MaxValue - _killBallPacketCount) {
                return;
            }
            int money = _player.Money;
            if (money >= _killBallPacketPrice) {
                _player.BuyKillBalls(_killBallPacketCount, _killBallPacketPrice);
                NotifyOfPropertyChange(() => KillBallCount);
                NotifyOfPropertyChange(() => Money);
            }
            else {
                BuyMoney();
            }
        }

        public void BuyWildcards() {
            if (_player.WildcardCount >= int.MaxValue - _wildcardPacketCount) {
                return;
            }
            int money = _player.Money;
            if (money >= _wildcardPacketPrice) {
                _player.BuyWildcards(_wildcardPacketCount, _wildcardPacketPrice);
                NotifyOfPropertyChange(() => WildcardCount);
                NotifyOfPropertyChange(() => Money);
            }
            else {
                BuyMoney();
            }
        }

        public void BuyAdditions() {
            if (_player.AdditionCount >= int.MaxValue - _additionPacketCount) {
                return;
            }
            int money = _player.Money;
            if (money >= _additionPacketPrice) {
                _player.BuyAdditions(_additionPacketCount, _additionPacketPrice);
                NotifyOfPropertyChange(() => AdditionCount);
                NotifyOfPropertyChange(() => Money);
            }
            else {
                BuyMoney();
            }
        }

        public async void BuyMoney() {
            if (_player.Money > GameConst.MaxMoneyValue) {
                string message = _stringCache.GetLocalizedString("UserMessage_TooMuchMoney");
                DialogHelper.ShowUserMessage(message);
                return;
            }

            BuyMoneyButton.IsEnabled = false;

            int waitingKey = UIHelp.CreateWaitingKey();
            NavigatedEventHandler waitingHandler = null;
            waitingHandler = async (sender, e) => {
                _navigationService.Navigated -= waitingHandler;
                if (!_navigationService.IsWaitingForKey(waitingKey)) {
                    return;
                }
                bool isUpdated = await _storeManager.UpdateCurrencyProductListings();
                if (!_navigationService.IsWaitingForKey(waitingKey)) {
                    return;
                }
                if (!isUpdated) {
                    _isStoreEnterFailed = true;
                    _navigationService.GoBack();
                    return;
                }
                _navigationService.UriFor<StorePageViewModel>().Navigate();
                _navigationService.RemoveBackEntry();
            };
            _navigationService.Navigated += waitingHandler;
            _navigationService
                .UriFor<WaitingPageViewModel>()
                .WithParam(vm => vm.WaitingKey, waitingKey)
                .Navigate();
        }

        protected override void OnActivate() {
            BuyMoneyButton.IsEnabled = true;
            if (_isStoreEnterFailed) {
                _isStoreEnterFailed = false;

                //Cannot load products message.
                string message = _stringCache.GetLocalizedString("UserMessage_ConnectToStoreFailed");
                DialogHelper.ShowUserMessage(message);
            }
            NotifyOfPropertyChange(() => Money);
        }
    }
}