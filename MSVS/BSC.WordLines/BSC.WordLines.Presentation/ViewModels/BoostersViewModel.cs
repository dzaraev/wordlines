﻿using System;
using System.Windows.Media;
using BSC.WordLines.Data;
using BSC.WordLines.Model;
using BSC.WordLines.Utils;
using Caliburn.Micro;

namespace BSC.WordLines.Presentation.ViewModels {
    public class BoostersViewModel : Screen, IHandle<BallSelectedEvent> {
        private readonly IEventAggregator _eventAggregator;
        private readonly IPlayer _player;
        private readonly IGame _game;
        private readonly AbstractResourceConverter _resourceConverter;
        private readonly ISoundsManager _soundsManager;
        private readonly INavigationService _navigationService;
        private bool _isKillBallSelected;
        private bool _isWildcardSelected;
        private bool _isAdditionSelected;

        public BoostersViewModel(
            IEventAggregator eventAggregator,
            IPlayer player,
            IGame game,
            AbstractResourceConverter resourceConverter,
            ISoundsManager soundsManager,
            INavigationService navigationService) {

            eventAggregator.EnsureNotNull("eventAggregator");
            player.EnsureNotNull("player");
            game.EnsureNotNull("game");
            resourceConverter.EnsureNotNull("resourceConverter");
            soundsManager.EnsureNotNull("soundsManager");
            navigationService.EnsureNotNull("navigationService");

            _eventAggregator = eventAggregator;
            _player = player;
            _game = game;
            _resourceConverter = resourceConverter;
            _soundsManager = soundsManager;
            _navigationService = navigationService;
            _isKillBallSelected = false;
            _isWildcardSelected = false;
        }

        public Brush SelectionBackground {
            get {
                string resourceKey = "brush_BoostersSelectionBackground{0}Mode".FormatWith(_game.Mode);
                return _resourceConverter.Convert<Brush>(resourceKey);
            }
        }

        public int KillBallCount {
            get { return _player.KillBallCount; }
        }
        public int WildcardCount {
            get { return _player.WildcardCount; }
        }
        public int AdditionCount {
            get { return _player.AdditionCount; }
        }

        public bool IsKillBallSelected {
            get { return _isKillBallSelected; }
            set {
                if (_isKillBallSelected != value) {
                    _isKillBallSelected = value;
                    if (!value) {
                        _eventAggregator.Publish(new ShowTooltipEvent());
                    }
                    NotifyOfPropertyChange(() => IsKillBallSelected);
                }
            }
        }
        public bool IsWildcardSelected {
            get { return _isWildcardSelected; }
            set {
                if (_isWildcardSelected != value) {
                    _isWildcardSelected = value;
                    if (!value) {
                        _eventAggregator.Publish(new ShowTooltipEvent());
                    }
                    NotifyOfPropertyChange(() => IsWildcardSelected);
                }
            }
        }
        public bool IsAdditionSelected {
            get { return _isAdditionSelected; }
            set {
                if (_isAdditionSelected != value) {
                    _isAdditionSelected = value;
                    NotifyOfPropertyChange(() => IsAdditionSelected);
                }
            }
        }

        public void ActivateKillBall() {
            _soundsManager.PlayClick();
            if (_game.CanApplyKillBallBooster()) {
                IsWildcardSelected = false;
                IsKillBallSelected = !IsKillBallSelected;
                _eventAggregator.Publish(new BallsSelectionModeEvent(IsKillBallSelected));
                if (IsKillBallSelected) {
                    _eventAggregator.Publish(new ShowTooltipEvent("TooltipKillBallSelection", false));
                }
            }
            else {
                IsKillBallSelected = true;
                _navigationService.UriFor<BoostersPageViewModel>().Navigate();
            }
        }

        public void ActivateWildcard() {
            _soundsManager.PlayClick();
            if (_game.CanApplyWildcardBootster()) {
                IsKillBallSelected = false;
                IsWildcardSelected = !IsWildcardSelected;
                _eventAggregator.Publish(new BallsSelectionModeEvent(IsWildcardSelected));
                if (IsWildcardSelected) {
                    _eventAggregator.Publish(new ShowTooltipEvent("TooltipWildcardSelection", "TooltipExtraWildcardSelection", false));
                }
            }
            else {
                IsWildcardSelected = true;
                _navigationService.UriFor<BoostersPageViewModel>().Navigate();
            }
        }

        public void ActivateAddition() {
            if (IsKillBallSelected || IsWildcardSelected) {
                _eventAggregator.Publish(new BallsSelectionModeEvent(false));
            }
            IsKillBallSelected = false;
            IsWildcardSelected = false;
            IsAdditionSelected = true;

            _soundsManager.PlayClick();

            if (_game.CanApplyAdditionBooster()) {
                _game.ApplyAdditionBooster();
                if (_game.Mode == EGameMode.Moves) {
                    _eventAggregator.Publish(new ShowTooltipEvent("TooltipAdditionToMoves", true));
                }
                else if (_game.Mode == EGameMode.Time) {
                    _eventAggregator.Publish(new ShowTooltipEvent("TooltipAdditionToTime", true));
                }
                else {
                    Diagnost.Assert(false, ErrorMessage.UnknownGameModeFormat.FormatWith(_game.Mode));
                }
                _soundsManager.PlayAddition();
            }
            else if (_game.IsAdditionBoosterRestricted()) {
                _eventAggregator.Publish(new ShowTooltipEvent("TooltipAdditionRestriction", true));
            }
            else {
                _navigationService.UriFor<BoostersPageViewModel>().Navigate();
            }
        }

        public void DeactivateAddition() {
            IsAdditionSelected = false;
        }

        public void Handle(BallSelectedEvent message) {
            if (IsKillBallSelected) {
                var killBallEvent = new TryApplyKillBallBoosterEvent(message.Ball);
                _eventAggregator.Publish(killBallEvent);
                if (killBallEvent.Handled) {
                    IsKillBallSelected = false;
                    _eventAggregator.Publish(new BallsSelectionModeEvent(false));
                }
            }
            else if (IsWildcardSelected) {
                IsWildcardSelected = false;
                _eventAggregator.Publish(new BallsSelectionModeEvent(false));

                if (_game.ApplyWildcardBooster(message.Ball.BallModel)) {
                    _soundsManager.PlayWildcardOn();
                }
                else {
                    _soundsManager.PlayWildcardOff();
                }
            }
        }

        protected override void OnActivate() {
            if (IsKillBallSelected || IsWildcardSelected) {
                _eventAggregator.Publish(new BallsSelectionModeEvent(false));
            }
            IsKillBallSelected = false;
            IsWildcardSelected = false;
            IsAdditionSelected = false;

            _eventAggregator.Subscribe(this);
            _player.Updated += HandlePlayerUpdated;
            UpdateCountProperties();
        }
        
        protected override void OnDeactivate(bool close) {
            _eventAggregator.Unsubscribe(this);
            _player.Updated -= HandlePlayerUpdated;
        }

        private void HandlePlayerUpdated(object sender, EventArgs e) {
            UpdateCountProperties();
        }

        private void UpdateCountProperties() {
            NotifyOfPropertyChange(() => KillBallCount);
            NotifyOfPropertyChange(() => WildcardCount);
            NotifyOfPropertyChange(() => AdditionCount);
        }
    }
}