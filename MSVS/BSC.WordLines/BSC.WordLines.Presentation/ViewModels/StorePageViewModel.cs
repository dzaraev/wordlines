﻿using System.Globalization;
using System.Linq;
using System.Windows.Navigation;
using BSC.WordLines.Model;
using BSC.WordLines.Utils;
using Caliburn.Micro;

namespace BSC.WordLines.Presentation.ViewModels {
    public class StorePageViewModel : Screen {
        private readonly ILocalizedStringCache _stringCache;
        private readonly INavigationService _navigationService;
        private readonly IObservableCollection<StoreProductViewModel> _products;
        private readonly ButtonViewModel _freePointsButton;
        private int? _purchasedMoneyNotifying;
        
        public StorePageViewModel(
            IStoreManager storeManager,
            ILocalizedStringCache stringCache,
            INavigationService navigationService,
            ViewModelFactory.CreateProduct productFactory,
            ViewModelFactory.CreateButton buttonFactory
            ) {
            storeManager.EnsureNotNull("storeManager");
            productFactory.EnsureNotNull("productFactory");
            stringCache.EnsureNotNull("stringCache");
            buttonFactory.EnsureNotNull("buttonFactory");

            _purchasedMoneyNotifying = null;
            _stringCache = stringCache;
            _navigationService = navigationService;
            
            var productListings = storeManager.CurrencyProductListings.ToList();
            productListings.Sort((p1, p2) => {
                int moneyP1 = storeManager.GetPurchasedMoney(p1);
                int moneyP2 = storeManager.GetPurchasedMoney(p2);
                if (moneyP1 > moneyP2) {
                    return 1;
                }
                if (moneyP1 < moneyP2) {
                    return -1;
                }
                return 0;
            });
            _products = new BindableCollection<StoreProductViewModel>(
                productListings.Select(listing => productFactory(listing, this)));

            _freePointsButton = buttonFactory(true);
            _freePointsButton.TextKey = "ButtonFreePoints";
            _freePointsButton.DefaultFillKey = "brush_ButtonDefaultBackgroundFreePoints";
            _freePointsButton.ActiveFillKey = "brush_ButtonActiveBackgroundFreePoints";
            _freePointsButton.PressAction = () => _navigationService.UriFor<FreePointsPageViewModel>().Navigate();
        }

        public ButtonViewModel FreePointsButton {
            get { return _freePointsButton; }
        }

        public bool IsProductsExists {
            get { return _products.Count > 0; }
        }

        public IObservableCollection<StoreProductViewModel> Products {
            get { return _products; }
        }

        public void NotifyPurchase(int purchasedMoney) {
            if (IsActive) {
                ShowPurchaseMessage(purchasedMoney);
            }
            else {
                _purchasedMoneyNotifying = purchasedMoney;
            }
        }

        protected override void OnActivate() {
            if (_purchasedMoneyNotifying.HasValue) {
                int purchasedMoney = _purchasedMoneyNotifying.Value;
                _purchasedMoneyNotifying = null;
                ShowPurchaseMessage(purchasedMoney);
            }
            FreePointsManager.Fulfill();
        }

        private void ShowPurchaseMessage(int purchasedMoney) {
            if (purchasedMoney > 0) {
                NavigatedEventHandler goBackHandler = null;
                goBackHandler = (s, e) => {
                    _navigationService.Navigated -= goBackHandler;
                    string purchasedMoneyString = purchasedMoney.ToString(CultureInfo.InvariantCulture);
                    string message = _stringCache
                        .GetLocalizedString("UserMessage_SuccessfulPurchaseFormat")
                        .FormatWith(purchasedMoneyString);
                    DialogHelper.ShowUserMessage(message);
                };
                _navigationService.Navigated += goBackHandler;
                _navigationService.GoBack();
            }
            else {
                string message = _stringCache.GetLocalizedString("UserMessage_PurchaseNotCompleted");
                DialogHelper.ShowUserMessage(message);
            }
        }
    }
}