﻿using System.Windows.Media;
using BSC.WordLines.Data;
using BSC.WordLines.Utils;
using Caliburn.Micro;
using Action = System.Action;

namespace BSC.WordLines.Presentation.ViewModels {
    public class ButtonViewModel : PropertyChangedBase {
        private readonly bool _autoResetSelection;
        private readonly ILocalizedStringCache _stringCache;
        private readonly AbstractResourceConverter _resourceConverter;
        private readonly ISoundsManager _soundsManager;
        private string _text;
        private bool _isSelected;
        private object _content;
        private string _textKey;
        private string _defaultFillKey;
        private string _activeFillKey;
        private bool _isInternalClick;
        private bool _isEnabled;
        private bool _isTransparent;

        public ButtonViewModel(
            bool autoResetSelection,
            ILocalizedStringCache stringCache,
            AbstractResourceConverter resourceConverter,
            ISoundsManager soundsManager
            ) {
            stringCache.EnsureNotNull("stringCache");
            resourceConverter.EnsureNotNull("resourceConverter");
            soundsManager.EnsureNotNull("soundsManager");

            _stringCache = stringCache;
            _resourceConverter = resourceConverter;
            _soundsManager = soundsManager;
            _autoResetSelection = autoResetSelection;
            _isEnabled = true;
            _isSelected = false;
            _isInternalClick = false;
            _isTransparent = false;
            
            PressAction = () => { };
            DefaultFillKey = "brush_ButtonDefaultBackground";
            ActiveFillKey = "brush_ButtonActiveBackground";
            HasLeftCap = true;
            HasRightCap = true;
            LongLeftOnSelection = false;
        }

        public bool IsEnabled {
            get { return _isEnabled; }
            set {
                if (value != _isEnabled) {
                    _isEnabled = value;
                    NotifyOfPropertyChange(() => IsEnabled);
                }
            }
        }

        public bool IsTransparent {
            get { return _isTransparent; }
            set {
                if (value != _isTransparent) {
                    _isTransparent = value;
                    NotifyOfPropertyChange(() => IsTransparent);
                }
            }
        }

        public Action PressAction { get; set; }

        public bool HasLeftCap { get; set; }

        public bool HasRightCap { get; set; }

        public bool LongLeftOnSelection { get; set; }

        public string DefaultFillKey {
            get { return _defaultFillKey; }
            set {
                if (value != _defaultFillKey) {
                    _defaultFillKey = value;
                    NotifyOfPropertyChange(() => FillBrush);
                }
            }
        }

        public string ActiveFillKey {
            get { return _activeFillKey; }
            set {
                if (value != _activeFillKey) {
                    _activeFillKey = value;
                    NotifyOfPropertyChange(() => FillBrush);
                }
            }
        }

        public Brush DefaultFill {
            get { return _resourceConverter.Convert<Brush>(DefaultFillKey); }
        }

        public Brush ActiveFill {
            get { return _resourceConverter.Convert<Brush>(ActiveFillKey); }
        }

        public Brush FillBrush {
            get { return _isSelected ? ActiveFill : DefaultFill; }
        }
        
        public string Text {
            get { return _text; }
            set {
                if (_text != value) {
                    _text = value;
                    NotifyOfPropertyChange(() => Text);
                }
            }
        }

        public string TextKey {
            get { return _textKey; }
            set {
                if (_textKey != value) {
                    _textKey = value;
                    NotifyOfPropertyChange(() => TextKey);
                    Text = _stringCache.GetLocalizedString(value);
                }
            }
        }

        public object Content {
            get { return _content; }
            set {
                if (!ReferenceEquals(_content, value)) {
                    _content = value;
                    NotifyOfPropertyChange(() => Content);
                }
            }
        }

        public bool IsSelected {
            get { return _isSelected; }
            set {
                if (_isSelected != value) {
                    _isSelected = value;
                    NotifyOfPropertyChange(() => IsSelected);
                    NotifyOfPropertyChange(() => FillBrush);
                }
            }
        }

        public void RefreshText() {
            Text = _stringCache.GetLocalizedString(TextKey);
        }

        public void PressOnce() {
            if (!IsEnabled) {
                return;
            }

            _isInternalClick = true;
            StartHold();
            FinishHold();
            _isInternalClick = false;
        }

        public void StartHold() {
            if (!IsEnabled) {
                return;
            }

            if (!_isInternalClick) {
                _soundsManager.PlayClick();
            }
            IsSelected = true;
            if (!_autoResetSelection && PressAction != null) {
                PressAction();
            }
        }

        public void FinishHold() {
            if (!IsEnabled) {
                return;
            }

            if (_autoResetSelection) {
                if (PressAction != null) {
                    PressAction();
                }
                IsSelected = false;
            }
        }
    }
}