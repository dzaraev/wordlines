﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using BSC.WordLines.Data;
using BSC.WordLines.Model;
using BSC.WordLines.Utils;
using Caliburn.Micro;
using Microsoft.Phone.Controls;
using Parse;

namespace BSC.WordLines.Presentation.ViewModels {
    public class LeaderboardViewModel : Screen {
        public delegate LeaderboardViewModel Factory(ELeaderboards leaderboardType);

        private readonly ILeaderboardManager _leaderboardManager;
        private readonly ILocalizedStringCache _stringCache;
        private readonly IPlayer _player;
        private readonly SettingsManager _settingsManager;
        private readonly ELeaderboards _selectedLeaderboard;
        private readonly ButtonViewModel _allTimePeriodButton;
        private readonly ButtonViewModel _weekPeriodButton;
        private readonly ButtonViewModel _facebookSignButton;
        private IEnumerable<HighScoreViewModel> _highScores;
        private EHighScorePeriods _selectedPeriod;
        private ELanguage _selectedLanguage;
        private EGameMode _selectedGameMode;
        private bool _isFacebookInvitationVisible;

        public LeaderboardViewModel(
            ELeaderboards leaderboardType,
            ILeaderboardManager leaderboardManager,
            ViewModelFactory.CreateButton createButton,
            ILocalizedStringCache stringCache,
            IPlayer player,
            SettingsManager settingsManager
            ) {
            leaderboardManager.EnsureNotNull("leaderboardManager");
            createButton.EnsureNotNull("createButton");
            stringCache.EnsureNotNull("stringCache");
            player.EnsureNotNull("player");
            settingsManager.EnsureNotNull("settingsManager");

            _leaderboardManager = leaderboardManager;
            _stringCache = stringCache;
            _player = player;
            _settingsManager = settingsManager;

            _selectedLeaderboard = leaderboardType;
            _isFacebookInvitationVisible = leaderboardType == ELeaderboards.Facebook && !FacebookHelper.IsFacebokSigned();

            string displayNameKey = leaderboardType == ELeaderboards.Local
                                        ? "Leaderboard_Local"
                                        : (leaderboardType == ELeaderboards.Global
                                               ? "Leaderboard_Global"
                                               : "Leaderboard_Facebook");
            DisplayName = _stringCache.GetLocalizedString(displayNameKey);

            _allTimePeriodButton = createButton(false);
            _allTimePeriodButton.TextKey = "ButtonPeriod_AllTime";
            _allTimePeriodButton.DefaultFillKey = GetButtonDefaultFillKey();
            _allTimePeriodButton.ActiveFillKey = GetButtonActiveFillKey();
            _allTimePeriodButton.HasLeftCap = false;
            _allTimePeriodButton.HasRightCap = true;
            _allTimePeriodButton.PressAction = () => {
                _weekPeriodButton.IsSelected = false;
                SelectedHighScorePeriod = EHighScorePeriods.AllTime;
                UpdateHighScores();
            };
            
            _weekPeriodButton = createButton(false);
            _weekPeriodButton.TextKey = "ButtonPeriod_Week";
            _weekPeriodButton.DefaultFillKey = GetButtonDefaultFillKey();
            _weekPeriodButton.ActiveFillKey = GetButtonActiveFillKey();
            _weekPeriodButton.HasLeftCap = true;
            _weekPeriodButton.HasRightCap = false;
            _weekPeriodButton.PressAction = () => {
                _allTimePeriodButton.IsSelected = false;
                SelectedHighScorePeriod = EHighScorePeriods.Week;
                UpdateHighScores();
            };

            /*_facebookSignButton = createButton(false);
            _facebookSignButton.TextKey = "ButtonSignWithFacebook";
            _facebookSignButton.DefaultFillKey = "brush_ButtonDefaultBackgroundSocial";
            _facebookSignButton.ActiveFillKey = "brush_ButtonActiveBackgroundSocial";
            _facebookSignButton.PressAction = SignUpFacebookAsync;*/
        }

        public bool IsFacebookInvitationVisible {
            get { return _isFacebookInvitationVisible; }
            set {
                if (value != _isFacebookInvitationVisible) {
                    _isFacebookInvitationVisible = value;
                    NotifyOfPropertyChange(() => IsFacebookInvitationVisible);
                }
            }
        }

        //public ButtonViewModel FacebookSignButton { get { return _facebookSignButton; } }

        public ButtonViewModel AllTimePeriodButton {
            get { return _allTimePeriodButton; }
        }

        public ButtonViewModel WeekPeriodButton {
            get { return _weekPeriodButton; }
        }

        public EGameMode SelectedGameMode {
            get { return _selectedGameMode; }
            set {
                _selectedGameMode = value;
                _allTimePeriodButton.DefaultFillKey = GetButtonDefaultFillKey();
                _allTimePeriodButton.ActiveFillKey = GetButtonActiveFillKey();
                _weekPeriodButton.DefaultFillKey = GetButtonDefaultFillKey();
                _weekPeriodButton.ActiveFillKey = GetButtonActiveFillKey();
            }
        }

        public ELanguage SelectedLanguage {
            get { return _selectedLanguage; }
            set { _selectedLanguage = value; }
        }

        public EHighScorePeriods SelectedHighScorePeriod {
            get { return _selectedPeriod; }
            set { _selectedPeriod = value; }
        }

        public IEnumerable<HighScoreViewModel> HighScores {
            get { return _highScores; }
            protected set {
                if (!ReferenceEquals(_highScores, value)) {
                    _highScores = value;
                    NotifyOfPropertyChange(() => HighScores);
                }
            }
        }

        public void UpdateHighScores() {
            var leaderboard = _selectedLeaderboard;
            var period = _selectedPeriod;
            var gameMode = _selectedGameMode;
            var language = _selectedLanguage;

            switch (leaderboard) {
                case ELeaderboards.Local:
                    var localScoresData = _leaderboardManager.GetLocalHighScoresTop(period, gameMode, language);
                    HighScores = CreateHighScoresViewModels(localScoresData);
                    break;
                case ELeaderboards.Global:
                    var globalScoresData = _leaderboardManager.GetGlobalHighScoresTop(period, gameMode, language);
                    HighScores = CreateHighScoresViewModels(globalScoresData);
                    break;
                case ELeaderboards.Facebook:
                    if (FacebookHelper.IsFacebokSigned()) {
                        var facebookScoresData = _leaderboardManager.GetFacebookHighScoresTop(period, gameMode, language);
                        HighScores = CreateHighScoresViewModels(facebookScoresData);
                    }
                    break;
                case ELeaderboards.Twitter:
                    HighScores = new HighScoreViewModel[] {};//TODO ever
                    break;
                default:
                    string message = ErrorMessage.UnexpectedlyEnumValue(leaderboard);
                    throw new InvalidOperationException(message);
            }
        }

        protected override void OnInitialize() {
            _weekPeriodButton.PressOnce();
        }

        private string GetButtonDefaultFillKey() {
            var gameMode = SelectedGameMode;
            if (gameMode == EGameMode.Moves) {
                return "brush_ButtonDefaultBackgroundMovesMode";
            }
            if (gameMode == EGameMode.Time) {
                return "brush_ButtonDefaultBackgroundTimeMode";
            }
            throw new InvalidOperationException(ErrorMessage.UnexpectedlyEnumValue(SelectedGameMode));
        }

        private string GetButtonActiveFillKey() {
            var gameMode = SelectedGameMode;
            if (gameMode == EGameMode.Moves) {
                return "brush_ButtonActiveBackgroundMovesMode";
            }
            if (gameMode == EGameMode.Time) {
                return "brush_ButtonActiveBackgroundTimeMode";
            }
            throw new InvalidOperationException(ErrorMessage.UnexpectedlyEnumValue(SelectedGameMode));
        }

        private IEnumerable<HighScoreViewModel> CreateHighScoresViewModels(IEnumerable<object> scoresData) {
            var viewModels = new List<HighScoreViewModel>();
            var scoresDataList = scoresData.ToList();
            for (int i = 0; i < scoresDataList.Count; i++) {
                var data = scoresDataList[i];
                int order = i + 1;

                var localHighScoreData = data as ILocalHighScoreData;
                if (localHighScoreData != null) {
                    viewModels.Add(new HighScoreViewModel {
                        Order = order,
                        Score = localHighScoreData.Score,
                        PlayerName = _player.DisplayName,
                    });
                    continue;
                }
                var globalHighScore = data as IGlobalHighScoreData;
                if (globalHighScore != null) {
                    viewModels.Add(new HighScoreViewModel {
                        Order = order,
                        Score = globalHighScore.Score,
                        PlayerName = globalHighScore.PlayerName,
                    });
                    continue;
                }
                var facebookHighScore = data as IFacebookHighScoreData;
                if (facebookHighScore != null) {
                    viewModels.Add(new HighScoreViewModel {
                        Order = order,
                        Score = facebookHighScore.Score,
                        PlayerName = facebookHighScore.PlayerName
                    });
                }
                Diagnost.Assert(false, ErrorMessage.UnrecognizedHighScoreData);
                viewModels.Add(new HighScoreViewModel {
                    Order = order,
                    Score = -1,
                });
            }
            return viewModels;
        }

        //TODO facebook future
        /*private async void SignUpFacebookAsync() {
            return;
            if (!FacebookHelper.IsFacebokSigned()) {
                Session.ActiveSession.LoginWithBehavior(
                    "basic_info,email,public_profile,user_friends", FacebookLoginBehavior.LoginBehaviorApplicationOnly);

                //var browser = new WebBrowser();
                /*var facebookLogInPanel = GetFacebookPanel();
                var loginButton = new LoginButton();
                facebookLogInPanel.Content = loginButton;
                loginButton.SessionStateChanged += HandleLoginButtonSessionStateChanged;
                facebookLogInPanel.Visibility = Visibility.Visible;
#1#
                /*try {
                                    facebookLogInPanel.Visibility = Visibility.Visible;
                                    var user = ParseUser.CurrentUser;
                                    await ParseFacebookUtils.LinkAsync(user, browser, null);
                                    await ParseFacebookUtils.LinkAsync(,,)
                                }
                                catch {
                                    _facebookSignButton.IsSelected = false;
                                    return;
                                }

                                await FacebookHelper.SyncPlayerFacebookUserAsync();
                                await _leaderboardManager.SynchronizeHighScoresWithServer(_settingsManager.CurrentLanguage);
                
                                if (IsActive) {
                                    facebookLogInPanel.Visibility = Visibility.Collapsed;
                                    UpdateHighScores();
                                }#1#
            }
        }

        private ContentControl GetFacebookPanel() {
            var view = (UserControl) GetView();
            var facebookLogInPanel = (ContentControl) view.FindName("FacebookLogInPanel");
            return facebookLogInPanel;
        }*/
    }
}