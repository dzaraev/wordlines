﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Windows;
using BSC.WordLines.Data;
using BSC.WordLines.Model;
using BSC.WordLines.Utils;
using Caliburn.Micro;

namespace BSC.WordLines.Presentation.ViewModels {
    public class SettingsPageViewModel : Screen {
        private const string DEFAULT_PLAYER_NAME = "Incognito";
        private readonly IPlayer _playerModel;
        private readonly SettingsManager _settingsManager;
        private readonly ILocalDataProvider _dataProvider;
        private readonly ISoundsManager _soundsManager;
        private readonly IGameStateSaver _gameStateSaver;
        private readonly IWordDictionary _wordDictionary;
        private readonly IAlphabet _alphabet;
        private readonly ILocalizedStringCache _localizedStringCache;
        private readonly ButtonViewModel _saveButton;
        private readonly List<IThemeData> _themes;
        private readonly List<ILanguageData> _languages;
        private string _playerName;
        private IThemeData _selectedTheme;
        private ILanguageData _selectedLanguage;

        public SettingsPageViewModel(
            IPlayer playerModel,
            INavigationService navigationService,
            SettingsManager settingsManager,
            ILocalDataProvider dataProvider,
            ISoundsManager soundsManager,
            IGameStateSaver gameStateSaver,
            ILocalizedStringCache localizedStringCache,
            IWordDictionary wordDictionary,
            IAlphabet alphabet,
            ViewModelFactory.CreateButton createButton
            ) {
            playerModel.EnsureNotNull("playerModel");
            createButton.EnsureNotNull("createButton");
            dataProvider.EnsureNotNull("dataProvider");
            settingsManager.EnsureNotNull("settingsManager");
            soundsManager.EnsureNotNull("soundsManager");
            gameStateSaver.EnsureNotNull("gameStateSaver");
            wordDictionary.EnsureNotNull("wordDictionary");
            localizedStringCache.EnsureNotNull("localizedStringCache");
            alphabet.EnsureNotNull("alphabet");
            navigationService.EnsureNotNull("navigationService");

            _playerModel = playerModel;
            _settingsManager = settingsManager;
            _dataProvider = dataProvider;
            _soundsManager = soundsManager;
            _gameStateSaver = gameStateSaver;
            _wordDictionary = wordDictionary;
            _alphabet = alphabet;
            _localizedStringCache = localizedStringCache;
            _playerName = DEFAULT_PLAYER_NAME;

            _saveButton = createButton(true);
            _saveButton.TextKey = "ButtonSave";
            _saveButton.PressAction = navigationService.GoBack;

            _themes = new List<IThemeData>(_dataProvider.GetAllThemes().OrderBy(d => d.Id));
            _selectedTheme = _themes.FirstOrDefault(theme => theme.Id == _settingsManager.CurrentSettings.CurrentThemeId);

            _languages = new List<ILanguageData>(_dataProvider.GetAllLanguages().OrderBy(l => l.Id));
            _selectedLanguage = _languages.FirstOrDefault(language => language.Id == _settingsManager.CurrentSettings.CurrentLanguageId);
        }

        public ButtonViewModel SaveButton {
            get { return _saveButton; }
        }

        public List<IThemeData> Themes {
            get { return _themes; }
        }

        public IThemeData SelectedTheme {
            get { return _selectedTheme; }
            set {
                if (!ReferenceEquals(value, _selectedTheme)) {
                    _selectedTheme = value;
                    _settingsManager.CurrentSettings.CurrentThemeId = value.Id;
                    NotifyOfPropertyChange(() => SelectedTheme);
                    InvalidateTheme();
                }
            }
        }

        public List<ILanguageData> Languages {
            get { return _languages; }
        }

        public ILanguageData SelectedLanguage {
            get { return _selectedLanguage; }
            set {
                if (!ReferenceEquals(value, _selectedLanguage)) {
                    _selectedLanguage = value;
                    _settingsManager.CurrentSettings.CurrentLanguageId = value.Id;
                    NotifyOfPropertyChange(() => SelectedLanguage);
                    InvalidateLanguage();
                }
            }
        }

        public string PlayerName {
            get { return _playerName; }
            set {
                if (_playerName != value) {
                    _playerName = value;
                    NotifyOfPropertyChange(() => PlayerName);
                }
            }
        }

        public bool IsSoundsEnabled {
            get { return _settingsManager.CurrentSettings.IsSoundsEnabled; }
            set {
                if (value != _settingsManager.CurrentSettings.IsSoundsEnabled) {
                    _settingsManager.CurrentSettings.IsSoundsEnabled = value;
                    NotifyOfPropertyChange(() => IsSoundsEnabled);
                }
            }
        }

        public void CheckIsSoundsEnabled() {
            IsSoundsEnabled = true;
        }

        public void UncheckIsSoundsEnabled() {
            IsSoundsEnabled = false;
        }

        protected override void OnActivate() {
            PlayerName = _playerModel.DisplayName;
        }

        protected override void OnDeactivate(bool close) {
            _settingsManager.SaveInDb();

            string newPlayerName = !string.IsNullOrWhiteSpace(_playerName) ? _playerName : DEFAULT_PLAYER_NAME;
            newPlayerName = newPlayerName.Trim();
            if (!string.Equals(_playerModel.DisplayName, newPlayerName)) {
                _playerModel.DisplayName = newPlayerName;
            }
        }

        private void InvalidateTheme() {
            _settingsManager.SaveInDb();
            AbstractResourceConverter.UpdateCache();
            _soundsManager.Update();
            
            Resources.ReAssignDeep((DependencyObject)GetView());
            _saveButton.Refresh();
        }
        
        private async void InvalidateLanguage() {
            _settingsManager.SaveInDb();
            await _wordDictionary.UpdateAsync();
            _alphabet.Update();
            _localizedStringCache.Update();
            _gameStateSaver.DeleteSavedGame();

            Text.ReAssignDeep((DependencyObject) GetView());
            _saveButton.RefreshText();
        }
    }
}