﻿using System;
using BSC.WordLines.Data;
using BSC.WordLines.Model;
using BSC.WordLines.Utils;
using Caliburn.Micro;
using Microsoft.Phone.Tasks;

namespace BSC.WordLines.Presentation.ViewModels {
    public class FreePointsPromotionPageViewModel : Screen {
        private readonly ILocalizedStringCache _localizedStringCache;
        private readonly string _buttonAwardMessageFormat;
        private readonly string _bannerAwardMessageFormat;
        private readonly string _clickOurAdsFormat;
        private readonly SettingsManager _settingsManager;

        public ButtonViewModel ReviewAppButton { get; set; }
        public ButtonViewModel ShareAppButton { get; set; }
        public ButtonViewModel SocialButton { get; set; }
        public ButtonViewModel TwitterButton { get; set; }
        public ButtonViewModel SponsorButton { get; set; }

        public FreePointsPromotionPageViewModel(
            ViewModelFactory.CreateButton buttonFactory,
            ILocalizedStringCache localizedStringCache
            ) {
            localizedStringCache.EnsureNotNull("localizedStringCache");

            _localizedStringCache = localizedStringCache;
            _settingsManager = IoC.Get<SettingsManager>();

            ReviewAppButton = buttonFactory(true);
            ReviewAppButton.TextKey = "ButtonReviewApp";
            ReviewAppButton.PressAction = ReviewHelper.ReviewAppAndShutUp;

            ShareAppButton = buttonFactory(true);
            ShareAppButton.TextKey = "ButtonShareApp";
            ShareAppButton.DefaultFillKey = "brush_ButtonDefaultBackgroundBuyBooster";
            ShareAppButton.ActiveFillKey = "brush_ButtonActiveBackgroundBuyBooster";
            ShareAppButton.PressAction = ShareApp;

            SocialButton = buttonFactory(true);
            SocialButton.TextKey = "ButtonSocialFreePoints";
            SocialButton.DefaultFillKey = "brush_ButtonDefaultBackgroundSocial";
            SocialButton.ActiveFillKey = "brush_ButtonActiveBackgroundSocial";
            SocialButton.PressAction = () => {
                FreePointsManager.OnSocialFollowed();
                Uri socialUri;
                if (_settingsManager.CurrentLanguage.Name == "Russian") {
                    socialUri = new Uri(GameConst.VkLink);
                }
                else {
                    socialUri = new Uri(GameConst.FbLink);
                }
                var web = new WebBrowserTask {Uri = socialUri};
                web.Show();
            };

            TwitterButton = buttonFactory(true);
            TwitterButton.TextKey = "ButtonTwitterFreePoints";
            TwitterButton.DefaultFillKey = "brush_ButtonDefaultBackgroundTimeMode";
            TwitterButton.ActiveFillKey = "brush_ButtonActiveBackgroundTimeMode";
            TwitterButton.PressAction = () => {
                FreePointsManager.OnTwitterFollowed();
                var web = new WebBrowserTask {Uri = new Uri(GameConst.TwitterLink)};
                web.Show();
            };

            SponsorButton = buttonFactory(true);
            SponsorButton.Text = SponsorHelper.SponsorButtonText;
            SponsorButton.DefaultFillKey = "brush_ButtonDefaultBackgroundSocial";
            SponsorButton.ActiveFillKey = "brush_ButtonActiveBackgroundSocial";
            SponsorButton.PressAction = () => {
                FreePointsManager.OnSponsorFollowed();
                var web = new WebBrowserTask {Uri = SponsorHelper.SponsorLinkUri};
                web.Show();
            };

            _buttonAwardMessageFormat = localizedStringCache.GetLocalizedString("FreePointsAwardFormat");
            _bannerAwardMessageFormat = localizedStringCache.GetLocalizedString("FreePointsAwardForBannerFormat");
            _clickOurAdsFormat = localizedStringCache.GetLocalizedString("FreePointsClickOurAdsFormat");
        }

        public bool IsReviewAppEnabled {
            get { return !_settingsManager.CurrentSettings.IsAppReviewed; }
        }

        public bool IsShareAppEnabled {
            get { return !bool.Parse(_settingsManager.DynamicSettings[EDynamicSetting.IsAppShared].Value); }
        }

        public bool IsSocialFollowEnabled {
            get { return !bool.Parse(_settingsManager.DynamicSettings[EDynamicSetting.IsVkFollowed].Value); }
        }

        public bool IsTwitterFollowEnabled {
            get { return !bool.Parse(_settingsManager.DynamicSettings[EDynamicSetting.IsTwitterFollowed].Value); }
        }

        public bool IsSponsorFollowEnabled {
            get {
                return
                    _settingsManager.CurrentLanguage.Name == "Russian" &&
                    !bool.Parse(_settingsManager.DynamicSettings[EDynamicSetting.IsSponsorFollowed].Value);
            }
        }

        public string ReviewAppAward {
            get {
                return
                    _buttonAwardMessageFormat.FormatWith(
                        _settingsManager.DynamicSettings[EDynamicSetting.AppReviewAwardPoints].Value);
            }
        }

        public string ShareAppAward {
            get {
                return
                    _buttonAwardMessageFormat.FormatWith(
                        _settingsManager.DynamicSettings[EDynamicSetting.AppSharedAwardPoints].Value);
            }
        }

        public string SocialFollowedAward {
            get {
                return
                    _buttonAwardMessageFormat.FormatWith(
                        _settingsManager.DynamicSettings[EDynamicSetting.VkFollowedAwardPoints].Value);
            }
        }

        public string TwitterFollowedAward {
            get {
                return
                    _buttonAwardMessageFormat.FormatWith(
                        _settingsManager.DynamicSettings[EDynamicSetting.TwitterFollowedAwardPoints].Value);
            }
        }

        public string SponsorFollowedAward {
            get {
                return
                    _buttonAwardMessageFormat.FormatWith(
                        _settingsManager.DynamicSettings[EDynamicSetting.SponsorFollowedAwardPoints].Value);
            }
        }

        public string BannerClickAward {
            get {
                return
                    _bannerAwardMessageFormat.FormatWith(
                        _settingsManager.DynamicSettings[EDynamicSetting.BannerClickAwardPoints].Value);
            }
        }

        public string ClickOurAdsMessage {
            get {
                return _clickOurAdsFormat.FormatWith(
                    _settingsManager.DynamicSettings[EDynamicSetting.BannerClickAwardPoints].Value);
            }
        }

        protected override void OnActivate() {
            FreePointsManager.Fulfill();
            Refresh();
        }
        
        private void ShareApp() {
            FreePointsManager.OnShared();

            var shareLinkTask = new ShareLinkTask {
                Title = _localizedStringCache.GetLocalizedString("ShareAppTaskTitle"),
                LinkUri = new Uri(GameConst.AppLink, UriKind.Absolute),
                Message = _localizedStringCache.GetLocalizedString("ShareAppTaskMessage")
            };
            shareLinkTask.Show();
        }
    }
}