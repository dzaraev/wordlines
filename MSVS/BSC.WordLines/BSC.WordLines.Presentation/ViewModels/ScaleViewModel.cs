﻿using System;
using System.Windows.Media;
using BSC.WordLines.Model;
using BSC.WordLines.Utils;
using Caliburn.Micro;

namespace BSC.WordLines.Presentation.ViewModels {
    public class ScaleViewModel : PropertyChangedBase {
        private readonly Brush _defaultFill;
        private readonly Brush _shortFill;
        private readonly Brush _mediumFill;
        private readonly Brush _longFill;
        private readonly IGameViewModel _game;

        public ScaleViewModel(IGameViewModel game) {
            game.EnsureNotNull("game");
            _game = game;
            _game.BallChain.CollectionChanged += (s, e) => NotifyOfPropertyChange("Item");
            var resourceConverter = IoC.Get<AbstractResourceConverter>();
            resourceConverter.EnsureNotNull("resourceConverter");
            _defaultFill = resourceConverter.Convert<Brush>("brush_GameBackground");
            _shortFill = resourceConverter.Convert<Brush>("brush_ScaleBackgroundShortWord");
            _mediumFill = resourceConverter.Convert<Brush>("brush_ScaleBackgroundMediumWord");
            _longFill = resourceConverter.Convert<Brush>("brush_ScaleBackgroundLongWord");
        }

        public Brush this[int unit] {
            get {
                if (unit < 0) {
                    throw new ArgumentOutOfRangeException("unit");
                }
                int unitLettersCount = unit + 1;
                if (unitLettersCount <= _game.BallChain.Count) {
                    switch (GameHelper.GetWordLength(unitLettersCount)) {
                        case EWordLength.Short: return _shortFill;
                        case EWordLength.Medium: return _mediumFill;
                        case EWordLength.Long: return _longFill;
                        default: return _defaultFill;
                    }
                }
                return _defaultFill;
            }
        }
    }
}