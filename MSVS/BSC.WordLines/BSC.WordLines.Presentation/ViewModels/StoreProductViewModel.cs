﻿using BSC.WordLines.Model;
using BSC.WordLines.Utils;
using Caliburn.Micro;
using Windows.ApplicationModel.Store;

namespace BSC.WordLines.Presentation.ViewModels {
    public class StoreProductViewModel : PropertyChangedBase, IHaveDisplayName {
        private readonly ProductListing _productListing;
        private readonly StorePageViewModel _storePage;
        private readonly IStoreManager _storeManager;
        private string _displayName;

        public StoreProductViewModel(
            ProductListing productListing,
            StorePageViewModel storePage,
            ViewModelFactory.CreateButton buttonFactory,
            IStoreManager storeManager,
            ILocalizedStringCache stringCache
            ) {
            buttonFactory.EnsureNotNull("buttonFactory");
            storePage.EnsureNotNull("storePage");
            productListing.EnsureNotNull("productListing");
            storeManager.EnsureNotNull("storeManager");
            stringCache.EnsureNotNull("stringCache");

            _productListing = productListing;
            _storePage = storePage;
            _storeManager = storeManager;

            PurchaseButton = buttonFactory(true);
            PurchaseButton.Text = productListing.FormattedPrice;
            PurchaseButton.PressAction = Purchase;

            DisplayName = stringCache
                .GetLocalizedString("MoneyProductDisplayNameFormat")
                .FormatWith(storeManager.GetPurchasedMoney(productListing));
        }

        public string DisplayName {
            get { return _displayName; }
            set {
                if (value != _displayName) {
                    _displayName = value;
                    NotifyOfPropertyChange(() => DisplayName);
                }
            }
        }

        public ButtonViewModel PurchaseButton { get; private set; }

        public async void Purchase() {
            int purchasedMoney = await _storeManager.PurchaseMoney(_productListing);
            if (purchasedMoney <= 0) {
                PurchaseButton.IsEnabled = true;
            }
            _storePage.NotifyPurchase(purchasedMoney);
        }
    }
}