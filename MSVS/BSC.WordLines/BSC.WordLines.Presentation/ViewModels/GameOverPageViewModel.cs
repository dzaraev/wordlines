﻿using BSC.WordLines.Data;
using BSC.WordLines.Model;
using BSC.WordLines.Utils;
using Caliburn.Micro;

namespace BSC.WordLines.Presentation.ViewModels {
    public class GameOverPageViewModel : Conductor<IScreen>.Collection.AllActive {
        private readonly RoundResultsViewModel _roundResults;
        private readonly LeaderboardListViewModel _leaderboardList;
        private readonly ILocalDataProvider _provider;
        private readonly IGame _gameModel;
        private readonly ButtonViewModel _playAgainButton;
        private EGameMode _gameMode;
        
        public GameOverPageViewModel(
            RoundResultsViewModel roundResults, 
            LeaderboardListViewModel leaderboardList, 
            ILocalDataProvider provider,
            LanguageDataMapper languageDataMapper,
            INavigationService navigationService,
            IGame gameModel,
            ViewModelFactory.CreateButton createButton
            ) {
            roundResults.EnsureNotNull("roundResults");
            leaderboardList.EnsureNotNull("leaderboardList");
            provider.EnsureNotNull("provider");
            languageDataMapper.EnsureNotNull("languageDataMapper");
            gameModel.EnsureNotNull("gameModel");
            createButton.EnsureNotNull("createButton");

            _roundResults = roundResults;
            _leaderboardList = leaderboardList;
            _provider = provider;
            _gameModel = gameModel;
            Items.Add(_roundResults);
            Items.Add(_leaderboardList);

            var currentLanguage = _provider.GetCurrentLanguage();
            ELanguage language = languageDataMapper.GetLanguage(currentLanguage);
            _leaderboardList.SelectLanguage(language);

            _playAgainButton = createButton(true);
            _playAgainButton.TextKey = "ButtonPlayAgain";
            _playAgainButton.PressAction = () => {
                _gameModel.Stop();
                navigationService.UriFor<GamePageViewModel>()
                    .WithParam(page => page.IsNewGamePending, true)
                    .Navigate();
                navigationService.RemoveBackEntry();
            };
        }

        protected override void OnActivate() {
            base.OnActivate();
            ReviewHelper.OfferAppReviewIfNeed();
        }

        public RoundResultsViewModel RoundResults {
            get { return _roundResults; }
        }

        public LeaderboardListViewModel LeaderboardList {
            get { return _leaderboardList; }
        }

        public ButtonViewModel PlayAgainButton {
            get { return _playAgainButton; }
        }

        public EGameMode GameMode {
            get { return _gameMode; }
            set {
                _gameMode = value;
                _leaderboardList.SelectGameMode(value);
            }
        }
    }
}