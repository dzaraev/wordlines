﻿using BSC.WordLines.Data;
using BSC.WordLines.Model;
using BSC.WordLines.Utils;
using Caliburn.Micro;
using Microsoft.Phone.Shell;

namespace BSC.WordLines.Presentation.ViewModels {
    public class GamePageViewModel : Conductor<GameViewModel> {
        private readonly GameViewModel _game;
        private readonly IPhoneService _phoneService;

        public GamePageViewModel(GameViewModel game, IPhoneService phoneService) {
            game.EnsureNotNull("game");
            phoneService.EnsureNotNull("phoneService");
            _game = game;
            _phoneService = phoneService;
            ActiveItem = game;
        }

        /// <summary>
        /// Gets instance of <see cref="GameViewModel"/>
        /// placed on the page. Cannot be <c>null</c>.
        /// </summary>
        public GameViewModel Game {
            get { return _game; }
        }

        /// <summary>
        /// When <c>true</c> a new game session will start 
        /// after UI for the <see cref="Game"/> referenced instance
        /// will be loaded.
        /// </summary>
        public bool IsNewGamePending {
            get { return _game.IsNewGamePending; }
            set { _game.IsNewGamePending = value; }
        }

        protected override void OnActivate() {
            base.OnActivate();
            if (_game.Mode == EGameMode.Moves) {
                _phoneService.UserIdleDetectionMode = IdleDetectionMode.Disabled;
            }
        }

        protected override void OnDeactivate(bool close) {
            base.OnDeactivate(close);
            if (_game.Mode == EGameMode.Moves) {
                _phoneService.UserIdleDetectionMode = IdleDetectionMode.Enabled;
            }
        }
    }
}