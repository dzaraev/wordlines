﻿using System.Text;
using BSC.WordLines.Data;
using BSC.WordLines.Utils;
using Caliburn.Micro;

namespace BSC.WordLines.Presentation.ViewModels {
    public class FreePointsPageViewModel : Screen {
        private readonly string _clickOurAdsFormat;
        private readonly SettingsManager _settingsManager;

        public ButtonViewModel FreePointsPromotionButton { get; set; }

        public FreePointsPageViewModel(
            ViewModelFactory.CreateButton buttonFactory,
            ILocalizedStringCache localizedStringCache,
            INavigationService navigationService
            ) {
            localizedStringCache.EnsureNotNull("localizedStringCache");
            navigationService.EnsureNotNull("navigationService");

            _settingsManager = IoC.Get<SettingsManager>();

            FreePointsPromotionButton = buttonFactory(true);
            FreePointsPromotionButton.TextKey = "ButtonFreePointsPromotion";
            FreePointsPromotionButton.DefaultFillKey = "brush_ButtonDefaultBackgroundBuyBooster";
            FreePointsPromotionButton.ActiveFillKey = "brush_ButtonActiveBackgroundBuyBooster";
            FreePointsPromotionButton.PressAction = () => navigationService.UriFor<FreePointsPromotionPageViewModel>().Navigate();

            _clickOurAdsFormat = localizedStringCache.GetLocalizedString("FreePointsClickOurAdsFormat");
        }

        public string ClickOurAdsMessage {
            get {
                return _clickOurAdsFormat.FormatWith(
                    _settingsManager.DynamicSettings[EDynamicSetting.BannerClickAwardPoints].Value);
            }
        }

        public bool IsSponsorsButtonEnabled {
            get {
                return
                    !_settingsManager.CurrentSettings.IsAppReviewed ||
                    !bool.Parse(_settingsManager.DynamicSettings[EDynamicSetting.IsAppShared].Value) ||
                    !bool.Parse(_settingsManager.DynamicSettings[EDynamicSetting.IsVkFollowed].Value) ||
                    !bool.Parse(_settingsManager.DynamicSettings[EDynamicSetting.IsTwitterFollowed].Value) ||
                    !bool.Parse(_settingsManager.DynamicSettings[EDynamicSetting.IsSponsorFollowed].Value);
            }
        }

        protected override void OnActivate() {
            FreePointsManager.Fulfill();
            Refresh();
        }
    }
}