﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Globalization;
using System.Windows;
using System.Windows.Media;
using System.Windows.Navigation;
using BSC.WordLines.Data;
using BSC.WordLines.Model;
using BSC.WordLines.Utils;
using Caliburn.Micro;

namespace BSC.WordLines.Presentation.ViewModels {
    public class MainMenuPageViewModel : Screen {
        private readonly ILeaderboardManager _leaderboardManager;
        private readonly INavigationService _navigationService;
        private readonly IGame _game;
        private readonly AbstractResourceConverter _resourceConverter;
        private readonly ISoundsManager _soundsManager;
        private readonly SettingsManager _settingsManager;
        private readonly IGameStateSaver _gameStateSaver;
        private readonly MovesModeSymbolViewModel _movesModeSymbol;
        private readonly TimeModeSymbolViewModel _timeModeSymbol;
        private readonly ButtonViewModel _movesModeButton;
        private readonly ButtonViewModel _timeModeButton;
        private readonly ButtonViewModel _newGameButton;
        private readonly ButtonViewModel _continueGameButton;
        private readonly ButtonViewModel _nextThemeButton;
        private readonly List<IThemeData> _availableThemes;
        private EGameMode _selectedMode;
        private bool _isContinueEnabled;
        private string _tooltip;
        private int _currentThemeId;
        private int _currentLanguageId;

        public MainMenuPageViewModel(
            ILeaderboardManager leaderboardManager,
            INavigationService navigationService,
            IGame game,
            ViewModelFactory.CreateButton createButton,
            AbstractResourceConverter resourceConverter,
            ILocalizedStringCache localizedStringCache,
            ISoundsManager soundsManager,
            SettingsManager settingsManager,
            IGameStateSaver gameStateSaver,
            ILocalDataProvider localDataProvider
            ) {
            leaderboardManager.EnsureNotNull("leaderboardManager");
            navigationService.EnsureNotNull("navigationService");
            game.EnsureNotNull("game");
            createButton.EnsureNotNull("createButton");
            resourceConverter.EnsureNotNull("resourceConverter");
            localizedStringCache.EnsureNotNull("localizedStringCache");
            soundsManager.EnsureNotNull("soundsManager");
            settingsManager.EnsureNotNull("settingsManager");
            gameStateSaver.EnsureNotNull("gameStateSaver");
            localDataProvider.EnsureNotNull("localDataProvider");

            _leaderboardManager = leaderboardManager;
            _navigationService = navigationService;
            _game = game;
            _resourceConverter = resourceConverter;
            _soundsManager = soundsManager;
            _settingsManager = settingsManager;
            _gameStateSaver = gameStateSaver;
            _timeModeSymbol = new TimeModeSymbolViewModel();
            _movesModeSymbol = new MovesModeSymbolViewModel();
            _availableThemes = new List<IThemeData>(localDataProvider.GetAllThemes().OrderBy(t => t.Id));
            _currentThemeId = _settingsManager.CurrentSettings.CurrentThemeId;
            _currentLanguageId = _settingsManager.CurrentSettings.CurrentLanguageId;

            _timeModeButton = createButton(false);
            _timeModeButton.Content = new TimeModeSymbolViewModel();
            _timeModeButton.PressAction = () => {
                //Order does matter.
                _movesModeButton.IsSelected = false;
                SelectedMode = EGameMode.Time;
                Tooltip = localizedStringCache.GetLocalizedString("TooltipGameMode_Time");
            };

            _movesModeButton = createButton(false);
            _movesModeButton.Content = new MovesModeSymbolViewModel();
            _movesModeButton.PressAction = () => {
                //Order does matter.
                _timeModeButton.IsSelected = false;
                SelectedMode = EGameMode.Moves;
                Tooltip = localizedStringCache.GetLocalizedString("TooltipGameMode_Moves");
            };

            _newGameButton = createButton(true);
            _newGameButton.TextKey = "ButtonStartNewGame";
            _newGameButton.PressAction = StartNewGame;

            _continueGameButton = createButton(true);
            _continueGameButton.TextKey = "ButtonContinueGame";
            _continueGameButton.PressAction = ContinueGame;

            _nextThemeButton = createButton(true);
            _nextThemeButton.IsTransparent = true;
            _nextThemeButton.TextKey = GetNextThemeButtonTextKey();
            _nextThemeButton.PressAction = SwitchNextTheme;

            _isContinueEnabled = false;
            MovesModeButton.PressOnce();
        }

        public event EventHandler ThemeInvalidated = delegate { };

        public event EventHandler LanguageInvalidated = delegate { };

        public static int PurchasedMoneyNotifying { get; set; }

        public static bool NavigationTroubleOccurred { get; set; }//Issue #73

        public EGameMode SelectedMode {
            get { return _selectedMode; }
            private set {
                _selectedMode = value;
                NotifyOfPropertyChange(() => SelectedMode);
            }
        }

        public object TimeModeSymbol {
            get { return _timeModeSymbol; }
        }

        public object MovesModeSymbol {
            get { return _movesModeSymbol; }
        }

        public string Tooltip {
            get { return _tooltip; }
            set {
                _tooltip = value;
                NotifyOfPropertyChange(() => Tooltip);
                NotifyOfPropertyChange(() => TooltipForeground);
            }
        }

        public Brush TooltipForeground {
            get { return _resourceConverter.Convert<Brush>("brush_ShowTooltipForeground{0}Mode".FormatWith(SelectedMode)); }
        }

        public ButtonViewModel TimeModeButton {
            get { return _timeModeButton; }
        }

        public ButtonViewModel MovesModeButton {
            get { return _movesModeButton; }
        }

        public ButtonViewModel NewGameButton {
            get { return _newGameButton; }
        }

        public ButtonViewModel ContinueGameButton {
            get { return _continueGameButton; }
        }

        public ButtonViewModel NextThemeButton {
            get { return _nextThemeButton; }
        }

        public bool IsContinueEnabled {
            get { return _isContinueEnabled; }
            private set {
                if (_isContinueEnabled != value) {
                    _isContinueEnabled = value;
                    NotifyOfPropertyChange(() => IsContinueEnabled);
                }
            }
        }

        public void ShowLeaderboards() {
            DisableButtons();
            _soundsManager.PlayClick();
            var currentLanguage = _settingsManager.CurrentLanguage;
            if (_leaderboardManager.IsRefreshTimeoutExpired(currentLanguage)) {
                int waitingKey = UIHelp.CreateWaitingKey();
                NavigatedEventHandler waitingHandler = null;
                waitingHandler = async (sender, e) => {
                    _navigationService.Navigated -= waitingHandler;
                    if (!_navigationService.IsWaitingForKey(waitingKey)) {
                        return;
                    }
                    await _leaderboardManager.SynchronizeHighScoresWithServer(currentLanguage);
                    if (!_navigationService.IsWaitingForKey(waitingKey)) {
                        return;
                    }

                    _navigationService.UriFor<LeaderboardsPageViewModel>().Navigate();
                    _navigationService.RemoveBackEntry();
                };
                _navigationService.Navigated += waitingHandler;
                _navigationService.UriFor<WaitingPageViewModel>()
                    .WithParam(vm => vm.WaitingKey, waitingKey)
                    .Navigate();
            }
            else {
                _navigationService.UriFor<LeaderboardsPageViewModel>().Navigate();
            }
        }

        public void ShowSettings() {
            DisableButtons();
            _soundsManager.PlayClick();
            _navigationService.UriFor<SettingsPageViewModel>().Navigate();
        }

        public void ShowBoosters() {
            DisableButtons();
            _soundsManager.PlayClick();
            _navigationService.UriFor<BoostersPageViewModel>().Navigate();
        }

        public void ShowMisc() {
            DisableButtons();
            _soundsManager.PlayClick();
            _navigationService.UriFor<MiscPageViewModel>().Navigate();
        }

        public void SwitchNextTheme() {
            int nextThemeIndex = GetNextAvailableThemeIndex();
            int nextThemeId = _availableThemes[nextThemeIndex].Id;
            _settingsManager.CurrentSettings.CurrentThemeId = nextThemeId;
            _settingsManager.SaveInDb();
            AbstractResourceConverter.UpdateCache();
            _soundsManager.Update();

            InvalidateNextThemeButton();
            InvalidateTheme();
            _currentThemeId = nextThemeId;
        }

        protected override void OnViewReady(object view) {
            base.OnViewReady(view);

            if (PurchasedMoneyNotifying > 0) {
                string purchasedMoneyString = PurchasedMoneyNotifying.ToString(CultureInfo.InvariantCulture);
                PurchasedMoneyNotifying = 0;
                string message = IoC.Get<ILocalizedStringCache>()
                    .GetLocalizedString("UserMessage_SuccessfulPurchaseFormat")
                    .FormatWith(purchasedMoneyString);
                DialogHelper.ShowUserMessage(message);
            }

            //Такое извращение из-за того, что если вызывать подряд,
            //то будет показан только второй диалог.
            FreePointsManager.Fulfill(FreePointsManager.NotifyAboutFreePointsFeature());

            if (NavigationTroubleOccurred) {
                NavigationTroubleOccurred = false;
                Diagnost.LogAsync(ErrorMessage.NavigationTroubleOccuredButAllRight, ELogType.Info);
            }
        }

        protected override void OnActivate() {
            if (_game.State == EGameState.NotStarted) {
                ELanguage loadedGameLanguage;
                _gameStateSaver.TryLoadGame(_game, out loadedGameLanguage);
            }
            IsContinueEnabled = _game.State == EGameState.InProcess;

            //Synchronize theme of possibly loaded page with current settings.
            int settingsCurrentThemeId = _settingsManager.CurrentSettings.CurrentThemeId;
            if (_currentThemeId != settingsCurrentThemeId) {
                InvalidateNextThemeButton();
                InvalidateTheme();
                _currentThemeId = settingsCurrentThemeId;
            }

            //Synchronize language of possibly loaded page with current settings.
            int settingsCurrentLanguageId = _settingsManager.CurrentSettings.CurrentLanguageId;
            if (_currentLanguageId != settingsCurrentLanguageId) {
                IsContinueEnabled = false;
                InvalidateLanguage();
                _currentLanguageId = settingsCurrentLanguageId;
            }

            EnableButtons();
        }

        private void StartNewGame() {
            DisableButtons();

            _game.Stop();
            _game.Mode = SelectedMode;
            if (_settingsManager.CurrentSettings.IsTutorialConfirmed) {
                _navigationService
                    .UriFor<GamePageViewModel>()
                    .WithParam(page => page.IsNewGamePending, true)
                    .Navigate();
            }
            else {
                _navigationService
                    .UriFor<TutorialPageViewModel>()
                    .WithParam(page => page.IsNewGamePending, true)
                    .Navigate();
            }
        }

        private void ContinueGame() {
            DisableButtons();

            _navigationService
                .UriFor<GamePageViewModel>()
                .Navigate();
        }

        private void EnableButtons() {
            MovesModeButton.IsEnabled = true;
            TimeModeButton.IsEnabled = true;
            NewGameButton.IsEnabled = true;
            ContinueGameButton.IsEnabled = true;
        }

        //In fact prevents press other buttons while navigating.
        private void DisableButtons() {
            MovesModeButton.IsEnabled = false;
            TimeModeButton.IsEnabled = false;
            NewGameButton.IsEnabled = false;
            ContinueGameButton.IsEnabled = false;
        }

        private string GetNextThemeButtonTextKey() {
            int currentThemeId = _settingsManager.CurrentSettings.CurrentThemeId;
            var currentTheme = _availableThemes.FirstOrDefault(t => t.Id == currentThemeId);
            int currentThemeIndex = currentTheme != null ? _availableThemes.IndexOf(currentTheme) : 0;
            return _availableThemes[currentThemeIndex].DisplayNameLocalizedStringKey;
        }

        private int GetNextAvailableThemeIndex() {
            int currentThemeId = _settingsManager.CurrentSettings.CurrentThemeId;
            var currentTheme = _availableThemes.FirstOrDefault(t => t.Id == currentThemeId);
            int nextThemeIndex = 0;
            if (currentTheme != null) {
                int currentThemeIndex = _availableThemes.IndexOf(currentTheme);
                if (currentThemeIndex < _availableThemes.Count - 1) {
                    nextThemeIndex = currentThemeIndex + 1;
                }
            }
            return nextThemeIndex;
        }

        private void InvalidateNextThemeButton() {
            _nextThemeButton.TextKey = GetNextThemeButtonTextKey();
        }

        private void InvalidateTheme() {
            Resources.ReAssignDeep((DependencyObject)GetView());
            _newGameButton.Refresh();
            _continueGameButton.Refresh();
            _nextThemeButton.Refresh();
            NotifyOfPropertyChange(() => TooltipForeground);

            ThemeInvalidated(this, EventArgs.Empty);
        }

        private void InvalidateLanguage() {
            Text.ReAssignDeep((DependencyObject)GetView());
            _newGameButton.RefreshText();
            _continueGameButton.RefreshText();
            _nextThemeButton.RefreshText();

            LanguageInvalidated(this, EventArgs.Empty);
        }
    }
}