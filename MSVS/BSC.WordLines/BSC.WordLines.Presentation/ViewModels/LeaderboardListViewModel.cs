﻿using BSC.WordLines.Data;
using Caliburn.Micro;

namespace BSC.WordLines.Presentation.ViewModels {
    public class LeaderboardListViewModel : Conductor<LeaderboardViewModel>.Collection.OneActive {
        public LeaderboardListViewModel(LeaderboardViewModel.Factory leaderboardFactory) {
            var localLeaderboard = leaderboardFactory(ELeaderboards.Local);
            var globalLeaderboard = leaderboardFactory(ELeaderboards.Global);
            Items.Add(localLeaderboard);
            Items.Add(globalLeaderboard);
            ActiveItem = localLeaderboard;
        }
        
        public void SelectGameMode(EGameMode gameMode) {
            Items.Apply(item => item.SelectedGameMode = gameMode);
        }

        public void SelectLanguage(ELanguage language) {
            Items.Apply(item => item.SelectedLanguage = language);
        }

        public void UpdateHighScores() {
            Items.Apply(item => item.UpdateHighScores());
        }
    }
}