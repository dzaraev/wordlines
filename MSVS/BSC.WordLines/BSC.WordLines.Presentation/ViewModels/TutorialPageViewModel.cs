﻿using BSC.WordLines.Data;
using BSC.WordLines.Utils;
using Caliburn.Micro;

namespace BSC.WordLines.Presentation.ViewModels {
    public class TutorialPageViewModel : Screen {
        private readonly INavigationService _navigationService;
        private readonly SettingsManager _settingsManager;
        private bool _confirmed;

        public ButtonViewModel ConfirmButton { get; set; }

        public bool IsNewGamePending { get; set; }

        public TutorialPageViewModel(
            ViewModelFactory.CreateButton buttonFactory,
            INavigationService navigationService,
            SettingsManager settingsManager) {

            buttonFactory.EnsureNotNull("buttonFactory");
            navigationService.EnsureNotNull("navigationService");
            settingsManager.EnsureNotNull("settingsManager");
            _navigationService = navigationService;
            _settingsManager = settingsManager;
            _confirmed = false;

            ConfirmButton = buttonFactory(true);
            ConfirmButton.PressAction = Confirm;

            IsNewGamePending = false;
        }

        protected override void OnActivate() {
            ConfirmButton.TextKey = IsNewGamePending ? "ButtonConfirmTutorial" : "ButtonCloseTutorial";
        }

        private void Confirm() {
            if (_confirmed) {
                return;
            }
            _confirmed = true;

            if (!_settingsManager.CurrentSettings.IsTutorialConfirmed) {
                _settingsManager.CurrentSettings.IsTutorialConfirmed = true;
                _settingsManager.SaveInDb();
            }

            if (!IsNewGamePending) {
                _navigationService.GoBack();
                return;
            }

            _navigationService
                .UriFor<GamePageViewModel>()
                .WithParam(page => page.IsNewGamePending, true)
                .Navigate();
            _navigationService.RemoveBackEntry();
        }
    }
}