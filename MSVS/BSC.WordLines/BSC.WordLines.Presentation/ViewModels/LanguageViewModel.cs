﻿using BSC.WordLines.Data;

namespace BSC.WordLines.Presentation.ViewModels {
    public class LanguageViewModel {
        private readonly ILanguageData _languageData;

        public LanguageViewModel(ILanguageData languageData) {
            _languageData = languageData;
        }

        public ILanguageData LanguageData {
            get { return _languageData; }
        }

        public string DisplayName {
            get { return _languageData.DisplayName; }
        }

        public string IconAbstractResourceKey {
            get { return _languageData.IconAbstractResourceKey; }
        }
    }
}