﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Navigation;
using BSC.WordLines.Data;
using BSC.WordLines.Model;
using BSC.WordLines.Presentation.Views;
using BSC.WordLines.Utils;
using Caliburn.Micro;

namespace BSC.WordLines.Presentation.ViewModels {
    public class GameViewModel
        : Conductor<object>.Collection.AllActive,
        IGameViewModel,
        IHandle<TryChainBallEvent>,
        IHandle<UnchainBallEvent>,
        IHandle<FinishChainEvent>,
        IHandle<TryApplyKillBallBoosterEvent>,
        IHandle<ShowTooltipEvent> {

        private const int DELAY_BEFORE_START_GAME_MS = 10;//need for turnstile animation

        private readonly IGame _gameModel;
        private readonly IBoardViewModel _board;
        private readonly IPlayer _playerModel;
        private readonly ILeaderboardManager _leaderboardManager;
        private readonly ILocalDataProvider _localProvider;
        private readonly IEventAggregator _eventAggregator;
        private readonly ObservableCollection<IBallViewModel> _ballChain;
        private readonly QueueAnimationScheduller _animationScheduller;
        private readonly BoostersViewModel _boosters;
        private readonly INavigationService _navigationService;
        private readonly ISoundsManager _soundsManager;
        private readonly AbstractResourceConverter _resourceConverter;
        private readonly ScaleViewModel _scale;
        private bool _isNewGamePending;
        private string _tooltip;
        private bool _isTooltipsAutoHideEnabled;
        private string _tooltipExtra;
        private bool _useTooltipExtra;

        public GameViewModel(
            IGame gameModel,
            IPlayer playerModel,
            IBoardViewModel board,
            ILeaderboardManager leaderboardManager,
            ILocalDataProvider localProvider,
            IEventAggregator eventAggregator,
            BoostersViewModel boosters,
            INavigationService navigationService,
            ISoundsManager soundsManager,
            AbstractResourceConverter resourceConverter
            ) {
            gameModel.EnsureNotNull("gameModel");
            board.EnsureNotNull("board");
            playerModel.EnsureNotNull("playerModel");
            leaderboardManager.EnsureNotNull("leaderboardManager");
            eventAggregator.EnsureNotNull("eventAggregator");
            boosters.EnsureNotNull("boosters");
            navigationService.EnsureNotNull("navigationService");
            localProvider.EnsureNotNull("localProvider");
            soundsManager.EnsureNotNull("soundsManager");
            resourceConverter.EnsureNotNull("resourceConverter");

            _isNewGamePending = false;
            _board = board;
            _playerModel = playerModel;
            _leaderboardManager = leaderboardManager;
            _localProvider = localProvider;
            _eventAggregator = eventAggregator;
            _ballChain = new ObservableCollection<IBallViewModel>();
            _boosters = boosters;
            _navigationService = navigationService;
            _soundsManager = soundsManager;
            _resourceConverter = resourceConverter;
            _gameModel = gameModel;
            _scale = new ScaleViewModel(this);
            _animationScheduller = new QueueAnimationScheduller(_eventAggregator);
            _board.AnimationSheduller = _animationScheduller;

            Items.Add(_board);
            Items.Add(_boosters);
            Items.Add(_scale);
        }

        public ObservableCollection<IBallViewModel> BallChain {
            get { return _ballChain; }
        }

        public bool IsNewGamePending {
            get { return _isNewGamePending; }
            set {
                if (_isNewGamePending != value) {
                    _isNewGamePending = value;
                    NotifyOfPropertyChange(() => IsNewGamePending);
                }
            }
        }

        public Brush TooltipForeground {
            get { return _resourceConverter.Convert<Brush>("brush_ShowTooltipForeground{0}Mode".FormatWith(Mode)); }
        }

        public EGameMode Mode {
            get { return _gameModel.Mode; }
        }

        public int CurrentScore {
            get { return _gameModel.CurrentScore; }
        }

        public int MovesCount {
            get { return _gameModel.MovesCount; }
        }

        public int SecondsCount {
            get { return _gameModel.SecondsCount; }
        }

        public ScaleViewModel Scale {
            get { return _scale; }
        }

        public string Tooltip {
            get { return _tooltip; }
            set {
                _tooltip = value;
                NotifyOfPropertyChange(() => Tooltip);
            }
        }

        public string TooltipExtra {
            get { return _tooltipExtra; }
            set {
                _tooltipExtra = value;
                NotifyOfPropertyChange(() => TooltipExtra);
            }
        }

        public bool UseTooltipExtra {
            get { return _useTooltipExtra; }
            set {
                _useTooltipExtra = value;
                NotifyOfPropertyChange(() => UseTooltipExtra);
            }
        }

        public bool IsTooltipsAutoHideEnabled {
            get { return _isTooltipsAutoHideEnabled; }
            set {
                _isTooltipsAutoHideEnabled = value;
                NotifyOfPropertyChange(() => IsTooltipsAutoHideEnabled);
            }
        }

        public IBoardViewModel Board {
            get { return _board; }
        }

        public BoostersViewModel Boosters {
            get { return _boosters; }
        }

        public IGame Model {
            get { return _gameModel; }
        }

        protected override void OnActivate() {
            _animationScheduller.Reset();
            _gameModel.CurrentScoreChanged += HandleCurrentScoreChanged;
            _gameModel.StateChanged += HandleGameStateChanged;
            _gameModel.MovesCountChanged += HandleGameMovesCountChanged;
            _gameModel.SecondsCountChanged += HandleGameSecondsCountChanged;
            _eventAggregator.Subscribe(this);
            base.OnActivate();
            _gameModel.Continue();
        }

        protected override void OnDeactivate(bool close) {
            _gameModel.Pause();
            _gameModel.CurrentScoreChanged -= HandleCurrentScoreChanged;
            _gameModel.StateChanged -= HandleGameStateChanged;
            _gameModel.MovesCountChanged -= HandleGameMovesCountChanged;
            _gameModel.SecondsCountChanged -= HandleGameSecondsCountChanged;
            _eventAggregator.Unsubscribe(this);
            _animationScheduller.Reset();
            ClearBallChain();
            base.OnDeactivate(close);
        }

        protected async override void OnViewLoaded(object view) {
            base.OnViewLoaded(view);

            await Task.Delay(DELAY_BEFORE_START_GAME_MS);

            if (IsNewGamePending) {
                IsNewGamePending = false;
                _gameModel.StartNew();
            }
            _animationScheduller.StartPublishTasks();
        }

        public void Handle(TryChainBallEvent message) {
            if (!IsActive) {
                return;
            }
            var ball = message.Ball;
            if (ball == null) {
                throw new InvalidOperationException(ErrorMessage.BallCannotBeNull);
            }
            if (ball.InChain) {
                return;
            }

            var checkBallChain = new List<IBallViewModel>(_ballChain) { ball };
            if (_gameModel.IsChainValid(GetBallModelChain(checkBallChain))) {
                ball.InChain = true;
                ball.IsLastInChain = true;
                var previousBall = _ballChain.LastOrDefault();
                if (previousBall != null) {
                    ball.PreviousBall = previousBall;
                    previousBall.IsLastInChain = false;
                }
                _ballChain.Add(ball);
            }
        }

        public void Handle(UnchainBallEvent message) {
            if (!IsActive) {
                return;
            }

            var ball = message.Ball;
            if (ball == null) {
                throw new InvalidOperationException(ErrorMessage.BallCannotBeNull);
            }
            if (_ballChain.Count == 0) {
                throw new InvalidOperationException(ErrorMessage.CannotUnchainBallChainEmpty);
            }
            if (!ReferenceEquals(_ballChain.Last(), ball)) {
                throw new InvalidOperationException(ErrorMessage.CannotUnchainBallWhichNotOnEndOfChain);
            }

            _ballChain.RemoveAt(_ballChain.Count - 1);

            ball.InChain = false;
            ball.IsLastInChain = false;
            var previousBall = ball.PreviousBall;
            if (previousBall != null) {
                previousBall.IsLastInChain = true;
            }
            ball.PreviousBall = null;
        }

        public void Handle(FinishChainEvent message) {
            if (!IsActive) {
                return;
            }
            if (_ballChain.Any() && !_animationScheduller.InProcess) {
                if (_gameModel.KillChain(GetBallModelChain(_ballChain)) &&
                    _gameModel.State == EGameState.InProcess) {

                    switch (GameHelper.GetWordLength(_ballChain.Count)) {
                        case EWordLength.Short:
                            _soundsManager.PlayShortChainKill();
                            break;
                        case EWordLength.Medium:
                            _soundsManager.PlayMediumChainKill();
                            break;
                        case EWordLength.Long:
                            _soundsManager.PlayLongChainKill();
                            break;
                    }
                    _animationScheduller.StartPublishTasks();
                }
            }
            ClearBallChain();
        }

        public void Handle(TryApplyKillBallBoosterEvent message) {
            if (!_animationScheduller.InProcess) {
                _gameModel.ApplyKillBallBooster(message.Ball.BallModel);
                _animationScheduller.StartPublishTasks();
                _soundsManager.PlayKillBall();
                message.Handled = true;
            }
        }

        public void Handle(ShowTooltipEvent message) {
            //Order does matter.
            IsTooltipsAutoHideEnabled = message.IsAutoHide;
            UseTooltipExtra = message.TooltipExtra != null;
            Tooltip = message.Tooltip;
            TooltipExtra = message.TooltipExtra;
        }

        private Queue<IBall> GetBallModelChain(IEnumerable<IBallViewModel> ballChain) {
            var ballModelChain = new Queue<IBall>();
            ballChain.Apply(ball => ballModelChain.Enqueue(ball.BallModel));
            return ballModelChain;
        }

        private void HandleCurrentScoreChanged(object sender, EventArgs e) {
            NotifyOfPropertyChange(() => CurrentScore);
        }

        private void HandleGameMovesCountChanged(object sender, EventArgs e) {
            NotifyOfPropertyChange(() => MovesCount);
        }

        private void HandleGameSecondsCountChanged(object sender, EventArgs e) {
            NotifyOfPropertyChange(() => SecondsCount);
            int newSecondsCount = SecondsCount;
            if (newSecondsCount > 0 && newSecondsCount <= GameConst.SecondsLeftForMetronome) {
                _soundsManager.PlayMetronome();
            }
        }

        private void HandleGameStateChanged(object sender, GameStateChangedEventArgs e) {
            ClearBallChain();
            if (e.NewState == EGameState.GameOver) {
                _soundsManager.PlayGameOver();

                int waitingKey = UIHelp.CreateWaitingKey();
                NavigatedEventHandler waitingHandler = null;
                waitingHandler = async (s, args) => {
                    _navigationService.Navigated -= waitingHandler;
                    if (!_navigationService.IsWaitingForKey(waitingKey)) {
                        return;
                    }
                    await _leaderboardManager.RegisterLocalScore(
                        Mode,
                        _localProvider.GetCurrentLanguage(),
                        _playerModel.Data,
                        _playerModel.LastRoundScore);

                    if (!_navigationService.IsWaitingForKey(waitingKey)) {
                        return;
                    }
                    _navigationService
                        .UriFor<GameOverPageViewModel>()
                        .WithParam(page => page.GameMode, Mode)
                        .Navigate();
                    _navigationService.RemoveBackEntry();
                };
                _navigationService.Navigated += waitingHandler;
                _navigationService
                    .UriFor<WaitingPageViewModel>()
                    .WithParam(vm => vm.WaitingKey, waitingKey)
                    .Navigate();
                _navigationService.RemoveBackEntry();
            }
        }

        private void ClearBallChain() {
            _ballChain.Apply(ball => {
                ball.InChain = false;
                ball.PreviousBall = null;
                ball.IsLastInChain = false;
            });
            _ballChain.Clear();
        }
    }
}