﻿using Caliburn.Micro;

namespace BSC.WordLines.Presentation.ViewModels {
    public class HighScoreViewModel : PropertyChangedBase {
        private int _order;
        private int _score;
        private string _playerName;

        public int Order {
            get { return _order; }
            set {
                if (value != _order) {
                    _order = value;
                    NotifyOfPropertyChange(() => Order);
                }
            }
        }

        public string PlayerName {
            get { return _playerName; }
            set {
                if (value != _playerName) {
                    _playerName = value;
                    NotifyOfPropertyChange(() => PlayerName);
                }
            }
        }

        public int Score {
            get { return _score; }
            set {
                if (value != _score) {
                    _score = value;
                    NotifyOfPropertyChange(() => Score);
                }
            }
        }
    }
}