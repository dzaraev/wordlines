﻿using System;
using BSC.WordLines.Data;
using BSC.WordLines.Model;
using BSC.WordLines.Utils;
using Caliburn.Micro;
using Microsoft.Phone.Tasks;

namespace BSC.WordLines.Presentation.ViewModels {
    public class MiscPageViewModel : Screen {
        private readonly INavigationService _navigationService;
        private readonly ILocalizedStringCache _localizedStringCache;
        private readonly IPlatformDataHelper _platformHelper;
        private readonly SettingsManager _settingsManager;
        public ButtonViewModel ReviewAppButton { get; set; }
        public ButtonViewModel ShareAppButton { get; set; }
        public ButtonViewModel EmailUsButton { get; set; }
        public ButtonViewModel SocialButton { get; set; }
        public ButtonViewModel TwitterButton { get; set; }
        public ButtonViewModel TutorialButton { get; set; }
        public ButtonViewModel RulesButton { get; set; }
        public string TechnicalInfo { get; set; }
        public string SponsorHeader { get { return SponsorHelper.SponsorDescription; } }
        public string SponsorLink { get { return SponsorHelper.SponsorLinkName; } }

        public MiscPageViewModel(
            ViewModelFactory.CreateButton buttonFactory,
            INavigationService navigationService,
            ILocalizedStringCache localizedStringCache,
            IPlatformDataHelper platformHelper,
            SettingsManager settingsManager) {

            navigationService.EnsureNotNull("navigationService");
            buttonFactory.EnsureNotNull("buttonFactory");
            localizedStringCache.EnsureNotNull("localizedStringCache");
            platformHelper.EnsureNotNull("platformHelper");
            settingsManager.EnsureNotNull("settingsManager");

            _navigationService = navigationService;
            _localizedStringCache = localizedStringCache;
            _platformHelper = platformHelper;
            _settingsManager = settingsManager;

            ReviewAppButton = buttonFactory(true);
            ReviewAppButton.TextKey = "ButtonReviewApp";
            ReviewAppButton.PressAction = ReviewHelper.ReviewAppAndShutUp;
            
            ShareAppButton = buttonFactory(true);
            ShareAppButton.TextKey = "ButtonShareApp";
            ShareAppButton.PressAction = ShareApp;

            EmailUsButton = buttonFactory(true);
            EmailUsButton.TextKey = "ButtonEmailUs";
            EmailUsButton.DefaultFillKey = "brush_ButtonDefaultBackgroundBuyBooster";
            EmailUsButton.ActiveFillKey = "brush_ButtonActiveBackgroundBuyBooster";
            EmailUsButton.PressAction = () => {
                var sendMailTask = new EmailComposeTask();
                sendMailTask.To = GameConst.GameSupportMail;
                sendMailTask.Subject = localizedStringCache.GetLocalizedString("EmailUsSubject");
                sendMailTask.Body =
                    "\n\n" + "---\n" + "technical info: " + GetTechnicalInfo();
                sendMailTask.Show();
            };

            SocialButton = buttonFactory(true);
            SocialButton.TextKey = "ButtonSocial";
            SocialButton.DefaultFillKey = "brush_ButtonDefaultBackgroundSocial";
            SocialButton.ActiveFillKey = "brush_ButtonActiveBackgroundSocial";
            SocialButton.PressAction = () => {
                FreePointsManager.OnSocialFollowed();
                var web = new WebBrowserTask();
                if (_settingsManager.CurrentLanguage.Name == "Russian") {
                    web.Uri = new Uri(GameConst.VkLink);
                }
                else {
                    web.Uri = new Uri(GameConst.FbLink);
                }
                web.Show();
            };

            TwitterButton = buttonFactory(true);
            TwitterButton.TextKey = "ButtonTwitter";
            TwitterButton.DefaultFillKey = "brush_ButtonDefaultBackgroundBuyBooster";
            TwitterButton.ActiveFillKey = "brush_ButtonActiveBackgroundBuyBooster";
            TwitterButton.PressAction = () => {
                FreePointsManager.OnTwitterFollowed();
                var web = new WebBrowserTask();
                web.Uri = new Uri(GameConst.TwitterLink);
                web.Show();
            };

            TutorialButton = buttonFactory(true);
            TutorialButton.TextKey = "ButtonTutorial";
            TutorialButton.DefaultFillKey = "brush_ButtonDefaultBackgroundTimeMode";
            TutorialButton.ActiveFillKey = "brush_ButtonActiveBackgroundTimeMode";
            TutorialButton.PressAction = () => _navigationService
                                                   .UriFor<TutorialPageViewModel>()
                                                   .WithParam(page => page.IsNewGamePending, false)
                                                   .Navigate();
            RulesButton = buttonFactory(true);
            RulesButton.TextKey = "ButtonRules";
            RulesButton.DefaultFillKey = "brush_ButtonDefaultBackgroundTimeMode";
            RulesButton.ActiveFillKey = "brush_ButtonActiveBackgroundTimeMode";
            RulesButton.PressAction = () => _navigationService.UriFor<RulesPageViewModel>().Navigate();

            TechnicalInfo = string.Format("{0} © 2014 v{1}", platformHelper.GetAppName(), platformHelper.GetAppVersion());
        }

        public bool IsSponsorLinkEnabled {
            get { return _settingsManager.CurrentLanguage.Name == "Russian"; }
        }

        public async void LaunchPrivacyPolicy() {
            Uri privacyPolicyUrl = new Uri(GameConst.PrivacyPolicyLink);
            bool result = await Windows.System.Launcher.LaunchUriAsync(privacyPolicyUrl);
        }

        public async void LaunchSponsorLink() {
            FreePointsManager.OnSponsorFollowed();
            bool result = await Windows.System.Launcher.LaunchUriAsync(SponsorHelper.SponsorLinkUri);
        }

        protected override void OnActivate() {
            FreePointsManager.Fulfill();
        }

        private void ShareApp() {
            FreePointsManager.OnShared();
            var shareLinkTask = new ShareLinkTask {
                Title = _localizedStringCache.GetLocalizedString("ShareAppTaskTitle"),
                LinkUri = new Uri(GameConst.AppLink, UriKind.Absolute),
                Message = _localizedStringCache.GetLocalizedString("ShareAppTaskMessage")
            };
            shareLinkTask.Show();
        }

        private string GetTechnicalInfo() {
            return string.Format(
                "{0} {1} {2}", _settingsManager.CurrentPlayer.Username, _platformHelper.GetSystemVersion(),
                _platformHelper.GetAppVersion());
        }
    }
}