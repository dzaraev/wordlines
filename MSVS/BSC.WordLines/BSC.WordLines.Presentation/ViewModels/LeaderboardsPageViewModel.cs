﻿using BSC.WordLines.Data;
using BSC.WordLines.Model;
using BSC.WordLines.Utils;
using Caliburn.Micro;

namespace BSC.WordLines.Presentation.ViewModels {
    public class LeaderboardsPageViewModel : Conductor<LeaderboardListViewModel> {
        private readonly LeaderboardListViewModel _leaderboardList;
        private readonly ILocalDataProvider _provider;
        private readonly ButtonViewModel _movesModeButton;
        private readonly ButtonViewModel _timeModeButton;

        public LeaderboardsPageViewModel(
            LeaderboardListViewModel leaderboardList,
            ILocalDataProvider provider,
            LanguageDataMapper languageDataMapper,
            ViewModelFactory.CreateButton createButton
            ) {
            leaderboardList.EnsureNotNull("leaderboardList");
            provider.EnsureNotNull("provider");
            languageDataMapper.EnsureNotNull("languageDataMapper");
            createButton.EnsureNotNull("createButton");

            _leaderboardList = leaderboardList;
            _provider = provider;
            ActiveItem = _leaderboardList;

            var languageData = _provider.GetCurrentLanguage();
            _leaderboardList.SelectLanguage(languageDataMapper.GetLanguage(languageData));
            _leaderboardList.SelectGameMode(EGameMode.Moves);

            _movesModeButton = createButton(false);
            _movesModeButton.Content = new MovesModeSymbolViewModel();
            _movesModeButton.PressAction = () => {
                _timeModeButton.IsSelected = false;
                if (_leaderboardList.IsActive) {
                    _leaderboardList.SelectGameMode(EGameMode.Moves);
                    _leaderboardList.UpdateHighScores();
                }
            };
            
            _timeModeButton = createButton(false);
            _timeModeButton.Content = new TimeModeSymbolViewModel();
            _timeModeButton.PressAction = () => {
                _movesModeButton.IsSelected = false;
                if (_leaderboardList.IsActive) {
                    _leaderboardList.SelectGameMode(EGameMode.Time);
                    _leaderboardList.UpdateHighScores();
                }
            };
        }

        public LeaderboardListViewModel LeaderboardList {
            get { return _leaderboardList; }
        }

        public ButtonViewModel MovesModeButton {
            get { return _movesModeButton; }
        }

        public ButtonViewModel TimeModeButton {
            get { return _timeModeButton; }
        }

        protected override void OnInitialize() {
            base.OnInitialize();
            EGameMode defaultMode = UIConst.DefaultLeaderboardPageGameMode;
            if (defaultMode == EGameMode.Moves) {
                _movesModeButton.PressOnce();
            }
            else if (defaultMode == EGameMode.Time) {
                _timeModeButton.PressOnce();
            }
            else {
                Diagnost.Assert(false, ErrorMessage.UnknownGameModeFormat.FormatWith(defaultMode));
            }
        }
    }
}