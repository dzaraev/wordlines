﻿using BSC.WordLines.Model;
using Caliburn.Micro;

namespace BSC.WordLines.Presentation.ViewModels {
    public sealed class CellViewModel : PropertyChangedBase, ICellViewModel {
        private int _row;
        private int _column;
        private readonly ICell _cellModel;

        public CellViewModel(ICell cellModel) {
            _cellModel = cellModel;
            Row = _cellModel.Row;
            Column = _cellModel.Column;
        }
        
        public int Row {
            get { return _row; }
            private set {
                if (value != _row) {
                    _row = value;
                    NotifyOfPropertyChange(() => Row);
                }
            }
        }

        public int Column {
            get { return _column; }
            private set {
                if (value != _column) {
                    _column = value;
                    NotifyOfPropertyChange(() => Column);
                }
            }
        }
    }
}