﻿using System.Collections.ObjectModel;
using System.Linq;
using BSC.WordLines.Model;
using BSC.WordLines.Utils;
using Caliburn.Micro;

namespace BSC.WordLines.Presentation.ViewModels {
    public sealed class BoardViewModel : Screen, IBoardViewModel {
        private readonly IBoard _boardModel;
        private readonly ObservableCollection<IBallViewModel> _balls;
        private readonly ObservableCollection<ICellViewModel> _cells;
        private readonly ViewModelFactory.CreateBall _createBall;
        private readonly ViewModelFactory.CreateCell _createCell;
        private readonly IEventAggregator _eventAggregator;
        private IAnimationSheduller _animationSheduller;
        private bool _isBallsAnimationEnabled;

        public BoardViewModel(
            IBoard boardModel,
            IEventAggregator eventAggregator,
            ViewModelFactory.CreateBall createBall,
            ViewModelFactory.CreateCell createCell
            ) {
            _boardModel = boardModel;
            _eventAggregator = eventAggregator;
            _createBall = createBall;
            _createCell = createCell;
            _balls = new ObservableCollection<IBallViewModel>();
            _cells = new ObservableCollection<ICellViewModel>();
            _animationSheduller = new NullAnimationSheduller();
            _isBallsAnimationEnabled = false;
        }

        public bool IsBallsAnimationEnabled {
            get { return _isBallsAnimationEnabled; }
            private set {
                if (_isBallsAnimationEnabled != value) {
                    _isBallsAnimationEnabled = value;
                    NotifyOfPropertyChange(() => IsBallsAnimationEnabled);
                }
            }
        }

        public ObservableCollection<IBallViewModel> Balls {
            get { return _balls; }
        }

        public ObservableCollection<ICellViewModel> Cells {
            get { return _cells; }
        }

        public int ColumnsCount {
            get { return _boardModel.ColumnsCount; }
        }

        public int RowsCount {
            get { return _boardModel.RowsCount; }
        }

        public IAnimationSheduller AnimationSheduller {
            get { return _animationSheduller; }
            set { _animationSheduller = value; }
        }

        protected override void OnActivate() {
            InitializeCellsAndBalls();
            _boardModel.BallsAdded += HandleBallsAdded;
            _boardModel.BallsFell += HandleBallsFell;
            _boardModel.BallsRemoved += HandleBallsRemoved;
            _boardModel.Reset += HandleBoardReset;
            _eventAggregator.Subscribe(this);
        }

        protected override void OnDeactivate(bool close) {
            _boardModel.BallsAdded -= HandleBallsAdded;
            _boardModel.BallsFell -= HandleBallsFell;
            _boardModel.BallsRemoved -= HandleBallsRemoved;
            _boardModel.Reset -= HandleBoardReset;
            _eventAggregator.Unsubscribe(this);
            DeinitializeCellsAndBalls();
        }

        private void InitializeCellsAndBalls() {
            _cells.Clear();
            _cells.AddRange(from cell in _boardModel.Cells
                            select _createCell(cell));
            
            _isBallsAnimationEnabled = false;
            _balls.Apply(ball => ball.Deactivate());
            _balls.Clear();
            var balls = (from ball in _boardModel.Balls
                        select _createBall(ball)).ToList();
            balls.ForEach(ball => {
                ball.Activate();
                ball.InChain = false;
                ball.PreviousBall = null;
                ball.IsLastInChain = false;
                ball.IsAnimate = false;
                _balls.Add(ball);
            });
            _isBallsAnimationEnabled = true;
        }

        private void DeinitializeCellsAndBalls() {
            _balls.Apply(ball => ball.Deactivate());
            _balls.Clear();
            _cells.Clear();
        }

        private void HandleBallsAdded(object sender, BallsEventArgs e) {
            var addedBalls = e.AffectedBalls.Select(ballModel => _createBall(ballModel)).ToList();
            if (addedBalls.Any()) {
                addedBalls.ForEach(ball => {
                    ball.Activate();
                    ball.IsAnimate = true;
                });
                _balls.AddRange(addedBalls);
                var animationTask = new BallsAnimationTask(addedBalls, EBallAnimation.Appear);
                animationTask.Completed += 
                    (s, args) => addedBalls.ForEach(ball => ball.IsAnimate = false);
                _animationSheduller.Shedule(animationTask);
            }
        }

        private void HandleBallsFell(object sender, BallsEventArgs e) {
            var fellBalls = (from ball in _balls
                             where e.AffectedBalls.ReferenceContains(ball.BallModel)
                             select ball).ToList();
            if (fellBalls.Any()) {
                fellBalls.ForEach(ball=> {
                    ball.IsAnimate = true;
                });
                var animationTask = new BallsAnimationTask(fellBalls, EBallAnimation.Fall);
                animationTask.Completed +=
                    (s, args) => fellBalls.ForEach(ball => ball.IsAnimate = false);
                _animationSheduller.Shedule(animationTask);
            }
        }

        private void HandleBallsRemoved(object sender, BallsEventArgs e) {
            var removedBalls = (from ball in _balls
                                where e.AffectedBalls.ReferenceContains(ball.BallModel)
                                select ball).ToList();
            if (removedBalls.Any()) {
                removedBalls.ForEach(ball => {
                    ball.IsAnimate = true;
                });
                var animationTask = new BallsAnimationTask(removedBalls, EBallAnimation.Disappear);
                animationTask.Completed += (s, args) => {
                    removedBalls.ForEach(ball => {
                        ball.IsAnimate = false;
                        ball.Deactivate();
                    });
                    _balls.RemoveRange(removedBalls);
                };
                _animationSheduller.Shedule(animationTask);
            }
        }

        private void HandleBoardReset(object sender, System.EventArgs e) {
            InitializeCellsAndBalls();
        }
    }
}