﻿using BSC.WordLines.Model;
using BSC.WordLines.Utils;
using Caliburn.Micro;

namespace BSC.WordLines.Presentation.ViewModels {
    public class RoundResultsViewModel : Screen {
        private readonly IGame _gameModel;
        private readonly IPlayer _player;
        private int _roundScore;
        private int _money;

        public RoundResultsViewModel(IGame gameModel, IPlayer player) {
            gameModel.EnsureNotNull("gameModel");
            player.EnsureNotNull("player");
            _gameModel = gameModel;
            _player = player;
        }

        public int RoundScore {
            get { return _roundScore; }
            private set {
                if (_roundScore != value) {
                    _roundScore = value;
                    NotifyOfPropertyChange(() => RoundScore);
                }
            }
        }

        public int Money {
            get { return _money; }
            set {
                if (_money != value) {
                    _money = value;
                    NotifyOfPropertyChange(() => Money);
                }
            }
        }

        protected override void OnActivate() {
            bool isGameOver = _gameModel.State == EGameState.GameOver; 
            Diagnost.Assert(isGameOver, ErrorMessage.GameMustBeInGameOverState);
            
            RoundScore = isGameOver ? _gameModel.RoundScore : 0;
            Money = _player.Money;
        }
    }
}