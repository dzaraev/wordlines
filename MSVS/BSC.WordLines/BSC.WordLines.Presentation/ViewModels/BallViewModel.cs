﻿using System;
using System.Windows.Media;
using BSC.WordLines.Model;
using BSC.WordLines.Utils;
using Caliburn.Micro;

namespace BSC.WordLines.Presentation.ViewModels {
    public sealed class BallViewModel : PropertyChangedBase, IBallViewModel, IHandle<BallsSelectionModeEvent> {
        private const string WILDCARD_SYMBOL = "*";

        private int? _row;
        private int? _column;
        private int? _previousRow;
        private bool _inChain;
        private readonly IBall _ballModel;
        private readonly IEventAggregator _eventAggregator;
        private readonly IGame _gameModel;
        private readonly AbstractResourceConverter _resourceConverter;
        private readonly ISoundsManager _soundsManager;
        private readonly INavigationService _navigationService;
        private IBallViewModel _previousBall;
        private bool _isLastInChain;
        private bool _isAnimate;
        private bool _inSelectionMode;

        public BallViewModel(
            IBall ballModel,
            IEventAggregator eventAggregator,
            IGame gameModel,
            AbstractResourceConverter resourceConverter,
            ISoundsManager soundsManager,
            INavigationService navigationService
            ) {
            ballModel.EnsureNotNull("ballModel");
            eventAggregator.EnsureNotNull("eventAggregator");
            gameModel.EnsureNotNull("gameModel");
            resourceConverter.EnsureNotNull("resourceConverter");
            soundsManager.EnsureNotNull("soundsManager");
            navigationService.EnsureNotNull("navigationService");

            _ballModel = ballModel;
            _eventAggregator = eventAggregator;
            _gameModel = gameModel;
            _resourceConverter = resourceConverter;
            _soundsManager = soundsManager;
            _navigationService = navigationService;

            Row = ballModel.Row;
            Column = ballModel.Column;
            PreviousRow = null;
            PreviousBall = null;
            InChain = false;
            IsLastInChain = false;
            IsAnimate = false;
            InSelectionMode = false;
        }

        public string Symbol {
            get {
                if (_ballModel.Type == EBallType.Letter) {
                    return _ballModel.Letter.ToString();
                }

                if (_ballModel.Type == EBallType.Wildcard) {
                    return WILDCARD_SYMBOL;
                }
                return null;
            }
        }

        public bool InSelectionMode {
            get { return _inSelectionMode; }
            private set {
                if (_inSelectionMode != value) {
                    _inSelectionMode = value;
                    NotifyOfPropertyChange(() => InSelectionMode);
                }
            }
        }

        public IBall BallModel {
            get { return _ballModel; }
        }

        public bool InChain {
            get { return _inChain; }
            set {
                if (value != _inChain) {
                    _inChain = value;
                    NotifyOfPropertyChange(() => InChain);
                }
            }
        }

        public bool IsLastInChain {
            get { return _isLastInChain; }
            set {
                if (_isLastInChain != value) {
                    _isLastInChain = value;
                    NotifyOfPropertyChange(() => IsLastInChain);
                }
            }
        }

        public IBallViewModel PreviousBall {
            get { return _previousBall; }
            set {
                if (!ReferenceEquals(_previousBall, value)) {
                    _previousBall = value;
                    NotifyOfPropertyChange(() => PreviousBall);
                }
            }
        }

        public int? Row {
            get { return _row; }
            private set {
                if (value != _row) {
                    _row = value;
                    NotifyOfPropertyChange(() => Row);
                }
            }
        }

        public int? Column {
            get { return _column; }
            private set {
                if (value != _column) {
                    _column = value;
                    NotifyOfPropertyChange(() => Column);
                }
            }
        }

        public int? PreviousRow {
            get { return _previousRow; }
            private set {
                if (value != _previousRow) {
                    _previousRow = value;
                    NotifyOfPropertyChange(() => PreviousRow);
                }
            }
        }

        public bool IsAnimate {
            get { return _isAnimate; }
            set {
                if (_isAnimate != value) {
                    _isAnimate = value;
                    NotifyOfPropertyChange(() => IsAnimate);
                }
            }
        }

        public Brush SelectionStroke {
            get { return _resourceConverter.Convert<Brush>("brush_BallSelectionStroke{0}Mode".FormatWith(_gameModel.Mode)); }
        }

        public void Activate() {
            _ballModel.AddedToBoard += HandleBallAdded;
            _ballModel.MovedToAnotherCell += HandleBallMoved;
            _ballModel.RemovedFromBoard += HandleBallRemoved;
            _ballModel.TypeChanged += HandleBallTypeChanged;
            _eventAggregator.Subscribe(this);
        }

        public void Deactivate() {
            _ballModel.AddedToBoard -= HandleBallAdded;
            _ballModel.MovedToAnotherCell -= HandleBallMoved;
            _ballModel.RemovedFromBoard -= HandleBallRemoved;
            _eventAggregator.Unsubscribe(this);
        }

        public void Select() {
            if (!InSelectionMode) {
                return;
            }
            _eventAggregator.Publish(new BallSelectedEvent(this));
        }

        public void ApplyWildcardBooster() {
            if (!_gameModel.CanApplyWildcardBootster()) {
                _navigationService.UriFor<BoostersPageViewModel>().Navigate();
                return;
            }
            
            if (_gameModel.ApplyWildcardBooster(BallModel)) {
                _soundsManager.PlayWildcardOn();
            }
            else {
                _soundsManager.PlayWildcardOff();
            }
        }

        public void Handle(BallsSelectionModeEvent message) {
            InSelectionMode = message.IsSelectionModeEnabled;
        }

        private void HandleBallAdded(object sender, EventArgs e) {
            Row = BallModel.Row;
            Column = BallModel.Column;
            PreviousRow = null;
        }

        private void HandleBallMoved(object sender, EventArgs e) {
            PreviousRow = Row;
            Row = BallModel.Row;
            Column = BallModel.Column;
        }

        private void HandleBallRemoved(object sender, EventArgs e) {
            PreviousRow = Row;
            Row = null;
            Column = null;
        }

        private void HandleBallTypeChanged(object sender, EventArgs e) {
            NotifyOfPropertyChange(() => Symbol);
        }
    }
}