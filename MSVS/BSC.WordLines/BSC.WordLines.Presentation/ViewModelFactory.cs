﻿using BSC.WordLines.Model;
using BSC.WordLines.Presentation.ViewModels;
using Windows.ApplicationModel.Store;

namespace BSC.WordLines.Presentation {
    public class ViewModelFactory {
        public delegate IBallViewModel CreateBall(IBall ball);
        public delegate ICellViewModel CreateCell(ICell cell);
        public delegate ButtonViewModel CreateButton(bool autoResetSelection);
        public delegate StoreProductViewModel CreateProduct(ProductListing productListing, StorePageViewModel storePage);
        public delegate UserMessageViewModel CreateUserMessage();
    }
}