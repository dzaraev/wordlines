﻿using System;
using System.Collections.Generic;
using System.Linq;
using BSC.WordLines.Data;
using BSC.WordLines.Utils;

namespace BSC.WordLines.Model {
    public class GameModeDataMapper {
        private readonly ILocalDataProvider _provider;
        private readonly Dictionary<EGameMode, IGameModeData> _gameModes;

        public GameModeDataMapper(ILocalDataProvider provider) {
            provider.EnsureNotNull("provider");
            _provider = provider;

            var gameModes = (_provider.GetGameModes() ?? new IGameModeData[] {}).ToList();
            if (gameModes.Count == 0) {
                throw new InvalidOperationException(ErrorMessage.NoGameModesFound);
            }

            var movesMode = gameModes.Find(
                gameModeData => IsDataMatchEnum(gameModeData, EGameMode.Moves));
            if (movesMode == null) {
                throw new InvalidOperationException(ErrorMessage.CannotFindGameModeInDb(EGameMode.Moves.ToString()));
            }
            var timeMode = gameModes.Find(
                gameModeData => IsDataMatchEnum(gameModeData, EGameMode.Time));
            if (timeMode == null) {
                throw new InvalidOperationException(ErrorMessage.CannotFindGameModeInDb(EGameMode.Time.ToString()));
            }

            _gameModes = new Dictionary<EGameMode, IGameModeData> {
                {EGameMode.Moves, movesMode},
                {EGameMode.Time, timeMode}
            };

            if (Enum.GetValues(typeof(EGameMode)).Length != _gameModes.Count) {
                throw new InvalidOperationException(ErrorMessage.NotAllGameModesSupportedByDb);
            }
        }

        public int GetGameModeDataId(EGameMode enumValue) {
            return _gameModes[enumValue].Id;
        }

        public IGameModeData GetGameModeData(EGameMode enumValue) {
            return _gameModes[enumValue];
        }

        private bool IsDataMatchEnum(IGameModeData gameModeData, EGameMode enumValue) {
            string gameModeName = gameModeData.Name ?? string.Empty;
            return string.Equals(gameModeName, enumValue.ToString(), StringComparison.Ordinal);
        }
    }
}