﻿using System;
using System.Collections.Generic;

namespace BSC.WordLines.Model {
    public class BallsEventArgs : EventArgs {
        public IEnumerable<IBall> AffectedBalls { get; private set; }

        public BallsEventArgs(IEnumerable<IBall> affectedBalls) {
            AffectedBalls = new List<IBall>(affectedBalls ?? new IBall[] {});
        }
    }
}