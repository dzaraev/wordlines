namespace BSC.WordLines.Model {
    public enum EGameState {
        NotStarted,
        InProcess,
        GameOver,
    }
}