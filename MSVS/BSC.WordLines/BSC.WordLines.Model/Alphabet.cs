using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using BSC.WordLines.Data;
using BSC.WordLines.Utils;

namespace BSC.WordLines.Model {
    public class Alphabet : IAlphabet {
        private const int MAX_DUPLICATE_FIX_ATTEMPTS = 1;
        private readonly ILocalDataProvider _localDataProvider;
        private List<ILetterData> _letters;
        private IEnumerable<ILetterData> _vowels;
        private IEnumerable<ILetterData> _nonVowels;
        private double _vowelsMaxChance;
        private double _nonVowelsMaxChance;
        private Random _random;
        private ILetterData _previousRandomLetter;

        public Alphabet(ILocalDataProvider provider) {
            _localDataProvider = provider;
            Update();
        }

        public void Update() {
            _letters = _localDataProvider.GetAlphabet().ToList();
            if (_letters.Count == 0) {
                throw new InvalidOperationException(ErrorMessage.NoLettersInAlphabet);
            }
            _vowels = _letters.Where(letter => letter.IsVowel);
            _nonVowels = _letters.Where(letter => !letter.IsVowel);

            _vowelsMaxChance = _vowels.Max(letter => letter.Chance);
            _nonVowelsMaxChance = _nonVowels.Max(letter => letter.Chance);

            _previousRandomLetter = null;
            InvalidateRandom();
        }

        public void InvalidateRandom() {
            int seed = (int) DateTime.Now.Ticks;
            _random = new Random(seed);
        }

        public ILetterData GetRandomLetter(bool? takeVowelOrNonVowel = null) {
            ILetterData resultLetter = GenerateRandomLetter(takeVowelOrNonVowel);

            int duplicateFixAttempts = MAX_DUPLICATE_FIX_ATTEMPTS;
            while (
                duplicateFixAttempts > 0 &&
                _previousRandomLetter != null &&
                _previousRandomLetter.Equals(resultLetter)
                ) {
                resultLetter = GenerateRandomLetter(takeVowelOrNonVowel);
                duplicateFixAttempts--;
            }
            _previousRandomLetter = resultLetter;
            return resultLetter;
        }

        private ILetterData GenerateRandomLetter(bool? takeVowelOrNonVowel = null) {
            double vowelChanceHit = _random.NextDouble() * _vowelsMaxChance;
            double nonVowelChanceHit = _random.NextDouble() * _nonVowelsMaxChance;

            bool isChanceHitsValid = vowelChanceHit < _vowelsMaxChance &&
                                     nonVowelChanceHit < _nonVowelsMaxChance;
            Diagnost.Assert(isChanceHitsValid, ErrorMessage.InvalidChanceHit);
            if (!isChanceHitsValid) {
                return GetRandomLetterFromList(_letters);
            }

            var volwels = _vowels.Where(letter => letter.Chance > vowelChanceHit);
            var nonVolwels = _nonVowels.Where(letter => letter.Chance > nonVowelChanceHit);
            
            bool pretendersExists = volwels.Any() && nonVolwels.Any();
            Diagnost.Assert(pretendersExists, ErrorMessage.CannotGetRandomLetter);
            if (!pretendersExists) {
                return GetRandomLetterFromList(_letters);
            }

            bool takeVowel = takeVowelOrNonVowel.HasValue
                ? takeVowelOrNonVowel.Value
                : _random.Next(0, 2) == 0;

            var resultLetter = takeVowel
                ? GetRandomLetterFromList(volwels.ToList())
                : GetRandomLetterFromList(nonVolwels.ToList());

            return resultLetter;
        }

        private ILetterData GetRandomLetterFromList(List<ILetterData> letters) {
            int letterIndex = _random.Next(0, letters.Count - 1);
            return letters[letterIndex];
        }
    }
}