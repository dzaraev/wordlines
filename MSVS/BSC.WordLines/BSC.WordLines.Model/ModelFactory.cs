﻿using BSC.WordLines.Data;

namespace BSC.WordLines.Model {
    public static class ModelFactory {
        public delegate IBall CreateBall(EBallType type, ILetterData letter = null);
        public delegate ICell CreateCell(int column, int row);
        public delegate void GetBoardSize(out int columnsCount, out int rowsCount);
    }
}