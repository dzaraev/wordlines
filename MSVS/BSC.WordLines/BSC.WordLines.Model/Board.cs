﻿using System;
using System.Collections.Generic;
using System.Linq;
using BSC.WordLines.Data;
using BSC.WordLines.Utils;

namespace BSC.WordLines.Model {

    /// <summary>
    /// Represents game board  where balls behave in a certain way. 
    /// Balls will fall down to empty spaces in direction from lesser row
    /// index to greater.
    /// Board's zero position resides in left-top corner.
    /// </summary>
    public sealed class Board : IBoard {
        private readonly int _columnsCount;
        private readonly int _rowsCount;
        private readonly ModelFactory.CreateCell _createCell;
        private readonly ModelFactory.CreateBall _createBall;
        private readonly IAlphabet _alphabet;
        private readonly List<ICell> _cells;
        private readonly List<IBall> _balls;

        /// <summary>
        /// Rises when a new balls added on the board.
        /// </summary>
        public event EventHandler<BallsEventArgs> BallsAdded;

        /// <summary>
        /// Rises when some balls are fell to empty cells from cells above.
        /// </summary>
        public event EventHandler<BallsEventArgs> BallsFell;

        /// <summary>
        /// Rises when some balls was removed from board.
        /// </summary>
        public event EventHandler<BallsEventArgs> BallsRemoved;

        /// <summary>
        /// Rises when all board state resetted to initial.
        /// </summary>
        public event EventHandler Reset;

        /// <summary>
        /// Creates a new board instance with specified size and factories.
        /// </summary>
        /// <param name="getBoardSize">Delegate that must return board size 
        /// as count of columns and count of rows 
        /// (both must be in range from 1 to int.MaxValue)</param>
        /// <param name="createCell">Factory for creating cells. Cannot be null.</param>
        /// <param name="createBall">Factory for creating balls. Cannot be null.</param>
        /// <param name="alphabet">Alphabet for letter generating. Cannot be null.</param>
        /// <exception cref="ArgumentOutOfRangeException">Thrown when 
        /// <paramref name="getBoardSize"/> returned board size values 
        /// that is out of range.</exception>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="getBoardSize"/> 
        /// or <paramref name="createCell"/> is <c>null</c>.</exception>
        public Board(
            ModelFactory.GetBoardSize getBoardSize,
            ModelFactory.CreateCell createCell,
            ModelFactory.CreateBall createBall,
            IAlphabet alphabet
            ) {
            alphabet.EnsureNotNull("alphabet");
            createCell.EnsureNotNull("createCell");
            createBall.EnsureNotNull("createBall");
            getBoardSize.EnsureNotNull("createBallSize");

            int columnsCount;
            int rowsCount;
            getBoardSize(out columnsCount, out rowsCount);
            if (columnsCount < 1 || columnsCount > int.MaxValue ||
                rowsCount < 1 || rowsCount > int.MaxValue) {
                throw new ArgumentOutOfRangeException("getBoardSize");
            }

            _columnsCount = columnsCount;
            _rowsCount = rowsCount;
            _createCell = createCell;
            _createBall = createBall;
            _alphabet = alphabet;
            _balls = new List<IBall>();
            _cells = new List<ICell>();

            GenerateCells();
        }

        /// <summary>
        /// Gets a number of board columns.
        /// </summary>
        public int ColumnsCount {
            get { return _columnsCount; }
        }

        /// <summary>
        /// Gets a number of board rows.
        /// </summary>
        public int RowsCount {
            get { return _rowsCount; }
        }

        /// <summary>
        /// Gets a number of board balls.
        /// </summary>
        public int BallsCount {
            get { return _balls.Count; }
        }

        /// <summary>
        /// Gets a number of board cells.
        /// </summary>
        public int CellsCount {
            get { return _cells.Count; }
        }

        /// <summary>
        /// Gets balls lying on the board.
        /// </summary>
        public IEnumerable<IBall> Balls {
            get { return _balls; }
        }

        /// <summary>
        /// Gets cells of the board.
        /// </summary>
        public IEnumerable<ICell> Cells {
            get { return _cells; }
        }

        /// <summary>
        /// Gets cell of the board with specified coordinates. 
        /// </summary>
        /// <param name="column">Column of the board. It must be 
        /// in range from 0 to <see cref="ColumnsCount"/>-1.</param>
        /// <param name="row">Row of the board. It must be 
        /// in range from 0 to <see cref="RowsCount"/>-1.</param>
        /// <returns>The cell with specified coordinates.</returns>
        /// <exception cref="ArgumentOutOfRangeException">
        /// Thrown when <paramref name="column"/> or <paramref name="row"/> 
        /// is out of range.</exception>
        public ICell this[int column, int row] {
            get {
                VerifyCoordinates(column, row);
                return _cells.FirstOrDefault(cell => cell.Column == column && cell.Row == row);
            }
        }

        /// <summary>
        /// Removes all balls from baord and recreates all cells.
        /// </summary>
        public void Clear() {
            _balls.Clear();
            GenerateCells();
            OnReset();
        }

        /// <summary>
        /// Adds a new balls on the board.
        /// </summary>
        public void DropBalls(int ballsCount) {
            if (ballsCount < 0) {
                throw new InvalidOperationException(ErrorMessage.CannotGenerateNegativeBallsCount);
            }
            if (ballsCount == 0) {
                return;
            }
            
            var emptyCellsCount = _cells.Count(cell => !cell.HasBall);
            bool boardOverflow = emptyCellsCount < ballsCount;
            Diagnost.Assert(!boardOverflow, ErrorMessage.NotEnoughEmptyCellsToAddedBalls);

            int newBallsCount = !boardOverflow ? ballsCount : emptyCellsCount;
            var newBalls = new List<IBall>();
            int nextColumn = 0;
            for (int i = 0; i < newBallsCount; i++) {
                var emptyColumns = (from cell in _cells
                                    where cell.Row == 0 && !cell.HasBall
                                    orderby cell.Column ascending
                                    select cell.Column).ToList();
                int startRow = 0;
                int startColumn = emptyColumns.Contains(nextColumn)
                                      ? nextColumn
                                      : emptyColumns.First();
                int targetRow;
                int targetColumn;
                CalculateBallFallTarget(startColumn, startRow, out targetColumn, out targetRow);

                bool? isVowelNeeded = IsMustBeVowelHere(targetColumn, targetRow);
                ILetterData letter = _alphabet.GetRandomLetter(isVowelNeeded);

                //Reduce letter duplication
                if (SameLettersCount(letter) > 0) {
                    letter = _alphabet.GetRandomLetter(isVowelNeeded);
                    if (SameLettersCount(letter) > 1) {
                        letter = _alphabet.GetRandomLetter(isVowelNeeded);
                        if(SameLettersCount(letter) > 2) {
                            letter = _alphabet.GetRandomLetter(isVowelNeeded);
                        }
                    }
                }
                
                //Insert new ball
                var newBall = InsertBall(EBallType.Letter, letter, column: targetColumn, row: targetRow);
                newBalls.Add(newBall);
                
                //Prepare to next iteration
                nextColumn = startColumn < ColumnsCount - 1
                                 ? startColumn + 1
                                 : 0;
            }
            OnBallsAdded(newBalls);
        }

        /// <summary>
        /// Adds specified ball on the board to specified column and row.
        /// </summary>
        public IBall InsertBall(EBallType type, ILetterData letter, int column, int row) {
            IBall newBall = _createBall(type, letter);
            newBall.EnsureNotNull("newBall");
            if (newBall.Column.HasValue || newBall.Row.HasValue) {
                throw new ArgumentException(ErrorMessage.NewBallForBoardShouldHaveNullCoordinates);
            }

            _balls.Add(newBall);
            this[column, row].AddBall(newBall);
            newBall.AddToBoard(column, row);

            return newBall;
        }

        /// <summary>
        /// Removes specified balls from board, that leads to 
        /// falling down of balls which is above deleted balls.
        /// </summary>
        public void RemoveBalls(IEnumerable<IBall> removedBalls) {
            var removedBallsList = removedBalls.ToList();
            if (!removedBallsList.Any()) {
                return;
            }
            Diagnost.Assert(removedBallsList.Any(ball => ball != null), ErrorMessage.BallForBoardIsNull);
            Diagnost.Assert(removedBallsList.Any(ball => ball.Column.HasValue && ball.Row.HasValue),
                            ErrorMessage.BallsSpecifiedToRemoveShouldHaveValidCoordinates);
            var nonBoardBalls = removedBallsList.Where(ball => !_balls.ReferenceContains(ball)).ToList();
            Diagnost.Assert(!nonBoardBalls.Any(), ErrorMessage.BallIsNotAttachedToBoard);
            nonBoardBalls.ForEach(ball => removedBallsList.Remove(ball));

            removedBallsList.ForEach(ball => {
                _balls.Remove(ball);
                this[ball.Column.Value, ball.Row.Value].RemoveBall();
                ball.RemoveFromBoard();
            });
            OnBallsRemoved(removedBallsList);

            var fellBalls = new List<IBall>();
            for (int row = RowsCount - 1; row >= 0; row--) {
                for (int column = 0; column < ColumnsCount; column++) {
                    var ball = this[column, row].Ball;
                    if (ball == null) {
                        continue;
                    }
                    if (TryDropBallDown(ball)) {
                        fellBalls.Add(ball);
                    }
                }
            }
            OnBallsFell(fellBalls);
        }

        private int SameLettersCount(ILetterData letter) {
            return _balls.Count(ball => ball.Letter != null && ball.Letter.Equals(letter));
        }

        private bool? IsMustBeVowelHere(int targetColumn, int targetRow) {
            VerifyCoordinates(targetColumn, targetRow);
            int vowelVoices = 0;
            int nonVowelVoices = 0;
            int vowelsCount = 0;
            int nonVowelsCount = 0;
            var nearbyCells = FindNearbyCells(targetColumn, targetRow);
            foreach (var nearCell in nearbyCells) {
                bool? isVowel = null;
                if (nearCell.HasBall && nearCell.Ball.Type == EBallType.Letter) {
                    isVowel = nearCell.Ball.Letter.IsVowel;
                }
                if (!isVowel.HasValue) {
                    continue;
                }

                bool isLetterCanVote =
                    nearCell.Column == targetColumn ||
                    nearCell.Row == targetRow;

                if (isVowel.Value) {
                    vowelsCount++;
                    if (isLetterCanVote) {
                        nonVowelVoices++;
                    }
                }
                else {
                    nonVowelsCount++;
                    if (isLetterCanVote) {
                        vowelVoices++;
                    }
                }
            }

            if (vowelVoices > nonVowelVoices) {
                return true;
            }
            if (vowelVoices < nonVowelVoices) {
                return false;
            }
            if (vowelsCount > nonVowelsCount) {
                return false;
            }
            if (vowelsCount < nonVowelsCount) {
                return true;
            }
            return null;
        }

        private List<ICell> FindNearbyCells(int centerColumn, int centerRow) {
            VerifyCoordinates(centerColumn, centerRow);
            var nearbyCells = new List<ICell>();
            if (centerRow > 0) {
                nearbyCells.Add(this[centerColumn, centerRow - 1]);
                if (centerColumn > 0) {
                    nearbyCells.Add(this[centerColumn - 1, centerRow - 1]);
                }
                if (centerColumn < ColumnsCount - 1) {
                    nearbyCells.Add(this[centerColumn + 1, centerRow - 1]);
                }
            }
            if (centerRow < RowsCount - 1) {
                nearbyCells.Add(this[centerColumn, centerRow + 1]);
                if (centerColumn > 0) {
                    nearbyCells.Add(this[centerColumn - 1, centerRow + 1]);
                }
                if (centerColumn < ColumnsCount - 1) {
                    nearbyCells.Add(this[centerColumn + 1, centerRow + 1]);
                }
            }
            if (centerColumn > 0) {
                nearbyCells.Add(this[centerColumn - 1, centerRow]);
            }
            if (centerColumn < ColumnsCount - 1) {
                nearbyCells.Add(this[centerColumn + 1, centerRow]);
            }
            return nearbyCells;
        }

        private void CalculateBallFallTarget(int ballColumn, int ballRow, out int targetColumn, out int targetRow) {
            VerifyCoordinates(ballColumn, ballRow);

            ICell targetCell = (from cell in _cells
                                where
                                    cell.Column == ballColumn &&
                                    !cell.HasBall &&
                                    ballRow < cell.Row
                                orderby cell.Row ascending
                                select cell).LastOrDefault();
            if (targetCell != null) {
                targetColumn = targetCell.Column;
                targetRow = targetCell.Row;
            }
            else {
                targetColumn = ballColumn;
                targetRow = ballRow;
            }
        }

        /// <summary>
        /// Drops ball, that on already on board - down, in direction from lesser row
        /// index to greater.
        /// </summary>
        /// <param name="ball">Falling ball.</param>
        /// <returns><c>True</c>, if ball actually fell, otherwise returns <c>false</c>.</returns>
        /// <exception cref="InvalidOperationException">Thrown when
        /// <see cref="Ball.Column"/> or <see cref="Ball.Row"/> 
        /// of <paramref name="ball"/> is <c>null</c>.</exception>
        /// <exception cref="ArgumentNullException">Thrown 
        /// when ball is <c>null</c>.</exception>
        private bool TryDropBallDown(IBall ball) {
            ball.EnsureNotNull("ball");
            
            if (!ball.Column.HasValue) {
                throw new InvalidOperationException(ErrorMessage.CannotDropingBallWithNullColumn);
            }
            if (!ball.Row.HasValue) {
                throw new InvalidOperationException(ErrorMessage.CannotDropingBallWithNullRow);
            }

            int ballColumn = ball.Column.Value;
            int ballRow = ball.Row.Value;

            ICell sourceCell = this[ballColumn, ballRow];
            int targetColumn;
            int targetRow;
            CalculateBallFallTarget(ballColumn, ballRow, out targetColumn, out targetRow);
            ICell targetCell = this[targetColumn, targetRow];

            if (!ReferenceEquals(targetCell, sourceCell)) {
                sourceCell.RemoveBall();
                targetCell.AddBall(ball);
                ball.MoveToCell(targetColumn, targetRow);
                return true;
            }
            return false;
        }

        private void GenerateCells() {
            _cells.Clear();
            for (int column = 0; column < _columnsCount; column++) {
                for (int row = 0; row < _rowsCount; row++) {
                    _cells.Add(_createCell(column, row));
                }
            }
        }

        /// <summary>
        /// Checks that specified coordinates are within board area
        /// and throws exception if it is false.
        /// </summary>
        /// <exception cref="ArgumentOutOfRangeException">See summary.</exception>
        private void VerifyCoordinates(int column, int row) {
            if (column < 0 || ColumnsCount <= column ||
                row < 0 || RowsCount <= row) {

                string message = string.Format("Invalid coordinates: column={0} row={1} " +
                                               "when ColumnsCount={2} and RowsCount={3}",
                                               column,
                                               row,
                                               ColumnsCount,
                                               RowsCount);
                throw new ArgumentOutOfRangeException(message);
            }
        }

        private void OnBallsAdded(IEnumerable<IBall> addedBalls) {
            var handler = BallsAdded;
            if (handler != null) {
                handler(this, new BallsEventArgs(addedBalls));
            }
        }

        private void OnBallsFell(IEnumerable<IBall> fellBalls) {
            var handler = BallsFell;
            if (handler != null) {
                handler(this, new BallsEventArgs(fellBalls));
            }
        }

        private void OnBallsRemoved(IEnumerable<IBall> removedBalls) {
            var handler = BallsRemoved;
            if (handler != null) {
                handler(this, new BallsEventArgs(removedBalls));
            }
        }

        private void OnReset() {
            var handler = Reset;
            if (handler != null)
                handler(this, EventArgs.Empty);
        }
    }
}