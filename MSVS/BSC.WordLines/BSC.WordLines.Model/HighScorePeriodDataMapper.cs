﻿using System;
using System.Collections.Generic;
using System.Linq;
using BSC.WordLines.Data;
using BSC.WordLines.Utils;

namespace BSC.WordLines.Model {
    public class HighScorePeriodDataMapper {
        private readonly ILocalDataProvider _provider;
        private readonly Dictionary<EHighScorePeriods, IHighScorePeriodData> _periods;

        public HighScorePeriodDataMapper(ILocalDataProvider provider) {
            if (provider == null) {
                throw new ArgumentNullException("provider");
            }
            _provider = provider;

            var highScorePeriods = (_provider.GetHighScorePeriods() ?? new IHighScorePeriodData[] {}).ToList();
            if (highScorePeriods.Count == 0) {
                throw new InvalidOperationException(ErrorMessage.NoHighScorePeriodsFound);
            }

            var allTimePeriod = highScorePeriods.Find(
                periodData => IsDataMatchEnum(periodData, EHighScorePeriods.AllTime));
            if (allTimePeriod == null) {
                throw new InvalidOperationException(
                    ErrorMessage.CannotFindHighScorePeriodInDb(EHighScorePeriods.AllTime.ToString()));
            }
            var weekPeriod = highScorePeriods.Find(
                periodData => IsDataMatchEnum(periodData, EHighScorePeriods.Week));
            if (weekPeriod == null) {
                throw new InvalidOperationException(
                    ErrorMessage.CannotFindHighScorePeriodInDb(EHighScorePeriods.Week.ToString()));
            }

            _periods = new Dictionary<EHighScorePeriods, IHighScorePeriodData> {
                {EHighScorePeriods.AllTime, allTimePeriod},
                {EHighScorePeriods.Week, weekPeriod}
            };

            if (Enum.GetValues(typeof(EHighScorePeriods)).Length != _periods.Count) {
                throw new InvalidOperationException(ErrorMessage.NotAllHighScorePeriodsSupportedByDb);
            }
        }

        public int GetHighScorePeriodDataId(EHighScorePeriods enumValue) {
            return GetHighScorePeriodData(enumValue).Id;
        }

        public IHighScorePeriodData GetHighScorePeriodData(EHighScorePeriods enumValue) {
            return _periods[enumValue];
        }

        public EHighScorePeriods GetHighScorePeriod(IHighScorePeriodData dataValue) {
            foreach (var keyValuePair in _periods) {
                if (IsDataMatchEnum(dataValue, keyValuePair.Key)) {
                    return keyValuePair.Key;
                }
            }
            throw new InvalidOperationException(ErrorMessage.CannotMapHighScorePeriodDataToEnumValue);
        }

        private bool IsDataMatchEnum(IHighScorePeriodData dataValue, EHighScorePeriods enumValue) {
            string periodName = dataValue.Name ?? string.Empty;
            return string.Equals(periodName, enumValue.ToString(), StringComparison.Ordinal);
        }
    }
}