using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BSC.WordLines.Data;
using BSC.WordLines.Utils;

namespace BSC.WordLines.Model {
    public class WordDictionary : IWordDictionary {
        private readonly IWordsProvider _provider;
        private readonly Dictionary<int, List<string>> _wordsDic;

        public WordDictionary(IWordsProvider provider) {
            provider.EnsureNotNull("provider");
            _provider = provider;
            _wordsDic = new Dictionary<int, List<string>>();
        }

        public async Task UpdateAsync() {
            _wordsDic.Clear();
            var allWords = await _provider.GetWordsAsync();
            foreach (var word in allWords) {
                int wordLength = word.Length;
                if (!_wordsDic.ContainsKey(wordLength)) {
                    _wordsDic.Add(wordLength, new List<string>());
                }
                _wordsDic[wordLength].Add(word);
            }
        }

        public string FindWord(Queue<ILetterData> letters, List<int> wildcardIndexes) {
            var wordLength = letters.Count;
            var isLengthExists = _wordsDic.ContainsKey(wordLength);
            Diagnost.Assert(isLengthExists, ErrorMessage.CannotFindWordWithLength(wordLength));
            if (!isLengthExists) {
                return null;
            }
            var wordList = _wordsDic[wordLength];
            return wordList.FirstOrDefault(word => WordEqualsLetters(word, letters, wildcardIndexes));
        }

        /// <summary>
        /// Architecture statement: FOR TEXT OF WORDS AND LETTERS IS TRUE THAT ONE CHAR IS A ONE LETTER.
        /// </summary>
        private bool WordEqualsLetters(string word, Queue<ILetterData> letters, List<int> wildcardIndexes) {
            if (wildcardIndexes.Count > 0 && wildcardIndexes.Any(index=> index >= letters.Count)) {
                throw new InvalidOperationException(ErrorMessage.WildcardIndexOutOfRanged);
            }
            
            if (word.Length != letters.Count) {
                return false;
            }
            var wordBuilder = new StringBuilder();
            var localLetters = new Queue<ILetterData>(letters);
            int letterIndex = 0;
            while (localLetters.Count > 0) {
                var letter = localLetters.Dequeue();
                if (wildcardIndexes.Count > 0 && wildcardIndexes.Contains(letterIndex)) {
                    wordBuilder.Append(word[letterIndex]);
                }
                else {
                    wordBuilder.Append(letter);
                }
                letterIndex++;
            }
            return string.Equals(word, wordBuilder.ToString(), StringComparison.OrdinalIgnoreCase);
        }
    }
}