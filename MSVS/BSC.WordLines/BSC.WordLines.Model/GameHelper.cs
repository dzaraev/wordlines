﻿namespace BSC.WordLines.Model {
    public static class GameHelper {
        public static EWordLength GetWordLength(int lettersCount) {
            if (lettersCount <= 0) {
                return EWordLength.Undefined;
            }
            if (lettersCount <= GameConst.ShortWordMaxLength) {
                return EWordLength.Short;
            }
            if (lettersCount <= GameConst.MediumWordMaxLength) {
                return EWordLength.Medium;
            }
            return EWordLength.Long;
        }
    }
}