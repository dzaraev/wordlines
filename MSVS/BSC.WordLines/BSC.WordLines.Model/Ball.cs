﻿using System;
using System.Collections.Generic;
using BSC.WordLines.Data;
using BSC.WordLines.Utils;

namespace BSC.WordLines.Model {
    public sealed class Ball : IBall {
        /// <summary>
        /// Rises when ball was attached to a board.
        /// </summary>
        public event EventHandler AddedToBoard;

        /// <summary>
        /// Rises when ball was moved to another cell
        /// </summary>
        public event EventHandler MovedToAnotherCell;

        /// <summary>
        /// Rises when ball was removed from a board.
        /// </summary>
        public event EventHandler RemovedFromBoard;

        /// <summary>
        /// Rises when property <see cref="Type"/> changed.
        /// </summary>
        public event EventHandler TypeChanged;

        /// <summary>
        /// Gets column where ball is lying on board.
        /// </summary>
        public int? Column { get; private set; }

        /// <summary>
        /// Gets row where ball is lying on board.
        /// </summary>
        public int? Row { get; private set; }

        /// <summary>
        /// Indicates whether ball is lying on board.
        /// </summary>
        public bool IsOnBoard { get; private set; }

        /// <summary>
        /// Gets associated letter if <see cref="Type"/> 
        /// equals <see cref="EBallType.Letter"/>,
        /// otherwise returns null.
        /// </summary>
        public ILetterData Letter { get; private set; }

        /// <summary>
        /// Gets type of ball.
        /// </summary>
        public EBallType Type { get; private set; }
        
        /// <summary>
        /// Initializes a new instance of <see cref="Ball"/> class. 
        /// </summary>
        /// <param name="type">Type of a new ball.</param>
        /// <param name="letter">Letter, associated with a new ball. 
        /// Must be <c>null</c> for <paramref name="type"/> 
        /// values other than <see cref="EBallType.Letter"/>.</param>
        /// <exception cref="ArgumentNullException">
        /// Thrown when <paramref name="letter"/> is null.</exception>
        public Ball(EBallType type, ILetterData letter) {
            letter.EnsureNotNull("letter");
            Type = type;
            Letter = letter;
        }

        /// <summary>
        /// Attaches ball to a board specified coordinates.
        /// </summary>
        /// <exception cref="InvalidOperationException">
        /// Thrown when ball is already on a board.</exception>
        public void AddToBoard(int column, int row) {
            if (IsOnBoard) {
                throw new InvalidOperationException(ErrorMessage.BallAlreadyOnBoard);
            }

            Column = column;
            Row = row;
            IsOnBoard = true;
            OnAddedToBoard();
        }

        /// <summary>
        /// Reattaches ball to another coordinates on a board.
        /// </summary>
        /// <exception cref="InvalidOperationException">
        /// Thrown when ball is not attached to board yet.</exception>
        public void MoveToCell(int column, int row) {
            if (!IsOnBoard) {
                throw new InvalidOperationException(ErrorMessage.BallIsNotAttachedToBoard);
            }

            Column = column;
            Row = row;
            OnMovedToAnotherCell();
        }

        /// <summary>
        /// Detaches ball from a board.
        /// </summary>
        public void RemoveFromBoard() {
            if (!IsOnBoard) {
                return;
            }
            Column = null;
            Row = null;
            IsOnBoard = false;
            OnRemovedFromBoard();
        }

        public void SetWildcard(bool isWildcardEnabled) {
            Type = isWildcardEnabled ? EBallType.Wildcard : EBallType.Letter;
            OnTypeChanged();
        }

        private void OnAddedToBoard() {
            var handler = AddedToBoard;
            if (handler != null) {
                handler(this, EventArgs.Empty);
            }
        }

        private void OnMovedToAnotherCell() {
            var handler = MovedToAnotherCell;
            if (handler != null) {
                handler(this, EventArgs.Empty);
            }
        }

        private void OnRemovedFromBoard() {
            var handler = RemovedFromBoard;
            if (handler != null) {
                handler(this, EventArgs.Empty);
            }
        }

        private void OnTypeChanged() {
            var handler = TypeChanged;
            if (handler != null) {
                handler(this, EventArgs.Empty);
            }
        }
    }
}