using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Text;
using BSC.WordLines.Data;
using BSC.WordLines.Utils;

namespace BSC.WordLines.Model {
    public class FileGameStateSaver : IGameStateSaver {

        //WARNING!!! NEVER CHANGE LETTER IDs IN DATABASE!

        private const string SAVE_FILE_NAME = "save";
        private const char BALL_VALUES_SEPARATOR = ' ';

        private readonly SettingsManager _settingsManager;
        private readonly LanguageDataMapper _languageDataMapper;
        private readonly ILocalDataProvider _dataProvider;

        public FileGameStateSaver(
            SettingsManager settingsManager,
            LanguageDataMapper languageDataMapper,
            ILocalDataProvider dataProvider) {

            settingsManager.EnsureNotNull("settingsManager");
            languageDataMapper.EnsureNotNull("langaugeDataMapper");
            dataProvider.EnsureNotNull("dataProvider");

            _settingsManager = settingsManager;
            _languageDataMapper = languageDataMapper;
            _dataProvider = dataProvider;
        }

        public void SaveGame(IGame game, IBoard board) {
            game.EnsureNotNull("game");
            board.EnsureNotNull("board");

            //WARNING!!! This order must match to loading order.
            CultureInfo culture = CultureInfo.InvariantCulture;
            StringBuilder content = new StringBuilder();
            content.AppendLine(_languageDataMapper.GetLanguage(_settingsManager.CurrentLanguage).ToString());
            content.AppendLine(game.Mode.ToString());
            content.AppendLine(game.CurrentScore.ToString(culture));
            content.AppendLine(game.CurrentMoney.ToString(culture));
            content.AppendLine(game.AdditionActivationsCount.ToString(culture));
            content.AppendLine(game.MovesCount.ToString(culture));
            content.AppendLine(game.SecondsCount.ToString(culture));
            foreach (var ball in board.Balls) {
                content.AppendLine(string.Format(
                    "{1}{0}{2}{0}{3}{0}{4}", BALL_VALUES_SEPARATOR, ball.Type, ball.Letter.Id, ball.Column, ball.Row));
            }

            using (var storage = IsolatedStorageFile.GetUserStoreForApplication()) {
                if (storage.FileExists(SAVE_FILE_NAME)) {
                    storage.DeleteFile(SAVE_FILE_NAME);
                }

                try {
                    using (var writer = new StreamWriter(storage.CreateFile(SAVE_FILE_NAME))) {
                        var bytes = Encoding.UTF8.GetBytes(content.ToString());
                        string base64 = Convert.ToBase64String(bytes);
                        writer.Write(base64);
                        writer.Close();
                    }
                }
                catch (Exception) {
                    if (storage.FileExists(SAVE_FILE_NAME)) {
                        storage.DeleteFile(SAVE_FILE_NAME);
                    }
                }
            }
        }

        public bool TryLoadGame(IGame game, out ELanguage language) {
            game.EnsureNotNull("game");
            var board = game.Board;
            board.EnsureNotNull("board");

            try {
                List<string> content = new List<string>();
                string base64Content;
                using (var storage = IsolatedStorageFile.GetUserStoreForApplication()) {
                    if (!storage.FileExists(SAVE_FILE_NAME)) {
                        language = default(ELanguage);
                        return false;
                    }
                    using (var fileStream = storage.OpenFile(SAVE_FILE_NAME, FileMode.Open, FileAccess.Read))
                    using (var reader = new StreamReader(fileStream)) {
                        base64Content = reader.ReadToEnd();
                        reader.Close();
                    }
                }
                var contentBytes = Convert.FromBase64String(base64Content);
                var contentString = Encoding.UTF8.GetString(contentBytes, 0, contentBytes.Length);

                using (var reader = new StringReader(contentString)) {
                    string line = reader.ReadLine();
                    while (line != null) {
                        content.Add(line);
                        line = reader.ReadLine();
                    }
                    reader.Close();
                }

                //WARNING!!! This order must match to saving order.
                language = (ELanguage) Enum.Parse(typeof (ELanguage), content[0]);
                EGameMode gameMode = (EGameMode) Enum.Parse(typeof (EGameMode), content[1]);
                int currentScore = int.Parse(content[2]);
                int currentMoney = int.Parse(content[3]);
                int additionActivationsCount = int.Parse(content[4]);
                int movesCount = int.Parse(content[5]);
                int secondsCount = int.Parse(content[6]);

                var balls = new List<Tuple<EBallType, int, int>>();//format: type, column, row
                var ballsLetterIds = new List<int>();
                for (int i = 7; i < 32; i++) {
                    var ballValues = content[i].Split(BALL_VALUES_SEPARATOR);
                    EBallType ballType = (EBallType)Enum.Parse(typeof(EBallType), ballValues[0]);
                    int letterId = int.Parse(ballValues[1]);
                    int column = int.Parse(ballValues[2]);
                    int row = int.Parse(ballValues[3]);
                    balls.Add(new Tuple<EBallType, int, int>(ballType, column, row));
                    ballsLetterIds.Add(letterId);
                }
                var ballsLetters = _dataProvider.GetLetters(ballsLetterIds);
                
                //SET BOARD STATE
                board.Clear();
                for (int i = 0; i < balls.Count; i++) {
                    board.InsertBall(balls[i].Item1, ballsLetters[i], balls[i].Item2, balls[i].Item3);
                }
                //SET GAME STATE
                game.Mode = gameMode;
                game.CurrentScore = currentScore;
                game.CurrentMoney = currentMoney;
                game.AdditionActivationsCount = additionActivationsCount;
                game.MovesCount = movesCount;
                game.SecondsCount = secondsCount;
                game.State = EGameState.InProcess;
            }
            catch (Exception ex) {
                using (var storage = IsolatedStorageFile.GetUserStoreForApplication()) {
                    if (storage.FileExists(SAVE_FILE_NAME)) {
                        storage.DeleteFile(SAVE_FILE_NAME);
                    }
                }
                Diagnost.LogHandledException(ex);
                language = default(ELanguage);
                return false;
            }
            return true;
        }

        public void DeleteSavedGame() {
            using (var storage = IsolatedStorageFile.GetUserStoreForApplication()) {
                if (storage.FileExists(SAVE_FILE_NAME)) {
                    storage.DeleteFile(SAVE_FILE_NAME);
                }
            }
        }
    }
}