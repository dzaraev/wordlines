﻿namespace BSC.WordLines.Model {
    public static class GameConst {
        //Gameplay constants.
        public static readonly int DefaultMovesCount = 10;
        public static readonly int DefaultSecondsCount = 120;
        public static readonly int MaxBallsInChainDistance = 1;
        public static readonly int MinBallsToKillChain = 2;
        public static readonly int AdditionToMoves = 1;
        public static readonly int AdditionToSeconds = 10;
        public static readonly int AdditionMaxActivations = 1;
        public static readonly int SecondsLeftForMetronome = 5;
        
        //TODO отправить нижние 5 констант в ресурсы в будущем (если для другого языка потребуются другие константы)
        public static readonly int ShortWordMaxLength = 4;
        public static readonly int MediumWordMaxLength = 6;
        public static readonly int ShortWordPointsPerBall = 1;
        public static readonly int MediumWordPointsPerBall = 2;
        public static readonly int LongWordPointsPerBall = 3;

        //Monetization key attributes (game currency - points).
        //Time current (game time without boosters): 10 minutes = 500 points.
        //One moves mode round cost: 2000 points
        //One time mode round cost: 500 points
        //Exchange rates (by four preset volumes): 
        //1$ = 1500 points (1$ purchase) => 1500 points => 3000 points (1$=3000)
        //1$ = 2250 points (2$ purchase) => 4500 points => 9000 points (1$=4500)
        //1$ = 3000 points (5$ purchase) => 15000 points => 30000 points (1$=6000)
        //1$ = 4500 points (10$ purchase) => 45000 points => deleted
        public static readonly int KillBallPrice = 130;
        public static readonly int WildcardPrice = 30;
        public static readonly int AdditionPrice = 50;
        public static readonly int KillBallPacketCount = 5;
        public static readonly int WildcardPacketCount = 5;
        public static readonly int AdditionPacketCount = 5;
        public static readonly int PurchasedMoneyCalcFailedValue = 30000;//Fail compensation by max course.
        public static readonly int MaxMoneyValue = int.MaxValue - 100000;//Guard value.
        public static readonly string CurrencyProductKeyword = "currency";

        //Misc
        public static readonly string GameSupportMail = "wordlordgame@hotmail.com";
        public static readonly string AppLink = "http://www.windowsphone.com/s?appid=a7b9bc52-23d8-458d-8af0-2438fae1cd4c";//RELEASE
        //public static readonly string AppLink = "http://www.windowsphone.com/s?appid=19e0c1ee-e671-4449-850f-54b68d720238";//BETA
        public static readonly string PrivacyPolicyLink = "http://myapppolicy.com/app/wordlord";
        public static readonly string TwitterLink = "http://twitter.com/bsc_apps";
        public static readonly string VkLink = "http://vk.com/bsc_apps";
        public static readonly string FbLink = "https://www.facebook.com/groups/612115302253142/";
    }
}