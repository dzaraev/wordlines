﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using BSC.WordLines.Utils;
using Windows.ApplicationModel.Store;

namespace BSC.WordLines.Model {
    public class StoreManager : IStoreManager {
        private readonly IPlayer _player;
        private ListingInformation _currencyProductsListingInfo;

        public StoreManager(IPlayer player) {
            player.EnsureNotNull("player");
            _player = player;
            _currencyProductsListingInfo = null;
        }

        public IEnumerable<ProductListing> CurrencyProductListings {
            get {
                return _currencyProductsListingInfo != null
                           ? _currencyProductsListingInfo.ProductListings.Values
                           : new ProductListing[] {};
            }
        }

        public async Task<bool> UpdateCurrencyProductListings() {
            try {
                //Load from store.
                _currencyProductsListingInfo = await CurrentApp.LoadListingInformationByKeywordsAsync(
                    new[] {GameConst.CurrencyProductKeyword});

                //Verifying products.
                if (_currencyProductsListingInfo.ProductListings.Values.Any(product => {
                    int purchasedMoney;
                    return product == null ||
                           product.ProductType != ProductType.Consumable ||
                           !int.TryParse(product.Tag, out purchasedMoney);
                })) {
                    //Error with product settings in store!
                    return false;
                }

                //Update OK.
                return true;
            }
            catch (Exception ex) {
                //Update unexpectedly failed!
                return false;
            }
        }

        public async Task<int> PurchaseMoney(ProductListing productListing) {
            productListing.EnsureNotNull("productListing");
            
            //Purchase
            try {
                _player.IsPurchaseNotFinished = true;
                await CurrentApp.RequestProductPurchaseAsync(productListing.ProductId, false);
            }
            catch (Exception) {
                /*DO NOTHING*/
            }

            int purchasedMoney = await EnsureFulfillmentAsync();
            return purchasedMoney;
        }

        public int GetPurchasedMoney(ProductListing consumableProduct) {
            int purchasedMoney;
            if (consumableProduct == null ||
                consumableProduct.ProductType != ProductType.Consumable ||
                !int.TryParse(consumableProduct.Tag, out purchasedMoney)) {

                Diagnost.Assert(false, ErrorMessage.CannotFetchPurchasedMoneyValueFromProductListing);
                purchasedMoney = GameConst.PurchasedMoneyCalcFailedValue;
            }
            return purchasedMoney;
        }

        public async Task<int> EnsureFulfillmentAsync() {
            if (!_player.IsPurchaseNotFinished) {
                return 0;
            }
            if (_currencyProductsListingInfo == null) {
                try {
                    //Load from store.
                    _currencyProductsListingInfo = await CurrentApp.LoadListingInformationByKeywordsAsync(
                        new[] {GameConst.CurrencyProductKeyword});
                    _currencyProductsListingInfo.EnsureNotNull("_currencyProductsListingInfo");
                }
                catch (Exception) {
                    return 0;
                }
            }
            var licenses = CurrentApp.LicenseInformation.ProductLicenses.Values;
            int fulfillmentSum = 0;
            foreach (var productLicense in licenses) {
                if (productLicense.IsConsumable && productLicense.IsActive) {
                    var product = _currencyProductsListingInfo.ProductListings[productLicense.ProductId];
                    int purchasedMoney = GetPurchasedMoney(product);
                    _player.Money += purchasedMoney;
                    CurrentApp.ReportProductFulfillment(productLicense.ProductId);
                    fulfillmentSum += purchasedMoney;
                }
            }
            _player.IsPurchaseNotFinished = false;
            return fulfillmentSum;
        }
    }
}