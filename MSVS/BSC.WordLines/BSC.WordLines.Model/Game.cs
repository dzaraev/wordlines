using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading;
using BSC.WordLines.Data;
using BSC.WordLines.Utils;

namespace BSC.WordLines.Model {
    public class Game : IGame {
        private const int TIMER_PERIOD_MS = 1000;
        
        private readonly IWordDictionary _words;
        private readonly IGameStateSaver _stateSaver;
        private readonly Timer _timer;
        private readonly IPlayer _player;
        private readonly IBoard _board;
        private readonly SynchronizationContext _timerContext;
        private int _currentMoney;
        private int _currentScore;
        private int _roundScore;
        private int _roundMoney;
        private int _movesCount;
        private int _secondsCount;
        private int _additionActivationsCount;
        private EGameState _state;
        private EGameMode _mode;

        public Game(IPlayer player, IBoard board, IWordDictionary words, IGameStateSaver stateSaver) {
            board.EnsureNotNull("board");
            words.EnsureNotNull("words");
            player.EnsureNotNull("player");
            stateSaver.EnsureNotNull("stateSaver");

            _player = player;
            _board = board;
            _words = words;
            _stateSaver = stateSaver;
            _currentMoney = 0;
            _currentScore = 0;
            _roundScore = 0;
            _movesCount = GameConst.DefaultMovesCount;
            _secondsCount = GameConst.DefaultSecondsCount;
            _additionActivationsCount = 0;

            _timerContext = SynchronizationContext.Current;
            _timer = new Timer(HandleTimerCallback, null, Timeout.Infinite, TIMER_PERIOD_MS);
            
            State = EGameState.NotStarted;
        }

        /// <summary>
        /// Rises when game state was changed.
        /// </summary>
        public event EventHandler<GameStateChangedEventArgs> StateChanged;
        public event EventHandler MovesCountChanged;
        public event EventHandler SecondsCountChanged;
        public event EventHandler CurrentScoreChanged;
        public event EventHandler RoundScoreChanged;
        public event EventHandler RoundMoneyChanged;

        public IBoard Board {
            get { return _board; }
        }

        /// <summary>
        /// Gets the game's current state.
        /// </summary>
        public EGameState State {
            get { return _state; }
            set {
                if (_state != value) {
                    EGameState oldState = _state;
                    _state = value;
                    OnStateChanged(oldState, value);
                }
            }
        }

        /// <summary>
        /// Gets the game's mode.
        /// </summary>
        public EGameMode Mode {
            get { return _mode; }
            set {
                if (_state != EGameState.NotStarted) {
                    throw new InvalidOperationException(ErrorMessage.GameModeCanBeChangedOnlyInNotStartedState);
                }
                _mode = value;
            }
        }

        /// <summary>
        /// Gets the how match moves left before end of the game,
        /// when game is in Moves mode.  
        /// </summary>
        public int MovesCount {
            get { return _movesCount; }
            set {
                if (_movesCount != value) {
                    _movesCount = value;
                    OnMovesCountChanged();
                }
            }
        }

        public int SecondsCount {
            get { return _secondsCount; }
            set {
                if (_secondsCount != value) {
                    _secondsCount = value;
                    OnSecondsCountChanged();
                }
            }
        }

        public int CurrentMoney {
            get { return _currentMoney; }
            set { _currentMoney = value; }
        }

        public int CurrentScore {
            get { return _currentScore; }
            set {
                if (_currentScore != value) {
                    _currentScore = value;
                    OnCurrentScoreChanged();
                }
            }
        }

        public int RoundScore {
            get { return _roundScore; }
            private set {
                if (_roundScore != value) {
                    _roundScore = value;
                    OnRoundScoreChanged();
                }
            }
        }

        public int RoundMoney {
            get { return _roundMoney; }
            private set {
                if (_roundMoney != value) {
                    _roundMoney = value;
                    OnRoundMoneyChanged();
                }
            }
        }

        public int AdditionActivationsCount {
            get { return _additionActivationsCount; }
            set { _additionActivationsCount = value; }
        }

        /// <summary>
        /// Reinitialize game as a new game with specified mode.
        /// </summary>
        public void StartNew() {
            _board.Clear();
            _board.DropBalls(_board.RowsCount * _board.ColumnsCount);
            _additionActivationsCount = 0;
            CurrentMoney = 0;
            CurrentScore = 0;
            RoundScore = 0;
            RoundMoney = 0;

            if (Mode == EGameMode.Moves) {
                MovesCount = GameConst.DefaultMovesCount;
            }
            else if (Mode == EGameMode.Time) {
                SecondsCount = GameConst.DefaultSecondsCount;
                StartTimer();
            }
            else {
                throw new InvalidOperationException(ErrorMessage.UnexpectedlyEnumValue(Mode));
            }

            State = EGameState.InProcess;
        }

        public void Stop() {
            _stateSaver.DeleteSavedGame();
            _board.Clear();
            RoundScore = 0;
            RoundMoney = 0;
            CurrentScore = 0;
            CurrentMoney = 0;
            StopTimer();
            SecondsCount = GameConst.DefaultSecondsCount;
            MovesCount = GameConst.DefaultMovesCount;
            State = EGameState.NotStarted;
        }

        public void Pause() {
            if (State == EGameState.InProcess) {
                StopTimer();
                _stateSaver.SaveGame(this, _board);
            }
        }

        public void Continue() {
            if (State == EGameState.InProcess) {
                StartTimer();
            }
        }

        public bool KillChain(Queue<IBall> balls) {
            if (State != EGameState.InProcess) {
                return false;
            }

            if (!IsChainValid(balls)) {
                return false;
            }

            if (balls.Count < GameConst.MinBallsToKillChain) {
                return false;
            }

            var letters = new Queue<ILetterData>(balls.Select(ball => ball.Letter));
            var wildcardIndexes = GetWildcardIndexes(balls);
            string word = _words.FindWord(letters, wildcardIndexes);
            if (word != null) {
                int lettersCount = letters.Count;
                
                int removedBallsCount = balls.Count;
                _board.RemoveBalls(balls);
                _board.DropBalls(removedBallsCount);

                //Score and money bonus per move
                int moveBonus = CalculateMoveBonus(lettersCount);
                CurrentScore += moveBonus;
                CurrentMoney += moveBonus;

                //Game over check
                if (Mode == EGameMode.Moves) {
                    MovesCount--;
                    if (MovesCount == 0) {
                        Finish();
                    }
                }
                return true;
            }
            return false;
        }

        public bool CanApplyKillBallBooster() {
            return _player.KillBallCount > 0;
        }

        //Optimisation.
        public void ApplyKillBallBooster(IBall ball) {
            if (!CanApplyKillBallBooster()) {
                Diagnost.Assert(false, ErrorMessage.CanNotApplyKillBallBooster);
                return;
            }

            _board.RemoveBalls(new[] {ball});
            _board.DropBalls(1);
            _player.KillBallCount--;
        }

        public bool CanApplyWildcardBootster() {
            bool isWildscardsExistsOnBoard = _board.Balls.Any(ball => ball.Type == EBallType.Wildcard);
            return _player.WildcardCount > 0 || isWildscardsExistsOnBoard;
        }
        
        public bool ApplyWildcardBooster(IBall targetBall) {
            if (!CanApplyWildcardBootster()) {
                Diagnost.Assert(false, ErrorMessage.CanNotApplyWildcardBooster);
                return false;
            }

            //Cancelation of wildcard.
            if (targetBall.Type == EBallType.Wildcard) {
                targetBall.SetWildcard(false);
                _player.WildcardCount++;
                return false;
            }

            //Cancel previous setted wildcard(s) before set a new one.
            int newWildcardCount = _player.WildcardCount;
            var ballsWithWildcard = _board.Balls.Where(ball => ball.Type == EBallType.Wildcard);
            foreach (var ball in ballsWithWildcard) {
                ball.SetWildcard(false);
                newWildcardCount++;
            }

            //Set only one wildcard at time.
            targetBall.SetWildcard(true);
            newWildcardCount--;
            _player.WildcardCount = newWildcardCount;
            return true;
        }

        public bool IsAdditionBoosterRestricted() {
            return _additionActivationsCount >= GameConst.AdditionMaxActivations;
        }

        public bool CanApplyAdditionBooster() {
            return _player.AdditionCount > 0 && !IsAdditionBoosterRestricted();
        }

        public void ApplyAdditionBooster() {
            if (!CanApplyAdditionBooster()) {
                Diagnost.Assert(false, ErrorMessage.CanNotApplyAdditionBooster);
                return;
            }

            if (Mode == EGameMode.Moves) {
                MovesCount += GameConst.AdditionToMoves;
            }
            else if (Mode == EGameMode.Time) {
                SecondsCount += GameConst.AdditionToSeconds;
            }
            else {
                throw new InvalidOperationException(ErrorMessage.UnknownGameModeFormat.FormatWith(Mode));
            }
            _additionActivationsCount++;
            _player.AdditionCount--;
        }

        public bool IsChainValid(Queue<IBall> chain) {
            var chainLocal = new Queue<IBall>(chain);
            while (chainLocal.Count > 1) {
                var currentBall = chainLocal.Dequeue();
                var nextBall = chainLocal.Peek();

                if (!currentBall.Row.HasValue ||
                    !nextBall.Row.HasValue ||
                    !currentBall.Column.HasValue ||
                    !nextBall.Column.HasValue) {

                    return false;
                }
                bool isRowsTooFar = Math.Abs(currentBall.Row.Value - nextBall.Row.Value) > GameConst.MaxBallsInChainDistance;
                bool isColumnsTooFar = Math.Abs(currentBall.Column.Value - nextBall.Column.Value) > GameConst.MaxBallsInChainDistance;
                if (isRowsTooFar || isColumnsTooFar) {
                    return false;
                }
            }
            return true;
        }

        private int CalculateMoveBonus(int killedWordLength) {
            if (killedWordLength <= GameConst.ShortWordMaxLength) {

                return GameConst.ShortWordPointsPerBall * killedWordLength;
            }
            if (killedWordLength > GameConst.ShortWordMaxLength &&
                killedWordLength <= GameConst.MediumWordMaxLength) {

                return GameConst.MediumWordPointsPerBall * killedWordLength;
            }
            if (killedWordLength > GameConst.MediumWordMaxLength) {
                return GameConst.LongWordPointsPerBall * killedWordLength;
            }
            throw new InvalidOperationException(ErrorMessage.CannotCalcMoveBonusWithWordLength(killedWordLength));
        }

        private void Finish() {
            _stateSaver.DeleteSavedGame();
            StopTimer();
            RoundScore = CurrentScore;
            RoundMoney = CurrentMoney;
            _player.Money += RoundMoney;
            _player.LastRoundScore = RoundScore;
            State = EGameState.GameOver;
            _board.Clear();
        }

        private List<int> GetWildcardIndexes(IEnumerable<IBall> balls) {
            List<int> wildcardIndexes = new List<int>();
            int ballIndex = 0;
            foreach (var ball in balls) {
                if (ball.Type == EBallType.Wildcard) {
                    wildcardIndexes.Add(ballIndex);
                }
                ballIndex++;
            }
            return wildcardIndexes;
        }

        private void StartTimer() {
            _timer.Change(TIMER_PERIOD_MS, TIMER_PERIOD_MS);
        }

        private void StopTimer() {
            _timer.Change(Timeout.Infinite, Timeout.Infinite);
        }

        private void OnCurrentScoreChanged() {
            var handler = CurrentScoreChanged;
            if (handler != null) {
                handler(this, EventArgs.Empty);
            }
        }

        private void OnRoundScoreChanged() {
            var handler = RoundScoreChanged;
            if (handler != null) {
                handler(this, EventArgs.Empty);
            }
        }

        private void OnRoundMoneyChanged() {
            var handler = RoundMoneyChanged;
            if (handler != null) {
                handler(this, EventArgs.Empty);
            }
        }

        private void OnMovesCountChanged() {
            var handler = MovesCountChanged;
            if (handler != null) {
                handler(this, EventArgs.Empty);
            }
        }

        private void OnSecondsCountChanged() {
            var handler = SecondsCountChanged;
            if (handler != null) {
                handler(this, EventArgs.Empty);
            }
        }

        private void OnStateChanged(EGameState oldState, EGameState newState) {
            var handler = StateChanged;
            if (handler != null) {
                handler(this, new GameStateChangedEventArgs(oldState, newState));
            }
        }

        private void HandleTimerCallback(object state) {
            _timerContext.Post(postState => {
                //Game over check
                if (Mode == EGameMode.Time) {
                    SecondsCount--;
                    if (SecondsCount == 0) {
                        Finish();
                    }
                }
            }, null);
        }
    }
}