﻿using System;

namespace BSC.WordLines.Model {
    public sealed class Cell : ICell {

        /// <summary>
        /// Rises when a ball was attached to cell.
        /// </summary>
        public event EventHandler BallAdded;

        /// <summary>
        /// Rises when ball was detached from cell.
        /// </summary>
        public event EventHandler BallRemoved;

        /// <summary>
        /// Gets board column where cell is arranged.
        /// </summary>
        public int Column { get; private set; }

        /// <summary>
        /// Gets board row where cell is arranged.
        /// </summary>
        public int Row { get; private set; }

        /// <summary>
        /// Gets cell's attached ball.
        /// </summary>
        public IBall Ball { get; private set; }

        /// <summary>
        /// Indicated whether some ball attached to cell.
        /// </summary>
        public bool HasBall { get; private set; }

        /// <summary>
        /// Initializes a new instance of cell 
        /// with specified coordinates on a board.
        /// </summary>
        public Cell(int column, int row) {
            Column = column;
            Row = row;
            Ball = null;
            HasBall = false;
        }

        /// <summary>
        /// Attaches a specified ball to cell.
        /// </summary>
        public void AddBall(IBall ball) {
            Ball = ball;
            HasBall = true;
            OnBallAdded();
        }

        /// <summary>
        /// Detaches ball away.
        /// </summary>
        public void RemoveBall() {
            Ball = null;
            HasBall = false;
            OnBallRemoved();
        }

        private void OnBallAdded() {
            var handler = BallAdded;
            if (handler != null) {
                handler(this, EventArgs.Empty);
            }
        }

        private void OnBallRemoved() {
            var handler = BallRemoved;
            if (handler != null) {
                handler(this, EventArgs.Empty);
            }
        }
    }
}