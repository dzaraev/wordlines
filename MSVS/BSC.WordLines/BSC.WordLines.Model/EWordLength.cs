﻿namespace BSC.WordLines.Model {
    public enum EWordLength {
        Undefined,
        Short,
        Medium,
        Long
    }
}