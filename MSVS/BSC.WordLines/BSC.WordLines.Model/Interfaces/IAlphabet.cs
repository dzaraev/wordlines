﻿using BSC.WordLines.Data;

namespace BSC.WordLines.Model {
    public interface IAlphabet {
        ILetterData GetRandomLetter(bool? takeVowelOrNonVowel = null);
        void InvalidateRandom();
        void Update();
    }
}