﻿using System;
using BSC.WordLines.Data;

namespace BSC.WordLines.Model {
    public interface IBall {
        /// <summary>
        /// Rises when ball was attached to a board.
        /// </summary>
        event EventHandler AddedToBoard;
        
        /// <summary>
        /// Rises when ball was moved to another cell
        /// </summary>
        event EventHandler MovedToAnotherCell;

        /// <summary>
        /// Rises when ball was removed from a board.
        /// </summary>
        event EventHandler RemovedFromBoard;

        /// <summary>
        /// Rises when property <see cref="Ball.Type"/> changed.
        /// </summary>
        event EventHandler TypeChanged;

        /// <summary>
        /// Gets column where ball is lying on board.
        /// </summary>
        int? Column { get; }

        /// <summary>
        /// Gets row where ball is lying on board.
        /// </summary>
        int? Row { get; }
        
        /// <summary>
        /// Indicates whether ball is lying on board.
        /// </summary>
        bool IsOnBoard { get; }

        /// <summary>
        /// Gets type of ball.
        /// </summary>
        EBallType Type { get; }

        /// <summary>
        /// Gets associated letter if <see cref="Type"/> 
        /// equals <see cref="EBallType.Letter"/>,
        /// otherwise returns null.
        /// </summary>
        ILetterData Letter { get; }

        /// <summary>
        /// Attaches ball to a board specified coordinates.
        /// </summary>
        /// <exception cref="InvalidOperationException">
        /// Thrown when ball is already on a board.</exception>
        void AddToBoard(int column, int row);

        /// <summary>
        /// Reattaches ball to another coordinates on a board.
        /// </summary>
        /// <exception cref="InvalidOperationException">
        /// Thrown when ball is not attached to board yet.</exception>
        void MoveToCell(int column, int row);

        /// <summary>
        /// Detaches ball from a board.
        /// </summary>
        void RemoveFromBoard();

        void SetWildcard(bool isWildcardEnabled);
    }
}