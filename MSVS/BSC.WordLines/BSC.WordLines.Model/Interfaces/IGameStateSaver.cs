﻿using BSC.WordLines.Data;

namespace BSC.WordLines.Model {
    public interface IGameStateSaver {
        void SaveGame(IGame game, IBoard board);
        bool TryLoadGame(IGame game, out ELanguage language);
        void DeleteSavedGame();
    }
}