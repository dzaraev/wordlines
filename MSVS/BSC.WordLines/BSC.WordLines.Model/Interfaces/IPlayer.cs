﻿using System;
using BSC.WordLines.Data;

namespace BSC.WordLines.Model {
    public interface IPlayer {
        string DisplayName { get; set; }
        int Money { get; set; }
        int KillBallCount { get; set; }
        int WildcardCount { get; set; }
        int AdditionCount { get; set; }
        int LastRoundScore { get; set; }
        IPlayerData Data { get; }
        bool IsRenamingNotFinished { get; }
        bool IsPurchaseNotFinished { get; set; }
        event EventHandler Updated;
        void BuyKillBalls(int itemsCount, int moneyPaid);
        void BuyWildcards(int itemsCount, int moneyPaid);
        void BuyAdditions(int itemsCount, int moneyPaid);
        void FinishRenamingAsync(string newPlayerName);
    }
}