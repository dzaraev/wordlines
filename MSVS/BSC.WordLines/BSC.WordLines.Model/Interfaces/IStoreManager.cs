using System.Collections.Generic;
using System.Threading.Tasks;
using Windows.ApplicationModel.Store;

namespace BSC.WordLines.Model {
    public interface IStoreManager {
        IEnumerable<ProductListing> CurrencyProductListings { get; }
        Task<bool> UpdateCurrencyProductListings();
        Task<int> PurchaseMoney(ProductListing productListing);
        int GetPurchasedMoney(ProductListing consumableProduct);
        Task<int> EnsureFulfillmentAsync();
    }
}