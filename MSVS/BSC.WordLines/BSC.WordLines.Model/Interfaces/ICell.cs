﻿using System;

namespace BSC.WordLines.Model {
    public interface ICell {

        /// <summary>
        /// Rises when a ball was attached to cell.
        /// </summary>
        event EventHandler BallAdded;
        /// <summary>
        /// Rises when ball was detached from cell.
        /// </summary>
        event EventHandler BallRemoved;
        /// <summary>
        /// Gets board column where cell is arranged.
        /// </summary>
        int Column { get; }
        /// <summary>
        /// Gets board row where cell is arranged.
        /// </summary>
        int Row { get; }
        /// <summary>
        /// Gets cell's attached ball.
        /// </summary>
        IBall Ball { get; }
        /// <summary>
        /// Indicated whether some ball attached to cell.
        /// </summary>
        bool HasBall { get; }

        /// <summary>
        /// Attaches a specified ball to cell.
        /// </summary>
        void AddBall(IBall ball);

        /// <summary>
        /// Detaches ball away.
        /// </summary>
        void RemoveBall();
    }
}