using System.Collections.Generic;
using System.Threading.Tasks;
using BSC.WordLines.Data;

namespace BSC.WordLines.Model {
    public interface ILeaderboardManager {
        Task RegisterLocalScore(EGameMode gameMode, ILanguageData languageData, IPlayerData playerData, int score);
        IEnumerable<ILocalHighScoreData> GetLocalHighScoresTop(EHighScorePeriods period, EGameMode gameMode, ELanguage language);
        IEnumerable<IFacebookHighScoreData> GetFacebookHighScoresTop(EHighScorePeriods period, EGameMode gameMode, ELanguage language);
        IEnumerable<IGlobalHighScoreData> GetGlobalHighScoresTop(EHighScorePeriods period, EGameMode gameMode, ELanguage language);
        bool IsRefreshTimeoutExpired(ILanguageData languageData, IGameModeData gameModeData = null);
        Task SynchronizeHighScoresWithServer(ILanguageData refreshFromServerLanguage, IGameModeData refreshFromServerGameMode = null);
    }
}