﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BSC.WordLines.Data;

namespace BSC.WordLines.Model {
    public interface IWordDictionary {
        string FindWord(Queue<ILetterData> letters, List<int> wildcardIndexes);
        Task UpdateAsync();
    }
}