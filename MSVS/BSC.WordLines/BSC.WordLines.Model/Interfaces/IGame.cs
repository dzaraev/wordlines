﻿using System;
using System.Collections.Generic;
using BSC.WordLines.Data;

namespace BSC.WordLines.Model {
    public interface IGame {
        event EventHandler<GameStateChangedEventArgs> StateChanged;
        event EventHandler CurrentScoreChanged;

        /// <summary>
        /// Gets the game's current state.
        /// </summary>
        EGameState State { get; set; }

        /// <summary>
        /// Gets the game's mode.
        /// </summary>
        EGameMode Mode { get; set; }

        /// <summary>
        /// Gets the how match moves left before end of the game,
        /// when game is in Moves mode.  
        /// </summary>
        int MovesCount { get; set; }

        /// <summary>
        /// Gets the how match seconds left before end of the game,
        /// when game is in Time mode.  
        /// </summary>
        int SecondsCount { get; set; }

        int CurrentScore { get; set; }
        int CurrentMoney { get; set; }
        int AdditionActivationsCount { get; set; }
        
        int RoundScore { get; }
        int RoundMoney { get; }
        IBoard Board { get; }

        /// <summary>
        /// Reinitialize game as a new game with specified mode.
        /// </summary>
        void StartNew();

        bool KillChain(Queue<IBall> chain);

        bool IsChainValid(Queue<IBall> chain);
        void ApplyKillBallBooster(IBall ball);
        event EventHandler MovesCountChanged;
        event EventHandler RoundMoneyChanged;
        event EventHandler RoundScoreChanged;
        event EventHandler SecondsCountChanged;
        void Stop();
        void Pause();
        void Continue();
        void ApplyAdditionBooster();
        bool IsAdditionBoosterRestricted();
        bool CanApplyWildcardBootster();

        /// <summary>
        /// Applies Wildcard booster and return flag that indicates type of booster result.
        /// </summary>
        /// <param name="targetBall">Target ball.</param>
        /// <returns>True, if WILDCARD applied; False, if WILDCARD cancelled.</returns>
        bool ApplyWildcardBooster(IBall targetBall);

        bool CanApplyKillBallBooster();
        bool CanApplyAdditionBooster();
    }
}