﻿using System;
using System.Collections.Generic;
using BSC.WordLines.Data;

namespace BSC.WordLines.Model {
    public interface IBoard {
        /// <summary>
        /// Rises when a new ball added on the board.
        /// </summary>
        event EventHandler<BallsEventArgs> BallsAdded;
        /// <summary>
        /// Rises when some ball was moved from one cell to another.
        /// </summary>
        event EventHandler<BallsEventArgs> BallsFell;
        /// <summary>
        /// Rises when some ball was removed from board.
        /// </summary>
        event EventHandler<BallsEventArgs> BallsRemoved;

        /// <summary>
        /// Rises when all board state resetted to initial.
        /// </summary>
        event EventHandler Reset;

        /// <summary>
        /// Gets a number of board columns.
        /// </summary>
        int ColumnsCount { get; }
        /// <summary>
        /// Gets a number of board rows.
        /// </summary>
        int RowsCount { get; }
        /// <summary>
        /// Gets a number of board balls.
        /// </summary>
        int BallsCount { get; }
        /// <summary>
        /// Gets a number of board cells.
        /// </summary>
        int CellsCount { get; }

        /// <summary>
        /// Gets balls lying on the board.
        /// </summary>
        IEnumerable<IBall> Balls { get; }

        /// <summary>
        /// Gets cells of the board.
        /// </summary>
        IEnumerable<ICell> Cells { get; }

        /// <summary>
        /// Removes all balls from baord and recreates all cells.
        /// </summary>
        void Clear();

        /// <summary>
        /// Gets cell of the board with specified coordinates. 
        /// </summary>
        /// <param name="column">Column of the board. It must be 
        /// in range from 0 to <see cref="ColumnsCount"/>-1.</param>
        /// <param name="row">Row of the board. It must be 
        /// in range from 0 to <see cref="RowsCount"/>-1.</param>
        /// <returns>The cell with specified coordinates.</returns>
        /// <exception cref="ArgumentOutOfRangeException">
        /// Thrown when <paramref name="column"/> or <paramref name="row"/> 
        /// is out of range.</exception>
        ICell this[int column, int row] { get; }

        /// <summary>
        /// Adds a new balls on the board.
        /// </summary>
        void DropBalls(int ballsCount);

        /// <summary>
        /// Removes specified balls from board, that leads to 
        /// falling down of balls which is above deleted balls.
        /// </summary>
        void RemoveBalls(IEnumerable<IBall> removedBalls);

        /// <summary>
        /// Adds specified ball on the board to specified column and row.
        /// </summary>
        /// <exception cref="ArgumentException">Thrown when one of balls is null,
        /// when one of balls has coordinates (already is on board).</exception>
        IBall InsertBall(EBallType type, ILetterData letter, int column, int row);
    }
}