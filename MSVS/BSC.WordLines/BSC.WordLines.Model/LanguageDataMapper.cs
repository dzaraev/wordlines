﻿using System;
using System.Collections.Generic;
using System.Linq;
using BSC.WordLines.Data;
using BSC.WordLines.Utils;

namespace BSC.WordLines.Model {
    public class LanguageDataMapper {
        private readonly ILocalDataProvider _provider;
        private readonly Dictionary<ELanguage, ILanguageData> _languages;

        public LanguageDataMapper(ILocalDataProvider provider) {
            if (provider == null) {
                throw new ArgumentNullException("provider");
            }
            _provider = provider;

            var languages = (_provider.GetAllLanguages() ?? new ILanguageData[] {}).ToList();
            if (languages.Count == 0) {
                throw new InvalidOperationException(ErrorMessage.NoLanguagesFound);
            }

            var russianLanguage = languages.Find(
                languageData => IsDataMatchEnum(languageData, ELanguage.Russian));
            if (russianLanguage == null) {
                throw new InvalidOperationException(ErrorMessage.CannotFindLanguageInDb(ELanguage.Russian.ToString()));
            }
            var englishLanguage = languages.Find(
                languageData => IsDataMatchEnum(languageData, ELanguage.English));
            if (englishLanguage == null) {
                throw new InvalidOperationException(ErrorMessage.CannotFindLanguageInDb(ELanguage.English.ToString()));
            }

            _languages = new Dictionary<ELanguage, ILanguageData> {
                {ELanguage.English, englishLanguage},
                {ELanguage.Russian, russianLanguage}
            };

            if (Enum.GetValues(typeof(ELanguage)).Length != _languages.Count) {
                throw new InvalidOperationException(ErrorMessage.NotAllLanguagesSupportedByDb);
            }
        }

        public int GetLanguageDataId(ELanguage enumValue) {
            return _languages[enumValue].Id;
        }

        public ILanguageData GetLanguageData(ELanguage enumValue) {
            return _languages[enumValue];
        }

        public ELanguage GetLanguage(ILanguageData data) {
            foreach (var keyValuePair in _languages) {
                if(IsDataMatchEnum(data, keyValuePair.Key)) {
                    return keyValuePair.Key;
                }
            }
            throw new InvalidOperationException(ErrorMessage.CannotMapLanguageDataToEnumValue);
        }

        private bool IsDataMatchEnum(ILanguageData languageData, ELanguage enumValue) {
            string languageName = languageData.Name ?? string.Empty;
            return string.Equals(languageName, enumValue.ToString(), StringComparison.Ordinal);
        } 
    }
}