using System;

namespace BSC.WordLines.Model {
    public class GameStateChangedEventArgs : EventArgs {
        public EGameState OldState { get; private set; }
        public EGameState NewState { get; private set; }

        public GameStateChangedEventArgs(EGameState oldState, EGameState newState) {
            OldState = oldState;
            NewState = newState;
        }
    }
}