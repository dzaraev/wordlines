﻿using System;
using BSC.WordLines.Data;

namespace BSC.WordLines.Model {
    public class GameModeLanguageKey : IEquatable<GameModeLanguageKey> {
        private readonly string _gameModeName;
        private readonly string _languageName;
        public GameModeLanguageKey(ILanguageData language, IGameModeData gameMode) {
            _languageName = language != null ? language.Name : null;
            _gameModeName = gameMode != null ? gameMode.Name : null;
        }

        public bool Equals(GameModeLanguageKey other) {
            return string.Equals(_gameModeName, other._gameModeName, StringComparison.Ordinal) &&
                   string.Equals(_languageName, other._languageName, StringComparison.Ordinal);
        }

        public override bool Equals(object obj) {
            if (ReferenceEquals(null, obj)) return false;
            return obj is GameModeLanguageKey && Equals((GameModeLanguageKey) obj);
        }

        public override int GetHashCode() {
            unchecked {
                return ((_gameModeName != null ? _gameModeName.GetHashCode() : 0) * 397) ^ (_languageName != null ? _languageName.GetHashCode() : 0);
            }
        }
    }
}