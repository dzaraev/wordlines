using System;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using BSC.WordLines.Data;
using BSC.WordLines.Utils;

namespace BSC.WordLines.Model {
    public class Player : IPlayer {
        private readonly ILocalDataProvider _provider;
        private readonly IServerDataProvider _serverProvider;
        private readonly IPlayerData _playerData;
        private readonly object _syncObject = new object();
        private int _lastRoundScore;

        public Player(ILocalDataProvider provider, IServerDataProvider serverProvider) {
            provider.EnsureNotNull("provider");
            serverProvider.EnsureNotNull("serverProvider");

            _provider = provider;
            _serverProvider = serverProvider;
            _playerData = _provider.GetCurrentPlayer();
            if (_playerData == null) {
                throw new InvalidOperationException(ErrorMessage.CannotGetCurrentPlayerDataFromProvider);
            }
        }

        public event EventHandler Updated;

        public string DisplayName {
            get { return _playerData.Name; }
            set {
                var playerData = _playerData;
                if (!string.Equals(playerData.Name, value, StringComparison.Ordinal)) {
                    playerData.Name = value;
                    playerData.IsRenamingNotFinished = true;
                    _provider.UpdatePlayerInDatabase(playerData);
                    FinishRenamingAsync(value);
                    OnUpdated();
                }
            }
        }

        public bool IsRenamingNotFinished {
            get { return _playerData.IsRenamingNotFinished; }
        }

        public bool IsPurchaseNotFinished {
            get { return _playerData.IsPurchaseNotFinished; }
            set {
                _playerData.IsPurchaseNotFinished = value;
                _provider.UpdatePlayerInDatabase(_playerData);
                OnUpdated();
            }
        }

        public int Money {
            get { return _playerData.Money; }
            set {
                var playerData = _playerData;
                if (playerData.Money != value) {
                    playerData.Money = value;
                    _provider.UpdatePlayerInDatabase(playerData);
                    OnUpdated();
                }
            }
        }

        public int LastRoundScore {
            get { return _lastRoundScore; }
            set {
                if (value != _lastRoundScore) {
                    _lastRoundScore = value;
                    OnUpdated();
                }
            }
        }

        public int KillBallCount {
            get { return _playerData.KillBallCount; }
            set {
                var playerData = _playerData;
                if (playerData.KillBallCount != value) {
                    playerData.KillBallCount = value;
                    _provider.UpdatePlayerInDatabase(playerData);
                    OnUpdated();
                }
            }
        }

        public int WildcardCount {
            get { return _playerData.WildcardCount; }
            set {
                var playerData = _playerData;
                if (playerData.WildcardCount != value) {
                    playerData.WildcardCount = value;
                    _provider.UpdatePlayerInDatabase(playerData);
                    OnUpdated();
                }
            }
        }

        public int AdditionCount {
            get { return _playerData.AdditionCount; }
            set {
                var playerData = _playerData;
                if (playerData.AdditionCount != value) {
                    playerData.AdditionCount = value;
                    _provider.UpdatePlayerInDatabase(playerData);
                    OnUpdated();
                }
            }
        }

        public IPlayerData Data {
            get { return _playerData; }
        }

        public void BuyKillBalls(int itemsCount, int moneyPaid) {
            bool isNewCountLessMax = _playerData.KillBallCount < int.MaxValue - itemsCount;
            Diagnost.Assert(_playerData.Money >= moneyPaid, ErrorMessage.NotEnoughMoneyForBuy("KillBalls"));
            if (_playerData.Money < moneyPaid || !isNewCountLessMax) {
                return;
            }
            _playerData.Money -= moneyPaid;
            _playerData.KillBallCount += itemsCount;
            _provider.UpdatePlayerInDatabase(_playerData);
            OnUpdated();
        }

        public void BuyWildcards(int itemsCount, int moneyPaid) {
            bool isNewCountLessMax = _playerData.WildcardCount < int.MaxValue - itemsCount;
            Diagnost.Assert(_playerData.Money >= moneyPaid, ErrorMessage.NotEnoughMoneyForBuy("Wildcards"));
            if (_playerData.Money < moneyPaid || !isNewCountLessMax) {
                return;
            }
            _playerData.Money -= moneyPaid;
            _playerData.WildcardCount += itemsCount;
            _provider.UpdatePlayerInDatabase(_playerData);
            OnUpdated();
        }

        public void BuyAdditions(int itemsCount, int moneyPaid) {
            bool isNewCountLessMax = _playerData.AdditionCount < int.MaxValue - itemsCount;
            Diagnost.Assert(_playerData.Money >= moneyPaid, ErrorMessage.NotEnoughMoneyForBuy("Additions"));
            if (_playerData.Money < moneyPaid || !isNewCountLessMax) {
                return;
            }
            _playerData.Money -= moneyPaid;
            _playerData.AdditionCount += itemsCount;
            _provider.UpdatePlayerInDatabase(_playerData);
            OnUpdated();
        }

        public void FinishRenamingAsync(string newPlayerName) {
            Task.Run(() => {
                //Still race state, but incredible in real life.
                lock (_syncObject) {
                    RenameOnServer(newPlayerName);
                }
            });
        }

        private void OnUpdated() {
            var handler = Updated;
            if (handler != null) {
                handler(this, EventArgs.Empty);
            }
        }

        //ASYNCHRONOUS!
        private void RenameOnServer(string newPlayerName) {
            //Try to rename on server
            try {
                bool success = _serverProvider.RenamePlayerRemote(_playerData, newPlayerName)
                    .Wait(DataConstants.NetTimeoutMilliseconds);
                if (!success) {
                    return;
                }

                Deployment.Current.Dispatcher.BeginInvoke(() => {
                    if (_playerData.Name != newPlayerName) {
                        _playerData.IsRenamingNotFinished = true;
                        _provider.UpdatePlayerInDatabase(_playerData);
                        return;
                    }
                    //Rename locally.
                    var globalScores = _provider.GetGlobalHighScores().ToList();
                    globalScores.ForEach(score => {
                        if (score.IsLocalPlayer) {
                            score.PlayerName = newPlayerName;
                        }
                    });
                    _provider.UpdateGlobalHighScores(globalScores.Where(score => score.IsLocalPlayer));
                    _playerData.IsRenamingNotFinished = false;
                    _provider.UpdatePlayerInDatabase(_playerData);
                });
            }
            catch (Exception ex) {
                Diagnost.Assert(false, ErrorMessage.ExceptionOccured(ex));
            }
        }
    }
}