﻿using System.Windows;

namespace BSC.WordLines.Utils {
    public class ResolutionHelper {
        public enum Resolutions {
            Unknown,
            WVGA,
            WXGA,
            HD,
        };

        private static bool IsWvga {
            get { return Application.Current.Host.Content.ScaleFactor == 100; }
        }

        private static bool IsWxga {
            get { return Application.Current.Host.Content.ScaleFactor == 160; }
        }

        private static bool IsHD {
            get { return Application.Current.Host.Content.ScaleFactor == 150; }
        }

        public static Resolutions CurrentResolution {
            get {
                if (IsWvga) return Resolutions.WVGA;
                if (IsWxga) return Resolutions.WXGA;
                if (IsHD) return Resolutions.HD;
                return Resolutions.Unknown;
            }
        }
    }
}