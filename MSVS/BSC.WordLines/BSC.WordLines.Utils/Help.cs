﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Caliburn.Micro;

namespace BSC.WordLines.Utils {
    public static class Help {
        public static void EnsureNotNull(this object target, string targetName) {
            if (target == null) {
                throw new ArgumentNullException(targetName);
            }
        }

        public static void EnsureNotNullOrWhiteSpace(this string target, string targetName) {
            if (string.IsNullOrWhiteSpace(target)) {
                throw new ArgumentOutOfRangeException(targetName);
            }
        }

        public static bool AssertNotNullFailed(this object target, string targetName = null) {
            Diagnost.Assert(target != null, ErrorMessage.ArgumentOrVarCannotBeNull(targetName ?? "target"));
            return target == null;
        }

        public static bool AssertFailed(bool assert, string message) {
            Diagnost.Assert(assert, message);
            return !assert;
        }

        public static bool ReferenceContains<TSource>(this IEnumerable<TSource> source, TSource value) {
            return source.Contains(value, new ReferenceEqualityComparer<TSource>());
        }

        public static void AddRange<T>(this ObservableCollection<T> collection, IEnumerable<T> items) {
            foreach (var item in items) {
                collection.Add(item);
            }
        }

        public static void RemoveRange<T>(this ObservableCollection<T> collection, IEnumerable<T> items) {
            foreach (var item in items) {
                collection.Remove(item);
            }
        }

        public static IEventAggregator ResolveEventAggregator(this SimpleContainer container) {
            return (IEventAggregator)container.GetInstance(typeof(IEventAggregator), null);
        }

        public static string BytesToString(this byte[] bytes) {
            if (bytes == null) {
                throw new ArgumentNullException("bytes");
            }
            var encoding = new UTF8Encoding();
            return encoding.GetString(bytes, 0, bytes.Length);
        }

        public static byte[] StringToBytes(this string str) {
            if (String.IsNullOrWhiteSpace(str)) {
                return new byte[] {};
            }
            var encoding = new UTF8Encoding();
            return encoding.GetBytes(str);
        }

        public static string FormatWith(this string source, params object [] args) {
            try {
                return string.Format(source, args);
            }
            catch (Exception) {
                return string.Empty;
            }
        }

        public static T Get<T>(this SimpleContainer ioc) {
            ioc.EnsureNotNull("ioc");
            return (T) ioc.GetInstance(typeof (T), null);
        }
    }
}