﻿using System;
using System.Globalization;

namespace BSC.WordLines.Utils {
    public static class DateTimeHelper {
        public static bool IsWithinCurrentWeek(string timestamp) {
            DateTime parsedTimestamp;
            return TryParseTimestampRoundtrip(timestamp, out parsedTimestamp) &&
                   IsWithinCurrentWeek(parsedTimestamp);
        }

        public static bool IsWithinCurrentWeek(DateTime utcTimestamp) {
            DateTime weekStartUtc;
            DateTime weekEndUtc;
            GetCurrentWeekBounds(out weekStartUtc, out weekEndUtc);
            return weekStartUtc < utcTimestamp && utcTimestamp < weekEndUtc;
        }

        public static void GetCurrentWeekBounds(out DateTime weekStartUtc, out DateTime weekEndUtc) {
            DateTime today = DateTime.UtcNow.Date;
            int backShift;
            switch (today.DayOfWeek) {
                case DayOfWeek.Monday:
                    backShift = 0;
                    break;
                case DayOfWeek.Tuesday:
                    backShift = 1;
                    break;
                case DayOfWeek.Wednesday:
                    backShift = 2;
                    break;
                case DayOfWeek.Thursday:
                    backShift = 3;
                    break;
                case DayOfWeek.Friday:
                    backShift = 4;
                    break;
                case DayOfWeek.Saturday:
                    backShift = 5;
                    break;
                case DayOfWeek.Sunday:
                    backShift = 6;
                    break;
                default: {
                        string message = ErrorMessage.UnexpectedlyEnumValue(today.DayOfWeek);
                        throw new InvalidOperationException(message);
                    }
            }
            weekStartUtc = today.AddDays(-1 * backShift);
            weekEndUtc = today.AddDays(7 - backShift);
        }
        
        public static string GetUtcTimestampString(DateTime utcDateTime) {
            return utcDateTime.ToString("o");
        }

        public static bool TryParseTimestampRoundtrip(string timestamp, out DateTime result) {
            if (DateTime.TryParse(timestamp, DateTimeFormatInfo.InvariantInfo, DateTimeStyles.RoundtripKind, out result)) {
                return true;
            }
            Diagnost.Assert(false, ErrorMessage.CannotParseTimestamp(timestamp));
            return false;
        }
    }
}