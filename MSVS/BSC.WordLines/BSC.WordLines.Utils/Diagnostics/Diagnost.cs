﻿using System;
using System.Threading.Tasks;
using Caliburn.Micro;

namespace BSC.WordLines.Utils {
    public static class Diagnost {
        public static void Assert(bool condition, string message) {
            IoC.Get<IDiagnost>().Assert(condition, message);
        }

        public static void LogHandledException(Exception exception) {
            IoC.Get<IDiagnost>().LogHandledException(exception);
        }

        public static void LogUnhandledException(Exception exception) {
            IoC.Get<IDiagnost>().LogUnhandledException(exception);
        }

        public static Task LogAsync(string message, ELogType type) {
            return IoC.Get<IDiagnost>().SaveLogAsync(message, type);
        }
    }
}