﻿using System;
using System.Text;

namespace BSC.WordLines.Utils {
    public static class ErrorMessage {
        //Data layer
        public const string CannotFetchFacebookScoresWithouSignUp = "Cannot fetch facebook score without signed up to facebook.";
        public const string UpdatePlayerInDbFailed = "Player is failed to update in database.";
        public const string CannotGetPlayerFromDb = "Cannot get player from database.";
        public const string CannotGetSettingsFromDb = "Cannot get settings record from database.";
        public const string CannotGetVersionsFromDb = "Cannot get versions record from database.";
        public const string CannotGetSettingsFromProvider = "Cannot get settings record from local data provider.";
        public const string NoGameModesFound = "Cannot found any game mode in database.";
        public const string NoLanguagesFound = "Cannot found any language in database.";
        public const string NoLeaderboardsFound = "Cannot found any leaderboard in database.";
        public static string CannotFindGameModeInDb(string gameMode) {
            return string.Format("Cannot find GameMode in database with name == {0}.", gameMode);
        }
        public static string CannotFindLeaderboardInDb(string leaderboard) {
            return string.Format("Cannot find Leaderboard in database with name == {0}.", leaderboard);
        }
        public const string NotAllGameModesSupportedByDb = "Not all EGameMode values supported by database.";
        public const string NotAllLeaderboardsSupportedByDb = "Not all ELeaderboard values supported by database.";
        public const string NoHighScorePeriodsFound = "Cannot found any highscore periods in database.";
        public static string CannotFindHighScorePeriodInDb(string highScorePeriod) {
            return string.Format("Cannot find HighScorePeriod in database with name == {0}.", highScorePeriod);
        }
        public const string NotAllHighScorePeriodsSupportedByDb = "Not all EHighScorePeriods values supported by database.";
        public static string CannotFindLanguageInDb(string language) {
            return string.Format("Cannot find Language in database with name == {0}.", language);
        }
        public const string NotAllLanguagesSupportedByDb = "Not all ELanguage values supported by database.";
        public const string UnrecognizedHighScoreData = "Unrecognized high score data type.";
        public static string NotEnoughMoneyForBuy(string product) {
            return string.Format("Not enough money for buy \"{0}\"", product);
        }
        public const string CannotGetCurrentPlayerDataFromProvider = "Cannot get current player data from data provider.";
        public const string CannotGetDynamicSettingsDataFromProvider = "Cannot get dynamic settings data from data provider.";
        public const string CannotGetCurrentLanguageDataFromProvider = "Cannot get current language data from data provider.";
        public const string CannotGetCurrentThemeDataFromProvider = "Cannot get current theme data from data provider.";
        public const string NeedAuthorizationFirst = "Need to make authorization before this operation.";
        public static string CannotParseTimestamp(string timestamp) {
            return "Cannot parse \"{0}\" to DateTime.";
        }
        public static string HighScoreRepresentsNotAnyBest(string objectId) {
            return string.Format("HighScore object is not best in AllTime and not best in Week: id={0}", objectId);
        }
        public static string UserHasInvalidBests(string userKey) {
            return string.Format("User with key \"{0}\" has incorrect bests combination!", userKey);
        }
        
        public const string UsernameGetException = "<username ex>";
        public const string UsernameGetNoDatabase = "<username nodb>";
        public static string UserAclSetupException(Exception ex) {
            return string.Format(
                "User ACL set up ex: {0}   INNER EX: {1}",
                ex.Message,
                ex.InnerException != null ? ex.InnerException.Message : "null");
        }

        public static string UserLogInException(Exception ex) {
            return string.Format(
                "User LogIn ex: {0}  INNER EX: {1}",
                ex.Message,
                ex.InnerException != null ? ex.InnerException.Message : "null");
        }

        public static string UserSignUpException(Exception ex) {
            return string.Format(
                "User SignUp ex: {0}  INNER EX: {1}",
                ex.Message,
                ex.InnerException != null ? ex.InnerException.Message : "null");
        }

        public static string HandledException(Exception ex) {
            return string.Format(
                "Handled Ex [{0}]: {1}  INNER EX: {2} STACK: {3}",
                ex.GetType().Name,
                ex.Message,
                ex.InnerException != null ? ex.InnerException.Message : "null",
                ex.StackTrace);
        }

        public static string UnhandledException(Exception ex) {
            return string.Format(
                "Unhandled Ex [{0}]: {1}  INNER EX: {2} STACK: {3}",
                ex.GetType().Name,
                ex.Message,
                ex.InnerException != null ? ex.InnerException.Message : "null",
                ex.StackTrace);
        }

        //Model layer
        public const string CannotFetchPurchasedMoneyValueFromProductListing =
            "Cannot fetch purchase money value from product listing!";
        public const string BallAlreadyOnBoard = "Ball already on a board.";
        public const string BallIsNotAttachedToBoard = "Ball is not attached to board yet.";
        public const string BallMustHaveLetter = "Ball of letter-type must have a letter.";
        public const string BallCannotHaveLetter = "Only ball of letter-type can have a letter.";
        public const string DifferentLanguages = "Different languages occured.";
        public const string NotEnoughEmptyCellsToAddedBalls = "Not enough empty cells to added balls.";
        public const string CannotDropingBallWithNullColumn = "Cannot dropping ball down when Column property is null.";
        public const string CannotDropingBallWithNullRow = "Cannot dropping ball down when Row property is null.";
        public const string BallForBoardIsNull= "One of specified balls is null.";
        public const string NewBallForBoardShouldHaveNullCoordinates = "New ball for board should have null coordinates.";
        public const string BallsSpecifiedToRemoveShouldHaveValidCoordinates = "Balls, that are specified to remove from a board, must have valid coordinates.";
        public const string NoLettersInAlphabet = "No letters occured in alphabet.";
        public const string InvalidChanceHit = "Invalid chance hit";
        public const string CannotGetRandomLetter = "Cannot get random letter.";
        public const string CannotGenerateNegativeBallsCount = "Cannot generate negative balls count.";
        public const string WildcardIndexOutOfRanged = "Wildcard index is out of range.";
        public const string GameMustBeInGameOverState = "Game must be in GameOver state.";
        public const string CannotMapLanguageDataToEnumValue = "Cannot map language data to enum value.";
        public const string CannotMapLeaderboardDataToEnumValue = "Cannot map leaderboard data to enum value.";
        public const string CannotMapHighScorePeriodDataToEnumValue = "Cannot map high score period data to enum value.";
        public const string UnknownGameModeFormat = "Unknown game mode: {0}";
        public const string UnknownPeriodFormat = "Unknown high score period: {0}";
        public const string UnknownLanguageFormat = "Unknown language: {0}";
        public static string CannotFindWordWithLength(int length) {
            return "Cannot find in words dictionary word with length = " + length;
        }
        public static string CannotCalcMoveBonusWithWordLength(int wordLength) {
            return "Cannot calculate bonus for word with length = " + wordLength;
        }
        public static string GameModeCanBeChangedOnlyInNotStartedState = "Game Mode can be changed only in NotStarted state.";
        public static string CanNotApplyAdditionBooster = "Can NOT apply Addition booster, but just tried.";
        public static string CanNotApplyWildcardBooster = "Can NOT apply Wildcard booster, but just tried.";
        public static string CanNotApplyKillBallBooster = "Can NOT apply KillBall booster, but just tried.";

        //ViewModel layer
        public const string SelectedBallIsNotInBoardCollection = "Selected ball is not in board ball collection.";
        public const string SelectedCellIsNotInBoardCollection = "Selected cell is not in board ball collection.";
        public const string AnimationTaskCompletedUnexpectedly = "Animation task completed unexpectedly.";
        public const string TaskPublishingAlreadyStarted = "Start publishing already started.";
        public const string BallRowHasNotValue = "Ball's Row property has not value (null)";
        public const string BallPreviousRowHasNotValue = "Ball's PreviousRow property has not value (null)";
        public const string BallColumnHasNotValue = "Ball's Column property has not value (null)";
        public static string UnexpectedBallAnimationType(string animationType) {
            return string.Format("Unexpected ball animation type: {0}", animationType);
        }

        //View layer
        public static string NoViewModelFoundOnLoadedFormat(Type uiType) {
            return string.Format("No viewmodel found on loaded: {0}", uiType.Name);
        }

        public const string NavigationTroubleOccuredButAllRight = "Navigation trouble occured, but now all right.";
        public const string BallViewModelNotAttachedToBallView = "BallViewModel not attached to ball view";
        public const string ViewModelNotAttachedYet = "ViewModel is not attached to View yet.";
        public const string CannotUnchainBallChainEmpty = "Can not unchain ball because chain empty.";
        public const string CannotUnchainBallWhichNotOnEndOfChain = "Can not unchain ball which is not in end of chain";
        public const string DuplicatedBallOnCanvas = "Duplicated ball on canvas.";
        public const string LeftMouseButtonNotPressedWhileChainChanged = "Left mouse button was not press while chain changed.";
        public const string BallViewInvalidDataContext = "Ball view has invalid DataContext value.";
        public const string NoBallsForAnimation = "No balls specified for animation.";
        public const string TargetMustBeFrameworkElement = "Target must be a FrameworkElement.";
        public const string ParentMustBeCanvas = "Parent must be a Canvas";
        public const string CanvasBehaviorAlignmentValueNotSupported = "CanvasBehavior.Alignment property value not supported.";
        public const string PhoneBehaviorTargetTypeUnsupported = "PhoneBehavior supports only PhoneApplicationPage target type.";
        public static string ArgumentTypeUnsupported(Type targetType, params Type[] allowedTypes) {
            StringBuilder builder=new StringBuilder();
            for (int i = 0; i < allowedTypes.Length; i++) {
                var type = allowedTypes[i];
                builder.Append(type);
                if (i < allowedTypes.Length - 1) {
                    builder.Append(", ");
                }
            }
            return string.Format("Argument type \"{0}\" unsupported. Allowed: {1}", targetType, builder);
        }
        public static string UnknownResolution(double width, double height) {
            return string.Format("Unknown resolution: {0} x {1}", width, height);
        }
        

        //Common
        public const string CannotBeNull = "Can not be null.";
        public static string DictionaryNotContainsKey(object key) {
            return string.Format("Dictionary not contains key=\"{0}\"", key ?? "null");
        }
        public static string ArgumentOutOfRange(string name) {
            return string.Format("Argument out of range: {0}", name);
        }
        public static string CannotParseValue(object value, Type desiredType) {
            return "Cannot parse value \"{0}\" into desired type \"{1}\".".FormatWith(value, desiredType);
        }
        public static string ArgumentOrVarCannotBeNull(string argumentName) {
            return string.Format("Argument or variable cannot be null: {0}", argumentName);
        }
        public static string ExceptionOccured(Exception exception) {
            return string.Format("Exception occured: {0}", exception);
        }
        public static string DbSchemaVersionUnsupported(int supportedDbVersion, int deployedDbVersion) {
            return string.Format("Db SCHEMA version unsupported! Supported ver: {0} Deployed ver: {1}.", supportedDbVersion, deployedDbVersion);
        }
        public static string DbReleaseVersionUnsupported(int supportedDbVersion, int deployedDbVersion) {
            return string.Format("Db RELEASE version unsupported! Supported ver: {0} Deployed ver: {1}.", supportedDbVersion, deployedDbVersion);
        }
        public const string EventAggregatorNotAccessable = "EventAggregatorNotAccessable";
        public static string VariableNotCompatibleWith<T>() {
            return string.Format("Variable not compatible with type: {0}", typeof(T).FullName);
        }
        public const string BallCannotBeNull = "Ball cannot be null.";
        public static string CannotResolveIoC<TService>() {
            return string.Format("Cannot resolve service \"{0}\" from IoC container.", typeof (TService));
        }
        public static string UnexpectedlyEnumValue(Enum enumValue) {
            return string.Format("Unexpectedly Enum value: {0}", enumValue);
        }

        public const string UnhandledExceptionUserMessageEmpty = "Unhandled exception user message (or caption) is empty!";

        //Resources
        public const string GlobalStringImplementationDuplicated = "String implementation with LanguageId == NULL, must be once for them LocalizedString.";
        public const string ConcreteResourceCannotBeNull = "Concrete resource can not be null.";
        public const string CachedResourceNotFound = "Concrete resource not found.";
        public static string CachedStringNotFound(string stringKey) {
            return "Concrete localized string not found: \"{0}\".".FormatWith(stringKey);
        }
        public const string ColorConcreteResourceContractError = "Color typed concrete resource contract error occured";
        public const string IntConcreteResourceContractError = "Int typed concrete resource contract error occured";
        public const string DoubleConcreteResourceContractError = "Double typed concrete resource contract error occured";
        public const string ImageConcreteResourceContractError = "Image typed concrete resource contract error occured";
        public const string LocalizedStringContractError = "Localized string contract error occured";
        public static string LocalizedStringNotFound(string key) {
            return string.Format("Localized string not found for key \"{0}\"", key);
        }
        public static string LocalizedStringImplNotFound(string key, string language) {
            return string.Format("Localized string implementation not found for key \"{0}\" and language \"{1}\"", key, language);
        }
        public const string ImageResourceCannotBeNull = "Image resource can not be null.";
        public const string IncorrectResourceAssigment = "Incorrect resource assignment occured.";
        public static string ResourceAssignmentPropertyNotFound(string propertyName) {
            return string.Format("Resource assignment: property \"{0}\" not found on target.", propertyName);
        }
        public const string ResourceAssignmentPropertySetValueException = "Exception occured while set resource value to traget property.";
        public static string AbstractResourceKeyHasIncorectFormat(string abstractResourceKey) {
            return string.Format("Abstract resource key has incorrect format: \"{0}\"", abstractResourceKey);
        }
        public const string AbstractResourceConcreteConverterNotFound = "Converter for concrete abstract resource type not found.";
        public const string AbstractResourceValueIsNull = "Abstract resource value is null.";
        public static string CannotConvertConvcreteValueToType(string value, string clrType) {
            return string.Format("Cannot convert string value \"{0}\" to CLR value \"{1}\"", value, clrType);
        }
    }
}