﻿namespace BSC.WordLines.Utils {
    //ATTENTION! String representations of identifiers must match 
    //same values in local DB (if using) and backend scheme.
    public enum ELogType {
        Error,
        Info,
    }
}