﻿using System;
using System.Threading.Tasks;

namespace BSC.WordLines.Utils {
    public interface IDiagnost {
        void Assert(bool condition, string message);
        void LogHandledException(Exception exception);
        void LogUnhandledException(Exception exception);
        Task SaveLogAsync(string message, ELogType logType);
    }
}