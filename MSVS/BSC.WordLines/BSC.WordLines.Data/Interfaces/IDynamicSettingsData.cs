﻿using SQLite;

namespace BSC.WordLines.Data {
    public interface IDynamicSettingsData {
        int Id { get; }
        string Key { get; }
        string Value { get; set; }
    }
}