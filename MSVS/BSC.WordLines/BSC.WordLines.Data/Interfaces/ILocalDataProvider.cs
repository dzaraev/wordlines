﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BSC.WordLines.Data.SqliteNet.ORM;

namespace BSC.WordLines.Data {
    public interface ILocalDataProvider {
        IEnumerable<IDynamicSettingsData> GetDynamicSettings();
        void SaveDynamicSettings(IEnumerable<IDynamicSettingsData> dynamicSettings);

        IEnumerable<ILanguageData> GetAllLanguages();
        /// <summary>
        /// Gets game settings, some of them can be changed by user.
        /// </summary>
        ISettingsData GetSettings();
        /// <summary>
        /// Saves specified game settings in database.
        /// </summary>
        void SaveSettings(ISettingsData changedSettings);
        
        /// <summary>
        /// Gets letters of alphabet for current language. 
        /// Current language can be assigned in <see cref="ISettingsData"/>.
        /// </summary>
        IEnumerable<ILetterData> GetAlphabet();

        IPlayerData GetPlayer(int playerId);
        void UpdatePlayerInDatabase(IPlayerData player);

        ILocalHighScoreData CreateLocalHighScore();
        IEnumerable<ILocalHighScoreData> GetLocalHighScores();
        void InsertLocalHighScore(ILocalHighScoreData highScore);
        void RemoveLocalHighScores(IEnumerable<ILocalHighScoreData> highScores);

        IEnumerable<IThemeData> GetAllThemes();
        string GetLocalizedString(string localizedStringKey);
        IConcreteResourceData GetConcreteResource(string abstractResourceKey);
        void SetCurrentTheme(IThemeData theme);
        IEnumerable<string> GetAbstractResourceKeys();
        IEnumerable<IAbstractResourceData> GetAbstractResources();
        IEnumerable<string> GetEnvironmentKeys();
        IEnumerable<IGameModeData> GetGameModes();
        IEnumerable<IHighScorePeriodData> GetHighScorePeriods();
        ILanguageData GetCurrentLanguage();
        IPlayerData GetCurrentPlayer();
        IEnumerable<IPlayerData> GetPlayers();
        void InsertAbstractResource(string key, string description);
        IEnumerable<IResourceImplementationData> GetResourceImplementations(IAbstractResourceData abstractResource);
        ILanguageData GetLanguage(int id);
        IThemeData GetTheme(int id);
        IEnvironmentData GetEnvironment(int id);
        IConcreteResourceData GetConcreteResource(int id);
        IEnumerable<IEnvironmentData> GetAllEnvironments();

        IResourceImplementationData InsertEmptyResourceImplementation(int abstractResourceId,
                                                                                      int concreteResourceId);

        void SaveResourceImplementation(IResourceImplementationData data);
        IConcreteResourceData InsertConcreteResource(string stringData);
        IConcreteResourceData InsertConcreteResource(byte[] data);
        IEnumerable<IResourceImplementationData> GetAllResourceImplementations();
        void DeleteResourceImplementation(int id);
        void DeleteConcreteResource(int id);
        IResourceImplementationData GetResourceImplementation(int id);
        void SaveConcreteResource(IConcreteResourceData data);
        IEnumerable<ILocalizedStringData> GetAllLocalizedStrings();
        IEnumerable<IStringImplementationData> GetStringImplementations(string localizedStringKey);
        void SaveStringImplementation(IStringImplementationData data);
        void InsertLocalizedString(string key, string description);

        IStringImplementationData InsertStringImplementation(int localizedStringId, int? languageId, string concreteString);

        void DeleteStringImplementation(IStringImplementationData data);
#if TOOLKIT
        /// <summary>
        /// Gets dictionary of words for current language. 
        /// Current language can be assigned in <see cref="ISettingsData"/>.
        /// </summary>
        /// <param name="optimized">When set to <c>True</c> only the
        /// <see cref="IWordData.Word"/> property will be assigned in fetched objects.</param>
        IEnumerable<IWordData> GetWords(bool optimized = false);
        void InsertWords(IEnumerable<string> words, int languageId);
        void DeleteWords(IEnumerable<IWordData> words);
        void SaveWords(IEnumerable<IWordData> words);
#endif

        IEnumerable<IGlobalHighScoreData> GetGlobalHighScores();
        void InsertGlobalHighScores(IEnumerable<IGlobalHighScoreData> highScores);
        void RemoveAllGlobalHighScores();
        void UpdateGlobalHighScores(IEnumerable<IGlobalHighScoreData> highScores);
        IDictionary<string, IConcreteResourceData> GetActualConcreteResourcesKeyed();
        IDictionary<string, string> GetActualConcreteStringsKeyed();
        IVersions GetVersions();
        void RemoveGlobalHighScores(ILanguageData languageData, IGameModeData gameModeData = null);
#if !TOOLKIT
        List<SaveHighScoreOnServerRequest> GetSaveHighScoreOnServerRequests();
#endif
        List<ILetterData> GetLetters(IEnumerable<int> lettersIds);
        IThemeData GetCurrentTheme();
        IEnumerable<IFacebookHighScoreData> GetFacebookHighScores();
        void UpdateFacebookHighScores(IEnumerable<IFacebookHighScoreData> highScores);
        void InsertFacebookHighScores(IEnumerable<IFacebookHighScoreData> highScores);
        void RemoveFacebookHighScores(ILanguageData languageData, IGameModeData gameModeData = null);
        void RemoveAllFacebookHighScores();
    }
}