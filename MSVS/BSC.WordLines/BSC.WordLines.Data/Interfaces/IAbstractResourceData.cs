﻿namespace BSC.WordLines.Data {
    public interface IAbstractResourceData {
        int Id { get; }
        string Key { get; }
        string Description { get; set; }
    }
}