namespace BSC.WordLines.Data {
    public interface ILocalHighScoreData {
        int Id { get; }
        int PlayerId { get; set; }
        int LanguageId { get; set; }
        int GameModeId { get; set; }
        /// <summary>
        /// Gets or sets timestamp when score obtained (UTC).
        /// Format is ISO8601.
        /// </summary>
        string Timestamp { get; set; }
        int Score { get; set; }
    }
}