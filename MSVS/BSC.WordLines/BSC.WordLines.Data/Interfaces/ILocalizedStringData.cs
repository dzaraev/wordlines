namespace BSC.WordLines.Data {
    public interface ILocalizedStringData {
        int Id { get; }
        string Key { get; }
        string Description { get; set; }
    }
}