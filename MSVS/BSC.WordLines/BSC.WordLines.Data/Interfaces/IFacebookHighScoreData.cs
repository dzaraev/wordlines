namespace BSC.WordLines.Data {
    public interface IFacebookHighScoreData {
        int Id { get; }
        string PlayerName { get; }
        bool IsLocalPlayer { get; }
        int LanguageId { get; }
        int GameModeId { get; }
        int HighScorePeriodId { get; }
        string Timestamp { get; }
        int Score { get; }
        string FacebookUser { get; }
    }
}