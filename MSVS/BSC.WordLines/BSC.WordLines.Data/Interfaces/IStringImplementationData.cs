namespace BSC.WordLines.Data {
    public interface IStringImplementationData {
        int Id { get; }
        int LocalizedStringId { get; set; }
        int? LanguageId { get; set; }
        string ConcreteString { get; set; }
    }
}