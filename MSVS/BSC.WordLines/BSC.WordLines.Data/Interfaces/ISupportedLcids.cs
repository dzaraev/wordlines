using SQLite;

namespace BSC.WordLines.Data {
    public interface ISupportedLcids {
        int Id { get; }
        string Lcid { get; }
        int LanguageId { get; }
    }
}