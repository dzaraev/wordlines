namespace BSC.WordLines.Data {
    public interface IVersions {
        int Id { get; }
        int SchemaVersion { get; }
        int ReleaseVersion { get; }
    }
}