namespace BSC.WordLines.Data {
    public interface IPlayerData {
        int Id { get; }
        string Name { get; set; }
        string Username { get; set; }
        string Password { get; set; }
        int Money { get; set; }
        int KillBallCount { get; set; }
        int WildcardCount { get; set; }
        int AdditionCount { get; set; }
        bool IsSigned { get; set; }
        bool IsRenamingNotFinished { get; set; }
        bool IsPurchaseNotFinished { get; set; }
    }
}