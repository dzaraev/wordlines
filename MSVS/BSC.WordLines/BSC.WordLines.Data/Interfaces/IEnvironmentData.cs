namespace BSC.WordLines.Data {
    public interface IEnvironmentData {
        int Id { get; }
        string Key { get; }
    }
}