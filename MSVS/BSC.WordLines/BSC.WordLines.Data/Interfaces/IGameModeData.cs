﻿namespace BSC.WordLines.Data {
    public interface IGameModeData {
        int Id { get; }
        string Name { get; }
    }
}