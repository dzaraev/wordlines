namespace BSC.WordLines.Data {
    public interface ILetterData {
        int Id { get; }
        int LanguageId { get; }
        string Letter { get; }
        double Chance { get; }
        bool IsVowel { get; }
    }
}