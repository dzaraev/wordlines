﻿namespace BSC.WordLines.Data {
    public interface IDataConfig {
        int SupportedReleaseVersion { get; }
        string LocalDbFileName { get; }
        string LocalDbFullPath { get; }

        /// <summary>
        /// Gets supported by app local DB SCHEMA version.
        /// </summary>
        int SupportedSchemaVersion { get; }

        string GetLocalWordsFileName(string languageName);
        string GetLocalWordsFileFullPath(string languageName);

        /// <summary>
        ///  Gets full path to specified file in isolated storage.
        /// </summary>
        string GetLocalFileFullPath(string fileName);
    }
}