﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace BSC.WordLines.Data {
    public interface IWordsProvider {
        Task<List<string>> GetWordsAsync();
    }
}