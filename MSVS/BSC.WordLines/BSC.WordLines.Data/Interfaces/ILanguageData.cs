namespace BSC.WordLines.Data {
    public interface ILanguageData {
        int Id { get; }
        string Name { get; }
        string DisplayName { get; }
        string IconAbstractResourceKey { get; }
    }
}