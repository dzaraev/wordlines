﻿namespace BSC.WordLines.Data {
    public interface IPlatformDataHelper {
        string GetAppVersion();
        string GetAppName();
        string GetSystemVersion();
    }
}