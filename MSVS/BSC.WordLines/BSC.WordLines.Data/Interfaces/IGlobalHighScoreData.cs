namespace BSC.WordLines.Data {
    public interface IGlobalHighScoreData {
        int Id { get; }
        int LanguageId { get; set; }
        int GameModeId { get; set; }
        int HighScorePeriodId { get; set; }
        string PlayerName { get; set; }
        bool IsLocalPlayer { get; set; }
        /// <summary>
        /// Gets or sets timestamp when score obtained (UTC).
        /// Format is ISO8601.
        /// </summary>
        string Timestamp { get; set; }
        int Score { get; set; }
    }
}