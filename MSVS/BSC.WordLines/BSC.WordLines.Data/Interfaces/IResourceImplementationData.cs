namespace BSC.WordLines.Data {
    public interface IResourceImplementationData {
        int Id { get; }
        int AbstractResourceId { get; set; }
        int? ThemeId { get; set; }
        int? LanguageId { get; set; }
        int? EnvironmentId { get; set; }
        int ConcreteResourceId { get; set; }
    }
}