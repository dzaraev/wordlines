﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BSC.WordLines.Utils;

namespace BSC.WordLines.Data {
    public interface IServerDataProvider {
        Task<IEnumerable<IGlobalHighScoreData>> FetchGlobalHighScores(
            IPlayerData playerData, ILanguageData languageData, IGameModeData gameModeData = null);

        Task SaveHighScoresOnServerAsync(
            IPlayerData playerData, List<SaveHighScoreOnServerRequest> saveOnServerRequests);

        Task RenamePlayerRemote(IPlayerData playerData, string newName);

        Task<IEnumerable<IFacebookHighScoreData>> FetchFacebookHighScores(
            IPlayerData playerData, ILanguageData languageData, IGameModeData gameModeData = null);

        Task SetPlayerFacebookUser(IPlayerData playerData, string facebookName);
    }
}