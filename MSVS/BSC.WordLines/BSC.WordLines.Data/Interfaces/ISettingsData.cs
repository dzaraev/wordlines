namespace BSC.WordLines.Data {
    public interface ISettingsData {
        int Id { get; }
        bool IsTutorialConfirmed { get; set; }
        bool IsFirstLaunch { get; set; }
        bool IsSoundsEnabled { get; set; }
        bool IsMusicEnabled { get; set; }
        int CurrentThemeId { get; set; }
        int CurrentLanguageId { get; set; }
        int CurrentEnvironmentId { get; set; }
        int CurrentPlayerId { get; }
        bool IsAppReviewed { get; set; }
        int IdleReviewOffersCount { get; set; }
    }
}