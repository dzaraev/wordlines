namespace BSC.WordLines.Data {
    public interface IConcreteResourceData {
        int Id { get; }
        byte[] Data { get; set; }
    }
}