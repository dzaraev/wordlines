namespace BSC.WordLines.Data {
    public interface IHighScorePeriodData {
        int Id { get; }
        string Name { get; }
    }
}