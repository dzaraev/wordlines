namespace BSC.WordLines.Data {
    public interface IThemeData {
        int Id { get; }
        string Name { get; }
        string DisplayNameLocalizedStringKey { get; }
    }
}