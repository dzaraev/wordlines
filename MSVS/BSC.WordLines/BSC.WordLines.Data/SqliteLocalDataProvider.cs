﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BSC.WordLines.Data.SqliteNet.ORM;
using BSC.WordLines.Utils;
using SQLite;

namespace BSC.WordLines.Data {
    public class SqliteLocalDataProvider : ILocalDataProvider {
        private readonly IDataConfig _dataConfig;
        private IPlayerData _cachedCurrentPlayer;//Not change by design.
        
        public SqliteLocalDataProvider(IDataConfig dataConfig) {
            dataConfig.EnsureNotNull("dataConfig");
            _dataConfig = dataConfig;
            _cachedCurrentPlayer = null;
        }

        #region Misc

        public IEnumerable<IDynamicSettingsData> GetDynamicSettings() {
            using (var db = OpenConnection()) {
                return db.Table<DynamicSettings>().ToList();
            }
        }

        public void SaveDynamicSettings(IEnumerable<IDynamicSettingsData> dynamicSettings) {
            if (!dynamicSettings.Any()) {
                return;
            }
            using (var db = OpenConnection()) {
                foreach (var setting in dynamicSettings) {
                    db.Update(setting);    
                }
            }
        }

        public IVersions GetVersions() {
            using (var db = OpenConnection()) {
                var versions = db.Table<Versions>().First();
                if (versions == null) {
                    throw new InvalidOperationException(ErrorMessage.CannotGetVersionsFromDb);
                }
                return versions;
            }
        }

        public IEnumerable<IGameModeData> GetGameModes() {
            using (var db = OpenConnection()) {
                return db.Table<GameModes>().ToList();
            }
        }

        public IEnumerable<IPlayerData> GetPlayers() {
            using (var db = OpenConnection()) {
                return db.Table<Players>().ToList();
            }
        }

        public IPlayerData GetPlayer(int playerId) {
            using (var db = OpenConnection()) {
                return db.Get<Players>(playerId);
            }
        }

        public IPlayerData GetCurrentPlayer() {
            if (_cachedCurrentPlayer == null) {
                using (var db = OpenConnection()) {
                    var settings = db.Table<Settings>().First();
                    _cachedCurrentPlayer = db.Get<Players>(settings.CurrentPlayerId);
                }
            }
            return _cachedCurrentPlayer;
        }

        public void UpdatePlayerInDatabase(IPlayerData player) {
            using (var db = OpenConnection()) {
                int updatedRecordsCount = db.Update(player);
                if (updatedRecordsCount != 1) {
                    throw new InvalidOperationException(ErrorMessage.UpdatePlayerInDbFailed);
                }
            }
        }

        public IEnumerable<ILanguageData> GetAllLanguages() {
            using (var db = OpenConnection()) {
                return db.Table<Languages>().ToList();
            }
        }

        public ILanguageData GetCurrentLanguage() {
            using (var db = OpenConnection()) {
                var settings = db.Table<Settings>().First();
                return db.Get<Languages>(settings.CurrentLanguageId);
            }
        }

        public IThemeData GetCurrentTheme() {
            using (var db = OpenConnection()) {
                var settings = db.Table<Settings>().First();
                return db.Get<Themes>(settings.CurrentThemeId);
            }
        }

        public ILanguageData GetLanguage(int id) {
            using (var db = OpenConnection()) {
                return db.Get<Languages>(id);
            }
        }

        public ISettingsData GetSettings() {
            using (var db = OpenConnection()) {
                var settings = db.Table<Settings>().First();
                if (settings == null) {
                    throw new InvalidOperationException(ErrorMessage.CannotGetSettingsFromDb);
                }
                return settings;
            }
        }

        public void SaveSettings(ISettingsData changedSettings) {
            using (var db = OpenConnection()) {
                int updatedRecordsCount = db.Update(changedSettings);
                Diagnost.Assert(updatedRecordsCount == 1, ErrorMessage.UpdatePlayerInDbFailed);
            }
        }

        public IEnumerable<string> GetEnvironmentKeys() {
            throw new NotImplementedException();
        }

        public IEnumerable<IEnvironmentData> GetAllEnvironments() {
            using (var db = OpenConnection()) {
                return db.Table<Environments>().ToList();
            }
        }

        public IEnvironmentData GetEnvironment(int id) {
            using (var db = OpenConnection()) {
                return db.Get<Environments>(id);
            }
        }

        public IThemeData GetTheme(int id) {
            using (var db = OpenConnection()) {
                return db.Get<Themes>(id);
            }
        }

        public void SetCurrentTheme(IThemeData theme) {
            throw new NotImplementedException();
        }

        public IEnumerable<IThemeData> GetAllThemes() {
            using (var db = OpenConnection()) {
                return db.Table<Themes>().ToList();
            }
        }

        public List<ILetterData> GetLetters(IEnumerable<int> lettersIds) {
            var letters = new List<ILetterData>();
            using (var db = OpenConnection()) {
                foreach (int id in lettersIds) {
                    letters.Add(db.Get<Letters>(id));
                }
            }
            return letters;
        }

        public IEnumerable<ILetterData> GetAlphabet() {
            using (var db = OpenConnection()) {
                var settings = db.Table<Settings>().First();
                var alphabet = new List<ILetterData>();
                foreach (var letter in db.Table<Letters>()) {
                    if (letter.LanguageId == settings.CurrentLanguageId) {
                        alphabet.Add(letter);
                    }
                }
                return alphabet;
            }
        }

        #endregion

        #region Words and Alphabet
#if TOOLKIT
        public IEnumerable<IWordData> GetWords(bool optimized = false) {
            using (var db = OpenConnection()) {
                int currentLanguageId = db.Table<Settings>().First().CurrentLanguageId;

                List<Words> words = optimized
                    ? db.Query<Words>("select Word from Words where LanguageId = ?", currentLanguageId)
                    : db.Query<Words>("select * from Words where LanguageId = ?", currentLanguageId);
                return words;
            }
        }

        public void InsertWords(IEnumerable<string> words, int languageId) {
            using (var db = OpenConnection()) {
                var wordsData = words.Select(word => new Words {
                    LanguageId = languageId,
                    Word = word
                });
                db.InsertAll(wordsData);
            }
        }
        public void DeleteWords(IEnumerable<IWordData> words) {
            using (var db = OpenConnection()) {
                foreach (var wordData in words) {
                    db.Delete(wordData);
                }
            }
        }

        public void SaveWords(IEnumerable<IWordData> words) {
            words.EnsureNotNull("words");
            using (var db = OpenConnection()) {
                db.UpdateAll(words);
            }
        }
#endif
        #endregion

        #region Localized Strings

        public IDictionary<string, string> GetActualConcreteStringsKeyed() {
            using (var db = OpenConnection()) {
                var keyedConcreteStrings = new Dictionary<string, string>();
                var localizedStringImpls = db.Table<StringImplementations>().ToList();
                var settings = db.Table<Settings>().First();//One record by design.
                foreach (var localizedString in db.Table<LocalizedStrings>()) {
                    foreach(var impl in localizedStringImpls) {
                        if (impl.LocalizedStringId == localizedString.Id &&
                            (impl.LanguageId == null ||
                            impl.LanguageId == settings.CurrentLanguageId)) {

                            string concreteString = impl.ConcreteString;
                            if (!concreteString.AssertNotNullFailed("concreteString")) {
                                keyedConcreteStrings.Add(localizedString.Key, concreteString);
                                break;
                            }
                        }
                    }
                }
                return keyedConcreteStrings;
            }
        }

        public string GetLocalizedString(string localizedStringKey) {
            using (var db = OpenConnection()) {
                var settings = db.Table<Settings>().First();//One record by design.
                var localizedString = db.Query<LocalizedStrings>(
                    "select * from LocalizedStrings where Key = ?", localizedStringKey)
                    .FirstOrDefault();
                if (localizedString != null) {
                    var stringImplemetations = db.Query<StringImplementations>(
                        "select * from StringImplementations where LocalizedStringId = ?", localizedString.Id);
                    var implementation = stringImplemetations.FirstOrDefault(
                        impl => impl.LanguageId == null || impl.LanguageId == settings.CurrentLanguageId);
                    if (implementation != null) {
                        if (implementation.LanguageId.HasValue || stringImplemetations.Count == 1) {
                            return implementation.ConcreteString;
                        }
                        Diagnost.Assert(false, ErrorMessage.GlobalStringImplementationDuplicated);
                        return string.Empty;
                    }
                    var language = db.Get<Languages>(settings.CurrentLanguageId);
                    Diagnost.Assert(false, ErrorMessage.LocalizedStringImplNotFound(localizedStringKey, language.Name));
                    return string.Empty;
                }
                Diagnost.Assert(false, ErrorMessage.LocalizedStringNotFound(localizedStringKey));
                return string.Empty;
            }
        }

        public IEnumerable<IStringImplementationData> GetStringImplementations(string localizedStringKey) {
            using (var db = OpenConnection()) {
                var localizedString = db.Query<LocalizedStrings>(
                    "select * from LocalizedStrings where Key = ?", localizedStringKey)
                    .FirstOrDefault();
                if (localizedString != null) {
                    var stringImplemetations = db.Query<StringImplementations>(
                        "select * from StringImplementations where LocalizedStringId = ?", localizedString.Id);
                    return stringImplemetations;
                }
                Diagnost.Assert(false, ErrorMessage.LocalizedStringNotFound(localizedStringKey));
                return new IStringImplementationData[] {};
            }
        }

        public void DeleteStringImplementation(IStringImplementationData data) {
            if (data == null) {
                throw new ArgumentNullException("data");
            }
            using (var db = OpenConnection()) {
                db.Delete<StringImplementations>(data.Id);
            }
        }
        
        public IEnumerable<ILocalizedStringData> GetAllLocalizedStrings() {
            using (var db = OpenConnection()) {
                return db.Table<LocalizedStrings>().ToList();
            }
        }

        public void SaveStringImplementation(IStringImplementationData data) {
            using (var db = OpenConnection()) {
                db.Update(data);
            }
        }

        public IStringImplementationData InsertStringImplementation(int localizedStringId, int? languageId,
                                                                    string concreteString) {
            using (var db = OpenConnection()) {
                var newData = new StringImplementations {
                    LocalizedStringId = localizedStringId,
                    LanguageId = languageId,
                    ConcreteString = concreteString,
                };
                db.Insert(newData);
                return newData;
            }
        }
        public void InsertLocalizedString(string key, string description) {
            using (var db = OpenConnection()) {
                var resource = new LocalizedStrings() {
                    Key = key,
                    Description = description,
                };
                db.Insert(resource);
            }
        }

        #endregion

        #region High Scores
#if !TOOLKIT
        public List<SaveHighScoreOnServerRequest> GetSaveHighScoreOnServerRequests() {
            var requests = new List<SaveHighScoreOnServerRequest>();
            using (var db = OpenConnection()) {
                var settings = db.Table<Settings>().First(); //Only one record be design.
                var currentPlayer = db.Get<Players>(settings.CurrentPlayerId);
                var periods = db.Table<HighScorePeriods>().ToList();
                var periodDictionary = new Dictionary<int, EHighScorePeriods> {
                    {periods.Find(period => period.Name == EHighScorePeriods.AllTime.ToString()).Id, EHighScorePeriods.AllTime},
                    {periods.Find(period => period.Name == EHighScorePeriods.Week.ToString()).Id, EHighScorePeriods.Week},
                };
                var languageList = db.Table<Languages>().ToList();
                var gameModeList = db.Table<GameModes>().ToList();
                var localHighScoreList = db.Table<LocalHighScores>().ToList();
                var globalHighScoreList = db.Table<GlobalHighScores>().ToList();
                var facebookHighScoresList = db.Table<FacebookHighScores>().ToList();
                foreach (var language in languageList) {
                    foreach (var gameMode in gameModeList) {
                        foreach (var period in periods) {
                            var periodEnum = periodDictionary[period.Id];

                            LocalHighScores playerLocalBest = GetLocalBestHighScore(
                                localHighScoreList, currentPlayer.Id, language.Id, gameMode.Id, periodEnum);

                            if (playerLocalBest == null) {
                                continue;
                            }

                            var globalHighScoresTop = GetGlobalHighScoresTop(
                                globalHighScoreList, language.Id, gameMode.Id, period.Id, periodEnum);

                            //Check save-on-server request necessity.
                            bool isRequestNeeded;
                            if (globalHighScoresTop.Count > 0) {
                                var playerGlobalBest = globalHighScoresTop.FirstOrDefault(score => score.IsLocalPlayer);
                                if (playerGlobalBest != null) {
                                    isRequestNeeded = playerLocalBest.Score > playerGlobalBest.Score;
                                }
                                else {
                                    isRequestNeeded =
                                        globalHighScoresTop.Count < DataConstants.HighScoresTopCount ||
                                        playerLocalBest.Score > globalHighScoresTop.Last().Score;
                                }
                            }
                            else {
                                isRequestNeeded = true;
                            }
                            
                            //Check also facebook leaderboard 
                            if (!isRequestNeeded && FacebookHelper.IsFacebokSigned()) {
                                var facebookHighScoresTop = GetFacebookHighScoresTop(
                                    facebookHighScoresList, language.Id, gameMode.Id, period.Id, periodEnum);
                                if (facebookHighScoresTop.Count > 0) {
                                    var playerFacebookBest = facebookHighScoresTop.FirstOrDefault(score => score.IsLocalPlayer);
                                    if (playerFacebookBest != null) {
                                        isRequestNeeded = playerLocalBest.Score > playerFacebookBest.Score;
                                    }
                                    else {
                                        isRequestNeeded =
                                            facebookHighScoresTop.Count < DataConstants.HighScoresTopCount ||
                                            playerLocalBest.Score > facebookHighScoresTop.Last().Score;
                                    }
                                }
                                else {
                                    isRequestNeeded = true;
                                }
                            }

                            if (isRequestNeeded) {
                                var saveOnServerRequest = new SaveHighScoreOnServerRequest(
                                    language.Name,
                                    gameMode.Name,
                                    period.Name,
                                    playerLocalBest.Score,
                                    playerLocalBest.Timestamp);
                                requests.Add(saveOnServerRequest);
                            }
                        }
                    }
                }
            }
            return requests;
        }
#endif
        public IEnumerable<IHighScorePeriodData> GetHighScorePeriods() {
            using (var db = OpenConnection()) {
                return db.Table<HighScorePeriods>().ToList();
            }
        }

        public ILocalHighScoreData CreateLocalHighScore() {
            return new LocalHighScores();
        }

        public IEnumerable<ILocalHighScoreData> GetLocalHighScores() {
            using (var db = OpenConnection()) {
                return db.Table<LocalHighScores>().ToList();
            }
        }

        public void InsertLocalHighScore(ILocalHighScoreData highScore) {
            using (var db = OpenConnection()) {
                db.Insert(highScore);
            }
        }

        public void RemoveLocalHighScores(IEnumerable<ILocalHighScoreData> highScores) {
            using (var db = OpenConnection()) {
                foreach (var data in highScores) {
                    db.Delete(data);
                }
            }
        }

        public IEnumerable<IGlobalHighScoreData> GetGlobalHighScores() {
            using (var db = OpenConnection()) {
                return db.Table<GlobalHighScores>().ToList();
            }
        }

        public void UpdateGlobalHighScores(IEnumerable<IGlobalHighScoreData> highScores) {
            using (var db = OpenConnection()) {
                foreach (var data in highScores) {
                    db.Update(data);
                }
            }
        }

        public void InsertGlobalHighScores(IEnumerable<IGlobalHighScoreData> highScores) {
            using (var db = OpenConnection()) {
                db.InsertAll(highScores);
            }
        }

        public void RemoveGlobalHighScores(ILanguageData languageData, IGameModeData gameModeData = null) {
            languageData.EnsureNotNull("languageData");
            using (var db = OpenConnection()) {
                List<GlobalHighScores> deleteList;
                if (gameModeData != null) {
                    deleteList =
                        db.Query<GlobalHighScores>(
                            "select * from GlobalHighScores where LanguageId = ? and GameModeId = ?",
                            languageData.Id, gameModeData.Id);
                }
                else {
                    deleteList =
                        db.Query<GlobalHighScores>(
                            "select * from GlobalHighScores where LanguageId = ?",
                            languageData.Id);
                }
                deleteList.ForEach(item => db.Delete<GlobalHighScores>(item.Id));
            }
        }

        public void RemoveAllGlobalHighScores() {
            using (var db = OpenConnection()) {
                db.DeleteAll<GlobalHighScores>();
            }
        }

        public IEnumerable<IFacebookHighScoreData> GetFacebookHighScores() {
            using (var db = OpenConnection()) {
                return db.Table<FacebookHighScores>().ToList();
            }
        }

        public void UpdateFacebookHighScores(IEnumerable<IFacebookHighScoreData> highScores) {
            using (var db = OpenConnection()) {
                foreach (var data in highScores) {
                    db.Update(data);
                }
            }
        }

        public void InsertFacebookHighScores(IEnumerable<IFacebookHighScoreData> highScores) {
            using (var db = OpenConnection()) {
                db.InsertAll(highScores);
            }
        }

        public void RemoveFacebookHighScores(ILanguageData languageData, IGameModeData gameModeData = null) {
            languageData.EnsureNotNull("languageData");
            using (var db = OpenConnection()) {
                List<FacebookHighScores> deleteList;
                if (gameModeData != null) {
                    deleteList =
                        db.Query<FacebookHighScores>(
                            "select * from FacebookHighScores where LanguageId = ? and GameModeId = ?",
                            languageData.Id, gameModeData.Id);
                }
                else {
                    deleteList =
                        db.Query<FacebookHighScores>(
                            "select * from FacebookHighScores where LanguageId = ?",
                            languageData.Id);
                }
                deleteList.ForEach(item => db.Delete<FacebookHighScores>(item.Id));
            }
        }

        public void RemoveAllFacebookHighScores() {
            using (var db = OpenConnection()) {
                db.DeleteAll<FacebookHighScores>();
            }
        }

        private LocalHighScores GetLocalBestHighScore(List<LocalHighScores> sourceScores, int playerId, int languageId, int gameModeId, EHighScorePeriods period) {
            var focusedScores = sourceScores.Where(
                score =>
                score.PlayerId == playerId &&
                score.LanguageId == languageId &&
                score.GameModeId == gameModeId);

            if (period == EHighScorePeriods.Week) {
                focusedScores = focusedScores.Where(score => DateTimeHelper.IsWithinCurrentWeek(score.Timestamp));
            }

            var focusedScoresList = focusedScores.ToList();
            return focusedScoresList.Count > 0
                ? focusedScoresList.OrderByDescending(scoreData => scoreData.Score).FirstOrDefault()
                : null;
        }

        private List<GlobalHighScores> GetGlobalHighScoresTop(List<GlobalHighScores> sourceScores, int languageId, int gameModeId, int periodId, EHighScorePeriods period) {
            var focusedScores = sourceScores.Where(
                score =>
                score.HighScorePeriodId == periodId &&
                score.LanguageId == languageId &&
                score.GameModeId == gameModeId);

            if (period == EHighScorePeriods.Week) {
                focusedScores = focusedScores.Where(score => DateTimeHelper.IsWithinCurrentWeek(score.Timestamp));
            }

            return focusedScores
                .OrderByDescending(scoreData => scoreData.Score)
                .Take(DataConstants.HighScoresTopCount)
                .ToList();
        }

        private List<FacebookHighScores> GetFacebookHighScoresTop(List<FacebookHighScores> sourceScores, int languageId, int gameModeId, int periodId, EHighScorePeriods period) {
            var focusedScores = sourceScores.Where(
                score =>
                score.HighScorePeriodId == periodId &&
                score.LanguageId == languageId &&
                score.GameModeId == gameModeId);

            if (period == EHighScorePeriods.Week) {
                focusedScores = focusedScores.Where(score => DateTimeHelper.IsWithinCurrentWeek(score.Timestamp));
            }

            return focusedScores
                .OrderByDescending(scoreData => scoreData.Score)
                .Take(DataConstants.HighScoresTopCount)
                .ToList();
        }

        #endregion

        #region Resources
        
        private bool AbstractResourceKeysEquals(string key1, string key2) {
            //By design.
            return string.Equals(key1, key2, StringComparison.Ordinal);
        }

        public IConcreteResourceData GetConcreteResource(string abstractResourceKey) {
            using (var db = OpenConnection()) {
                var abstractResource = db.Table<AbstractResources>()
                    .Where(resource => resource.Key == abstractResourceKey)
                    .FirstOrDefault();
                if (abstractResource == null) {
                    return null;
                }
                var settings = db.Table<Settings>().First(); //Because one record by design.
                ResourceImplementations resourceImpl = null;
                foreach (var impl in db.Table<ResourceImplementations>()) {
                    if (IsResourceImplActual(impl, abstractResource, settings)) {
                        resourceImpl = impl;
                        break;
                    }
                }
                if (resourceImpl == null) {
                    return null;
                }
                return db.Get<ConcreteResources>(resourceImpl.ConcreteResourceId);
            }
        }

        public IConcreteResourceData GetConcreteResource(int id) {
            using (var db = OpenConnection()) {
                return db.Get<ConcreteResources>(id);
            }
        }

        public IDictionary<string, IConcreteResourceData> GetActualConcreteResourcesKeyed() {
            using (var db = OpenConnection()) {
                var concreteResources = new Dictionary<string, IConcreteResourceData>();
                var resourceImpls = db.Table<ResourceImplementations>().ToList();
                var settings = db.Table<Settings>().First(); //Because one record by design.
                foreach (var abstractResource in db.Table<AbstractResources>()) {
                    foreach (var impl in resourceImpls) {
                        if (!IsResourceImplActual(impl, abstractResource, settings)) {
                            continue;
                        }
                        var concrete = db.Get<ConcreteResources>(impl.ConcreteResourceId);
                        if (!concrete.AssertNotNullFailed("concrete")) {
                            concreteResources.Add(abstractResource.Key, concrete);
                            break;
                        }
                    }
                }
                return concreteResources;
            }
        }

        public IEnumerable<string> GetAbstractResourceKeys() {
            return GetAbstractResources().Select(resource => resource.Key);
        }

        public IEnumerable<IAbstractResourceData> GetAbstractResources() {
            using (var db = OpenConnection()) {
                return db.Table<AbstractResources>().ToList();
            }
        }

        public void InsertAbstractResource(string key, string description) {
            using (var db = OpenConnection()) {
                var resource = new AbstractResources {
                    Key = key,
                    Description = description,
                };
                db.Insert(resource);
            }
        }

        public IEnumerable<IResourceImplementationData> GetAllResourceImplementations() {
            using (var db = OpenConnection()) {
                return db.Table<ResourceImplementations>().ToList();
            }
        }

        public IEnumerable<IResourceImplementationData> GetResourceImplementations(
            IAbstractResourceData abstractResource) {
            using (var db = OpenConnection()) {
                return db.Query<ResourceImplementations>(
                    "select * from ResourceImplementations where AbstractResourceId = ?", abstractResource.Id);
            }
        }

        public IResourceImplementationData GetResourceImplementation(int id) {
            using (var db = OpenConnection()) {
                return db.Get<ResourceImplementations>(id);
            }
        }

        public IResourceImplementationData InsertEmptyResourceImplementation(
            int abstractResourceId, int concreteResourceId) {
            using (var db = OpenConnection()) {
                var newData = new ResourceImplementations {
                    AbstractResourceId = abstractResourceId,
                    ConcreteResourceId = concreteResourceId,
                };
                db.Insert(newData);
                return newData;
            }
        }

        public void SaveResourceImplementation(IResourceImplementationData data) {
            using (var db = OpenConnection()) {
                db.Update(data);
            }
        }

        public IConcreteResourceData InsertConcreteResource(string stringData) {
            var data = stringData.StringToBytes();
            return InsertConcreteResource(data);
        }

        public void DeleteResourceImplementation(int id) {
            using (var db = OpenConnection()) {
                db.Delete<ResourceImplementations>(id);
            }
        }

        public void SaveConcreteResource(IConcreteResourceData data) {
            using (var db = OpenConnection()) {
                db.Update(data);
            }
        }

        public IConcreteResourceData InsertConcreteResource(byte[] data) {
            using (var db = OpenConnection()) {
                var resourceData = new ConcreteResources {
                    Data = data ?? new byte[] { }
                };
                db.Insert(resourceData);
                return resourceData;
            }
        }

        public void DeleteConcreteResource(int id) {
            using (var db = OpenConnection()) {
                db.Delete<ConcreteResources>(id);
            }
        }

        private bool IsResourceImplActual(ResourceImplementations impl, AbstractResources abstractResource, Settings settings) {
            return impl.AbstractResourceId == abstractResource.Id &&
                   (!impl.EnvironmentId.HasValue ||
                    impl.EnvironmentId.Value == settings.CurrentEnvironmentId) &&
                   (!impl.LanguageId.HasValue ||
                    impl.LanguageId.Value == settings.CurrentLanguageId) &&
                   (!impl.ThemeId.HasValue ||
                    impl.ThemeId.Value == settings.CurrentThemeId);
        }

        #endregion

        private SQLiteConnection OpenConnection() {
            return new SQLiteConnection(_dataConfig.LocalDbFullPath, DataConstants.SqliteDateTimeAsTicks);
        }

        private SQLiteAsyncConnection OpenAsyncConnection() {
            return new SQLiteAsyncConnection(_dataConfig.LocalDbFullPath, DataConstants.SqliteDateTimeAsTicks);
        }
    }
}