﻿using SQLite;

namespace BSC.WordLines.Data.SqliteNet.ORM {
    public class Environments : IEnvironmentData {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        [Unique]
        public string Key { get; set; }
    }
}