﻿using SQLite;

namespace BSC.WordLines.Data.SqliteNet.ORM {
    public class StringImplementations : IStringImplementationData {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public int LocalizedStringId { get; set; }
        public int? LanguageId { get; set; }
        public string ConcreteString { get; set; }
    }
}