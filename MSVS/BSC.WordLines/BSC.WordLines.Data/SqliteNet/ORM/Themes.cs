﻿using SQLite;

namespace BSC.WordLines.Data.SqliteNet.ORM {
    public class Themes : IThemeData {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        [Unique]
        public string Name { get; set; }

        [Unique]
        public string DisplayNameLocalizedStringKey { get; set; }
    }
}