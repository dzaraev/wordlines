﻿using SQLite;

namespace BSC.WordLines.Data.SqliteNet.ORM {
    public class ConcreteResources : IConcreteResourceData {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        public byte[] Data { get; set; }
    }
}