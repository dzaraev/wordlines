﻿using SQLite;

namespace BSC.WordLines.Data.SqliteNet.ORM {
    public class LocalHighScores : ILocalHighScoreData {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        public int PlayerId { get; set; }

        public int LanguageId { get; set; }

        public int GameModeId { get; set; }

        public string Timestamp { get; set; }

        public int Score { get; set; }
    }
}