﻿using System;
using SQLite;

namespace BSC.WordLines.Data.SqliteNet.ORM {
    public sealed class Letters : ILetterData {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        public int LanguageId { get; set; }

        public string Letter { get; set; }

        public double Chance { get; set; }

        public bool IsVowel { get; set; }

        public override string ToString() {
            return Letter;
        }

        private bool Equals(Letters other) {
            return string.Equals(Letter, other.Letter, StringComparison.Ordinal);
        }

        public override bool Equals(object obj) {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Letters)obj);
        }

        public override int GetHashCode() {
            return (Letter != null ? Letter.GetHashCode() : 0);
        }
    }
}