﻿using SQLite;

namespace BSC.WordLines.Data.SqliteNet.ORM {
    public class GameModes : IGameModeData{
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        [Unique]
        public string Name { get; set; }
    }
}