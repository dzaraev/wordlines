﻿using SQLite;

namespace BSC.WordLines.Data.SqliteNet.ORM {
    public class Languages : ILanguageData {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        [Unique]
        public string Name { get; set; }

        [Unique]
        public string DisplayName { get; set; }

        public string IconAbstractResourceKey { get; set; }
    }
}