﻿using SQLite;

namespace BSC.WordLines.Data.SqliteNet.ORM {
    public class DynamicSettings : IDynamicSettingsData{
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        [Unique]
        public string Key { get; set; }

        public string Value { get; set; }
    }
}