﻿using SQLite;

namespace BSC.WordLines.Data.SqliteNet.ORM {
    public class ResourceImplementations : IResourceImplementationData {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public int AbstractResourceId { get; set; }
        public int? ThemeId { get; set; }
        public int? LanguageId { get; set; }
        public int? EnvironmentId { get; set; }
        public int ConcreteResourceId { get; set; }
    }
}