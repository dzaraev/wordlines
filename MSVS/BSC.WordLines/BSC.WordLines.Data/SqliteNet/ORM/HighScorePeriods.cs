﻿using SQLite;

namespace BSC.WordLines.Data.SqliteNet.ORM {
    public class HighScorePeriods : IHighScorePeriodData {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        [Unique]
        public string Name { get; set; }
    }
}