﻿using SQLite;

namespace BSC.WordLines.Data.SqliteNet.ORM {
    public class Players : IPlayerData {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        [Unique]
        public string Name { get; set; }
        [Unique]
        public string Username { get; set; }

        public string Password { get; set; }

        public int Money { get; set; }

        public int KillBallCount { get; set; }

        public int WildcardCount { get; set; }

        public int AdditionCount { get; set; }

        public bool IsSigned { get; set; }

        public bool IsRenamingNotFinished { get; set; }
        
        public bool IsPurchaseNotFinished { get; set; }
    }
}