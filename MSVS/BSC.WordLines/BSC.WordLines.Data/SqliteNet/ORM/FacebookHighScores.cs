using SQLite;

namespace BSC.WordLines.Data.SqliteNet.ORM {
    public class FacebookHighScores : IFacebookHighScoreData {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        public string PlayerName { get; set; }
        public bool IsLocalPlayer { get; set; }
        public int LanguageId { get; set; }
        public int GameModeId { get; set; }
        public int HighScorePeriodId { get; set; }
        public string Timestamp { get; set; }
        public int Score { get; set; }
        public string FacebookUser { get; set; }
    }
}