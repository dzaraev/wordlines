﻿using SQLite;

namespace BSC.WordLines.Data.SqliteNet.ORM {
    public class Versions : IVersions {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; } 
        public int SchemaVersion { get; set; }
        public int ReleaseVersion { get; set; }
    }
}
