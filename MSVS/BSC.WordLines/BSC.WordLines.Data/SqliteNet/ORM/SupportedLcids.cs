﻿using SQLite;

namespace BSC.WordLines.Data.SqliteNet.ORM {
    public class SupportedLcids : ISupportedLcids {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        [Unique]
        public string Lcid { get; set; }
        public int LanguageId { get; set; }
    }
}