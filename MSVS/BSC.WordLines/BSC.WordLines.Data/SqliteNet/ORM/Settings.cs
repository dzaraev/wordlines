﻿using SQLite;

namespace BSC.WordLines.Data.SqliteNet.ORM {
    public class Settings : ISettingsData {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        public bool IsFirstLaunch { get; set; }
        public bool IsAppReviewed { get; set; }
        public bool IsTutorialConfirmed { get; set; }
        public bool IsSoundsEnabled { get; set; }
        public bool IsMusicEnabled { get; set; }
        public int CurrentThemeId { get; set; }
        public int CurrentLanguageId { get; set; }
        public int CurrentEnvironmentId { get; set; }
        public int CurrentPlayerId { get; set; }
        public int IdleReviewOffersCount { get; set; }
    }
}