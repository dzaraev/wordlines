﻿using SQLite;

namespace BSC.WordLines.Data.SqliteNet.ORM {
    public class AbstractResources : IAbstractResourceData {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        [Unique]
        public string Key { get; set; }

        public string Description { get; set; }
    }
}