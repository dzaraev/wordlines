﻿using System;

namespace BSC.WordLines.Data {
    public sealed class SaveHighScoreOnServerRequest : IEquatable<SaveHighScoreOnServerRequest> {
        public string LanguageName { get; private set; }
        public string GameModeName { get; private set; }
        public string PeriodName { get; private set; }
        public int HighScore { get; private set; }
        public string Timestamp { get; private set; }

        public SaveHighScoreOnServerRequest(string languageName, string gameModeName, string periodName, int highScore, string timestamp) {
            LanguageName = languageName;
            GameModeName = gameModeName;
            PeriodName = periodName;
            HighScore = highScore;
            Timestamp = timestamp;
        }

        public override bool Equals(object obj) {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((SaveHighScoreOnServerRequest)obj);
        }
        
        public bool Equals(SaveHighScoreOnServerRequest other) {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return 
                string.Equals(LanguageName, other.LanguageName) && 
                string.Equals(PeriodName, other.PeriodName) &&
                string.Equals(GameModeName, other.GameModeName) &&
                HighScore == other.HighScore &&
                string.Equals(Timestamp, other.Timestamp);
        }

        public override int GetHashCode() {
            unchecked {
                int hashCode = (LanguageName != null ? LanguageName.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (PeriodName != null ? PeriodName.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (GameModeName != null ? GameModeName.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ HighScore;
                hashCode = (hashCode * 397) ^ (Timestamp != null ? Timestamp.GetHashCode() : 0);
                return hashCode;
            }
        }
    }
}