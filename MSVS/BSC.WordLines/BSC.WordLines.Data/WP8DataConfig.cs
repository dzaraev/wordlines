using BSC.WordLines.Utils;
using Windows.Storage;

namespace BSC.WordLines.Data {
    public class WP8DataConfig : IDataConfig {
        private const string DB_FILE_NAME = "data.db";//WARNING! Never change this, or db upgrade will broken.

        private readonly string _fullPath;

        /// <summary>
        /// Gets supported by app local DB RELEASE version.
        /// </summary>
        public int SupportedReleaseVersion { get; private set; }

        /// <summary>
        /// Gets supported by app local DB SCHEMA version.
        /// </summary>
        public int SupportedSchemaVersion { get; private set; }

        /// <summary>
        /// Gets just name of file of DB.
        /// </summary>
        public string LocalDbFileName { get; private set; }

        /// <summary>
        /// Gets full path to DB in isolated storage.
        /// </summary>
        public string LocalDbFullPath { get; private set; }

        /// <summary>
        /// <paramref name="languageName"/> must match Name field of Languages table in DB (BY DESIGN).
        /// </summary>
        public string GetLocalWordsFileName(string languageName) {
            return "{0}.dic".FormatWith(languageName);
        }

        /// <summary>
        /// <paramref name="languageName"/> must match Name field of Languages table in DB (BY DESIGN).
        /// </summary>
        public string GetLocalWordsFileFullPath(string languageName) {
            return _fullPath + GetLocalWordsFileName(languageName);
        }

        /// <summary>
        ///  Gets full path to specified file in isolated storage.
        /// </summary>
        public string GetLocalFileFullPath(string fileName) {
            return _fullPath + fileName;
        }

        public WP8DataConfig() {
            _fullPath = ApplicationData.Current.LocalFolder.Path + "\\";//Cannot using format string. Only concatenation.

            LocalDbFileName = DB_FILE_NAME;
            LocalDbFullPath = _fullPath + LocalDbFileName;
            SupportedReleaseVersion = 6;
            SupportedSchemaVersion = 3;
        }
    }
}