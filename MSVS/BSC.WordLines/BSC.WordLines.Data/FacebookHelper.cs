﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows;
using BSC.WordLines.Utils;
using Caliburn.Micro;
using Parse;

namespace BSC.WordLines.Data {
    public static class FacebookHelper {
         public static bool IsFacebokSigned() {
             ParseUser currentUser = ParseUser.CurrentUser;
             if (currentUser != null) {
                 return ParseFacebookUtils.IsLinked(currentUser);
             }
             //alternative way
             var settingsManager = IoC.Get<SettingsManager>();
             string facebookUser = settingsManager.DynamicSettings[EDynamicSetting.PlayerFacebookUser].Value;
             return !string.IsNullOrWhiteSpace(facebookUser);
         }

        public static async Task<List<string>> FetchFriendFacebookUsers() {
            /*var fb = new FacebookClient { AccessToken = ParseFacebookUtils.AccessToken };
            dynamic friends = await fb.GetTaskAsync("me/friends");
            var friendUsers = new List<string>();
            foreach (var myFriend in friends) {
                friendUsers.Add((string) myFriend.id);
            }
            return friendUsers;*/
            //todo facebook future
            return new List<string>();
        }

        public static async Task SyncPlayerFacebookUserAsync() {
            //TODO facebook future
            return;
             /*var settingsManager = IoC.Get<SettingsManager>();
             var playerFacebookUserSetting = settingsManager.DynamicSettings[EDynamicSetting.PlayerFacebookUser];
             if (IsFacebokSigned() && string.IsNullOrWhiteSpace(playerFacebookUserSetting.Value)) {
                 try {
                     var fb = new FacebookClient { AccessToken = ParseFacebookUtils.AccessToken };
                     dynamic me = await fb.GetTaskAsync("me");
                     string facebookUser = me.id;
                     playerFacebookUserSetting.Value = facebookUser;
                     settingsManager.SaveDynamicSettings();

                     var serverProvider = IoC.Get<IServerDataProvider>();
                     await serverProvider.SetPlayerFacebookUser(settingsManager.CurrentPlayer, facebookUser);
                 }
                 catch (Exception ex) {
                     Diagnost.LogHandledException(ex);
                 }
             }*/
        }

        /*public static async Task LoginWithSession(AccessTokenData session) {
            try {
                var user = ParseUser.CurrentUser;
                //TODO и тут, после удачного логина, эти пидарасы дают сессию с FacebookId==null
                await ParseFacebookUtils.LinkAsync(user, session.FacebookId, session.AccessToken, session.Expires);
                await SyncPlayerFacebookUserAsync();
            }
            catch {
                
                return;
            }
        }*/
    }
}