﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System.Windows;
using BSC.WordLines.Utils;

namespace BSC.WordLines.Data {
    public class FileWordsProvider : IWordsProvider {
        private readonly IDataConfig _dataConfig;
        private readonly ILocalDataProvider _dataProvider;

        public FileWordsProvider(IDataConfig dataConfig, ILocalDataProvider dataProvider) {
            dataConfig.EnsureNotNull("dataConfig");
            dataProvider.EnsureNotNull("dataProvider");
            _dataConfig = dataConfig;
            _dataProvider = dataProvider;
        }

        public Task<List<string>> GetWordsAsync() {
            return Task.Factory.StartNew(() => {
                var settings = _dataProvider.GetSettings();
                var currentLanguage = _dataProvider.GetLanguage(settings.CurrentLanguageId);
                string wordsFileName = _dataConfig.GetLocalWordsFileName(currentLanguage.Name);
                var wordsList = new List<string>();
                using (Stream fileStream = GetInstalledDictionaryStream(wordsFileName)) {
                    fileStream.Position = 0;
                    using (TextReader reader = new StreamReader(fileStream)) {
                        string line = reader.ReadLine();
                        while (line != null) {
                            wordsList.Add(line);
                            line = reader.ReadLine();
                        }
                    }
                }
                return wordsList;
            });
        }
    
        private Stream GetInstalledDictionaryStream(string fileName) {
            return Application.GetResourceStream(new Uri(fileName, UriKind.Relative)).Stream;
        }
    }
}