﻿using System;
using System.Linq;
using System.Collections.Generic;
using BSC.WordLines.Data.SqliteNet.ORM;
using BSC.WordLines.Utils;

namespace BSC.WordLines.Data {
    public class SettingsManager {
        private readonly ILocalDataProvider _dataProvider;
        private ISettingsData _cachedSettings;
        private IPlayerData _cachedCurrentPlayer;
        private ILanguageData _cachedCurrentLanguage;
        private Dictionary<EDynamicSetting, IDynamicSettingsData> _dynamicSettings; 


        public SettingsManager(ILocalDataProvider dataProvider) {
            dataProvider.EnsureNotNull("dataProvider");
            _dataProvider = dataProvider;
        }
        /// <summary>
        /// Gets current player object. Cannot be null.
        /// </summary>
        /// <exception cref="InvalidOperationException">Thrown when cannot fetch player from DB.</exception>
        public IPlayerData CurrentPlayer {
            get {
                if (_cachedCurrentPlayer == null) {
                    UpdateFromDb();
                    if (_cachedCurrentPlayer == null) {
                        throw new InvalidOperationException(ErrorMessage.CannotGetCurrentPlayerDataFromProvider);
                    }
                }
                return _cachedCurrentPlayer;
            }
        }

        /// <summary>
        /// Gets current settings object. Cannot be null.
        /// </summary>
        public ISettingsData CurrentSettings {
            get {
                if (_cachedSettings == null) {
                    UpdateFromDb();
                    if (_cachedSettings.AssertNotNullFailed("_cachedSettings")) {
                        var failCauseSettings = new Settings();
                        failCauseSettings.CurrentEnvironmentId = 1;
                        failCauseSettings.CurrentLanguageId = 1;
                        failCauseSettings.CurrentThemeId = 1;
                        failCauseSettings.CurrentPlayerId = 1;
                        failCauseSettings.IsTutorialConfirmed = true;
                        failCauseSettings.IsFirstLaunch = false;
                        failCauseSettings.IsMusicEnabled = true;
                        failCauseSettings.IsSoundsEnabled = true;
                        _cachedSettings = failCauseSettings;
                    }
                }
                return _cachedSettings;
            }
        }

        public Dictionary<EDynamicSetting, IDynamicSettingsData> DynamicSettings {
            get {
                if (_dynamicSettings == null) {
                    UpdateFromDb();
                    if (_dynamicSettings == null) {
                        throw new InvalidOperationException(ErrorMessage.CannotGetDynamicSettingsDataFromProvider);
                    }
                }
                return _dynamicSettings;
            }
        }

        public ILanguageData CurrentLanguage {
            get {
                if (_cachedCurrentLanguage == null) {
                    UpdateFromDb();
                    if (_cachedCurrentLanguage == null) {
                        throw new InvalidOperationException(ErrorMessage.CannotGetCurrentLanguageDataFromProvider);
                    }
                }
                return _cachedCurrentLanguage;
            }
        }

        public void SaveInDb() {
            _dataProvider.SaveSettings(CurrentSettings);
            _cachedCurrentLanguage = _dataProvider.GetCurrentLanguage();
        }

        public void SaveDynamicSettings() {
            if (_dynamicSettings != null) {
                _dataProvider.SaveDynamicSettings(_dynamicSettings.Values);
            }
        }

        public void UpdateFromDb() {
            _cachedSettings = _dataProvider.GetSettings();
            _cachedCurrentPlayer = _dataProvider.GetCurrentPlayer();
            _cachedCurrentLanguage = _dataProvider.GetCurrentLanguage();
            _dynamicSettings = _dataProvider
                .GetDynamicSettings()
                .ToDictionary(setting => (EDynamicSetting) Enum.Parse(typeof (EDynamicSetting), setting.Key), setting => setting);
            Diagnost.Assert(_cachedSettings != null, ErrorMessage.CannotGetSettingsFromProvider);
            Diagnost.Assert(_cachedCurrentLanguage != null, ErrorMessage.CannotGetCurrentLanguageDataFromProvider);
            Diagnost.Assert(_cachedCurrentPlayer != null, ErrorMessage.CannotGetCurrentPlayerDataFromProvider);
        }
    }
}