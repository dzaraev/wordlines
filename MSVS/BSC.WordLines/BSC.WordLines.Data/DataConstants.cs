﻿using System;

namespace BSC.WordLines.Data {
    public static class DataConstants {
        public static int HighScoresTopCount { get { return 10; } }//Cannot be less than 1.
        public static TimeSpan GlobalHighScoresRefreshPeriod { get { return new TimeSpan(0, 30, 0); } }
        public static int NetTimeoutMilliseconds { get { return 20000; } }
        public static bool SqliteDateTimeAsTicks { get { return true; } }
    }
}