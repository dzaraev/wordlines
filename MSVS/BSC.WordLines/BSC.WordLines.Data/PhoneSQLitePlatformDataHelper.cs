using System;
using System.Xml;
using BSC.WordLines.Utils;

namespace BSC.WordLines.Data {
    public class PhoneSQLitePlatformDataHelper : IPlatformDataHelper {

        public string GetAppVersion() {
            return GetAppManifestInfo("Version");
        }

        public string GetAppName() {
            return GetAppManifestInfo("Title");
        }

        public string GetSystemVersion() {
            return Environment.OSVersion.Version + "  " + ResolutionHelper.CurrentResolution;
        }

        private string GetAppManifestInfo(string field) {
            try {
                var xmlReaderSettings = new XmlReaderSettings {
                    XmlResolver = new XmlXapResolver()
                };

                using (var xmlReader = XmlReader.Create("WMAppManifest.xml", xmlReaderSettings)) {
                    xmlReader.ReadToDescendant("App");

                    return xmlReader.GetAttribute(field);
                }
            }
            catch (Exception) {
                return "N/A";
            }
        }
    }
}