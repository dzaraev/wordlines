using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using BSC.WordLines.Data.SqliteNet.ORM;
using BSC.WordLines.Utils;
using Parse;

namespace BSC.WordLines.Data {
    //�����������:
    //��� ������ ���������� Mode � Language,
    //� ����� ������ ������ ���� ������ ���� ������,
    //���������� �� AllTimeBest � ���� ������, ���������� �� 
    //WeekTimeBest. ��� ����� ���� ���� � ��� �� ������.
    //���� - � ����� �� ������ ���� �� ������ ������� HighScore.
    //��������� ������� ���������� ��������� ��� �� ������� (��� ���������� ����� �������� HighScore)
    //��� � �� ������� (��� ������ �������� � ��. ���������).
    
    //����� Parse ��� �������� �� ������.
    //class HighScore
    //pointer User == ParseUser.CurrentUser
    //string PlayerName == IPlayerData.Name
    //string Period == "AllTime" or "Week"
    //string Mode == IGameModeData.Name
    //string Language == ILanguageData.Name 
    //DateTime Timestamp
    //int Score

    //class Log
    //pointer User
    //string Message
    //DateTime Timestamp
    //string TechnicalInfo
    //string Type : "Error" or "Info"

    public class ParseServerDataProvider : IServerDataProvider {
        private readonly ILocalDataProvider _localDataProvider;
        private const string HIGHSCORE_CLASS = "HighScore";
        private const string USER_FIELD = "User";
        private const string PLAYER_NAME_FIELD = "PlayerName";
        private const string PERIOD_FIELD = "Period";
        private const string MODE_FIELD = "Mode";
        private const string LANGUAGE_FIELD = "Language";
        private const string SCORE_FIELD = "Score";
        private const string TIMESTAMP_FIELD = "Timestamp";
        private const string FACEBOOK_USER_FIELD = "FacebookUser";

        private readonly Dictionary<string, int> _gameModeIds;
        private readonly Dictionary<string, int> _languageIds;
        private readonly Dictionary<string, int> _periodsIds; 
        private readonly string _weekPeriodKey;
        private readonly string _allTimePeriodKey;

        public ParseServerDataProvider(ILocalDataProvider localDataProvider) {
            localDataProvider.EnsureNotNull("localDataProvider");

            _weekPeriodKey = EHighScorePeriods.Week.ToString();
            _allTimePeriodKey = EHighScorePeriods.AllTime.ToString();
            _localDataProvider = localDataProvider;
            _gameModeIds = new Dictionary<string, int>();
            _languageIds = new Dictionary<string, int>();
            _periodsIds = new Dictionary<string, int>();
            foreach (var modeData in localDataProvider.GetGameModes()) {
                _gameModeIds.Add(modeData.Name, modeData.Id);
            }
            foreach (var languageData in localDataProvider.GetAllLanguages()) {
                _languageIds.Add(languageData.Name, languageData.Id);
            }
            foreach (var periodData in localDataProvider.GetHighScorePeriods()) {
                _periodsIds.Add(periodData.Name, periodData.Id);
            }
        }

        public async Task SaveHighScoresOnServerAsync(IPlayerData playerData, List<SaveHighScoreOnServerRequest> saveOnServerRequests) {
            if (saveOnServerRequests.AssertNotNullFailed("saveOnServerRequests")) {
                return;
            }

            //Autorization
            ParseUser currenUser = await Authorize(playerData);

            var requestModes = saveOnServerRequests.Select(r => r.GameModeName).Distinct().ToList();
            var requestLanguages = saveOnServerRequests.Select(r => r.LanguageName).Distinct().ToList();

            //Fetch user's scores from server (give me my old scores).
            var oldHighScoresQuery = ParseObject.GetQuery(HIGHSCORE_CLASS)
                .WhereEqualTo(USER_FIELD, currenUser)
                .WhereContainedIn(LANGUAGE_FIELD, requestLanguages)
                .WhereContainedIn(MODE_FIELD, requestModes);
            oldHighScoresQuery = MakeUniqueQueryOf_HIGHSCORE_CLASS(oldHighScoresQuery, PLAYER_NAME_FIELD);
            var oldHighScoreList = (await oldHighScoresQuery.FindAsync() ?? new ParseObject[] { }).ToList();

            var scoresToSave = new List<ParseObject>();
            string playerName = playerData.Name;
            string facebookUserName = await GetPlayerFacebookUserIdentifierAsync();
            foreach (var highScoreRequest in saveOnServerRequests) {
                var oldHighScore = oldHighScoreList.FirstOrDefault(
                    score =>
                    score.Get<string>(LANGUAGE_FIELD) == highScoreRequest.LanguageName &&
                    score.Get<string>(MODE_FIELD) == highScoreRequest.GameModeName &&
                    score.Get<string>(PERIOD_FIELD) == highScoreRequest.PeriodName);

                DateTime highScoreRequestTimestamp;
                if (!DateTimeHelper.TryParseTimestampRoundtrip(highScoreRequest.Timestamp, out highScoreRequestTimestamp)) {
                    Diagnost.Assert(false, ErrorMessage.CannotParseTimestamp(highScoreRequest.Timestamp));
                    highScoreRequestTimestamp = DateTime.UtcNow;
                }
                if (oldHighScore != null) {
                    //Updating old score entry.
                    int newHighScoreValue = highScoreRequest.HighScore;
                    int oldHighScoreValue = oldHighScore.Get<int>(SCORE_FIELD);
                    bool isNewGreaterThanOld = newHighScoreValue > oldHighScoreValue;

                    DateTime oldTimestampValue = oldHighScore.Get<DateTime>(TIMESTAMP_FIELD);
                    bool isOldWithinCurrentWeek = DateTimeHelper.IsWithinCurrentWeek(oldTimestampValue);
                    bool isOldWasWeekBest = oldHighScore.Get<string>(PERIOD_FIELD) == _weekPeriodKey;

                    if (isNewGreaterThanOld || isOldWasWeekBest && !isOldWithinCurrentWeek) {
                        UpdateHighScore(oldHighScore, newHighScoreValue, highScoreRequestTimestamp, facebookUserName);
                        scoresToSave.Add(oldHighScore);
                    }
                }
                else {
                    //Creating new score entry.
                    var newScoreObject = CreateHighScoreObject(
                        currenUser,
                        playerName,
                        facebookUserName,
                        highScoreRequest.PeriodName,
                        highScoreRequest.GameModeName,
                        highScoreRequest.LanguageName,
                        highScoreRequest.HighScore,
                        highScoreRequestTimestamp);
                    scoresToSave.Add(newScoreObject);
                }
            }
            if (scoresToSave.Count > 0) {
                await ParseObject.SaveAllAsync(scoresToSave);
            }
            //Second chance to set up facebook username to highscores
            if (!string.IsNullOrWhiteSpace(facebookUserName) &&
                oldHighScoreList.Exists(hs => string.IsNullOrWhiteSpace(hs.Get<string>(FACEBOOK_USER_FIELD)))) {

                var highScoresToSetFacebookQuery = ParseObject.GetQuery(HIGHSCORE_CLASS).WhereEqualTo(USER_FIELD, currenUser);
                highScoresToSetFacebookQuery = MakeUniqueQueryOf_HIGHSCORE_CLASS(highScoresToSetFacebookQuery, PLAYER_NAME_FIELD);
                var highScoresToSetFacebook = (await highScoresToSetFacebookQuery.FindAsync() ?? new ParseObject[] {}).ToList();
                foreach (var record in highScoresToSetFacebook) {
                    record[FACEBOOK_USER_FIELD] = facebookUserName;
                }
                await ParseObject.SaveAllAsync(highScoresToSetFacebook);
            }
        }

        public async Task<IEnumerable<IGlobalHighScoreData>> FetchGlobalHighScores(
            IPlayerData playerData, ILanguageData languageData, IGameModeData gameModeData = null) {

            //Autorization.
            ParseUser currenUser = await Authorize(playerData);

            //Fetch global highscores.
            List<ParseObject> topScores;
            string languageName = languageData.Name;
            if (gameModeData != null) {
                ParseQuery<ParseObject> allTimeTopQuery;
                ParseQuery<ParseObject> weekTopQuery;
                MakeGlobalHighScoresQueries(gameModeData.Name, languageName, out allTimeTopQuery, out weekTopQuery);
                topScores = (await allTimeTopQuery.FindAsync() ?? new ParseObject[] {})
                    .Concat(await weekTopQuery.FindAsync() ?? new ParseObject[] {})
                    .ToList();
            }
            else {
                ParseQuery<ParseObject> movesModeAllTimeTopQuery;
                ParseQuery<ParseObject> movesModeWeekTopQuery;
                ParseQuery<ParseObject> timeModeAllTimeTopQuery;
                ParseQuery<ParseObject> timeModeWeekTopQuery;
                MakeGlobalHighScoresQueries(EGameMode.Moves.ToString(), languageName, out movesModeAllTimeTopQuery, out movesModeWeekTopQuery);
                MakeGlobalHighScoresQueries(EGameMode.Time.ToString(), languageName, out timeModeAllTimeTopQuery, out timeModeWeekTopQuery);
                topScores = (await movesModeAllTimeTopQuery.FindAsync() ?? new ParseObject[] { })
                    .Concat(await movesModeWeekTopQuery.FindAsync() ?? new ParseObject[] { })
                    .Concat(await timeModeAllTimeTopQuery.FindAsync() ?? new ParseObject[] { })
                    .Concat(await timeModeWeekTopQuery.FindAsync() ?? new ParseObject[] { })
                    .ToList();
            }
            
            //Map Parse objects to local db objects.
            var scoresDatas = topScores.Select(score => new GlobalHighScores {
                Score = score.Get<int>(SCORE_FIELD),
                GameModeId = _gameModeIds[score.Get<string>(MODE_FIELD)],
                LanguageId = _languageIds[score.Get<string>(LANGUAGE_FIELD)],
                HighScorePeriodId = _periodsIds[score.Get<string>(PERIOD_FIELD)],
                IsLocalPlayer = IsUserOwnsHighScore(currenUser, score),
                Timestamp = DateTimeHelper.GetUtcTimestampString(score.Get<DateTime>(TIMESTAMP_FIELD)),
                PlayerName = score.Get<string>(PLAYER_NAME_FIELD),
            });
            return scoresDatas;
        }

        public async Task<IEnumerable<IFacebookHighScoreData>> FetchFacebookHighScores(
            IPlayerData playerData, ILanguageData languageData, IGameModeData gameModeData = null) {

            if (!FacebookHelper.IsFacebokSigned()) {
                throw new InvalidOperationException(ErrorMessage.CannotFetchFacebookScoresWithouSignUp);
            }

            //Autorization.
            ParseUser currenUser = await Authorize(playerData);

            string playerFacebookUserName = await GetPlayerFacebookUserIdentifierAsync();
            playerFacebookUserName.EnsureNotNullOrWhiteSpace("playerFacebookUserName");
            var relatedFacebookUsers = await FacebookHelper.FetchFriendFacebookUsers();


            //Fetch facebook highscores.
            List<ParseObject> topScores;
            string languageName = languageData.Name;
            if (gameModeData != null) {
                ParseQuery<ParseObject> allTimeTopQuery;
                ParseQuery<ParseObject> weekTopQuery;
                MakeFacebookHighScoresQueries(gameModeData.Name, languageName, relatedFacebookUsers, out allTimeTopQuery,
                                              out weekTopQuery);
                topScores = (await allTimeTopQuery.FindAsync() ?? new ParseObject[] {})
                    .Concat(await weekTopQuery.FindAsync() ?? new ParseObject[] {})
                    .ToList();
            }
            else {
                ParseQuery<ParseObject> movesModeAllTimeTopQuery;
                ParseQuery<ParseObject> movesModeWeekTopQuery;
                ParseQuery<ParseObject> timeModeAllTimeTopQuery;
                ParseQuery<ParseObject> timeModeWeekTopQuery;
                MakeFacebookHighScoresQueries(
                    EGameMode.Moves.ToString(), languageName, relatedFacebookUsers, out movesModeAllTimeTopQuery,
                    out movesModeWeekTopQuery);
                MakeFacebookHighScoresQueries(
                    EGameMode.Time.ToString(), languageName, relatedFacebookUsers, out timeModeAllTimeTopQuery,
                    out timeModeWeekTopQuery);
                topScores = (await movesModeAllTimeTopQuery.FindAsync() ?? new ParseObject[] {})
                    .Concat(await movesModeWeekTopQuery.FindAsync() ?? new ParseObject[] {})
                    .Concat(await timeModeAllTimeTopQuery.FindAsync() ?? new ParseObject[] {})
                    .Concat(await timeModeWeekTopQuery.FindAsync() ?? new ParseObject[] {})
                    .ToList();
            }

            //Map Parse objects to local db objects.
            var scoresDatas = topScores.Select(score => new FacebookHighScores {
                FacebookUser = score.Get<string>(FACEBOOK_USER_FIELD),
                Score = score.Get<int>(SCORE_FIELD),
                GameModeId = _gameModeIds[score.Get<string>(MODE_FIELD)],
                LanguageId = _languageIds[score.Get<string>(LANGUAGE_FIELD)],
                HighScorePeriodId = _periodsIds[score.Get<string>(PERIOD_FIELD)],
                IsLocalPlayer = IsUserOwnsHighScore(currenUser, score),
                Timestamp = DateTimeHelper.GetUtcTimestampString(score.Get<DateTime>(TIMESTAMP_FIELD)),
                PlayerName = score.Get<string>(PLAYER_NAME_FIELD),
            });
            return scoresDatas;
        }

        public async Task RenamePlayerRemote(IPlayerData playerData, string newName) {
            playerData.EnsureNotNull("playerData");
            //CRUTCH! Cannot use Authorize method because RenamePlayerRemote will call asynchronous
            //and thus can be race between two calls of Authorize method.
            ParseUser currenUser = ParseUser.CurrentUser;
            if (currenUser == null) {
                throw new InvalidOperationException(ErrorMessage.NeedAuthorizationFirst);
            }
            var playerHighScoresQuery = ParseObject.GetQuery(HIGHSCORE_CLASS).WhereEqualTo(USER_FIELD, currenUser);
            playerHighScoresQuery = MakeUniqueQueryOf_HIGHSCORE_CLASS(playerHighScoresQuery, PLAYER_NAME_FIELD);
            var playerHighScores = ((await playerHighScoresQuery.FindAsync()) ?? new List<ParseObject>()).ToList();
            foreach (var highScore in playerHighScores) {
                highScore[PLAYER_NAME_FIELD] = newName;
            }
            await playerHighScores.SaveAllAsync();
        }

        public async Task SetPlayerFacebookUser(IPlayerData playerData, string facebookName) {
            ParseUser currenUser = await Authorize(playerData);
            var playerHighScoresQuery = ParseObject.GetQuery(HIGHSCORE_CLASS).WhereEqualTo(USER_FIELD, currenUser);
            playerHighScoresQuery = MakeUniqueQueryOf_HIGHSCORE_CLASS(playerHighScoresQuery, PLAYER_NAME_FIELD);
            var playerHighScores = ((await playerHighScoresQuery.FindAsync()) ?? new List<ParseObject>()).ToList();
            foreach (var highScore in playerHighScores) {
                highScore[FACEBOOK_USER_FIELD] = facebookName;
            }
            await playerHighScores.SaveAllAsync();
        }

        private bool IsUserOwnsHighScore(ParseUser user, ParseObject highScore) {
            user.EnsureNotNull("user");
            highScore.EnsureNotNull("highScore");

            var highScoreUser = highScore.Get<ParseUser>(USER_FIELD);
            bool usersMatch = highScoreUser.ObjectId == user.ObjectId;
            
            return usersMatch;
        }

        private void MakeGlobalHighScoresQueries(
            string gameModeName,
            string languageName,
            out ParseQuery<ParseObject> allTimeTopQuery,
            out ParseQuery<ParseObject> weekTopQuery) {

            string mode = gameModeName;
            string language = languageName;
            allTimeTopQuery = ParseObject.GetQuery(HIGHSCORE_CLASS)
                .WhereEqualTo(PERIOD_FIELD, _allTimePeriodKey)
                .WhereEqualTo(MODE_FIELD, mode)
                .WhereEqualTo(LANGUAGE_FIELD, language)
                .OrderByDescending(SCORE_FIELD)
                .Limit(DataConstants.HighScoresTopCount);
            allTimeTopQuery = MakeUniqueQueryOf_HIGHSCORE_CLASS(allTimeTopQuery, PLAYER_NAME_FIELD);

            DateTime weekStartUtc;
            DateTime weekEndUtc;
            DateTimeHelper.GetCurrentWeekBounds(out weekStartUtc, out weekEndUtc);
            weekTopQuery = ParseObject.GetQuery(HIGHSCORE_CLASS)
                .WhereEqualTo(PERIOD_FIELD, _weekPeriodKey)
                .WhereEqualTo(MODE_FIELD, mode)
                .WhereEqualTo(LANGUAGE_FIELD, language)
                .WhereGreaterThan(TIMESTAMP_FIELD, weekStartUtc)
                .WhereLessThan(TIMESTAMP_FIELD, weekEndUtc)
                .OrderByDescending(SCORE_FIELD)
                .Limit(DataConstants.HighScoresTopCount);
            weekTopQuery = MakeUniqueQueryOf_HIGHSCORE_CLASS(weekTopQuery, PLAYER_NAME_FIELD);
        }

        private void MakeFacebookHighScoresQueries(
            string gameModeName,
            string languageName,
            List<string> relatedFacebookUsers,
            out ParseQuery<ParseObject> allTimeTopQuery,
            out ParseQuery<ParseObject> weekTopQuery) {

            string mode = gameModeName;
            string language = languageName;
            allTimeTopQuery = ParseObject.GetQuery(HIGHSCORE_CLASS)
                .WhereEqualTo(PERIOD_FIELD, _allTimePeriodKey)
                .WhereEqualTo(MODE_FIELD, mode)
                .WhereEqualTo(LANGUAGE_FIELD, language)
                .WhereContainedIn(FACEBOOK_USER_FIELD, relatedFacebookUsers)
                .OrderByDescending(SCORE_FIELD)
                .Limit(DataConstants.HighScoresTopCount);
            allTimeTopQuery = MakeUniqueQueryOf_HIGHSCORE_CLASS(allTimeTopQuery, PLAYER_NAME_FIELD);

            DateTime weekStartUtc;
            DateTime weekEndUtc;
            DateTimeHelper.GetCurrentWeekBounds(out weekStartUtc, out weekEndUtc);
            weekTopQuery = ParseObject.GetQuery(HIGHSCORE_CLASS)
                .WhereEqualTo(PERIOD_FIELD, _weekPeriodKey)
                .WhereEqualTo(MODE_FIELD, mode)
                .WhereEqualTo(LANGUAGE_FIELD, language)
                .WhereContainedIn(FACEBOOK_USER_FIELD, relatedFacebookUsers)
                .WhereGreaterThan(TIMESTAMP_FIELD, weekStartUtc)
                .WhereLessThan(TIMESTAMP_FIELD, weekEndUtc)
                .OrderByDescending(SCORE_FIELD)
                .Limit(DataConstants.HighScoresTopCount);
            weekTopQuery = MakeUniqueQueryOf_HIGHSCORE_CLASS(weekTopQuery, PLAYER_NAME_FIELD);
        }

        private void UpdateHighScore(ParseObject oldHighScore, int newHighScore, DateTime newTimestamp, string facebookUserName) {
            oldHighScore.EnsureNotNull("oldHighScore");

            oldHighScore[SCORE_FIELD] = newHighScore;
            oldHighScore[TIMESTAMP_FIELD] = newTimestamp;

            //TODO or not todo using facebookUserName here.
        }

        private ParseObject CreateHighScoreObject(
            ParseUser user,
            string playerName,
            string facebookUserName,
            string period,
            string mode,
            string language,
            int highScoreValue,
            DateTime timestamp) {

            user.EnsureNotNull("user");
            playerName.EnsureNotNullOrWhiteSpace("playerName");
            period.EnsureNotNullOrWhiteSpace("period");
            mode.EnsureNotNullOrWhiteSpace("mode");
            language.EnsureNotNullOrWhiteSpace("language");

            var highScore = new ParseObject(HIGHSCORE_CLASS);
            highScore[USER_FIELD] = user;
            highScore[PLAYER_NAME_FIELD] = playerName;
            highScore[PERIOD_FIELD] = period;
            highScore[MODE_FIELD] = mode;
            highScore[LANGUAGE_FIELD] = language;
            highScore[SCORE_FIELD] = highScoreValue;
            highScore[TIMESTAMP_FIELD] = timestamp;
            
            //Facebook user setting
            if (!string.IsNullOrWhiteSpace(facebookUserName)) {
                highScore[FACEBOOK_USER_FIELD] = facebookUserName;
            }

            //Security.
            ParseACL acl = new ParseACL();
            acl.PublicReadAccess = true;
            acl.PublicWriteAccess = false;
            acl.SetWriteAccess(user, true);
            highScore.ACL = acl;
            return highScore;
        }
        
        private async Task<ParseUser> Authorize(IPlayerData playerData) {
            playerData.EnsureNotNull("playerData");
            string username = playerData.Username;
            string password = playerData.Password;
            ParseUser currenUser = ParseUser.CurrentUser;
            if (currenUser == null) {
                if (playerData.IsSigned) {
                    string error = null;
                    try {
                        currenUser = await ParseUser.LogInAsync(username, password);
                    }
                    catch (Exception e) {
                        error = ErrorMessage.UserLogInException(e);
                    }
                    if (error != null) {
                        await Diagnost.LogAsync(error, ELogType.Error);
                        throw new InvalidOperationException(error);
                    }
                }
                else {
                    string error = null;
                    try {
                        var newUser = new ParseUser {
                            Username = username,
                            Password = password,
                        };
                        await newUser.SignUpAsync();

                        //Sign up locally.
                        playerData.IsSigned = true;
                        _localDataProvider.UpdatePlayerInDatabase(playerData);
                        currenUser = newUser;
                    }
                    catch (Exception e) {
                        error = ErrorMessage.UserSignUpException(e);
                    }
                    if (error != null) {
                        //Cannot signup - try login instead (reinstalled app for example).
                        try {
                            currenUser = await ParseUser.LogInAsync(username, password);
                            //Restore sign up.
                            playerData.IsSigned = true;
                            playerData.IsRenamingNotFinished = true;
                            _localDataProvider.UpdatePlayerInDatabase(playerData);
                            error = null;
                        }
                        catch (Exception e) {
                            error += "  [TRIED LOGIN INSTEAD:]  " + ErrorMessage.UserLogInException(e);
                        }
                        if (error != null) {
                            await Diagnost.LogAsync(error, ELogType.Error);
                            throw new InvalidOperationException(error);
                        }
                    }
                }
            }
            return currenUser;
        }

        private async Task<string> GetPlayerFacebookUserIdentifierAsync() {
            return null;
            //TODO facebook future
            await FacebookHelper.SyncPlayerFacebookUserAsync();
            var playerFacebookUserSetting = _localDataProvider.GetDynamicSettings().FirstOrDefault(
                s => s.Key == EDynamicSetting.PlayerFacebookUser.ToString());
            return playerFacebookUserSetting != null ? playerFacebookUserSetting.Value : null;
        }

        /// <summary>
        /// Workaround of fucking Parse's unavailable query cache.
        /// </summary>
        private ParseQuery<ParseObject> MakeUniqueQueryOf_HIGHSCORE_CLASS(ParseQuery<ParseObject> query, string fieldNameThanNotEqualsRandomValue) {
            DateTime now = DateTime.Now;
            string randomValue = now.ToShortDateString() + now.ToLongTimeString();
            return query.WhereNotEqualTo(fieldNameThanNotEqualsRandomValue, randomValue);
        }
    }
}