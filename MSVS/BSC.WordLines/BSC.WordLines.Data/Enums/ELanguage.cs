﻿namespace BSC.WordLines.Data {
    //ATTENTION! String representations of identifiers must match 
    //same values in local DB and backend scheme.
    public enum ELanguage {
        English,
        Russian
    }
}