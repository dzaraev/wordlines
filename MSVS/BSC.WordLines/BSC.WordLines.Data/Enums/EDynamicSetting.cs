﻿namespace BSC.WordLines.Data {
    public enum EDynamicSetting {
        AppReviewAwardPoints,
        IsAppReviewAwardFulfilled,
        AppSharedAwardPoints,
        IsAppSharedAwardFulfilled,
        IsAppShared,
        VkFollowedAwardPoints,
        IsVkFollowedAwardFulfilled,
        IsVkFollowed,
        SponsorFollowedAwardPoints,
        IsSponsorFollowedAwardFulfilled,
        IsSponsorFollowed,
        TwitterFollowedAwardPoints,
        IsTwitterFollowedAwardFulfilled,
        IsTwitterFollowed,
        BannerClickAwardPoints,
        IsBannerClicked,
        IsFreePointsFeatureNotified,
        PlayerFacebookUser
    }
}